<?php
 /***********************************************************************
 * EM2D - Gerenciador de Controle de LOJISTAS, LOGISTICA E ADMINISTRATIVO	 
 * @author EM2D Development Team * Arquivo de configuracoes gerais
 ************************************************************************/
	/**
	* EXIBICAO DE ERROS
	* @author Daniel Triboni
    */
    ini_set('error_reporting', -1);
    ini_set('display_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(E_ALL & ~E_NOTICE);
    date_default_timezone_set('America/Sao_Paulo');
    
    
   	/**	* BUSCAR PARAMETROS DE CONEXAO AO BANCO DE DADOS		
     * @tutorial NOTA: Qualquer modificacao pode resultar em erros no sistema!!!	
     * @author Daniel Triboni	
     */
    
    try 	{
        if (!@include_once("_db/params.ini.php"))
            throw new Exception("Erro ao inicializar o sistema!");
        else{
            require_once "_db/params.ini.php";
        }
    }catch(Exception $e){
        die($e->getMessage());	}
    
    /**	 
     * Check Time Remaing to start or if Site is out service
     */
    
    if (SIS_MANUTENCAO || SIS_INAUGURACAO != '')
    {
        $reloadToOffline = false;
        if (SIS_INAUGURACAO != '')
        {
            $today = new DateTime();
            $date = new DateTime(SIS_INAUGURACAO);
            $interval = $today->diff($date);
            $checkInterval = (int)($interval->format("%r%i%s"));
            if ($checkInterval > 0)
                $reloadToOffline = true;
        }
        if (SIS_MANUTENCAO)
            $reloadToOffline = true;
        if ($reloadToOffline)
        {
            if($_SERVER['SCRIPT_NAME'] != '/teaser.php')
            {
                header('Location: '.SIS_URL.'offline');
                exit;
            }
        }else{
            if($_SERVER['SCRIPT_NAME'] == '/teaser.php')
            {
                header('Location: '.SIS_URL.'home');
                exit;
            }
        }
    }

    $arrayStatusDelivery = array (0 => "AGUARDANDO CONFIRMA&Ccedil;&Atilde;O", 
                                  1 => "CONFIRMADO - DIRIGINDO PARA COLETA", 
                                  2 => "EST&Aacute; A CAMINHO",
                                  3 => "PEDIDO ENTREGUE");

    $arrayTypePerson = array (1 => "PERFIL COMPRADOR", 
                              2 => "PERFIL LOJISTA", 
                              3 => "PERFIL LOGÍSTICA",
                              4 => "PERFIL ADMIN EM2D");

    $arrayTypePersonColor = array (1 => "info", 
                                    2 => "success", 
                                    3 => "warning",
                                    4 => "danger");

    $arrContextOptions=array(
                             "ssl"=>array(
                                          "verify_peer"=>false,
                                          "verify_peer_name"=>false,
                                          ),
                             );
    
    /**	 * DEFININDO NOME DE SESSAO PARA CADA USUARIO DIFERENTE	 */
    function session_start2()
    {
        if (SIS_SECURE == true){
            $session_name = md5('em2d'.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
            $httponly = true;
            if (ini_set('session.use_only_cookies', 1) === FALSE) {
                die('Could not initiate a safe session (ini_set)');
            }
            $cookieParams = session_get_cookie_params();
            session_set_cookie_params($cookieParams["lifetime"],
                                      $cookieParams["path"],
                                      $cookieParams["domain"],
                                      true,
                                      $httponly);
            session_name($session_name);
            session_start();
            session_regenerate_id();
        }else{
            session_start();
        }
    }
    
	/**	* AUTOLOAD DAS CLASSES	*	
     * @author    Daniel Triboni	
     * @param	 string Nome da Classe	
     * @return	 include File	*/
    //function __autoload($class_name) {
    //require_once $_SERVER["DOCUMENT_ROOT"]."/_includes/_classes/".$class_name.".class.php";
    spl_autoload_register(function ($class_name) {
        require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_classes/".$class_name.".class.php";
    });
    //}
