<?php

/**
* Classe para execucao de Queries
* @author EM2D Development Team
*/
class queries extends mysqlconn {
		
	private $sqlQuery;
	
	private $sqlQueryCompl;
	
	private $sqlQueryInner;
	
	private $sqlQueryGroup;
	
	private $retRecords;
	
	private $retHTML;
	
	public $retAPID;
	
	/**
	* Class Constructor
	*
	* @author    Daniel Triboni
	* @return    resource
	*/
	public function __construct() {	
		parent::__construct();
	}


	/**
	 * Query Persons Logged after 24H
	 * @return array
	 */
	public function fQueryProfile($usrid){
	
		$this->sqlQuery = "SELECT 
								u.usrid,
								u.name AS user,
								u.email,
								DATE_FORMAT(u.borndate, '%d/%m/%Y') AS borndate,
								s.strid,
								s.store AS store,
								s.cpfcnpj,
								c.category AS category
							FROM users u 
							LEFT JOIN stores s ON s.usrid = u.usrid 
							LEFT JOIN categories c ON c.catid = s.catid
							WHERE u.usrid = {$usrid}";	
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	/**
	 * Query Save LOG Actions
	 * @return array
	 */
	public function fQuerySaveLOG($usrid, $strid, $logid, $action)
	{
		$this->sqlQuery = "INSERT INTO logs (usrid, strid, logid, action)
							VALUES ({$usrid}, {$strid}, {$logid}, '".$action."')";	
		$this->fExecuteSql($this->sqlQuery);
	}



	/**
	 * Query Persons Logged after 24H
	 * @return array
	 */
	public function fQueryAddresses($strid, $type){
	
		$this->sqlQuery = "SELECT 
								sa.*,
								s.store
							FROM storeaddresses sa 
							INNER JOIN stores s ON s.strid = sa.strid 
							WHERE sa.active = 1 ";
		if ($type != 4){
			$this->sqlQuery .= "AND sa.strid = {$strid} ";	
		}
		$this->sqlQuery .= "ORDER BY s.store ASC";								
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	/**
	 * Query Persons Logged after 24H
	 * @return array
	 */
	public function fQueryLogistics($usrid, $type){
	
		$this->sqlQuery = "SELECT 
								l.*,
								CASE WHEN (l.taxmode = 1) THEN
									'TAXA FIXA'
								WHEN (l.taxmode = 2) THEN
									'PERCENTUAL POR DIST&ACIRC;NCIA'	
								ELSE
									'FRETE PESO (CUBAGEM)'
								END AS taxmodes,	
								CASE WHEN (l.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								u.name AS resp
							FROM logistics l 
							INNER JOIN users u ON u.usrid = l.usrid 
							WHERE l.removed = 0 ";
		if ($type != 4){
			$this->sqlQuery .= "AND l.usrid = {$usrid} ";	
		}
		$this->sqlQuery .= "ORDER BY l.name ASC";								
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	/**
	 * Query Person Address
	 * @return array
	 */
	public function fQueryAddress($addid, $usrid, $type){
	
		$this->sqlQuery = "SELECT 
								sa.*,
								p.phoid,
								p.photo		
							FROM storeaddresses sa 
							INNER JOIN stores s ON s.strid = sa.strid
							LEFT JOIN photos p ON p.addid = sa.addid  
							WHERE sa.addid = {$addid} ";
		if ($type != 4){
			$this->sqlQuery .= "AND s.usrid = {$usrid}";	
		}					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}


	/**
	 * Query Person Logistic
	 * @return array
	 */
	public function fQueryLogistic($logid, $usrid, $type){
	
		$this->sqlQuery = "SELECT 
								l.*
							FROM logistics l 
							INNER JOIN users u ON u.usrid = l.usrid
							WHERE l.logid = {$logid} ";
		if ($type != 4){
			$this->sqlQuery .= "AND u.usrid = {$usrid}";	
		}					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}


	/**
	 * Query Person Address
	 * @return array
	 */
	public function fQueryStores($usrid, $type){
		if ($type != 4){
			$this->sqlQueryCompl = "";	
		}else{
			$this->sqlQueryCompl = ", (SELECT 
										DATEDIFF(spy.expires, now()) 
									FROM storeplans sp 
									INNER JOIN storepayments spy ON spy.stpid = sp.stpid 
									WHERE sp.strid = s.strid 
									AND spy.paid = 1
									AND spy.payid IS NOT NULL 
									ORDER BY spy.payid DESC LIMIT 1) AS diffdays, 
									(SELECT 
										spy.paid
									FROM storeplans sp 
									INNER JOIN storepayments spy ON spy.stpid = sp.stpid 
									WHERE sp.strid = s.strid 
									AND spy.payid IS NOT NULL 
									ORDER BY spy.payid DESC LIMIT 1) AS paid,
									IFNULL((SELECT 
										spy.stpid
									FROM storeplans sp 
									INNER JOIN storepayments spy ON spy.stpid = sp.stpid 
									WHERE sp.strid = s.strid 
									AND spy.payid IS NOT NULL 
									ORDER BY spy.payid DESC LIMIT 1), 0) AS stpid, 
									(SELECT 
										spy.cancelled
									FROM storeplans sp 
									INNER JOIN storepayments spy ON spy.stpid = sp.stpid 
									WHERE sp.strid = s.strid 
									AND spy.payid IS NOT NULL 
									ORDER BY spy.payid DESC LIMIT 1) AS cancelled";
		}	
		$this->sqlQuery = "SELECT 
								*,
								s.active AS storeactive,
								s.cpfcnpj AS cpf_cnpj,
								CASE WHEN (s.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								CONCAT(s.store, ' - CPF/CNPJ: ', s.cpfcnpj) AS storedata, 
								(SELECT COUNT(1)
								 FROM storeaddresses sa
								 WHERE sa.strid = s.strid AND sa.active = 1) AS numaddress,
								 (SELECT COUNT(1)
								 FROM offers o
								 INNER JOIN storeaddresses sa ON sa.addid = o.addid 
								 WHERE sa.strid = s.strid AND sa.active = 1
								 AND o.active = 1) AS numoffers
								 ".$this->sqlQueryCompl."  
							FROM stores s 
							INNER JOIN categories c ON c.catid = s.catid 
							INNER JOIN users u ON u.usrid = s.usrid ";
		if ($type != 4){
			$this->sqlQuery .= "WHERE s.usrid = {$usrid}";	
		}
		$this->sqlQuery .= " ORDER BY s.store ASC";					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}



	/**
	 * Query Person Offers
	 * @return array
	 */
	public function fQueryOffers($usrid, $type){
		
		$this->sqlQuery = "SELECT 
								o.prodserv,
								o.active,
								o.ofeid,
								o.stock,
								o.stockmin,
								o.type,
								o.modality,
								DATE_FORMAT(o.start, '%d/%m/%Y %H:%i:%s') AS start,
								DATE_FORMAT(o.end, '%d/%m/%Y %H:%i:%s') AS end,
							CASE WHEN (o.active = 1) THEN
								'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
							ELSE
								'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
							END AS actived,		
							CASE WHEN (o.type = 1) THEN
								'PRODUTO'
							ELSE
								'SERVI&Ccedil;O'
							END AS otype,
							CASE WHEN (o.modality = 1) THEN
								'OFERTA'
							WHEN (o.modality = 3) THEN
								'DESCONTO'	
							ELSE
								'PROMO&Ccedil;&Atilde;O'
							END AS omod,
							CASE WHEN (o.type = 1) THEN
								o.stock
							ELSE
								'-'
							END AS ostock,
							CASE WHEN (o.modality = 3) THEN
								'-'
							ELSE
								o.oldprice
							END AS oldprice,
							CASE WHEN (o.modality = 1 OR o.modality = 3) THEN
								'-'
							ELSE
								o.newprice
							END AS newprice,
							CASE WHEN (o.modality = 1) THEN
								'-'
							ELSE
								o.discount
							END AS discount		
							FROM offers o
							INNER JOIN storeaddresses sa ON sa.addid = o.addid 
							WHERE o.removed = 0 ";
		if ($type == 2){
			 $this->sqlQuery .= "AND sa.strid = {$usrid}";	
		}					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
		
	}


	/**
	 * Query Person Offer
	 * @return array
	 */
	public function fQueryOffer($ofeid, $strid, $type){
		
		$this->sqlQuery = "SELECT 
								o.*,
								p.phoid,
								p.photo,
								DATE_FORMAT(o.start, '%Y-%m-%d') AS starts,
								DATE_FORMAT(o.end, '%Y-%m-%d') AS ends			
							FROM offers o
							LEFT JOIN photos p ON p.ofeid = o.ofeid
							INNER JOIN storeaddresses sa ON sa.addid = o.addid 
							WHERE o.removed = 0 ";
		if ($type == 2){
			$this->sqlQuery .= "AND sa.strid = {$strid} AND o.ofeid = {$ofeid}";
		}else{
			$this->sqlQuery .= "AND o.ofeid = {$ofeid}";	
		}					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
		
	}


	public function fQueryOffersRemaining($strid, $type){
		$this->sqlQuery = "SELECT IFNULL((SELECT 
							(SELECT SUM(p.numoffers) - COUNT(1) 
								FROM offers o 
							INNER JOIN storeaddresses a ON a.addid = o.addid
							WHERE a.strid = {$strid}) 
							FROM plans p
							INNER JOIN storeplans sp ON sp.plaid = p.plaid
							INNER JOIN storepayments spy ON spy.stpid = sp.stpid
							WHERE sp.strid = {$strid} 
							AND spy.paid = 1
							AND spy.gatid IS NOT NULL), 0) AS numoffers,
							IFNULL((SELECT 
								DATEDIFF(spy.expires, now()) 
								FROM storepayments spy
								INNER JOIN storeplans sp ON sp.stpid = spy.stpid	
								WHERE sp.strid = {$strid}
								AND spy.paid = 1
								AND spy.gatid IS NOT NULL  
								ORDER BY spy.expires DESC LIMIT 1), 0) AS expires,
							IFNULL((SELECT 
								CONCAT('Seu plano expira em <strong>', DATE_FORMAT(spy.expires, '%d/%m/%Y'), '!</strong>') 
								FROM storepayments spy
								INNER JOIN storeplans sp ON sp.stpid = spy.stpid	
								WHERE sp.strid = {$strid}
								AND spy.paid = 1 
								AND spy.gatid IS NOT NULL 
								ORDER BY spy.expires DESC LIMIT 1), 0) AS expirestext";
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		if ($type == 4){
			return "9999|9999";
		}else{
			return $this->retRecords[0]['numoffers']."|".$this->retRecords[0]['expires']."|".$this->retRecords[0]['expirestext'];
		}				
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryStore($strid, $usrid, $type){
	
		$this->sqlQuery = "SELECT 
								*,
								s.cpfcnpj AS cpfcnpj,
								s.logo AS logo
							FROM stores s 
							INNER JOIN categories c ON c.catid = s.catid 
							INNER JOIN users u ON u.usrid = s.usrid 
							WHERE s.strid = {$strid} ";
		if ($type != 4){
			$this->sqlQuery .= "AND s.usrid = {$usrid}";	
		}					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryUsers($ftype, $usrid, $type){
	
		if ($type == 4){
			$this->sqlQuery = "SELECT 
								u.* ,
								CASE WHEN (u.gender = 'Male') THEN
									'Masculino'
								ELSE
									'Feminino'
								END AS sexo,
								CASE WHEN (u.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								DATE_FORMAT(u.borndate, '%d/%m/%Y') AS nascimento,
								DATE_FORMAT(u.created_at, '%d/%m/%Y %H:%i') AS cadastro
							FROM users u
								WHERE u.password = '0000'
								AND u.active = 0 ";
			$this->sqlQuery .= " ORDER BY u.created_at ASC, u.name ASC";					 
			$this->fExecuteSql($this->sqlQuery);
			$arr1 = $this->fShowRecords();
		}else{
			$arr1 = [];
		}

		$this->sqlQuery = "SELECT 
								u.* ,
								CASE WHEN (u.gender = 'Male') THEN
									'Masculino'
								ELSE
									'Feminino'
								END AS sexo,
								CASE WHEN (u.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								DATE_FORMAT(u.borndate, '%d/%m/%Y') AS nascimento,
								DATE_FORMAT(u.created_at, '%d/%m/%Y %H:%i') AS cadastro
							FROM users u ";
		if ($type != 4){
			$this->sqlQuery .= "WHERE u.type IN (1,2,3,4) AND u.usrid = {$usrid} ";	
		}else{
			$this->sqlQuery .= "WHERE ".($ftype > 0 ? "u.type = ".$ftype : "u.type IN (1,2,3,4) ");	
		}

		$this->sqlQuery .= " ORDER BY u.type ASC, u.name ASC";					 
		$this->fExecuteSql($this->sqlQuery);
		$arr2 = $this->fShowRecords();

		$this->retRecords = array_merge($arr1, $arr2);

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryUser($usrid, $userid, $type){

		if ($type != 4){
			if ($usrid != $userid){
				return false;
			}
		}
	
		$this->sqlQuery = "SELECT 
								u.* ,
								CASE WHEN (u.gender = 'Male') THEN
									'Masculino'
								ELSE
									'Feminino'
								END AS sexo,
								CASE WHEN (u.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								DATE_FORMAT(u.borndate, '%d/%m/%Y') AS nascimento,
								DATE_FORMAT(u.created_at, '%d/%m/%Y %H:%i') AS cadastro
							FROM users u 
							WHERE u.usrid = {$usrid} 
							ORDER BY u.type ASC, u.name ASC";					 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryOrders($received, $period, $logid, $strid, $type){
	
		$queryPeriod = ($period == 0 ? "" : " AND DATE_FORMAT(o.created_at, '%m-%Y') = '$period'");

		if ($type == 2){
			$this->sqlQueryCompl = "WHERE b.strid = {$strid}";
						$compl2 = "AND b1.strid = {$strid}";
		}
		 if ($type == 3){
			$this->sqlQueryCompl = "WHERE l.usrid = {$logid}
									AND o1.type = 1"; //flag somente para produtos
		}
		if ($type == 4){
			$this->sqlQueryCompl = ($received == true ? "WHERE o.refunded = 0 
									AND (DATE_FORMAT(now(), '%d') BETWEEN '10' AND '25')" : "WHERE 1 ");
		}
		$this->sqlQuery = "SELECT 
								o.ord_alias,
								o.ordid,
								o.refunded,
								o.shipping,
								DATE_FORMAT(o.created_at, '%d/%m/%Y %h:%i') AS date,
								(SELECT SUM((b1.qtd * b1.vlunit) - b1.discount)
									FROM buy b1
									WHERE b1.ordid = b.ordid 
									".$compl2.") AS total,
								CASE WHEN (o.paid = 0) THEN
									'AGUARDANDO PAGAMENTO'
								ELSE
									'PAGO'
								END AS paid,
								CASE WHEN (o.delivery_status = 0 AND o.paid = 1) THEN
									'AGUARDANDO RETIRADA'
								WHEN (o.delivery_status = 1 AND o.paid = 1) THEN
									'SAIU PARA ENTREGA'	
								WHEN (o.delivery_status = 2 AND o.paid = 1) THEN
									'ENTREGUE'	
								ELSE
									'AGUARDANDO PAGAMENTO'
								END AS delivery,
								u.name AS buyer,
								l.name
							FROM orders o 
							INNER JOIN buy b ON b.ordid = o.ordid
							LEFT JOIN logistics l ON l.logid = o.logid
							INNER JOIN offers o1 ON o1.ofeid = b.ofeid 
							INNER JOIN users u ON u.usrid = b.usrid
							LEFT JOIN users u1 ON u1.usrid = l.usrid 
							".$this->sqlQueryCompl.$queryPeriod." 
							GROUP BY o.ord_alias,
										o.ordid,
										o.refunded,
										o.shipping,
										date,
										total,
										o1.type,
										o.paid,
										o.delivery_status,
										u.name,
										l.name
							ORDER BY o.delivery_status ASC, o.created_at ASC";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
		
	}


	public function fQueryCheck4NewOrders($logid, $strid, $type){
		if ($type == 2){
			$this->sqlQueryCompl = "(SELECT COUNT(1)
									FROM orders o
									INNER JOIN buy b ON b.ordid = o.ordid
									WHERE b.strid = {$strid}) AS numordersseller";
		}
		 if ($type == 3){
			$this->sqlQueryCompl = "(SELECT COUNT(1)
									FROM orders o
									INNER JOIN logistics l ON l.logid = o.logid
									WHERE l.usrid = {$logid}) AS numorderslogger";
		}

		$this->sqlQuery = "SELECT ".$this->sqlQueryCompl;
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}

	public function fQueryTaxLog($logid)
	{
		$this->sqlQuery = "SELECT
								l.taxmode,
								l.vltax,
								l.maxdist
							FROM logistics l
							WHERE l.usrid = {$logid}";
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}

	public function fQueryUpdateTaxLog($obj, $logid)
	{
		$this->fQuerySaveLOG($logid, 0, 0, "Atualizou taxas e abrangencia de logistica (usrid): ".$logid);
		$this->sqlQuery = "UPDATE logistics SET
								taxmode = ".$obj['taxmode'].",
								vltax = ".($obj['vltax'] == '' ? 0 : $obj['vltax']).",
								maxdist = ".$obj['maxdist']." 
							WHERE usrid = {$logid}";
		return $this->fExecuteSql($this->sqlQuery);	
	}

	public function fQueryGetCubageOrderItems($ordid)
	{
		$this->sqlQuery = "SELECT o.description,
									c.dimensions,
									c.weight,		
									l.taxmode,
									l.vltax,
									l.maxdist
							FROM caracteristics c
							INNER JOIN buy b ON b.ofeid = c.ofeid
							INNER JOIN offers o ON o.ofeid = c.ofeid
							INNER JOIN orders o1 ON o1.ordid = b.ordid
							INNER JOIN logistics l ON l.logid = o1.logid
							WHERE b.ordid = {$ordid} 
							AND o.type = 1";
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}

	public function fQueryReceivePayments($received, $logid, $strid, $type)
	{
		if ($type == 2){
			$this->sqlQueryCompl = "AND b1.strid = {$strid}  ";
			$compl2 = "(SELECT SUM((b.qtd * b.vlunit) - b.discount) 
						FROM buy b 
						INNER JOIN orders or1 ON or1.ordid = b.ordid
						INNER JOIN offers o1 ON o1.ofeid = b.ofeid 
						INNER JOIN stores s ON s.strid = b.strid 
						INNER JOIN storeaddresses sa ON sa.strid = s.strid 
						WHERE DATE_FORMAT(or1.created_at, '%m/%Y') = periodmonth AND b.strid = {$strid}
						AND o1.addid = sa.addid ) AS grossstore,";
			$compl3 = "INNER JOIN buy b1 ON b1.ordid = o.ordid  ";			
		}else if ($type == 3){
			$this->sqlQueryCompl = "AND l.usrid = {$logid} ";
			$compl2 = "1 AS grossstore,";
			$compl3 = "";
		}else{
			$this->sqlQueryCompl = ($received == true ? "AND o.refunded = 0 
									AND (DATE_FORMAT(now(), '%d') BETWEEN '10' AND '25')" : "");
			$compl2 = "(SELECT SUM((b.qtd * b.vlunit) - b.discount) 
						FROM buy b 
						INNER JOIN orders or1 ON or1.ordid = b.ordid 
						WHERE DATE_FORMAT(or1.created_at, '%m/%Y') = periodmonth 
						) AS grossstore,";
			$compl3 = "";
		}
		
		$this->sqlQuery = "SELECT 
								{$type} AS type,
								DATE_FORMAT(o.created_at, '%m/%Y') AS periodmonth,
								DATE_FORMAT(o.created_at, '%m-%Y') AS periodlink,
								COUNT(1) AS totorders,
								{$compl2}
								SUM(o.shipping) AS grosslogistic,
								o.refunded AS status
							FROM orders o
							{$compl3} 
							LEFT JOIN logistics l ON l.logid = o.logid
							WHERE o.paid = 1
							{$this->sqlQueryCompl}
							AND o.delivery_status = 2
							GROUP by periodmonth, periodlink, status, type, grossstore
							ORDER BY periodmonth DESC";
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;	
	}

	public function fQueryIndexChart($logid, $strid, $type){
		if ($type == 2){
			$this->sqlQueryCompl = "SELECT 
										DISTINCT(DATE_FORMAT(o.created_at, '%Y-%m-%d')) AS date,
										SUM((b1.qtd * b1.vlunit) - b1.discount) AS value
									FROM buy b1
									INNER JOIN orders o ON o.ordid = b1.ordid
									WHERE b1.strid = {$strid}
									AND o.paid = 1
									AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."'
									GROUP BY date";
		}else if ($type == 3){
			$this->sqlQueryCompl = "SELECT 
										DISTINCT(DATE_FORMAT(o.created_at, '%Y-%m-%d')) AS date,
										SUM(o.shipping) AS value
									FROM orders o 
									INNER JOIN logistics l ON l.logid = o.logid
									WHERE l.usrid = {$logid}
									AND o.paid = 1
									AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."'
									GROUP BY date";
		}else{
			$this->sqlQueryCompl = "SELECT 
										DISTINCT(DATE_FORMAT(o.created_at, '%Y-%m-%d')) AS date,
										SUM((b1.qtd * b1.vlunit) - b1.discount) AS value
									FROM buy b1
									INNER JOIN orders o ON o.ordid = b1.ordid
									WHERE o.paid = 1
									AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."'
									GROUP BY date";
		}
		$this->sqlQuery = $this->sqlQueryCompl;
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	public function fQueryTypeOffers($strid, $type){
		if ($type == 2){
			$this->sqlQueryCompl = " AND sa.strid = {$strid}";
		}	
			$this->sqlQuery = "SELECT
				(SELECT COUNT(1) 
				FROM offers o 
				INNER JOIN storeaddresses sa ON sa.addid = o.addid
				WHERE o.type = 1
				AND o.active = 1 AND o.removed = 0
				$this->sqlQueryCompl) AS product,
				(SELECT COUNT(1) 
				FROM offers o 
				INNER JOIN storeaddresses sa ON sa.addid = o.addid
				WHERE o.type = 2
				AND o.active = 1 AND o.removed = 0
				$this->sqlQueryCompl) AS service,
				(SELECT COUNT(1) 
				FROM offers o 
				INNER JOIN storeaddresses sa ON sa.addid = o.addid
				WHERE o.modality = 1
				AND o.active = 1 AND o.removed = 0
				$this->sqlQueryCompl) AS offer,
				(SELECT COUNT(1) 
				FROM offers o 
				INNER JOIN storeaddresses sa ON sa.addid = o.addid
				WHERE o.modality = 2
				AND o.active = 1 AND o.removed = 0
				$this->sqlQueryCompl) AS promotion,
				(SELECT COUNT(1) 
				FROM offers o 
				INNER JOIN storeaddresses sa ON sa.addid = o.addid
				WHERE o.modality = 3
				AND o.active = 1 AND o.removed = 0
				$this->sqlQueryCompl) AS discount,
				(SELECT COUNT(1) 
				FROM offers o 
				INNER JOIN storeaddresses sa ON sa.addid = o.addid
				WHERE o.active = 1 AND o.removed = 0
				$this->sqlQueryCompl) AS numoffers";
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	public function fQueryTotals4Home($logid, $strid, $type){
		if ($type == 2){

			$date=date_create(date("Y-m-d"));
			date_sub($date,date_interval_create_from_date_string("30 days"));
			$previousMonth = date_format($date,"Y-m"); 

			 $this->sqlQueryCompl = "(SELECT COUNT(1)
										FROM orders o
										INNER JOIN buy b ON b.ordid = o.ordid
										WHERE b.strid = {$strid}
										AND o.paid = 1 
										AND o.refunded = 0 
										AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."') AS numtotseller,
									IFNULL((SELECT SUM((b1.qtd * b1.vlunit) - b1.discount)
										FROM buy b1
										INNER JOIN orders o ON o.ordid = b1.ordid
										WHERE b1.strid = {$strid}
										AND o.paid = 1 
										AND o.refunded = 0 
										AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."'),0) AS totseller,
									IFNULL((SELECT SUM((b1.qtd * b1.vlunit) - b1.discount)
										FROM buy b1
										INNER JOIN orders o ON o.ordid = b1.ordid
										WHERE b1.strid = {$strid}
										AND o.paid = 1 
										AND o.refunded = 1 
										AND o.created_at BETWEEN '".$previousMonth."-01 00:00:00' AND '".$previousMonth."-28 23:59:59'),0) AS totsellerprevious	";
		}
		if ($type == 3){

			$date=date_create(date("Y-m-d"));
			date_sub($date,date_interval_create_from_date_string("30 days"));
			$previousMonth = date_format($date,"Y-m"); 

			$this->sqlQueryCompl = "(SELECT COUNT(1)
										FROM orders o
										INNER JOIN logistics l ON l.logid = o.logid
										WHERE l.usrid = {$logid}
										AND o.paid = 1
										AND o.refunded = 0 
										AND o.delivery_status = 2
										AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."') AS numtotlogger,
									IFNULL((SELECT SUM(o.shipping)
										FROM orders o
										INNER JOIN logistics l ON l.logid = o.logid
										WHERE l.usrid = {$logid}
										AND o.paid = 1 
										AND o.refunded = 0 
										AND o.delivery_status = 2
										AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."'), 0) AS totlogger,
									IFNULL((SELECT SUM(o.shipping)
										FROM orders o
										INNER JOIN logistics l ON l.logid = o.logid
										WHERE l.usrid = {$logid}
										AND o.paid = 1
										AND o.refunded = 1 
										AND o.delivery_status = 2
										AND o.created_at BETWEEN '".$previousMonth."-01 00:00:00' AND '".$previousMonth."-28 23:59:59'), 0) AS totloggerprevious";
		}
		if ($type == 4){
			$this->sqlQueryCompl = "(SELECT COUNT(1)
									   FROM users u
									   ) AS numtotusers,
									(SELECT COUNT(1)
									   FROM offers o
									WHERE o.removed = 0 
									AND o.active = 1   
									   ) AS numtotactiveoffers,
									(SELECT COUNT(1)
									   FROM offers o
									WHERE o.removed = 0 
									AND o.active = 0   
									   ) AS numtotinactiveoffers,
									IFNULL((SELECT SUM((b1.qtd * b1.vlunit) - b1.discount)
									   FROM buy b1
									   INNER JOIN orders o ON o.ordid = b1.ordid
									   WHERE o.paid = 1 
									   AND o.refunded = 0 
									   AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."'),0) AS totseller_r ,
									(SELECT SUM(o.shipping)
									   FROM orders o
									   INNER JOIN logistics l ON l.logid = o.logid
									   WHERE o.paid = 1 
									   AND o.refunded = 0
									   AND o.created_at BETWEEN '".date("Y-m")."-01 00:00:00' AND '".date("Y-m-d H:i:s")."') AS totlogger_r           
								   ";
	   }

		$this->sqlQuery = "SELECT ".$this->sqlQueryCompl;
		$this->fExecuteSql($this->sqlQuery);	
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryOrderDetails($ordid, $logid, $strid, $type){
	
		 if ($type == 2){
			$this->sqlQueryCompl = "AND b.strid = {$strid} AND o1.addid = sa.addid";
						$compl2 = "AND b1.strid = {$strid}";
		}
		if ($type == 3){
			$this->sqlQueryCompl = "AND l.usrid = {$logid}
									AND o1.type = 1 AND o1.addid = sa.addid"; //flag somente para produtos
		}
		if ($type == 4){
			$this->sqlQueryCompl = "AND o1.addid = sa.addid";
						$compl2 = "";
		}
		$this->sqlQuery = "SELECT 
								(SELECT SUM((b1.qtd * b1.vlunit) - b1.discount)
									FROM buy b1
									WHERE b1.ordid = b.ordid ". 
									$compl2.") AS total_order,
								(SELECT COUNT(b2.strid)
								 FROM buy b2
								 WHERE b2.ordid = o.ordid) AS numstores,		
								o.ord_alias,
								o.refunded,
								o.ordid,
								o.logid,
								DATE_FORMAT(o.created_at, '%d/%m/%Y %H:%i') AS date,
								IFNULL(DATE_FORMAT(o.payment_date, '%d/%m/%Y %H:%i'), 0) AS paymentdate,
								o.paid AS paids,
								o.delivery_status,
								o.shipping,
								CASE WHEN (o.paid = 0) THEN
									'<strong class=btn-danger>&nbsp;AGUARDANDO PAGAMENTO&nbsp;</strong>'
								ELSE
									'<strong class=btn-success>&nbsp;PEDIDO PAGO&nbsp;</strong>'
								END AS paid,
								CASE WHEN (o.delivery_status = 0 AND o.paid = 1) THEN
									'AGUARDANDO RETIRADA'
								WHEN (o.delivery_status = 1 AND o.paid = 1) THEN
									'SAIU PARA ENTREGA'	
								WHEN (o.delivery_status = 2 AND o.paid = 1) THEN
									'ENTREGUE'	
								ELSE
									'AGUARDANDO PAGAMENTO'
								END AS delivery,
								b.qtd,
								b.vlunit,
								b.discount,
								u.name AS buyer,
								u1.name AS logger,
								u2.name AS seller,
								o1.prodserv AS offer,
								o1.description,
								s.store,
								s.cpfcnpj,
								s.bank,
								s.agency,
								s.account,
								s.owner,
								CONCAT(l.address, ', ', l.number, ' ', IFNULL(l.complement, ''), ', ', l.neighboor, '<br>CEP ', l.zipcode, ' - ', l.city, '/', l.state) AS fulladdresslogger,
								CONCAT(sa.address, ', ', sa.number, ' ', IFNULL(sa.complement, ''), ', ', sa.neighboor, '<br>CEP ', sa.zipcode, ' - ', sa.city, '/', sa.state) AS fulladdress,
								(SELECT CONCAT(ua.address, ', ', ua.number, ' ', IFNULL(ua.complement, ''), ', ', ua.neighboor, '<br>CEP ', ua.zipcode, ' - ', ua.city, '/', ua.state, '<br>Contato: <li class=\"fa fa-whatsapp\"></li> <strong><a target=\"_blank\" href=\"https://web.whatsapp.com/send?phone=55', ua.tel1, '\">', ua.tel1, '</a></strong>')
									FROM useraddresses ua
									WHERE ua.usrid = b.usrid
									AND ua.type = 1 LIMIT 1) AS useraddress, 
								sa.latitude,
								sa.longitude,
								l.latitude AS latitude1,
								l.longitude As longitude1,
								l.bank AS lbank,
								l.agency AS lagency,
								l.account AS laccount,
								l.owner AS lowner,
								l.cpfcnpj AS lcpfcnpj,
								sa.email,
								sa.whatsapp,
								u1.email AS emaillogger,
								l.whatsapp AS whatsapplogger,
								l.name
							FROM orders o 
							INNER JOIN buy b ON b.ordid = o.ordid
							LEFT JOIN logistics l ON l.logid = o.logid
							INNER JOIN offers o1 ON o1.ofeid = b.ofeid 
							INNER JOIN stores s ON s.strid = b.strid 
							INNER JOIN storeaddresses sa ON sa.strid = s.strid
							INNER JOIN users u ON u.usrid = b.usrid
							LEFT JOIN users u1 ON u1.usrid = l.usrid 
							INNER JOIN users u2 ON u2.usrid = s.usrid 
							WHERE o.ordid = {$ordid} 
							".$this->sqlQueryCompl;		 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		if (count($this->retRecords) == 0){
			return false;
		}else{
			return $this->retRecords;
		}
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryUpdateOrderDeliveryStatus($obj)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Atualizou o pedido: ".$obj['id']." para status (delivery_status): ".$obj['type']);
		
		$this->sqlQuery = "UPDATE orders SET   
								delivery_status = ".$obj['type']." 
								WHERE ordid = ".$obj['id'];

		return $this->fExecuteSql($this->sqlQuery);	
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryUpdateOrderRefundStatus($obj)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Atualizou o pedido: ".$obj['id']." para repasse efetuado.");

		$this->sqlQuery = "UPDATE orders SET   
								refunded = 1 
								WHERE ordid = ".$obj['id'];

		return $this->fExecuteSql($this->sqlQuery);	
	}


	/**
	 * Query Person Store
	 * @return array
	 */
	public function fQueryCategories(){
	
		$this->sqlQuery = "SELECT 
								c.*,
								CASE WHEN (c.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								(SELECT COUNT(1)
								 FROM stores s
								 WHERE s.catid = c.catid AND s.active = 1) AS numstores 
							FROM categories c 
							ORDER BY c.category ASC";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		return $this->retRecords;
	}


	/**
	 * Query Person Category Caracteristics
	 * @return array
	 */
	public function fQueryCategoryCaracteristics($strid, $type){
		if ($type == 4){
			return "1|1|1|1|1|Admin";
		}
		$this->sqlQuery = "SELECT 
								c.dimension,
								c.size,
								c.color,
								c.voltage,
								c.stocks,
								c.category  
							FROM categories c 
							INNER JOIN stores s ON s.catid = c.catid
							WHERE s.strid = {$strid}
							ORDER BY c.category ASC";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		$caracteristics = $this->retRecords[0]['dimension']."|".
							$this->retRecords[0]['size']."|".
							$this->retRecords[0]['color']."|".
							$this->retRecords[0]['voltage']."|".
							$this->retRecords[0]['stocks']."|".
							$this->retRecords[0]['category'];
		return $caracteristics;
	}


	public function fQueryOfferCaracteristics($ofeid){
		
		$this->sqlQuery = "SELECT 
								c.*  
							FROM caracteristics c 
							WHERE c.ofeid = {$ofeid}";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		unset($_SESSION['sCaracteristicData']);
		$_SESSION['sCaracteristicData'] = $this->retRecords;
		return $this->retRecords;
	}


	public function fQueryPaymentStatus($strid)
	{	
		$this->sqlQuery = "SELECT 
								spy.gatid,
								spy.paid,
								spy.payid,
								p.periodmonth
							FROM storepayments spy
							INNER JOIN storeplans sp ON sp.stpid = spy.stpid 
							INNER JOIN plans p ON p.plaid = sp.plaid
							WHERE sp.strid = {$strid}
							AND spy.paid = 0
							ORDER BY spy.payid DESC LIMIT 1";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		return $this->retRecords;
	}


	public function fQueryUpdatePaymentStatus($payid, $status, $period)
	{	
		$expires = date('Y-m-d H:i:s', strtotime("+".($period * 30)." days"));

		$this->sqlQuery = "UPDATE storepayments SET
									paid = ".($status == 3 || $status == 4 ? 1 : 0).",
									expires = ".($status == 3 || $status == 4 ? "'".$expires."'" : "expires").",
									cancelled = ".($status == 6 || $status == 7 ? 1 : 0)."
							WHERE payid = {$payid}";			 
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	public function fQuerySaveOfferPhoto($ofeid, $photo)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Inseriu a foto: ".$photo." para a oferta ".$ofeid);

		$this->sqlQuery = "INSERT INTO photos 
											(ofeid,
											photo) 
										VALUES 
											($ofeid, 
											'$photo')";

		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	public function fQuerySaveStorePhoto($addid, $photo)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Inseriu a foto: ".$photo." para o local (addid): ".$addid);

		$this->sqlQuery = "INSERT INTO photos 
											(addid,
											photo) 
										VALUES 
											($addid, 
											'$photo')";

		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	public function fQuerySaveStoreImage($strid, $photo)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], $strid, 0, "Atualizou a logo ".$photo." para o estabelecimento (strid): ".$strid);

		$this->sqlQuery = "UPDATE stores SET 
								logo = '{$photo}' 
							WHERE strid = {$strid}";
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	public function fQueryRemoveImage($phoid)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Removeu a foto (phoid): ".$phoid);

		$this->sqlQuery = "DELETE FROM photos 
							WHERE phoid = {$phoid}";
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	public function fQueryRemoveStoreImage($strid)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], $strid, 0, "Atualizou a logo do estabelecimento para nula.");

		$this->sqlQuery = "UPDATE stores SET
								logo = NULL 
							WHERE strid = {$strid}";
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	 * Query Plans
	 * @return array
	 */
	public function fQueryPlans($actives){
		if ($actives){
			$this->sqlQueryCompl = " WHERE p.active = 1";
			$ord = " ORDER BY p.order ASC";
		}else{
			$this->sqlQueryCompl = "";
		}	
		$this->sqlQuery = "SELECT 
								p.*,
								CASE WHEN (p.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								CASE WHEN (p.numoffers = 9999) THEN
									'ILIMITADO'
								ELSE
									CONCAT(p.numoffers, ' ofertas/promo&ccedil;&otilde;es')
								END AS offers,
								(SELECT COUNT(1)
								 FROM storeplans sp
								 INNER JOIN stores s ON s.strid = sp.strid 
								 WHERE sp.plaid = p.plaid AND s.active = 1) AS numusers 
							FROM plans p ".$this->sqlQueryCompl.$ord;			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		return $this->retRecords;
	}


	/**
	 * Query Coupons
	 * @return array
	 */
	public function fQueryCoupons($strid, $type){
		if ($type == 2){
			$this->sqlQueryCompl = " AND c.strid = {$strid}";
		}
		$this->sqlQuery = "SELECT 
								c.*,
								IFNULL(s.store, 'Geral Se Liga A&iacute;') AS store,
								IFNULL((SELECT COUNT(1) 
								 FROM usercoupons uc
								 WHERE uc.couid = c.couid), 0) AS numusers,
								DATE_FORMAT(c.expires, '%d/%m/%Y') AS expires,
								CASE WHEN(DATEDIFF(c.expires, now()) < 0) THEN
									'<strong class=btn-danger>&nbsp;EXPIRADO&nbsp;</strong>'
								WHEN(DATEDIFF(c.expires, now()) < 4) THEN
									'<strong class=btn-warning>&nbsp;VENCENDO&nbsp;</strong>'	
								END AS diffdate 
							FROM coupons c 
							LEFT JOIN stores s ON s.strid = c.strid 
							WHERE c.removed = 0 ".$this->sqlQueryCompl;			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		return $this->retRecords;
	}


	/**
	 * Query Coupons
	 * @return array
	 */
	public function fQueryCoupom($couid, $strid, $type){
		if ($type == 2){
			$this->sqlQueryCompl = " AND c.strid = {$strid}";
		}
		$this->sqlQuery = "SELECT 
								c.*,
								DATE_FORMAT(c.expires, '%Y-%m-%d') AS expires 
							FROM coupons c 
							WHERE c.removed = 0
							AND c.couid = {$couid} ".$this->sqlQueryCompl;			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		return $this->retRecords;
	}


	/**
	 * Query Plan Details
	 * @return array
	 */
	public function fQueryPlanDetails($plaid){
		$this->sqlQuery = "SELECT 
								p.*,
								CASE WHEN (p.active = 1) THEN
									'<strong class=btn-success>&nbsp;SIM&nbsp;</strong>'
								ELSE
									'<strong class=btn-danger>&nbsp;N&Atilde;O&nbsp;</strong>'
								END AS actived,	
								CASE WHEN (p.numoffers = 9999) THEN
									'ILIMITADO'
								ELSE
									CONCAT(p.numoffers, ' ofertas/promo&ccedil;&otilde;es')
								END AS offers,
								(SELECT COUNT(1)
								 FROM storeplans sp
								 INNER JOIN stores s ON s.strid = sp.strid 
								 WHERE sp.plaid = p.plaid AND s.active = 1) AS numusers 
							FROM plans p 
							WHERE p.plaid = {$plaid}
							AND p.active = 1";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		return $this->retRecords;
	}


	/**
	 * Query Plans VER MELHOR A LOGICA
	 * @return array
	 */
	public function fQueryStorePlans($strid, $type){
		if ($type == 2){
			$this->sqlQueryCompl = "AND s.strid = {$strid}";
		}
		$this->sqlQuery = "SELECT 
								s.strid,
								sp.stpid,
								p.plan,
								s.store,
								spy.value,
								spy.cancelled,
								p.numoffers,
								CASE WHEN (spy.paid = 0) THEN
									'AGUARDANDO PAGAMENTO'
								ELSE
									'PAGO'
								END AS paid,
								DATEDIFF(spy.expires, now()) AS expired,
								CASE WHEN(DATEDIFF(spy.expires, now()) < 0 AND spy.paid = 1) THEN
									'<strong class=btn-danger>&nbsp;EXPIRADO&nbsp;</strong>'
								WHEN(DATEDIFF(spy.expires, now()) < 6 AND spy.paid = 1) THEN
									'<strong class=btn-warning>&nbsp;VENCENDO&nbsp;</strong>'	
								WHEN spy.paid = 0 AND spy.cancelled = 0 THEN
									'<strong class=btn-danger>&nbsp;AGUARDANDO PAGAMENTO&nbsp;</strong>'
								WHEN spy.paid = 0 AND spy.cancelled = 1 THEN
									'<strong class=btn-dark>&nbsp;CANCELADO&nbsp;</strong>'		
								ELSE
									'<strong class=btn-success>&nbsp;VIGENTE&nbsp;</strong>'
								END AS diffdate,
								DATE_FORMAT(spy.expires, '%d/%m/%Y %H:%i') AS expires,
								IFNULL((SELECT
											COUNT(o.ofeid)
										FROM offers o
										INNER JOIN storeaddresses sa ON sa.addid = o.addid
										WHERE sa.strid = s.strid
										AND o.plaid = p.plaid), 0) AS used
							FROM storepayments spy
							INNER JOIN storeplans sp ON sp.stpid = spy.stpid
							INNER JOIN stores s ON s.strid = sp.strid
							INNER JOIN plans p ON p.plaid = sp.plaid
							{$this->sqlQueryCompl}
							GROUP BY s.strid,
									sp.stpid,
									p.plan,
									s.store,
									spy.value,
									spy.cancelled,
									p.numoffers,
									paid,
									diffdate,
									expired,
									expires,
									used";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();

		return $this->retRecords;
	}


	/**
	 * Query Plans VER MELHOR A LOGICA
	 * @return array
	 */
	public function fQueryPlanPreviouslyAquired($strid)
	{
		$this->sqlQuery = "SELECT 
								(SELECT 
									DATEDIFF(spy.expires, now()) 
								FROM storeplans sp 
								INNER JOIN storepayments spy ON spy.stpid = sp.stpid 
								WHERE sp.strid = {$strid} 
								AND spy.paid = 1
								AND spy.payid IS NOT NULL 
								ORDER BY spy.payid DESC
								LIMIT 1) AS diffdays,
								spy.paid,
                                spy.cancelled
							FROM storepayments spy
							INNER JOIN storeplans sp ON sp.stpid = spy.stpid
							INNER JOIN stores s ON s.strid = sp.strid
							WHERE s.strid = {$strid}
							ORDER BY spy.payid DESC LIMIT 1";			 
		$this->fExecuteSql($this->sqlQuery);
		return $this->retRecords = $this->fShowRecords();
	}


	/**
	 * Update Address
	 *
	 * @author    Daniel Triboni
	 * @param	 object $_REQUEST
	 * @return	 boolean
	 */
	public function fQueryUpdateAddress($obj){
		
		if (!$this->fQueryAddress($obj['id'], $_SESSION['sPersonID'], $_SESSION['sPersonType']))
		{
			return false;

		}else{

			$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Atualizou o endereco (addid): ".$obj['id']);

			$office_day = ($obj['deUI']." ".$obj['deUF']." - ".$obj['hUI']." ".$obj['hUF']);
			$office_day_we = "<br>".($obj['sI']." ".$obj['sF']."<br>".$obj['dI']." ".$obj['dF']);

			if ($obj['deUI'] != "" && $obj['deUF'] != "")
				$open24H = $office_day.$office_day_we;
			else
				$open24H = "Informa&ccedil;&otilde;es n&atilde;o dispon&iacute;veis!";

			$this->sqlQuery = "UPDATE storeaddresses SET   
										address = '".$obj['address']."', 												  
										number = '".$obj['number']."', 
										complement = '".$obj['complement']."', 
										neighboor = '".$obj['neighboor']."',
										zipcode = '".$obj['zipcode']."', 
										city = '".$obj['city']."', 
										state = '".$obj['state']."', 
										tel1 = '".$obj['tel1']."',
										tel2 = '".$obj['tel2']."', 
										whatsapp = '".$obj['whatsapp']."', 
										url = '".$obj['url']."', 
										email = '".$obj['email']."',
										twitter = '".$obj['twitter']."',
										open24h = '".$open24H."',
										instagram = '".$obj['instagram']."', 
										latitude = '".$obj['latitude']."', 
										longitude = '".$obj['longitude']."' ";
			
			$this->sqlQuery	.= "WHERE addid = ".$obj['id'];
			return $this->fExecuteSql($this->sqlQuery);	
		}								
	}


	/**
	 * Update Logistic
	 *
	 * @author    Daniel Triboni
	 * @param	 object $_REQUEST
	 * @return	 boolean
	 */
	public function fQueryUpdateLogistic($obj){
		
		if (!$this->fQueryLogistic($obj['id'], $_SESSION['sPersonID'], $_SESSION['sPersonType']))
		{
			return false;

		}else{

			$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, $obj['id'], "Atualizou os dados do entregador (logid): ".$obj['id']);

			$this->sqlQuery = "UPDATE logistics SET   
										name = '".$obj['name']."',
										address = '".$obj['address']."', 												  
										number = '".$obj['number']."', 
										complement = '".$obj['complement']."', 
										neighboor = '".$obj['neighboor']."',
										zipcode = '".$obj['zipcode']."', 
										city = '".$obj['city']."', 
										state = '".$obj['state']."', 
										tel = '".$obj['tel']."',
										cellphone = '".$obj['cellphone']."', 
										whatsapp = '".$obj['whatsapp']."', 
										bank = '".$obj['bank']."', 
										agency = '".$obj['agency']."',
										account = '".$obj['account']."',
										owner = '".$obj['owner']."', 
										latitude = '".$obj['latitude']."', 
										longitude = '".$obj['longitude']."' ";
			
			$this->sqlQuery	.= "WHERE logid = ".$obj['id'];
			return $this->fExecuteSql($this->sqlQuery);	
		}								
	}


/**
	 * Update Offer
	 *
	 * @author    Daniel Triboni
	 * @param	 object $_REQUEST
	 * @return	 boolean
	 */
	public function fQueryUpdateOffer($obj){
		
		if (!$this->fQueryOffer($obj['id'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']))
		{
			return false;

		}else{

			$oldprice = ($obj['modality'] == 3 ? 0 : $obj['oldprice']);
			$newprice = ($obj['modality'] == 1 || $obj['modality'] == 3 ? 0 : $obj['newprice']);
			$discount = ($obj['modality'] == 3 ? $obj['discount'] : (($obj['oldprice'] - $newprice) / $obj['oldprice']) * 100);
			$discount = round($obj['modality'] == 1 ? 0 : $discount);

			$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Atualizou os dados da oferta (ofeid): ".$obj['id']);

			$this->sqlQuery = "UPDATE offers SET   
										addid = ".$obj['addid'].", 												  
										start = '".$obj['start']." 00:00:00',
										end = '".$obj['end']." 23:59:59', 
										modality = ".$obj['modality'].", 
										onlinesell = ".$obj['onlinesell'].", 
										oldprice = ".$oldprice.", 
										newprice = ".$newprice.",
										discount = ".$discount.", 
										description = '".$obj['description']."', 
										prodserv = '".$obj['prodserv']."', 
										type = ".$obj['type']." ";
			
			$this->sqlQuery	.= "WHERE ofeid = ".$obj['id'];
			return $this->fExecuteSql($this->sqlQuery);	
		}								
	}



	/**
	* Save New Person Offer
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewOffer($obj){

		$this->sqlQuery = "SELECT 
								sp.plaid
							FROM storeplans sp 
							INNER JOIN storeaddresses sa ON sa.strid = sp.strid
							INNER JOIN storepayments spy ON spy.stpid = spy.stpid 
							WHERE sa.addid = ".$obj['addid']." 
							AND spy.paid = 1
							ORDER BY spy.payid DESC
							LIMIT 1";			 
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		$plaid = $this->retRecords[0]['plaid'];

		$oldprice = ($obj['modality'] == 3 ? 0 : $obj['oldprice']);
		$newprice = ($obj['modality'] == 1 || $obj['modality'] == 3 ? 0 : $obj['newprice']);
		$discount = ($obj['modality'] == 3 ? $obj['discount'] : (($obj['oldprice'] - $newprice) / $obj['oldprice']) * 100);
		$discount = round($obj['modality'] == 1 ? 0 : $discount);

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Incluiu nova oferta para o endereco (addid): ".$obj['addid']);

		$this->sqlQuery = "INSERT INTO offers (addid, proid, prodserv, description, modality,
														start, end, oldprice, newprice, discount,
														type, plaid, onlinesell)
										 VALUES (".$obj['addid'].", 0, '".$obj['prodserv']."', '".$obj['description']."', ".$obj['modality'].", 
										 	     '".$obj['start']." 00:00:00', '".$obj['end']." 23:59:59', ".$oldprice.",  ".$newprice.", ".$discount.",  
										 	     ".$obj['type'].", ".$plaid.", ".$obj['onlinesell'].")";
		$this->fExecuteSql($this->sqlQuery);
		return $this->fGetLastInsertID();
	}


	/**
	* Save New Person Store
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewUser($obj)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Incluiu novo usuario (email): ".$obj['email']);

		if ($_SESSION['sPersonID'] == 4)
		{
			$cua = ($obj['type'] == 2 || $obj['type'] == 3 ? $obj['canuseadmin'] : 0);
			$cus = ($obj['type'] == 2 ? $obj['canseller'] : 0);

			$query1 = ", canuseadmin, canseller";
			$query2 = ", {$cua}, {$cus}";
		
		}else{

			$query1 = "";
			$query2 = "";
		}

		$this->sqlQuery = "INSERT INTO users (type, signup, name, email, gender,
												password, borndate {$query1})
										 VALUES (".$obj['type'].", '".$obj['signup']."', '".$obj['name']."', '".$obj['email']."', '".$obj['gender']."',
										 '".md5("seligaai")."', '".$obj['borndate']."' {$query2})";
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	* Save New Pre User
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewPreUser($obj)
	{
		$this->fQuerySaveLOG(0, 0, 0, "Incluido novo pre-usuario (email): ".$obj['email']);

		$this->sqlQuery = "INSERT INTO users (type, signup, name, email, gender,
												password, borndate, canuseadmin, canseller, active, cellphone)
										 VALUES (".$obj['type'].", 'email', '".$obj['name']."', '".$obj['usr']."', '',
										 '0000', '', 0, 0, 0, '".$obj['tel']."')";
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	* Save New Person Coupom
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewCoupom($obj){

		if ($_SESSION['sPersonType'] == 4){
			$strid = $obj['strid'];
		}else{
			$strid = $_SESSION['sPersonStoreID'];
		}

		$this->fQuerySaveLOG($_SESSION['sPersonID'], $strid, 0, "Incluiu novo cupom de desconto para estabelecimento (strid): ".$strid);

		$this->sqlQuery = "INSERT INTO coupons (strid, expires, value, 
												maxusagepershop, code)
										 VALUES (".$strid.", '".$obj['expires']."', ".$obj['value'].", 
										 		".$obj['maxusagepershop'].", '".$obj['code']."')";
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	* Update Person Coupom
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQueryUpdateCoupom($obj){

		if ($_SESSION['sPersonType'] == 4){
			$strid = $obj['strid'];
		}else{
			$strid = $_SESSION['sPersonStoreID'];
		}

		$this->fQuerySaveLOG($_SESSION['sPersonID'], $strid, 0, "Alterou o cupom de desconto para estabelecimento (strid): ".$strid);

		$this->sqlQuery = "UPDATE coupons SET strid = ".$strid.", 
										      expires = '".$obj['expires']."', 
											  value = ".$obj['value'].", 
											  maxusagepershop = ".$obj['maxusagepershop']."
							 WHERE couid = ".$obj['id'];
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	* Save New Person Store
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQueryUpdateUser($obj){

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Alterou os dados do usuario (usrid): ".$obj['id']);

		if ($_SESSION['sPersonType'] == 4)
		{
			$cua = ($obj['type'] == 2 || $obj['type'] == 3 ? $obj['canuseadmin'] : 0);
			$cus = ($obj['type'] == 2 ? $obj['canseller'] : 0);

			$query1 = ", canuseadmin = {$cua}, canseller = {$cus} ";
		
		}else{

			$query1 = "";
		}

		$this->sqlQuery = "UPDATE users SET  name = '".$obj['name']."', 
											 gender = '".$obj['gender']."',
											 password = '".md5($obj['pwd'])."', 
											 borndate = '".$obj['borndate']."', 
											 type = ".$obj['type']." 
											 {$query1}   
							WHERE usrid = ".$obj['id'];
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}



	/**
	* Save New Person Store
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewStore($obj)
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Incluiu novo estabelecimento LOJISTA para o usuario (usrid): ".$obj['usrid']);

		$this->sqlQuery = "INSERT INTO stores (usrid, catid, store, description, cpfcnpj,
												bank, agency, account, owner, youtube)
										 VALUES (".$obj['usrid'].", ".$obj['catid'].", '".$obj['store']."', '".nl2br($obj['description'])."', '".$obj['cpfcnpj']."',
										 '".$obj['bank']."', '".$obj['agency']."', '".$obj['account']."', '".$obj['owner']."', '".$obj['youtube']."')";
		$this->fExecuteSql($this->sqlQuery);
		return $this->fGetLastInsertID();
	}


	/**
	* Save New Person Store
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQueryUpdateStore($obj, $type){
		if ($type == 4){
			$this->sqlQueryCompl = "usrid = ".$obj['usrid'].", ";
		}

		$this->fQuerySaveLOG($_SESSION['sPersonID'], $obj['id'], 0, "Alterou o estabelecimento LOJISTA (strid): ".$obj['id']);

		$this->sqlQuery = "UPDATE stores SET 
								catid = ".$obj['catid'].",
								bank = '".$obj['bank']."',
								agency = '".$obj['agency']."',
								account = '".$obj['account']."',
								owner = '".$obj['owner']."', 
								cpfcnpj = '".$obj['cpfcnpj']."', 
								{$this->sqlQueryCompl}
								store = '".$obj['store']."',
								youtube = '".$obj['youtube']."', 
								description = '".nl2br($obj['description'])."'  
							WHERE strid = ".$obj['id'];
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	* Set Plan As Paid
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySetPlanAsPaid($stpid, $type){
		if ($type != 4){
			return false;
		}

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Alterou o plano contratado (stpid): ".$stpid." como PAGO.");

		$this->sqlQuery = "UPDATE storepayments SET 
								paid = 1 
							WHERE stpid = ".$stpid;
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}


	/**
	* Set Plan As Paid
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySetPlanAsCancelled($stpid, $type){
		if ($type != 4){
			return false;
		}

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Alterou o plano contratado (stpid): ".$stpid." como CANCELADO.");

		$this->sqlQuery = "UPDATE storepayments SET 
								cancelled = 1 
							WHERE stpid = ".$stpid;
		$this->fExecuteSql($this->sqlQuery);
		return true;
	}



	/**
	* Save Person Offer Caracteristics
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveOfferCaracteristics($ofeid, $caracteristics){

		if (count($caracteristics) > 0)
		{
			$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Removeu caracteristicas da oferta (ofeid): ".$ofeid);

			$this->sqlQuery = "DELETE 
								FROM caracteristics  
								WHERE ofeid = {$ofeid}";			 
			$this->fExecuteSql($this->sqlQuery);
		}

		for ($c = 0; $c < count($caracteristics); $c++)
		{
			$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Incluiu a caracteristica ".$caracteristics[$c]['weight']." kg, 
																						 dimensao: ".$caracteristics[$c]['dimensions'].",
																						 estoque: ".$caracteristics[$c]['stock']."/ min  
																						 ".$caracteristics[$c]['stockmin'].", 
																						 tamanho: ".$caracteristics[$c]['size'].",
																						 cor: ".$caracteristics[$c]['color']."', 
																						 ".$caracteristics[$c]['voltage']."volts, para a oferta (ofeid): ".$ofeid);

			$this->sqlQuery = "INSERT INTO caracteristics 
										(ofeid, weight, dimensions, 
										stock, stockmin, size, 
										color, voltage) 
								VALUES ({$ofeid}, ".$caracteristics[$c]['weight'].", '".$caracteristics[$c]['dimensions']."',
								".$caracteristics[$c]['stock'].", ".$caracteristics[$c]['stockmin'].", '".$caracteristics[$c]['size']."',
								'".$caracteristics[$c]['color']."', '".$caracteristics[$c]['voltage']."')";
			$this->fExecuteSql($this->sqlQuery);
		}
		return true;
	}



	/**
	* Save New Person Address
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewAddress($obj){
		
		if ($_SESSION['sPersonType'] == 4){
			$strid = $obj['strid'];
		}else{
			$strid = $_SESSION['sPersonStoreID'];
		}

		$this->sqlQuery = "SELECT s.store
								FROM stores s
							WHERE s.strid = {$strid}";
		$this->fExecuteSql($this->sqlQuery);
		$this->retRecords = $this->fShowRecords();
		$branchname = $this->retRecords[0]['store'];

		$this->fQuerySaveLOG($_SESSION['sPersonID'], $strid, 0, "Inseriu endereco para o estabelecimento (strid): ".$strid);

		$office_day = ($obj['deUI']." ".$obj['deUF']." - ".$obj['hUI']." ".$obj['hUF']);
		$office_day_we = "<br>".($obj['sI']." ".$obj['sF']."<br>".$obj['dI']." ".$obj['dF']);

		if ($obj['deUI'] != "" && $obj['deUF'] != "")
			$open24H = $office_day.$office_day_we;
		else
			$open24H = "Informa&ccedil;&otilde;es n&atilde;o dispon&iacute;veis!";	

		$this->sqlQuery = "INSERT INTO storeaddresses (strid, address, branchname, number, complement, neighboor,
														zipcode, city, state, tel1, tel2,
														url, email, whatsapp, twitter, open24h, 
														instagram, latitude, longitude)
										 VALUES (".$strid.", '".$obj['address']."', '".$branchname."', '".$obj['number']."', '".$obj['complement']."', '".$obj['neighboor']."', 
										 	     '".$obj['zipcode']."', '".$obj['city']."', '".$obj['state']."',  '".$obj['tel1']."', '".$obj['tel2']."',  
										 	     '".$obj['url']."', '".$obj['email']."', '".$obj['whatsapp']."', '".$obj['twitter']."', '".$open24H."',  
										 		 '".$obj['instagram']."', '".$obj['latitude']."', '".$obj['longitude']."')";
		
		$this->fExecuteSql($this->sqlQuery);
		return $this->fGetLastInsertID();
	}


	/**
	* Save New Person Logistic
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQuerySaveNewLogistic($obj){

		if ($_SESSION['sPersonType'] == 4){
			$usrid = $obj['usrid'];
		}else{
			$usrid = $_SESSION['sPersonID'];
		}

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Inseriu estabelecimento LOGISTICO para o usuario (usrid): ".$usrid);

		$this->sqlQuery = "INSERT INTO logistics (usrid, name, cpfcnpj, address, number, complement, neighboor,
														zipcode, city, state, tel, cellphone,
														whatsapp, bank, agency, account
														owner, latitude, longitude)
										 VALUES (".$usrid.", '".$obj['name']."', '".$obj['cpfcnpj']."', '".$obj['address']."', '".$obj['number']."', '".$obj['complement']."', '".$obj['neighboor']."', 
										 	     '".$obj['zipcode']."', '".$obj['city']."', '".$obj['state']."',  '".$obj['tel']."', '".$obj['cellphone']."',  
										 	     '".$obj['whatsapp']."', '".$obj['bank']."', '".$obj['agency']."', '".$obj['account']."',  
										 		 '".$obj['owner']."', '".$obj['latitude']."', '".$obj['longitude']."')";
		
		return $this->fExecuteSql($this->sqlQuery);
	}


	/**
	* Remove Record Logically
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQueryRemoveRecord($reg, $field, $id){

		// Transpile entity names AND create removed field in all tables
		if ($reg == 'sa') $reg = "storeaddresses";
		if ($reg == 'ua') $reg = "useraddresses";
		if ($reg == 'co') $reg = "coupons";
		if ($reg == 'o') $reg = "offers";
		if ($reg == 's') $reg = "stores";
		if ($reg == 'u') $reg = "users";
		if ($reg == 'l') $reg = "logistics";

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Removeu logicamente (".$field.") o registro ".$id);

		$this->sqlQuery = "UPDATE {$reg} SET 
								removed = 1 
							WHERE {$field} = {$id}";
		
		return $this->fExecuteSql($this->sqlQuery);
	}


	/**
	* Change Record Logically
	*	
	* @author    Daniel Triboni
	* @param	 object $_REQUEST 
	* @return	 boolean
	*/
	public function fQueryChangeRecord($reg, $field, $id, $status){

		// Transpile entity names
		if ($reg == 's') $reg = "stores";
		if ($reg == 'u') $reg = "users";
		if ($reg == 'c') $reg = "categories";
		if ($reg == 'p') $reg = "plans";
		if ($reg == 'o') $reg = "offers";
		if ($reg == 'l') $reg = "logistics";

		$this->fQuerySaveLOG($_SESSION['sPersonID'], 0, 0, "Atualizou status de visibilidade de (".$field.") o registro ".$id." para ".($status == 1 ? "ATIVO." : "INATIVO."));

		$this->sqlQuery = "UPDATE {$reg} SET 
								active = {$status} 
							WHERE {$field} = {$id}";
		
		return $this->fExecuteSql($this->sqlQuery);
	}
	
	
	/**
	 * Save New Person plan
	 *
	 * @author    Daniel Triboni
	 * @param	 object $_REQUEST
	 * @return	 boolean
	 */
	public function fQuerySavePersonPlan($obj, $paid = 0){

        $this->sqlQuery = "INSERT INTO storeplans (strid, plaid) 
						   VALUES (".$obj['strid'].", ".$obj['plaid'].")";
		
		if($this->fExecuteSql($this->sqlQuery))
		{
			$stpid = $this->fGetLastInsertID();				   

			$expires = date('Y-m-d H:i:s', strtotime("+".($obj['periodmonth'] * 30)." days"));
			
			$this->fQuerySaveLOG($_SESSION['sPersonID'], $obj['strid'], 0, "Contratou novo plano (plaid): ".$obj['plaid']." para o estabelecimento (strid): ".$obj['strid']);

			$this->sqlQuery = "INSERT INTO storepayments (stpid, value, expires, paid, gatid) 
                               VALUES ($stpid, ".$obj['value'].", '$expires', {$paid}, '".$obj['gatid']."')";

			if($this->fExecuteSql($this->sqlQuery))
			{
				return true;
				
			}else{
				
				return false;
			}
		}else{

			return false;
		}
	}
	
    
    /**
     * Get Query Person/User Email
     *
     * @author    Daniel Triboni
     * @return	 boolean
     */
    public function fQueryPersonEmail($email, $table){
    	$this->sqlQuery = "SELECT
					    		p.email
					    	FROM {$table} p
							WHERE p.email = '{$email}' 
							AND p.type > 1";
    	$this->fExecuteSql($this->sqlQuery);
    	$this->retRecords = $this->fShowRecords();
    	return (count($this->retRecords) == 0 ? true : false);
	}
	

	/**
     * Get Query Person/User Email
     *
     * @author    Daniel Triboni
     * @return	 boolean
     */
    public function fQueryBanks(){
    	$this->sqlQuery = "SELECT
					    		*
					    	FROM banks
							ORDER BY banid ASC";
    	$this->fExecuteSql($this->sqlQuery);
    	$this->retRecords = $this->fShowRecords();
    	return $this->retRecords;
    }
    
    
    /**
     * Get Query User Email
     *
     * @author    Daniel Triboni
     * @return	 boolean
     */
    public function fQueryUserEmail($email){
    	$this->sqlQuery = "SELECT
					    		u.email
					    	FROM users u
					    	WHERE u.email = '{$email}'";
    	$this->fExecuteSql($this->sqlQuery);
    	$this->retRecords = $this->fShowRecords();
    	return (count($this->retRecords) == 0 ? true : false);
	}
	
	/**
     * Get Query User Register
     *
     * @author    Daniel Triboni
     * @return	 boolean
     */
    public function fQueryUserRegister($strid){
    	$this->sqlQuery = "SELECT
								s.store,
								s.cpfcnpj,
								DATE_FORMAT(u.borndate, '%d/%m/%Y') AS datanasc,
								u.email,
					    		sa.tel1,
								sa.tel2,
								sa.whatsapp
					    	FROM users u
							INNER JOIN stores s ON s.usrid = u.usrid
							INNER JOIN storeaddresses sa ON sa.strid = s.strid
					    	WHERE s.strid = {$strid}";
    	$this->fExecuteSql($this->sqlQuery);
    	$this->retRecords = $this->fShowRecords();
    	return $this->retRecords;
    }
    
    
    /**
     * Get Query Person Email
     *
     * @author    Daniel Triboni
     * @return	 array
     */
    public function fQueryRetrievePassword($email, $table){
    	$this->sqlQuery = "SELECT
						    		p.*
						    	FROM {$table} p
						    	WHERE p.email = '{$email}'";    	
    	$this->fExecuteSql($this->sqlQuery);
    	$this->retRecords = $this->fShowRecords();
    	return $this->retRecords;
    }
    
    
    /**
     * Query to Update Person Credentials
     * @param string $email
     * @param string $password
     */
	public function fQueryUpdatePersonCredentials($email, $password) 
	{
		$this->fQuerySaveLOG(0, 0, 0, "Alterou a senha para o email: ".$email);

    	$this->sqlQuery = "UPDATE users SET
    							password = '".md5($password)."'
					    	WHERE email = '{$email}'";
    	$this->fExecuteSql($this->sqlQuery);    	
    	return true;
    }
}