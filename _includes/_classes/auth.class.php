<?php

/** ***************************************************************
* Classe de Autenticacoes - extendida da classe functions
*
* @author    Daniel Triboni
* @copyright (c) 2013 - EM2D - Todos os Direitos Reservados
******************************************************************/
class auth extends functions {

	
	/**
	* Metodo Construtor da Classe
	*
	* @author    Daniel Triboni
	* 
	* @return    resource
	*/
	public function __construct() {
		parent::__construct(false);
	}
	
	
	/**
	 * Authenticate Persons in Admin
	 *
	 * @author    Daniel Triboni
	 * 
	 * @param     string Usuario
	 * 
	 * @param     string Senha
	 * 
	 * @return    boolean
	 */
	public function fAuthenticatePerson($strEmail, $strPass){
		if(!(empty($strEmail)) && (!empty($strPass))){
			$sql = "SELECT
						p.*,
						IFNULL(s.strid, 0) AS strid,
						IFNULL(l.usrid, 0) AS logger,
						(SELECT COUNT(1)
						FROM orders o
						WHERE o.logid = l.logid) AS numorderslogger,
						(SELECT COUNT(1)
						FROM orders o
						INNER JOIN buy b ON b.ordid = o.ordid
						WHERE b.strid = s.strid) AS numordersseller
					FROM
						users p
					LEFT JOIN stores s ON s.usrid = p.usrid 
					LEFT JOIN logistics l ON l.usrid = p.usrid 
					WHERE
						p.active = 1
					AND p.signup = 'email' 
					AND p.type IN (2,3,4) 
					AND	p.email = '".$strEmail."' 
					AND p.canuseadmin = 1 
					AND p.password = '".md5($strPass)."'";
			$this->fExecuteSql($sql);
			if ($this->fNumRows() > 0){
				$ret = $this->fShowRecords();
				$_SESSION['sPersonLogged'] = true;
				$_SESSION['sPersonID'] = $ret[0]['usrid'];
				$_SESSION['sPersonStoreID'] = $ret[0]['strid'];
				$_SESSION['sPersonLoggerID'] = $ret[0]['logger'];
				$_SESSION['sPersonLoggerOrders'] = $ret[0]['numorderslogger'];
				$_SESSION['sPersonSellerOrders'] = $ret[0]['numordersseller'];
				$_SESSION['sPersonType'] = $ret[0]['type'];
				$_SESSION['sPersonName'] = $ret[0]['name'];
				$_SESSION['sPersonPreName'] = $this->fReduceName($ret[0]['name']);				
				$_SESSION['sPersonEmail'] = $ret[0]['email'];
				$_SESSION['sPersonGender'] = $ret[0]['gender'];
				$_SESSION['sPersonSessionTime'] = time();

				$this->fQuerySaveLOG($_SESSION['sPersonID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonLoggerID'], "Entrou no portal.");

				return true;
				
			}else{

				$this->fQuerySaveLOG(0,0,0, "Tentativa de login com email ".$strEmail.".");
				
				return false;
			}
			
		}else{
			
			return false;
		}
	}

	
	/**
	 * Logoff Person and User
	 *
	 * @author    Daniel Triboni
	 * 
	 * @return    true
	 */
	public function fLogoutPerson()
	{
		$this->fQuerySaveLOG($_SESSION['sPersonID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonLoggerID'], "Saiu do portal.");
		unset($_SESSION);
		unset($_REQUEST);		
		session_destroy();
		header('Location: '.SIS_URL.'login');
		exit;
	}
	
    
	
	
	
	/**
	 * Check if Person is Logged
	 *
	 * @author    Daniel Triboni
	 * 
	 * @return    string
	 */
	public function fCheckIsPersonLogged(){
		if ($_SESSION['sPersonSessionTime'] < (time() - SIS_TEMPO)) {
			$_SESSION['sPersonLogged'] = false;
		}else{
			$_SESSION['sSessionTime'] = time();
			return true;
		}
		if($_SESSION['sPersonLogged'] == false){
			$this->fLogoutPerson();
			return false;
		}
	}

		
	/**
	* Check if user email already exists
	*
	* @author    Daniel Triboni
	* 
	* @param     string Email
	*/
	private function fCheckIfUserEmailExists($strEmail){
		if(!(empty($strEmail))){
			$sql = "SELECT 
						u.email
					FROM 
						usuarios u 
					WHERE 
						u.email = '".$this->fEscapeString($strEmail)."'";			
			$this->fExecuteSql($sql);	
			if ($this->fNumRows() > 0){
				return true;			
			}else 
				return false;
		}	
	}
    
	
	/**
	* Check if user nickname already exists
	*
	* @author    Daniel Triboni
	* 
	* @param     string Apelido
	*/
	private function fCheckIfUserNickExists($strNick){
		if(!(empty($strNick))){
			$sql = "SELECT 
						u.apelido
					FROM 
						usuarios u 
					WHERE 
						u.apelido = '".$this->fEscapeString($strNick)."'";			
			$this->fExecuteSql($sql);	
			if ($this->fNumRows() > 0){
				return true;			
			}else 
				return false;
		}	
	}
	
	
	/**
	 * Check if person email already exists
	 *
	 * @author    Daniel Triboni
	 * 
	 * @param     string Email
	 */
	private function fCheckIfPersonEmailExists($strEmail){
		if(!(empty($strEmail))){
			$sql = "SELECT
						p.email
					FROM
						pessoas p
					WHERE
						p.email = '".$this->fEscapeString($strEmail)."'";
			$this->fExecuteSql($sql);
			if ($this->fNumRows() > 0){
				return true;
			}else
				return false;
		}
	}
	
	
	/**
	 * Check if person nickname already exists
	 *
	 * @author    Daniel Triboni
	 * 
	 * @param     string Apelido
	 */
	private function fCheckIfPersonNickExists($strNick){
		if(!(empty($strNick))){
			$sql = "SELECT
						p.apelido
					FROM
						pessoas p
					WHERE
						p.apelido = '".$this->fEscapeString($strNick)."'";
			$this->fExecuteSql($sql);
			if ($this->fNumRows() > 0){
				return true;
			}else
				return false;
		}
	}

	public function fUpdatePersonCredentials($usr, $pwd){
		return $this->fQueryUpdatePersonCredentials($usr, $pwd);
	}
}