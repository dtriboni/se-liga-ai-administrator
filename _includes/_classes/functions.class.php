<?php

/**
* Classe Coletanea de funcoes - extensao da classe de conexao
* @author EM2D Development Team
*/
class functions extends queries {
	
	private $hashKeys = '$2a$07$bda7ac69d6c64931a0c53116d';

	private $retHTML;
	
	private $complHTML;
	
	private $javaScript;
	
	private $retRecords;
	
	public $myGender = null;
	
	public $genderPrefer = null;
	
	public $servicePrefer = null;
	
	public $cookieLatitude;
	
	public $cookieLongitude;
	
	public $funcMsg;
	
	
	/**
	* Class Constructor
	*
	* @author    Daniel Triboni
	* @return    resource
	*/
	public function __construct($checkLogged = true) {	
		
		parent::__construct();

		/*	PREPARAR TODAS URLs AMIGAVEIS PARA NAO APONTAR PARA O ARQUIVO DIRETO - CRIAR NO HTACCESS 
		$arrRequestUri = explode(".", $_SERVER['REQUEST_URI']);
		if(substr($arrRequestUri[1], 0, 3) == "php"){
			header('Location: '.SIS_URL.'login');
			exit;
		} */
		
		 if ($checkLogged != false)
		{
					
			if (isset($_SESSION['sPersonLogged']))
			{
				//list($this->myGender, $this->genderPrefer, $this->servicePrefer) = explode("_", $_COOKIE['cUserDefinedData']);
				//print_r($_SESSION);

				if ($_SESSION['sPersonSessionTime'] < (time() - SIS_TEMPO)) 
				{

					$_SESSION['sPersonLogged'] = false;

				}else{

					$_SESSION['sPersonSessionTime'] = time();
				}

				if($_SESSION['sPersonLogged'] == false)
				{
					unset($_SESSION);
					unset($_REQUEST);		
					session_destroy();
					header('Location: '.SIS_URL.'login');
					exit;
				}

			}else{
				
				header('Location: '.SIS_URL.'login');
				exit;		
			}
		}
	}

	
	/**
	 * Encrypt Person Data before Recover Password
	 * @param string $text
	 */
	public function fEncrypt($text) {
		return urlencode(rawurlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->hashKeys, $text, "ecb")));
	}
	
	
	/**
	 * Decrypt Person Data after Recover Password
	 * @param string $text
	 */
	public function fDecrypt($text) {		
		return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->hashKeys, rawurldecode(urldecode($text)), "ecb");
	}
	
	
	/**
	* Remove Grades - UTF8 without BOM
	*
	* @author    Daniel Triboni
	* @param	 string Word	
	* @return    string
	*/
	private function fRemoveGrades($string){
		$a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ?®:@#$%&!*()+=ªº"¨{}[]~^´`,:;/|';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                               ';
		$string = utf8_decode($string);
		$string = strtr($string, utf8_decode($a), $b);
		$string = str_replace(".","",$string);
		$string = str_replace(" ","",$string);
		return utf8_encode($string);
	}
	
	
	/**
	 * Format to URL Friendly
	 *
	 * @author    Daniel Triboni
	 * @param     string Titulo
	 * @return    string
	 */
	public function fFormatTitle4Url($title){
		$title = str_replace("'", "-", trim($title));
		$title = str_replace(" ", "-", trim($title));
		return strtolower($this->fRemoveGrades($title));
	}
	
	
	/**
	 * Sanitize Request Fields And Format Dates and URL Friendly
	 *
	 * @author    Daniel Triboni
	 * @param     object $_REQUEST
	 * @return    object sanitized
	 */
	public function fSanitizeRequest($request){
		$newRequest = array();	
		foreach ($request as $obj => $value)
		{
			if ($obj == 'vltax' || 
				$obj == 'oldprice' || 
				$obj == 'newprice' || 
				$obj == 'value' || 
				$obj == 'price' || 
				$obj == 'weight')
			{
				$newRequest[$obj] = $this->fFormatCurrency($this->fEscapeString($request[$obj]));
			}elseif ($obj == 'nascimento')
				$newRequest['nascimento'] = $this->fInvertDateUSA($this->fEscapeString($request[$obj]));
			elseif ($obj == 'modalidades' || 
					$obj == 'modalidades-adic' || 
					$obj == 'pessoasatendimento' || 
					$obj == 'idiomas' || 
					$obj == 'localidades')
				$newRequest[$obj] = $request[$obj];			
			else 
				$newRequest[$obj] = $this->fEscapeString($request[$obj]);
		}
		return $newRequest;
	}

	private function fFormatCurrency($value){
		$value = str_replace(".", "", trim($value));
		$value = str_replace(",", ".", trim($value));
		return $value;
	}
	
	
	/**
	* Convert Integer Time to String
	*
	* @author    Daniel Triboni
	* @param	 integer Time
	* @return    string
	*/
	public function fTime2Text($time){	
		$time = ($time * 24 * 3600);	
		$response=array();
		$years = floor($time/(86400*365));
		$time=$time%(86400*365);
		$months = floor($time/(86400*30));
		$time=$time%(86400*30);
		$days = floor($time/86400);
		$time=$time%86400;
		$hours = floor($time/(3600));
		$time=$time%3600;
		$minutes = floor($time/60);
		$seconds=$time%60;
		if($years>0)  $response[] = $years.' ano'. ($years>1?'s':'');
		if($months>0) $response[] = $months.' mes'.($months>1?'es':'');
		if($days>0)   $response[] = $days.' dia' .($days>1?'s':'');
		if($hours>0)  $response[] = $hours.' hora'.($hours>1?'s':'');
		if($minutes>0)$response[] = $minutes.' minuto' . ($minutes>1?'s':'');
		if($seconds>0)$response[] = $seconds.' segundo' . ($seconds>1?'s':'');
		return implode(', ',$response);
	}	
    
    
	/**
	* Invert BRASILEIRO > AMERICAN date format
	*
	* @author    Daniel Triboni
	* @param	 string Date DD/MM/YYYY
	* @return    string Date YYYY/MM/DD
	*/
	public function fInvertDateUSA($data){			
		return substr($data, 6, 4).'-'.substr($data, 3, 2).'-'.substr($data, 0, 2);
	}

	
	/**
	* Invert AMERICAN > BRASILEIRO date format
	*
	* @author    Daniel Triboni
	* @param	 string Date YYYY/MM/DD
    * @param	 $mes = N : Convert extensible month
	* @return    string Date DD/MM/YYYY
	*/	
	public function fInvertDateBrazil($data, $retHour,$mes=""){
		if (strlen($data) > 10)
			$hour = " ".substr($data, 11);
		else 
			$hour = "";	
		if($mes=="n"){
		  $return = substr($data, 8, 2).'/'.$this->fGetReduceMonth(substr($data, 5, 2)).'/'.substr($data, 0, 4);  
		}else{
		  $return = substr($data, 8, 2).'/'.substr($data, 5, 2).'/'.substr($data, 0, 4);
		}
        
        if($retHour==true){
            $return.= $hour;
        }
        
        return $return;
	}    

	
	/**
	* Find Latitude and Longitude at Google Maps
	*
	* @author    Leonardo Pricevicius
	* @param	 string Address
	* @param	 string Number
	* @param	 string Neighborhood
	* @param	 string State
	* @param	 string Country
	* @return    string
	*/
    public function fGetLatLngGoogleMaps($end, $num, $bairro, $estado, $pais){
        $_ENDERECO = str_replace(" ","+", $this->fRemoveGrades($end));
        $_BAIRRO = str_replace(" ","+", $this->fRemoveGrades($bairro));
        $_ESTADO = str_replace(" ","+", $this->fRemoveGrades($estado));
        $_PAIS = str_replace(" ","+", $this->fRemoveGrades($pais));
        $_URL = "http://maps.googleapis.com/maps/api/geocode/json?address=".$_ENDERECO.",".$num."+-+".$_BAIRRO.",+,".$_ESTADO."&sensor=true";
        $_FILE = file_get_contents($_URL);
        $_DECODE = json_decode($_FILE, true);
        return $_DECODE["results"];
    }
    
    
    /**
	* Convert Latitude and Longitude in KM or MILE
	*
	* @author    Leonardo Pricevicius
	* @param	 float Latitude 1
	* @param	 float Longitude 1
	* @param	 float Latitude 2
	* @param	 float Longitude 2
	* @return    integer
	*/
    public function fConvertLatLng2Km($_LAT_01, $_LNG_01, $_LAT_02, $_LNG_02) {
        $_LATITUDE = $_LNG_01 - $_LNG_02;
        $_DISTANCIA = sin(deg2rad($_LAT_01)) * sin(deg2rad($_LAT_02)) +  cos(deg2rad($_LAT_01)) * cos(deg2rad($_LAT_02)) * cos(deg2rad($_LATITUDE));
        $_DISTANCIA = acos($_DISTANCIA);
        $_DISTANCIA = rad2deg($_DISTANCIA);
        $_MILHAS = $_DISTANCIA * 60 * 1.1515;
        return round(($_MILHAS * 1.609344));
    }


    /**
     * Check CPF is valid (Brazilian People Social Security)
     * @param integer $cpf
     * @return boolean
     */
	public function fcheckCPF($cpf){
		$cpf = $this->fremoveNumFormat($cpf);
		if(strlen($cpf) > 0){
			$digito =  ($cpf[0] * 10);
			$digito += ($cpf[1] * 9);
			$digito += ($cpf[2] * 8);
			$digito += ($cpf[3] * 7);
			$digito += ($cpf[4] * 6);
			$digito += ($cpf[5] * 5);
			$digito += ($cpf[6] * 4);
			$digito += ($cpf[7] * 3);
			$digito += ($cpf[8] * 2);
			$digito = ($digito*10) % 11;
			if($digito == 10)
				$digito = 0;
			if($digito == $cpf[9]){
				$digito =  ($cpf[0] * 11);
				$digito += ($cpf[1] * 10);
				$digito += ($cpf[2] * 9);
				$digito += ($cpf[3] * 8);
				$digito += ($cpf[4] * 7);
				$digito += ($cpf[5] * 6);
				$digito += ($cpf[6] * 5);
				$digito += ($cpf[7] * 4);
				$digito += ($cpf[8] * 3);
				$digito += ($cpf[9] * 2);
				$digito = ($digito*10) % 11;
				if($digito == 10)
					$digito = 0;
				if($digito == $cpf[10]){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else
			return false;
	}

	
	/**
	 * Calculate age between current year and born date
	 * @param DateTime $born
	 * @return integer
	 */	
	public function fGetAge($born){
		if($born != '')
		{
		    list($y, $m, $d) = explode('/', $born);
		    $now = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
		    $born = mktime( 0, 0, 0, $m, $d, $y);
		    $age = floor((((($now - $born) / 60) / 60) / 24) / 365.25);
		    return $age." anos";
		}
 	}
 	
 	
 	/**
 	 * Translate gender person
 	 * @param	 string gender
 	 * @return   string
 	 */
 	public function fGetGenderPerson($gender){
 		if ($gender == 'M') $gender = 'Homem';
 		if ($gender == 'F') $gender = 'Mulher';
 		if ($gender == 'T') $gender = 'Transg&ecirc;nero';
 		return $gender;
 	}
	
 	
	/**
	* Convert integer month to string month
	* @param	 integer month
	* @return    string
	*/	
	public function fGetMonth($month){
		$numMonth = array (1 => "Janeiro", 2 => "Fevereiro", 3 => "Mar&ccedil;o", 4 => "Abril", 
						   5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 
						   9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
		return $numMonth[(int)$month];					
	}

	
	/**
	* Convert integer month to string reduced month
	* @param	 integer month
	* @return    string
	*/	
	public function fGetReduceMonth($month){
		$numMonth = array (1 => "JAN", 2 => "FEV", 3 => "MAR", 4 => "ABR", 
							5 => "MAI", 6 => "JUN", 7 => "JUL", 8 => "AGO", 
							9 => "SET", 10 => "OUT", 11 => "NOV", 12 => "DEZ");
		return $numMonth[(int)$month];					
	}
	
	
	/**
	* Convert Reduced Month name to representative value
	* @param	 string Month
	* @return    string
	*/	
	public function fGetConvertReduceMonth2Number($month){
		$numMonth = array ("JAN" => "01", "FEV" => "02", "MAR" => "03", "ABR" => "04", 
							"MAI" => "05", "JUN" => "06", "JUL" => "07", "AGO" => "08", 
							"SET" => "09", "OUT" => "10", "NOV" => "11", "DEZ" => "12");
		return $numMonth[$month];					
	}
	
	
    /**
	* Generate random password
	* @param	 integer num size
	* @return    string
	*/
	public function fRandomPassword($numsize){  
	   $words = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9";  
	   $array = explode(",", $words);  
	   shuffle($array);  
	   $pwd = implode($array, "");  
	   return substr(strtolower($pwd), 0, $numsize);  
	}	

	
    /**
	* Anti SQL Injection
	*
	* @author    Daniel Triboni
	* @param	 string SQL
	* @return    string SQL
	*/
    public function fAntiInjection($sql){
        $sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$sql);
        $sql = trim($sql);
        $sql = strip_tags($sql);
        $sql = addslashes($sql);
        $sql = $this->fEscapeString($sql);        
    	return $sql;
    }
	
    
	/**
	* Show warnings
	* @return    string
	*/    
	public function fWarnings(){
	   $strErro = $_SESSION["sShowWarning"];
		if (count($strErro) > 0){			
		  unset($_SESSION["sShowWarning"]);
		  foreach ($strErro as $strTextoAviso){
			list($mensagem, $tipo) = explode("|", $strTextoAviso);
			if($tipo == "e") $img = "msgerror";
			if($tipo == "s") $img = "msgsuccess";
			if($tipo == "i") $img = "msginfo";
			if($tipo == "a") $img = "msgalert";
			$layout_aviso = '<div class="notification '.$img.'">
					<a class="close"></a>
					<p>'.$mensagem.'</p>
				</div>';
		  }
			echo $layout_aviso;
		}
	}	
	

	/**
	* Limit words in a string
	* @param	 string Text
	* @param	 integer character limit
	* @param	 boolean broke words (Default: true)
	* @return    string
	*/
 	public function fLimitWords($texto, $limite, $quebra = true, $url=null){
       $tamanho = strlen($texto);
       if($tamanho <= $limite){
          $novo_texto = $texto;
       }else{
          if($quebra == true){
             $novo_texto = trim(substr($texto, 0, $limite))."...";
          }else{
             $ultimo_espaco = strrpos(substr($texto, 0, $limite), " ");
             $novo_texto = trim(substr($texto, 0, $ultimo_espaco))." ... <br><br><a href='".$url."'><strong>MAIS SOBRE MIM</strong></a>";
          }
       }
       return $novo_texto;
    }
    

	/**
	* Remove number formatting
	* @param	 string Valor	
	* @return    float
	*/
	public function fRemoveNumFormat($valor){
		return preg_replace("/[^0-9]/", '', $valor);
	}

	
	/**
	* Reduce full name by first name
	* @param	 string name	
	* @return    string
	*/
	public function fReduceName($name){
		$strName = explode(" ", $name);
		return $strName[0];
	}
	
	
	/**
	 * Strip HTML Tags in text content
	 * @param string $text
	 * @param string $tags
	 * @param string $invert
	 */
	public function fStripTagsContent($text, $tags = '', $invert = FALSE) {
		$text = preg_replace('|https?://www\.[a-z\.A-Z]+|i', '', $text);
		$text = preg_replace('|www\.[a-z\.A-Z]+|i', '', $text);		
		//$text = str_replace(range(0, 9), null, $text);
		$text = preg_replace('/\d+$/', null, $text);
		
		preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
		$tags = array_unique($tags[1]);
	
		if(is_array($tags) AND count($tags) > 0) {
			if($invert == FALSE) {
				return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
			}
			else {
				return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
			}
		}
		elseif($invert == FALSE) {
			return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
		}
		
		return $text;
	}

	
	/**
	* Welcome message to user
	* @return    string
	*/	
	public function fWelcomeMessage($name, $gender, $comeback = 0){
		$name = $this->fReduceName($name);
		$comeback = ($comeback == 1 ? 'novamente ' : '');
		if ($gender == "M")
			$str = "Bem Vindo ".$comeback.$name;
		else	
			$str = "Bem Vinda ".$comeback.$name;
				
		//$str .= ($title ? " ao ".SIS_TITULO : "");
			
		return $str."!";	
	}
	
	
	/**
	 * Welcome message to user
	 * @return    string
	 */
	public function fPlanExpires($days){
		if ($days < 6 && $days > 1)
			$str = "ir&aacute; expirar <strong>em ".$days." DIAS</strong>";
		elseif($days == 1)
			$str = "ir&aacute; expirar <strong>AMANH&Atilde;</strong>";
		elseif($days == 0)
			$str = "expira <strong>HOJE</strong>";
		else 
			$str = "expirou";
		return $str;
	}
	
	
	/**
	* Reference to user by gender
	* @return    string
	*/
	public function fReference($gender){
		if ($gender == "M")
			$str = "o";
		else
			$str = "a";
		return $str;
	}


	public function fBeaultyNum($num, $type)
	{
		if ($num > 999999){
			$retNum = number_format(($num / 1000000), 1, ",", "")."MI";
			$retNumLbl = number_format($num, 0, "", ".");
		}elseif ($num > 999){
			$retNum = number_format(($num / 1000), 1, ",", "")."K";
			$retNumLbl = number_format($num, 0, "", ".");	
		}else{
			$retNum = number_format($num, 0, "", ".");
			$retNumLbl = "at&eacute; o momento!";
		}
		if ($type == 1){
			return $retNum;
		}else{
			return $retNumLbl;
		}
	}


	public function randomPassword() {
		$alphabet = '1234567890';
		$pass = array();
		$alphaLength = strlen($alphabet) - 1; 
		for ($i = 0; $i < 6; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
	}
	
	public function base64_to_jpeg($base64_string, $w, $h, $path){
		if (substr($base64_string, 0, 4) != "http"){
			$randName = $this->randomPassword();
			$filename = SIS_UPLOAD_PATH.$path."/str_".$randName.".jpeg";

			/* $ifp = fopen($filename, 'wb'); 
			$data = explode(',', $base64_string);
			fwrite($ifp, base64_decode($data[1]));
			fclose($ifp);  */

			$data = explode(',', $base64_string);
			$imageData = base64_decode($data[1]);
			$source = imagecreatefromstring($imageData);
			$imageSave = imagejpeg($source, $filename, 50);
			imagedestroy($source);

			return "https://em2d.com.br/seligaai/".$path."/str_".$randName.".jpeg";
		}else{
			return $base64_string;
		}
	}
	
	public function resize_image($file, $w, $h, $crop=FALSE) {
		list($width, $height) = getimagesize($file);
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}
		$src = imagecreatefromjpeg($file);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	}


	public function fRemoveImage($phoid)
	{
		$this->retRecords = $this->fQueryRemoveImage($phoid);
			
		return true;
	}


	public function fRemoveStoreImage($strid)
	{
		$this->retRecords = $this->fQueryRemoveStoreImage($strid);
			
		return true;
	}


	public function fGetPaymentStatus($strid)
	{
		$this->retRecords = $this->fQueryPaymentStatus($strid);
			
		return $this->retRecords;
	}


	/**
	 * Get User Profile
	 * @param null
	 */
	public function fGetProfile()
	{
		$this->retRecords = $this->fQueryProfile($_SESSION['sPersonID']);
			
		return $this->retRecords;
	}


	/**
	 * Get User Profile
	 * @param null
	 */
	public function fGetAddresses()
	{
		$this->retRecords = $this->fQueryAddresses($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
			
		return $this->retRecords;
	}


	/**
	 * Get Address
	 * @param null
	 */
	public function fGetAddress($addid)
	{
		$this->retRecords = $this->fQueryAddress($addid, $_SESSION['sPersonID'], $_SESSION['sPersonType']);
			
		return $this->retRecords;
	}


	/**
	 * Get User Profile
	 * @param null
	 */
	public function fGetLogistics()
	{
		$this->retRecords = $this->fQueryLogistics($_SESSION['sPersonLoggerID'], $_SESSION['sPersonType']);
			
		return $this->retRecords;
	}


	/**
	 * Get Logistic
	 * @param null
	 */
	public function fGetLogistic($logid)
	{
		$this->retRecords = $this->fQueryLogistic($logid, $_SESSION['sPersonID'], $_SESSION['sPersonType']);
			
		return $this->retRecords;
	}


	 /**
     * Update Person Address
     * @param 
     */
    public function fUpdateAddress($obj)
    {
    	return $this->fQueryUpdateAddress($obj);
	}
	

	/**
     * Save Person Address
     * @param 
     */
    public function fSaveNewAddress($obj)
    {
    	return $this->fQuerySaveNewAddress($obj);
	}


	 /**
     * Update Person Logistic
     * @param 
     */
    public function fUpdateLogistic($obj)
    {
    	return $this->fQueryUpdateLogistic($obj);
	}
	

	/**
     * Save Person Logistic
     * @param 
     */
    public function fSaveNewLogistic($obj)
    {
    	return $this->fQuerySaveNewLogistic($obj);
	}


	/**
     * Update Person Coupom
     * @param 
     */
    public function fUpdateCoupom($obj)
    {
    	return $this->fQueryUpdateCoupom($obj);
	}
	

	/**
     * Save Person Coupom
     * @param 
     */
    public function fSaveNewCoupom($obj)
    {
    	return $this->fQuerySaveNewCoupom($obj);
	}


	/**
     * Save Person Address
     * @param 
     */
    public function fSaveNewUser($obj)
    {
    	return $this->fQuerySaveNewUser($obj);
	}


	/**
     * Save Pre User Signup
     * @param 
     */
    public function fSaveNewPreUser($obj)
    {
    	return $this->fQuerySaveNewPreUser($obj);
	}


	/**
     * Save Person Address
     * @param 
     */
    public function fUpdateUser($obj)
    {
    	return $this->fQueryUpdateUser($obj);
	}


	/**
     * Update Person Offer
     * @param 
     */
    public function fUpdateOffer($obj)
    {
    	return $this->fQueryUpdateOffer($obj);
	}


	/**
     * Save Person Offer
     * @param 
     */
    public function fSaveNewOffer($obj)
    {
    	return $this->fQuerySaveNewOffer($obj);
	}


	/**
     * Save Person Offer
     * @param 
     */
    public function fSaveNewStore($obj)
    {
    	return $this->fQuerySaveNewStore($obj);
	}


	/**
     * Save Person Offer
     * @param 
     */
    public function fGetBanks()
    {
    	return $this->fQueryBanks();
	}


	/**
     * Save Person Store
     * @param 
     */
    public function fUpdateStore($obj)
    {
    	return $this->fQueryUpdateStore($obj, $_SESSION['sPersonType']);
	}


	public function fSaveOfferPhotos($ofeid, $photo)
	{
		return $this->fQuerySaveOfferPhoto($ofeid, $photo);
	}


	public function fSaveStorePhotos($strid, $photo)
	{
		return $this->fQuerySaveStorePhoto($strid, $photo);
	}


	public function fSaveStoreImage($strid, $photo)
	{
		return $this->fQuerySaveStoreImage($strid, $photo);
	}


	/**
     * Save Person Offer
     * @param 
     */
    public function fSaveOfferCaracteristics($ofeid, $type)
    {
		if ($type == 2){
			return true;
		}else{
			return $this->fQuerySaveOfferCaracteristics($ofeid, $_SESSION['sCaracteristicData']);
		}
	}


	public function fGetCubageOrderItems($ordid) 
	{
		return $this->fQueryGetCubageOrderItems($ordid);
	}



	/**
	 * Get Address
	 * @param null
	 */
	public function fGetStore($strid)
	{
		$this->retRecords = $this->fQueryStore($strid, $_SESSION['sPersonID'], $_SESSION['sPersonType']);
			
		return $this->retRecords;
	}


	/**
     * Get Categories
     * @param 
     */
    public function fGetCategories()
    {
    	return $this->fQueryCategories();
	}


	public function fSendNotification($obj)
	{

		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://onesignal.com/api/v1/notifications",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "{\n  \"app_id\": \"0e98359d-3601-407a-9d8b-c2847331e5f3\",\n  \n  \"included_segments\": [\"All\"],\n  \n  \"filters\": [\n  \t{\"field\": \"tag\", \"key\": \"client_id\", \"relation\": \"=\", \"value\": \"Daniel\"}\n  ],\n  \n  \"data\": {\"datetime\": \"01/03/2019 11:35\", \"message\": \"mensagem de teste 2 aaabbb\"},\n  \"contents\": {\"en\": \"Falae Leo 2\"}\n}",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Basic OTc5MjU5N2MtYmZiOS00ZjA2LWJlMzQtNGNmNjlkYTVlNDdj",
			"Content-Type: application/json",
			"Postman-Token: 3075a9c0-7ad5-4e95-9876-90f8ea4854f0",
			"cache-control: no-cache"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
			return false;
			
		} else {

			//return $this->fQuerySaveNotificationsLog($response, $obj);
		}

	}

	public function fGetReceivePayments($received = false)
	{
    	return $this->fQueryReceivePayments($received, $_SESSION['sPersonLoggerID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}

	public function fGetTaxLog()
	{
    	return $this->fQueryTaxLog($_SESSION['sPersonLoggerID']);
	}

	public function fUpdateTaxLog($obj)
	{
    	return $this->fQueryUpdateTaxLog($obj, $_SESSION['sPersonLoggerID']);
	}

	/**
     * Get Categories
     * @param 
     */
    public function fGetCategoryCaracteristics()
    {
    	return $this->fQueryCategoryCaracteristics($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Categories
     * @param 
     */
    public function fGetOfferCaracteristics($ofeid)
    {
    	return $this->fQueryOfferCaracteristics($ofeid);
	}


	/**
     * Get Stores
     * @param 
     */
    public function fGetStores()
    {
    	return $this->fQueryStores($_SESSION['sPersonID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Stores
     * @param 
     */
    public function fGetOffers()
    {
    	return $this->fQueryOffers($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Offer
     * @param 
     */
    public function fGetOffer($id)
    {
    	return $this->fQueryOffer($id, $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Offer
     * @param 
     */
    public function fGetOffersRemaining()
    {
    	return $this->fQueryOffersRemaining($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Users
     * @param 
     */
    public function fGetUsers($ftype = 0)
    {
    	return $this->fQueryUsers($ftype, $_SESSION['sPersonID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Users
     * @param 
     */
    public function fGetUser($usrid)
    {
    	return $this->fQueryUser($usrid, $_SESSION['sPersonID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Plans
     * @param 
     */
    public function fGetPlans($actives = false)
    {
    	return $this->fQueryPlans($actives);
	}


	/**
     * Get Coupons
     * @param 
     */
    public function fGetCoupons()
    {
    	return $this->fQueryCoupons($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}

	/**
     * Get Coupom
     * @param 
     */
    public function fGetCoupom($couid)
    {
    	return $this->fQueryCoupom($couid, $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Plan Details
     * @param 
     */
    public function fGetPlanDetails($plaid)
    {
    	return $this->fQueryPlanDetails($plaid);
	}


	/**
     * Set Plan As Paid
     * @param 
     */
    public function fSetPlanAsPaid($stpid)
    {
    	return $this->fQuerySetPlanAsPaid($stpid, $_SESSION['sPersonType']);
	}


	/**
     * Set Plan Cancelled
     * @param 
     */
    public function fSetPlanAsCancelled($stpid)
    {
    	return $this->fQuerySetPlanAsCancelled($stpid, $_SESSION['sPersonType']);
	}


	/**
     * Get Plan Details
     * @param 
     */
    public function fGetPlanPreviouslyAquired($strid)
    {
		$strid = ($strid == 0 ? $_SESSION['sPersonStoreID'] : $strid);
    	return $this->fQueryPlanPreviouslyAquired($strid);
	}


	/**
     * Get Plans
     * @param 
     */
    public function fGetStorePlans()
    {
    	return $this->fQueryStorePlans($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	/**
     * Get Plans
     * @param 
     */
    public function fGetOrders($received = false, $period = 0)
    {
    	return $this->fQueryOrders($received, $period, $_SESSION['sPersonLoggerID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	public function fGetTotals4Home()
	{
		return $this->fQueryTotals4Home($_SESSION['sPersonLoggerID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}


	public function fGetIndexChart()
	{
		return $this->fQueryIndexChart($_SESSION['sPersonLoggerID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}

	public function fGetTypeOffers()
	{
		return $this->fQueryTypeOffers($_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}

	/**
     * update Order Delivery Statys
     * @param 
     */
	public function fUpdateOrderDeliveryStatus($obj)
	{
		return $this->fQueryUpdateOrderDeliveryStatus($obj);
	}


	/**
     * update Order Delivery Statys
     * @param 
     */
	public function fUpdateOrderRefundStatus($obj)
	{
		return $this->fQueryUpdateOrderRefundStatus($obj);
	}


	/**
     * update Order Delivery Statys
     * @param 
     */
	public function fCheck4NewOrders()
	{
		return $this->fQueryCheck4NewOrders($_SESSION['sPersonLoggerID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}



	/**
     * Get Plans
     * @param 
     */
    public function fGetOrderDetails($ordid)
    {
    	return $this->fQueryOrderDetails($ordid, $_SESSION['sPersonLoggerID'], $_SESSION['sPersonStoreID'], $_SESSION['sPersonType']);
	}
	

	/**
     * Remove Record Dynamically
     * @param 
     */
    public function fRemoveRecord($reg, $field, $id)
    {
    	return $this->fQueryRemoveRecord($reg, $field, $id);
	}
	

	/**
     * Change Record Dynamically
     * @param 
     */
    public function fChangeRecord($reg, $field, $id, $status)
    {
		$arrayGrantedtoAdmin = array('c', 's', 'u');
		if (in_array($reg, $arrayGrantedtoAdmin)){
			if ($_SESSION['sPersonType'] == 4){
				return $this->fQueryChangeRecord($reg, $field, $id, $status);
			}else{
				return false;
			}	
		}else{
			return $this->fQueryChangeRecord($reg, $field, $id, $status);
		}	
    }















	/**
	 * Check User Level Profile by Page
	 * @param null
	 */
	public function fCheckLevelProfile($level = null)
	{
		$block = true;
		if ($_SESSION['sPersonType'] != 4)
		{
			$lv = explode(",", $level);
			foreach($lv as $lev)
			{
				if ($_SESSION['sPersonType'] == $lev)
				{
					$block = false;
					break;
				}
			}
			if ($block)
			{
				header('Location: '.SIS_URL.'home');
				exit;
			}
		}
	}
	
	
	/**
	 * Format Number to Counter
	 * @param integer $number
	 */
	public function fStripNumbers($number)
	{
		$retNumber = ($number > 1999 && $number < 1000000 ? '+ '.floor($number/1000).'K' : $number);
		
		$retNumber = ($number > 999999 ? '+ '.floor($number/1000000).'M' : $retNumber);
		
		return $retNumber;
	}
    
    
    /**
     * Get Person/User Email Result if Exists
     * @param unknown $email
     * @return boolean
     */
    public function fGetEmail($email, $table)
    {
    	return $this->fQueryPersonEmail($this->fEscapeString($email), $table);
    }
    
    
    /**
     * Get User Email Result if Exists
     * @param unknown $email
     * @return boolean
     */
    public function fGetUserEmail($email)
    {
    	return $this->fQueryUserEmail($this->fEscapeString($email));
    }
    
    
    /**
     * Save User to Person Testimonial
     * @param object $obj
     * @return boolean
     */
    public function fAddTestimonial($obj)
    {
    	return $this->fQueryAddTestimonial($obj);
    }
    
    
    /**
     * Update Person Testimonial
     * @param object $obj
     * @return boolean
     */
    public function fUpdateTestimonial($obj)
    {
    	return $this->fQueryUpdateTestimonial($obj);
    }
    
    
    /**
     * Remove Testimonial Logically
     * @param unknown $tesid
     * @return boolean
     */
    public function fRemoveTestimonial($tesid)
    {
    	return $this->fQueryRemoveTestimonial($this->fEscapeString($tesid));
    }
    
    
    /**
     * Remove Person Logically
     * @param unknown $pesid
     * @return boolean
     */
    public function fRemovePerson($pesid)
    {
    	return $this->fQueryRemovePerson($this->fEscapeString($pesid));
    }
    
    
    /**
     * Remove User Logically
     * @param unknown $pesid
     * @return boolean
     */
    public function fRemoveUser($usuid)
    {
    	return $this->fQueryRemoveUser($this->fEscapeString($usuid));
    }
    
    
    /**
     * Get Person Email Retrieve Password
     * @param unknown $email
     * @return boolean
     */
    public function fRetrievePassword($email, $table)
    {
    	return $this->fQueryRetrievePassword($this->fEscapeString($email), $table);
    }
    
    
    /**
     * Update Person Credentials
     * @param string $email
     * @param string $password
     */
    public function fUpdatePersonCredentials($email, $password)
    {
    	return $this->fQueryUpdatePersonCredentials($this->fEscapeString($email), $this->fEscapeString($password));
    }
    
    
    /**
     * Update User Credentials
     * @param string $email
     * @param string $password
     */
    public function fUpdateUserCredentials($email, $password)
    {
    	return $this->fQueryUpdateUserCredentials($this->fEscapeString($email), $this->fEscapeString($password));
    }
    
    
    /**
     * Get Person Register in Signup Page
     * @param unknown $person
     * @return array
     */
    public function fGetPersonRegister($person)
    {
    	return $this->fQueryPersonRegister($this->fFormatTitle4Url($this->fEscapeString($person)));
    }
    
    
    /**
     * Get User Register in Signup Page
     * @param unknown $person
     * @return array
     */
    public function fGetUserRegister($strid)
    {
    	return $this->fQueryUserRegister($strid);
    }
    
    
    /**
     * Convert integer data to TimeStamp
     * @param integer $integer
     * @return string
     */
    private function fConvertIntToTime($integer)
    {
    	$x = explode('.', $integer);    	
    	$min = 60 * ($x[1]/10);    	
    	if(!empty($x[0]) && !empty($min))
    		return $x[0].':'.$min.'h';
    	elseif(!empty($x[0]) && empty($x[1]))
    		return $x[0].'h';
    	else
    		return $min.'min';
    }
    
    
    /**
     * Format Work Days with Hours
     * @param string $dayhourwork
     * @param boolean $bday Business Day
     */
    public function fFormatDayHourWork($dayhourwork, $bday = true)
    {
    	$dayhour = explode("-", $dayhourwork);
    	
    	if ($bday)
    	{
    		if ($dayhour[0] > 0 && $dayhour[1] > 0)
    			$strDHW = "De ".$this->fFormatWeekdays($dayhour[0])." &agrave; ".$this->fFormatWeekdays($dayhour[1]);
    		elseif ($dayhour[0] > 0 && $dayhour[1] == '0')
    			$strDHW = $this->fFormatWeekdays($dayhour[0])."s feiras";
    		elseif ($dayhour[0] == '0' && $dayhour[1] == '0')
    			$strDHW = "De segunda &agrave; sexta";
    		else 
    			$strDHW = $this->fFormatWeekdays($dayhour[1])."s feiras";
    		
    		if ($dayhour[2] != '99' && $dayhour[3] != '99')
    			$strDHW .= ", das ".$dayhour[2]."h &agrave;s ".$dayhour[3]."h";
    		elseif ($dayhour[2] != '99' && $dayhour[3] == '99')
    			$strDHW .= ", a partir das ".$dayhour[2]."h";
    		elseif ($dayhour[2] == '99' && $dayhour[3] == '99')
    			$strDHW .= ", a qualquer hora!";
    		else
    			$strDHW .= ", a partir das ".$dayhour[3]."h";
    	}else{
    		
    		if ($dayhour[0] > 0 && $dayhour[1] > 0)
    			$strDHW = $this->fFormatWeekdays($dayhour[0])."s e ".$this->fFormatWeekdays($dayhour[1])."s";
    		elseif ($dayhour[0] > 0 && $dayhour[1] == '0')
    			$strDHW = "Aos ".$this->fFormatWeekdays($dayhour[0])."s";
    		elseif ($dayhour[0] == '0' && $dayhour[1] == '0')
    			$strDHW = "Aos finais de semana";
    		else
    			$strDHW = "Aos ".$this->fFormatWeekdays($dayhour[1])."s";
    		
    		if ($dayhour[2] != '99' && $dayhour[3] != '99')
    			$strDHW .= ", das ".$dayhour[2]."h &agrave;s ".$dayhour[3]."h";
    		elseif ($dayhour[2] != '99' && $dayhour[3] == '99')
    			$strDHW .= ", a partir das ".$dayhour[2]."h";
    		elseif ($dayhour[2] == '99' && $dayhour[3] == '99')
    			$strDHW .= ", a qualquer hora!";
    		else
    			$strDHW .= ", a partir das ".$dayhour[3]."h";
    	}
    	
    	return $strDHW;
    }
    
    
    /**
     * Format week days
     * @param integer $day
     */
    private function fFormatWeekdays($day)
    {
    	$arrWeek = array(1 => "Domingo", 2 => "Segunda", 3 => "Ter&ccedil;a", 4 => "Quarta", 5 => "Quinta", 6 => "Sexta", 7 => "S&aacute;bado");
    	return $arrWeek[$day];
	}
	
	
	public function fDateDiff( $str_interval, $dt_menor, $dt_maior, $relative=false)
	{
		if( is_string( $dt_menor)) $dt_menor = date_create( $dt_menor);
       if( is_string( $dt_maior)) $dt_maior = date_create( $dt_maior);

       $diff = date_diff( $dt_menor, $dt_maior, ! $relative);
       
       switch( $str_interval){
           case "y": 
               $total = $diff->y + $diff->m / 12 + $diff->d / 365.25; break;
           case "m":
               $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
               break;
           case "d":
               $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
               break;
           case "h": 
               $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
               break;
           case "i": 
               $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
               break;
           case "s": 
               $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
               break;
          }
       if( $diff->invert)
        return -1 * $total;
	   else    
	   	return $total;
	}
    
    
    /**
     * Upload Files Dinamically
     * @param unknown $files
     * @param unknown $imagePath
     * @return boolean|string
     */
    public function fUploadFiles($files, $imagePath)
    {
    	$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
    	
    	foreach ($files as $key => $file)
    	{
    		$ext = explode(".", $file["name"]);
    		$extension = end($ext);
    		
    		if (!file_exists($imagePath))
    			mkdir($imagePath, 0777, true);
    		
    		if (in_array($extension, $allowedExts))
    		{
    			if ($file["error"] > 0)
    			{
    				$this->funcMsg = 'Erro: '. $file["error"];    				
    				return false;
    				
    			}else{
    			
    				$hash = $this->fRandomPassword(12);
    				
    				$newFileName = "doc-".$hash.".".strtolower($extension);
    					 
    				move_uploaded_file($file["tmp_name"],  $imagePath.$newFileName);
    					 
    				$FILES[$key] = $newFileName;
    			}
    				
    		}else{
    				
    			$this->funcMsg = "Documento {$file["name"]} com formato incorreto. Utilize apenas imagens nos formatos (gif, jpeg, jpg ou png)!";    			
    			return false;
    		}	    		
    	} 
    	
    	return $FILES;
    }
}		
