<?php  

require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-admin/_includes/_config/config.ini.php";

$functions = new functions(false);

$queries = new queries();

$_REQUEST = $functions->fSanitizeRequest($_GET);

$de = $_REQUEST["decode"];  
 
$retLongURL = $queries->fQueryShortenedUrl($de);

for ($x = 0; $x < count($retLongURL); $x++)
{
    if ($retLongURL[$x]['longurl'] !== '')
    {
        header("location:".$retLongURL[$x]['longurl']);
    }
}