<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile(2);

   unset($_SESSION['sCaracteristicData']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Listar Ofertas/Promo&ccedil;&otilde;es</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Clique no &iacute;tem desejado para expandir campos ocultos e exibir mais a&ccedil;&otilde;es
                    </p>

                    <?php $retOffers = $functions->fGetOffers(); ?>
					
                    <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Produto/Servi&ccedil;o</th>
                          <th>ATIVO?</th>
                          <th>Tipo</th>
                          <th>Modalidade</th>
                          <th>Qtd. Estoque</th>
                          <th>Estoque M&iacute;n.</th>
                          <th>In&iacute;cio</th>
                          <th>Fim</th>
                          <th>Valor Real</th>
                          <th>Valor Promocional</th>
                          <th>Desconto</th>
                          <th>A&ccedil;&atilde;o</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php for ($a = 0; $a < count($retOffers); $a++){ ?>
                        <tr id="row_<?php echo $retOffers[$a]['ofeid']; ?>">
                          <td><?php echo $retOffers[$a]['prodserv']; ?></td>
                          <td><?php echo $retOffers[$a]['actived']; ?></td>
                          <td><?php echo $retOffers[$a]['otype']; ?></td>
                          <td><?php echo $retOffers[$a]['omod']; ?></td>
                          <?php if($retOffers[$a]['type'] == 1){ ?>
                            <td><strong class='btn-<?php echo (($retOffers[$a]['stock'] - $retOffers[$a]['stockmin']) < 0 ? "danger" : "success"); ?>'>&nbsp;&nbsp;<?php echo $retOffers[$a]['ostock']; ?>&nbsp;&nbsp;</strong></td>
                          <?php }else{ ?>  
                            <td>-</td>
                          <?php } ?>  
                          <?php if($retOffers[$a]['type'] == 1){ ?>
                            <td><strong class='btn-success'>&nbsp;&nbsp;<?php echo $retOffers[$a]['stockmin']; ?>&nbsp;&nbsp;</strong></td>
                          <?php }else{ ?>  
                            <td>-</td>
                          <?php } ?>      
                          <td><?php echo $retOffers[$a]['start']; ?></td>
                          <td><?php echo $retOffers[$a]['end']; ?></td>
                          <td><strong><?php echo ($retOffers[$a]['oldprice'] == '-' ? '-' : "R$ ".number_format($retOffers[$a]['oldprice'], 2, ",", ".")); ?></strong></td>
                          <td><strong><?php echo ($retOffers[$a]['newprice'] == '-' ? '-' : "R$ ".number_format($retOffers[$a]['newprice'], 2, ",", ".")); ?></strong></td>
                          <td><strong><?php echo ($retOffers[$a]['discount'] == '-' ? '-' : number_format($retOffers[$a]['discount'], 0)."%"); ?></strong></td>
                          <td>
                              <a href="<?php echo SIS_URL."offer/".$retOffers[$a]['ofeid'] ;?>" title="Alterar Registro"><li class="fa fa-edit"></li></a> | 
                              <?php if($retOffers[$a]['active'] == 1){ ?>
                                <a href="javascript:;" onclick="changeRecord(<?php echo $retOffers[$a]['ofeid']; ?>, 'ofeid', 'o', 0)" title="Desativar"><li class="fa fa-eye-slash"></li></a>
                              <?php }else{ ?>
                                <a href="javascript:;" onclick="changeRecord(<?php echo $retOffers[$a]['ofeid']; ?>, 'ofeid', 'o', 1)" title="Ativar"><li class="fa fa-eye"></li></a>
                              <?php } ?>  
                              | <a href="javascript:;" onclick="removeRecord(<?php echo $retOffers[$a]['ofeid']; ?>, 'ofeid', 'o')" title="Excluir Registro"><li class="fa fa-remove"></li></a>
                            </td>
                        </tr>
                      <?php } ?>
                                             
                      </tbody>
                    </table>
                    <div class="ln_solid"></div>
                    <div class="col-md-12">
                    <a href="<?php echo SIS_URL;?>offer" class="btn btn-primary">Cadastrar Nova Oferta/Promo&ccedil;&atilde;o</a>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>