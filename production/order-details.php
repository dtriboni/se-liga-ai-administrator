<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile("2,3");

   $_REQUEST = $functions->fSanitizeRequest($_GET);

   $theid = $_REQUEST['id'];

   if ($theid != '0' && $theid != ''){
    if (!$retOrder = $functions->fGetOrderDetails($theid)){
      header('Location: '.SIS_URL.'home');
      exit;
    }
  }else{
    header('Location: '.SIS_URL.'home');
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom styling plus plugins -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.min.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

           
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Detalhes do Pedido</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                 
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                              <i class="fa fa-shopping-cart"></i> Pedido #<?php echo $retOrder[0]['ord_alias']; ?>
                              <small class="pull-right">Data: <?php echo $retOrder[0]['date']; ?></small>
                          </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        <li class="fa fa-user"></li> COMPRADOR
                          <address>
                            <br><strong><?php echo $retOrder[0]['buyer']; ?></strong>
                            <br><?php echo $retOrder[0]['useraddress']; ?>
                            <br><?php echo $retOrder[0]['paid']; ?>
                          </address>
                        </div>


                        <?php if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ 
                          
                          $sumKM = 0;

                          ?>

                          <!-- /.col VISAO DO ENTREGADOR -->
                          <div class="col-sm-<?php echo ($_SESSION['sPersonType'] == 4 ? "4" : "8"); ?> invoice-col">
                            <li class="fa fa-building-o"></li> VENDEDOR<?php echo ($retOrder[0]['numstores'] > 1 ? "ES" : ""); ?>
                            <address>
                                <?php for ($a = 0; $a < count($retOrder); $a++){ 

                                  $km = $functions->fConvertLatLng2Km($retOrder[$a]['latitude'], $retOrder[$a]['longitude'], $retOrder[$a]['latitude1'], $retOrder[$a]['longitude1']);
                                  $sumKM += $km;
                                ?>
                                
                                      <br><strong><?php echo $retOrder[$a]['store']; ?></strong> - <?php echo $retOrder[$a]['fulladdress']; ?> <strong class="green">(<?php echo $km;?> km)</strong>
                                      <br>Contato: <?php echo $retOrder[$a]['seller']; ?> - <strong><a target="_blank" href="https://web.whatsapp.com/send?phone=55<?php echo $functions->fRemoveNumFormat($retOrder[$a]['whatsapp']); ?>"><li class="fa fa-whatsapp"></li> <?php echo $retOrder[$a]['whatsapp']; ?></a></strong>
                                      <br>Email: <?php echo $retOrder[$a]['email']; ?><br><br>
                                
                                <?php } ?>
                            </address>
                          </div>

                        <?php } ?>
 

                        <?php if (($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4) && $retOrder[0]['logid'] > 0){ ?>

                          <!-- /.col VISUAL DO VENDEDOR -->
                          <div class="col-sm-<?php echo ($_SESSION['sPersonType'] == 4 ? "4" : "8"); ?> invoice-col">
                          <li class="fa fa-truck"></li> ENTREGADOR
                          <address>
                                          <br><strong><?php echo $retOrder[0]['name']; ?></strong>
                                          <br><?php echo $retOrder[0]['fulladdresslogger']; ?>
                                          <br>Contato: <?php echo $retOrder[0]['logger']; ?> - <strong><a target="_blank" href="https://web.whatsapp.com/send?phone=55<?php echo $functions->fRemoveNumFormat($retOrder[0]['whatsapplogger']); ?>"><li class="fa fa-whatsapp"></li> <?php echo $retOrder[0]['whatsapplogger']; ?></a></strong>
                                          <br>Email: <?php echo $retOrder[0]['emaillogger']; ?>
                                          <br><strong class="red"><?php echo $retOrder[0]['delivery']; ?></strong>
                                      </address>
                          </div>

                        <?php } ?>


                       
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <?php if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ ?>
                                  <th>Vendedor</th> <!-- VISAO DO ENTREGADOR -->
                                <?php } ?>
                                <th>Produto/Servi&ccedil;o</th>
                                <th>Descri&ccedil;&atilde;o</th>
                                <th>Qtde</th>
                                <th>Valor Unit&aacute;rio</th>
                                <th>Desconto</th>
                                <th>Subtotal</th>
                              </tr>
                            </thead>
                            <tbody>

                            <?php for ($p = 0; $p < count($retOrder); $p++){ ?>
                              
                              <tr>
                                <?php if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ ?>
                                  <td>Retirar na <?php echo $retOrder[$p]['store'];?></td> <!-- VISAO DO ENTREGADOR -->
                                <?php } ?>
                                <td><?php echo $retOrder[$p]['offer'];?></td>
                                <td><?php echo $retOrder[$p]['description'];?></td>
                                <td><?php echo $retOrder[$p]['qtd'];?></td>
                                <td>R$ <?php echo number_format($retOrder[$p]['vlunit'], 2, ",", ".");?></td>
                                <td>-R$ <?php echo number_format($retOrder[$p]['discount'], 2, ",", ".");?></td>
                                <td>R$ <?php echo number_format(($retOrder[$p]['vlunit'] * $retOrder[$p]['qtd']) - $retOrder[$p]['discount'], 2, ",", ".");?></td>
                              </tr>

                            <?php } ?>
                           

                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                      
                      
                        <div class="row">

                          <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ ?>
                            <!-- accepted payments column -->
                            <div class="col-xs-6">
                              <p class="lead">Forma de Pagamento:</p>
                              <legend class="media red"><li class="fa fa-barcode"></li> Boleto Banc&aacute;rio</legend>
                            <!--  <legend class="media green"><li class="fa fa-credit-card"></li> Cart&atilde;o de Cr&eacute;dito</legend>
                              <legend class="media"><li class="fa fa-money"></li> Dinheiro</legend> -->
                              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                <?php if ($retOrder[0]['paymentdate'] != 0){ ?>
                                  Compensa&ccedil;&atilde;o realizada em <?php echo $retOrder[0]['paymentdate'];?>
                                <?php }else{ ?>
                                  Aguardando compensa&ccedil;&atilde;o
                                <?php } ?>  
                              </p>
                            </div>
                            <!-- /.col -->
                          <?php } ?>

                          <div class="col-xs-6">
                            <p class="lead">Valores</p>
                            <div class="table-responsive">
                              <table class="table">
                                <tbody>

                                <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ ?>
                                  <tr>
                                    <th style="width:50%">Subtotal &Iacute;tens:</th>
                                    <td>R$ <?php echo number_format($retOrder[0]['total_order'], 2, ",", ".");?></td>
                                  </tr>
                                <?php } ?>

                                  <?php if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ ?>
                                    <tr>
                                      <th>Taxa de Entrega:</th>
                                      <td>R$ <?php echo number_format($retOrder[0]['shipping'], 2, ",", ".");?> - <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><strong>(Ver C&aacute;lculo de Cubagem)</strong></a></td>
                                    </tr>
                                  <?php } ?>

                                  <!-- <tr>
                                    <th style="width:50%">Subtotal Geral:</th>
                                    <td>R$ <?php echo number_format($retOrder[0]['total_order'] + $retOrder[0]['shipping'], 2, ",", ".");?></td>
                                  </tr> -->

                                  <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ ?>
                                    <tr>
                                      <th>Repasse EM2D (<?php echo number_format(SIS_EM2D_COMISSION_STORES_PERCENT, 1, ",", "."); ?>)%</th>
                                      <td><strong class="red">-R$ <?php echo number_format((SIS_EM2D_COMISSION_STORES_PERCENT * ($retOrder[0]['total_order'])) / 100, 2, ",", ".");?></strong></td>
                                    </tr>
                                    <tr>
                                      <th><strong>TOTAL LOJISTA:</strong></th>
                                      <td><strong class="green">R$ <?php echo number_format($retOrder[0]['total_order'] - ((SIS_EM2D_COMISSION_STORES_PERCENT * ($retOrder[0]['total_order'])) / 100), 2, ",", ".");?></strong></td>
                                    </tr>
                                  <?php } if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ ?>
                                    <tr>
                                      <th>Repasse EM2D (<?php echo number_format(SIS_EM2D_COMISSION_LOGISTIC_PERCENT, 1, ",", "."); ?>)%</th>
                                      <td><strong class="red">-R$ <?php echo number_format((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retOrder[0]['shipping'])) / 100, 2, ",", ".");?></strong></td>
                                    </tr>
                                    <tr>
                                      <th><strong>TOTAL LOG&Iacute;STICA:</strong></th>
                                      <td><strong class="green">R$ <?php echo number_format($retOrder[0]['shipping'] - ((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retOrder[0]['shipping'])) / 100), 2, ",", ".");?></strong></td>
                                    </tr>
                                  <?php } ?>
                                  <tr>
                                      <th><strong>STATUS REPASSE:</strong></th>
                                      <td>
                                          <?php 
                                          if ($retOrder[0]['refunded'] == 1){ ?>
                                            <strong class='btn-success'>&nbsp;&nbsp;REPASSE EFETUADO&nbsp;&nbsp;</strong>
                                          <?php }else{ ?>
                                            <strong class='btn-danger'>&nbsp;&nbsp;AGUARDANDO FECHAMENTO&nbsp;&nbsp;</strong>
                                          <?php } ?>
                                      </td>
                                    </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <!-- /.col -->



                          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                  <div class="modal-content">

                                  <?php $retCubage = $functions->fGetCubageOrderItems($retOrder[0]['ordid']); ?>

                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                      </button>
                                      <h4 class="modal-title" id="myModalLabel">C&aacute;lculo de Cubagem</h4>
                                    </div>
                                    <div class="modal-body">
                                      <h4>F&oacute;rmula para c&aacute;lculo de peso cubado!</h4>
                                      <p>Alguns campos s&atilde;o <code>mandat&oacute;rios</code> baseado no tipo de produto &agrave; ser cadastrado.
                                        <br>Por exemplo, para o produto que estiver lan&ccedil;ando, voc&ecirc; <strong>DEVE</strong> informar peso, o(s) devidos tamanho(s) E/OU cor(es),
                                        <br>Adotado coeficiente 6000 (6.000 cm&sup3; ou 0,006m&sup3;) conforme padr&atilde;o IATA (International Air Transport Association).</p>
                                      
                                        <?php 

                                            $sumWeight = 0;

                                            for ($c = 0; $c < count($retCubage); $c++){ 
                                          
                                              list ($l, $a, $p) = explode("x", $retCubage[$c]['dimensions']);

                                              $calcCubage = ((($l/10) * ($a/10) * ($p/10)) / 6000);

                                              if ($calcCubage > $retCubage[$c]['weight']){
                                                $sumWeight += $calcCubage;
                                                $lblWeight = "";
                                                $lblCubage = "<strong class='btn-warning'>&nbsp;&nbsp;C&Aacute;LCULO SOBRE PESO CUBADO&nbsp;&nbsp;</strong>";
                                              }else{
                                                $sumWeight += $retCubage[$c]['weight'];
                                                $lblWeight = "<strong class='btn-warning'>&nbsp;&nbsp;C&Aacute;LCULO SOBRE PESO REAL&nbsp;&nbsp;</strong>";
                                                $lblCubage = "";
                                              }
                                          ?>
                                        
                                          <p><strong>Peso CUBADO de <?php echo $retCubage[$c]['description'];?></strong> = (<?php echo $l/10;?> x <?php echo $a/10;?> x <?php echo $p/10;?>) cm &divide; 6000 = <strong><?php echo number_format($calcCubage, 3, ",", "");?> kg/m&sup3;</strong> <?php echo $lblCubage; ?><br>
                                              <strong>Peso REAL de <?php echo $retCubage[$c]['description'];?></strong> = <strong><?php echo number_format($retCubage[$c]['weight'], 3, ",", "");?> kg</strong>  <?php echo $lblWeight; ?></p>

                                        <?php } ?>

                                        <?php

                                            $calcShipping = 0;

                                            if ($retCubage[0]['taxmode'] == 1)
                                            {
                                                $calcShipping = number_format($retCubage[0]['vltax'], 2, ",", ".");
                                                $lblTaxModeLabel = "TAXA DE ENTREGA FIXA";

                                            }elseif ($retCubage[0]['taxmode'] == 2){

                                                $calcShipping = number_format($retCubage[0]['vltax'] * $sumKM, 2, ",", ".");
                                                $lblTaxModeLabel = "DIST&Acirc;NCIA PERCORRIDA (".number_format($retCubage[0]['vltax'], 2, ",", ".")." x {$sumKM} km)";

                                            }elseif ($retCubage[0]['taxmode'] == 3){

                                              $calcShipping = number_format($sumWeight / $retCubage[0]['vltax'], 2, ",", ".");
                                              $lblTaxModeLabel = "FRETE PESO (".number_format($sumWeight, 3, ",", ".")." &divide; ".number_format($retCubage[0]['vltax'], 2, ",", ".").")";

                                            }

                                        ?>
                                    
                                        <p>PESO TOTAL: <strong><?php echo number_format($sumWeight, 3, ",", "");?> kg</strong></p>
                                        <p>MODALIDADE DE COBRAN&Ccedil;A: <strong><?php echo $lblTaxModeLabel;?></strong></p>
                                        <p>FRETE TOTAL: <strong class="green">R$ <?php echo $calcShipping;?></strong></p>

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <div class="modal fade bs-refund" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close modRefund" data-dismiss="modal"><span aria-hidden="true">×</span>
                                      </button>
                                      <h4 class="modal-title" id="myModalLabel">Confirma&ccedil;&atilde;o de Repasse</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Abaixo est&atilde;o os dados banc&aacute;rios do(s) Vendedores(s) e do Entregador, referentes a este pedido!<p>
                                      <p>Certifique-se de ter efetuado os respectivos dep&oacute;sitos ou transfer&ecirc;ncias banc&aacute;rias corretamente,
                                        <br>de acordo com os valores calculados.</p>
                                      <p>O repasse <code>SOMENTE</code> dever&aacute; ser efetuado SE, E SOMENTE SE o pedido constar como <strong class='btn-success'>&nbsp;&nbsp;PEDIDO PAGO&nbsp;&nbsp;</strong> e <strong class='red'>ENTREGUE</strong>.</p>  
                                      
                                        <br><div class="col-sm-12 invoice-col">
                                          <li class="fa fa-building-o"></li> VENDEDOR<?php echo ($retOrder[0]['numstores'] > 1 ? "ES" : ""); ?>
                                        </div>

                                        <?php for ($a = 0; $a < count($retOrder); $a++){ ?>
                                          <div class="col-sm-<?php echo (12 / count($retOrder)); ?> invoice-col">
                                              <address>
                                                <br><strong><?php echo $retOrder[$a]['store']; ?></strong>
                                                <br>Titular (Correntista): <?php echo $retOrder[$a]['owner']; ?> 
                                                <br>CPF/CNPJ: <?php echo $retOrder[$a]['cpfcnpj']; ?>
                                                <br>Banco: <?php echo $retOrder[$a]['bank']; ?>
                                                <br>Ag&ecirc;ncia: <?php echo $retOrder[$a]['agency']; ?>
                                                <br>Conta: <?php echo $retOrder[$a]['account']; ?>
                                                <br><strong>Subtotal LOJISTA: R$ <?php echo number_format(($retOrder[$a]['vlunit'] * $retOrder[$a]['qtd']) - $retOrder[$a]['discount'], 2, ",", ".");?></strong>
                                                <br><strong class="red">Repasse EM2D (<?php echo number_format(SIS_EM2D_COMISSION_STORES_PERCENT, 1, ",", "."); ?>)%: -R$ <?php echo number_format((SIS_EM2D_COMISSION_STORES_PERCENT * (($retOrder[$a]['vlunit'] * $retOrder[$a]['qtd']) - $retOrder[$a]['discount'])) / 100, 2, ",", ".");?></strong>
                                                <br><h4><strong class="green">TOTAL &Agrave; REPASSAR: R$ <?php echo number_format(($retOrder[$a]['vlunit'] * $retOrder[$a]['qtd']) - $retOrder[$a]['discount'] - ((SIS_EM2D_COMISSION_STORES_PERCENT * (($retOrder[$a]['vlunit'] * $retOrder[$a]['qtd']) - $retOrder[$a]['discount'])) / 100), 2, ",", ".");?></strong></h4><br><br>
                                              </address>
                                            </div>
                                          <?php } ?>

                                          <div class="col-sm-12 invoice-col">
                                            <li class="fa fa-truck"></li> ENTREGADOR
                                              <address>
                                                <br><strong><?php echo $retOrder[0]['name']; ?></strong>
                                                <br>Titular (Correntista): <?php echo $retOrder[0]['lowner']; ?> 
                                                <br>CPF/CNPJ: <?php echo $retOrder[0]['lcpfcnpj']; ?>
                                                <br>Banco: <?php echo $retOrder[0]['lbank']; ?>
                                                <br>Ag&ecirc;ncia: <?php echo $retOrder[0]['lagency']; ?>
                                                <br>Conta: <?php echo $retOrder[0]['laccount']; ?>
                                                <br><strong>Subtotal LOG&Iacute;STICA: R$ <?php echo number_format($retOrder[0]['shipping'], 2, ",", ".");?></strong>
                                                <br><strong class="red">Repasse EM2D (<?php echo number_format(SIS_EM2D_COMISSION_LOGISTIC_PERCENT, 1, ",", "."); ?>)%: -R$ <?php echo number_format((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retOrder[0]['shipping'])) / 100, 2, ",", ".");?></strong>
                                                <br><h4><strong class="green">TOTAL &Agrave; REPASSAR: R$ <?php echo number_format($retOrder[0]['shipping'] - ((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retOrder[0]['shipping'])) / 100), 2, ",", ".");?></strong></h4>
                                            </address>
                                          </div>

                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-primary" onclick="confirmRefund(<?php echo $retOrder[0]['ordid'];?>);">Confirmar Repasse</button>
                                    </div>
                                  </div>
                                </div>
                              </div>


                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <i class="fa fa-warning"></i> ATEN&Ccedil;&Atilde;O:<br>N&atilde;o se esque&ccedil;a de gerar a NF para o(s) respectivo(s) &iacute;tem(ns) deste pedido!
                          <button class="btn btn-primary pull-right" onclick="window.print();" style="margin-right: 5px;"><i class="fa fa-print"></i> Imprimir</button>
                          <?php if ($_SESSION['sPersonType'] == 4 && $retOrder[0]['refunded'] == 0){ ?>
                            <button data-toggle="modal" data-target=".bs-refund" id="btn-refund" class="btn btn-danger pull-right"><i class="fa fa-money"></i> Confirmar Repasse
                          <?php } if ($_SESSION['sPersonType'] == 3 && $retOrder[0]['paids'] == 1){ ?>
                            <?php if ($retOrder[0]['delivery_status'] == 0){ ?>
                              <button onclick="confirmDelivery(<?php echo $retOrder[0]['ordid'];?>, 1);" id="btn-delivery-1" class="btn btn-warning pull-right"><i class="fa fa-truck"></i> Sair p/ Retirada</button>
                            <?php } if ($retOrder[0]['delivery_status'] == 1){ ?>
                              <button onclick="confirmDelivery(<?php echo $retOrder[0]['ordid'];?>, 2);" id="btn-delivery-2" class="btn btn-warning pull-right"><i class="fa fa-check"></i> Confirmar Entrega</button>
                          <?php }} ?>
                          <button class="btn btn-default pull-right" onclick="location.href='<?php echo SIS_URL; ?>orders-manage'" style="margin-right: 5px;">Voltar</button>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>

    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
  </body>
</html>