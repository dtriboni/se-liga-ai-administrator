<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
    $functions = new functions();
   
    $functions->fCheckLevelProfile(2);

    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $theid = $_REQUEST['id'];

    if ($theid != '0' && $theid != ''){
      if (!$retStore = $functions->fGetStore($theid)){
        header('Location: '.SIS_URL.'home');
		exit;
      }
    }else{
        if($_SESSION['sPersonType'] != 4){
            header('Location: '.SIS_URL.'home');
            exit;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Cropper.js -->
    <link href="<?php echo SIS_URL; ?>vendors/cropper/dist/cropper.min.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="<?php echo SIS_URL; ?>vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Estabelecimento</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-store" class="form-horizontal form-label-left" enctype="multipart/form-data" data-parsley-validate>

                    <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/store" target="_blank"><strong>neste link!</strong></a>
                      </p>
                        <br>
                        
                        <?php

                          if ($_SESSION['sPersonType'] == 4){
                            
                            $retUser = $functions->fGetUsers();

                            if (is_array($retStore))
                            {
                              foreach ($retStore as $sanity => $user)
                                $users[] = $user['usrid'];
                            }else{
                                $users[] = null;
                            }
                            
                        ?> 

                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catid">Respons&aacute;vel <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="usrid" id="usrid" class="form-control" required="required">
                              <option selected value="">Selecione o Respons&aacute;vel pelo Estabelecimento</option>
                              <?php for ($u = 0; $u < count($retUser); $u++){ 
                                        if ($retUser[$u]['active'] == 1 ){ ?>
                                <option <?php echo (in_array($retUser[$u]['usrid'], $users) ? "selected" : ""); ?> value="<?php echo $retUser[$u]['usrid']; ?>"><?php echo $retUser[$u]['name']; ?> (<?php echo $retUser[$u]['email']; ?>)</option>
                              <?php } } ?>
                          </select>
                        </div>
                      </div>

                      <?php }else{ ?>

                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="complement">Respons&aacute;vel <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="complement" value="<?php echo $retStore[0]['name']; ?>" readonly class="form-control col-md-7 col-xs-12" name="complement" placeholder="Ex: CJ 10, Sala 156" type="text">
                          </div>
                        </div>

                      <?php } ?>  

                        <?php
                            
                            $retCategory = $functions->fGetCategories();

                            if (is_array($retStore))
                            {
                              foreach ($retStore as $sanity => $store)
                                $stores[] = $store['catid'];
                            }else{
                                $stores[] = null;
                            }
                            
                        ?> 

                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catid">Ramo do Estabelecimento <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="catid" id="catid" class="form-control" required="required">
                              <option selected value="">Selecione o Ramo</option>
                              <?php for ($c = 0; $c < count($retCategory); $c++){ 
                                        if ($retCategory[$c]['active'] == 1 ){ ?>
                                <option <?php echo (in_array($retCategory[$c]['catid'], $stores) ? "selected" : ""); ?> value="<?php echo $retCategory[$c]['catid']; ?>"><?php echo $retCategory[$c]['category']; ?></option>
                              <?php } } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store">Nome Fantasia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="store" value="<?php echo $retStore[0]['store']; ?>" class="form-control col-md-7 col-xs-12" name="store" placeholder="Ex: Empresa Teste" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store">Descri&ccedil;&atilde;o da Empresa <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="description" class="form-control col-md-7 col-xs-12" rows="6" name="description" placeholder="Uma breve descri&ccedil;&atilde;o de sua empresa. Conte a hist&oacute;ria dela." required="required"><?php echo ($retStore[0]['description']); ?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="youtube">URL YouTube 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="youtube" value="<?php echo $retStore[0]['youtube']; ?>" class="form-control col-md-7 col-xs-12" name="youtube" placeholder="Ex: https://www.youtube.com/watch?v=..." type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpfcnpj">CPF/CNPJ <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="cpfcnpj" value="<?php echo $retStore[0]['cpfcnpj']; ?>" onblur="formatDoc()" <?php echo ($retStore[0]['cpfcnpj'] == '' ? '' : 'readonly'); ?> class="form-control col-md-7 col-xs-12" name="cpfcnpj" placeholder="CPF ou CNPJ" required="required" type="text">
                          <ul class="parsley-errors-list" id="carac-error" style="display:none">
                            <li class="parsley-required"><br>Informe o CPF/CNPJ Corretamente.</li>
                          </ul> 
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >&nbsp;</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <h5>Dados Banc&aacute;rios para Repasse</h5>
                          Informe os dados banc&aacute;rios do <code>titular do CPF/CNPJ</code> para que a EM2D efetue os repasses mensais.
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bank">Banco <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                        <select name="bank" id="bank" class="form-control" required="required">
                              <option selected value="">Selecione o Banco</option>
                              <?php 
                              $retBank = $functions->fGetBanks();
                              for ($b = 0; $b < count($retBank); $b++){ ?>
                                <option <?php echo ($retStore[0]['bank'] == $retBank[$b]['bankname'] ? "selected" : ""); ?> value="<?php echo $retBank[$b]['bankname']; ?>"><?php echo $retBank[$b]['bankname']; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="agency">Ag&ecirc;ncia <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="agency" value="<?php echo $retStore[0]['agency']; ?>" data-inputmask="'mask' : '9999-9'" class="form-control col-md-7 col-xs-12" name="agency" placeholder="Ex: 9999-1" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="account">Conta <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="account" value="<?php echo $retStore[0]['account']; ?>"  class="form-control col-md-7 col-xs-12" name="account" placeholder="Ex: 12345-8" required="required" type="number">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="owner">Nome do Titular (Correntista) <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="owner" value="<?php echo $retStore[0]['owner']; ?>" class="form-control col-md-7 col-xs-12" name="owner" placeholder="Digite seu Nome Completo" required="required" type="text">
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="neighboor">Logotipo <span class="required">*</span><br>M&aacute;x. 1 imagem&nbsp;&nbsp;
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 dropzone" id="myDropzone">
                          <div class="dz-default dz-message" data-dz-message><li class="fa fa-upload"></li> Arraste a imagem desejada aqui ou <strong>clique para upload!</strong><br>M&aacute;x. 1 imagem (JPG, GIF, PNG)
                          <br><br><li class="fa fa-warning"></li> Aten&ccedil;&atilde;o: As imagens podem estar sujeitas a modera&ccedil;&atilde;o!</div>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-12">
                            <input type="hidden" name="id" id="id" value="<?php echo ($theid != '' ? $theid : '0'); ?>">
                            <button type="button" onclick="location.href='<?php echo SIS_URL; ?>store-list'" class="btn btn-primary">Cancelar</button>
                            <button id="send" type="submit" class="btn btn-success"><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Estabelecimento</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Cropper.js -->
    <script src="<?php echo SIS_URL; ?>vendors/cropper/dist/cropper.min.js"></script>
    <!-- Dropzone.js -->
    <script src="<?php echo SIS_URL; ?>vendors/dropzone/dist/dropzone.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script>
        <?php if($retStore[0]['logo'] != ''){ ?>
          $ADMIN_UPLOADED_FILES[0] = {"name": "<?php echo $retStore[0]['strid']; ?>", "directory": 'stores', "url" : "<?php echo $retStore[0]['logo']; ?>"};
        <?php } ?>  
    </script> 
    <script src="<?php echo SIS_URL; ?>build/js/upload_s.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/store/store.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
  </body>
</html>