<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile("2,4");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Planos Contratados p/ Estabelecimento</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Clique no plano desejado para expandir campos ocultos e exibir mais a&ccedil;&otilde;es
                    </p>

                    <?php $retPlans = $functions->fGetStorePlans(); ?>
					
                    <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Estabelecimento</th>
                          <th>Plano</th>
                          <th>Valor</th>
                          <th>Data Expira&ccedil;&atilde;o</th>
                          <th>Utiliza&ccedil;&atilde;o (ofertas/promo&ccedil;&otilde;es)</th>
                          <th>Status</th>
                          <?php if($_SESSION['sPersonType'] == 4){ ?>
                            <th>A&ccedil;&atilde;o</th>
                          <?php } ?>
                        </tr>
                      </thead>
                      <tbody>

                      <?php for ($a = 0; $a < count($retPlans); $a++){ 
                          
                          $lblActives = ($retPlans[$a]['active'] == 1 ? "<strong class='btn-success'>CONTRATARAM</strong>" : "<strong class='btn-danger'>CONTRATARAM</strong>");
                          
                        ?>

                        <tr id="row_<?php echo $retPlans[$a]['stpid']; ?>">
                          <td><?php echo $retPlans[$a]['store']; ?></td>
                          <td><?php echo $retPlans[$a]['plan']; ?></td>
                          <td><strong>
                          <?php if ((int)$retPlans[$a]['value'] != 0){ ?>
                                R$ <?php echo number_format($retPlans[$a]['value'], 2, ",", "."); ?>
                          <?php }else{ ?>
                                GRATUITO
                          <?php } ?>
                          </td></strong>
                          <td><strong><?php echo $retPlans[$a]['expires']; ?> <?php echo $retPlans[$a]['diffdate']; ?></strong></td>
                          <td>
                              <?php 
                                  if ($retPlans[$a]['paid'] != 'PAGO'){
                                    echo $retPlans[$a]['numoffers']; ?> dispon&iacute;veis no plano <?php echo $retPlans[$a]['plan'];
                                  }else{
                                    echo ($retPlans[$a]['used']); ?> de <?php echo $retPlans[$a]['numoffers']; ?> dispon&iacute;veis no plano <?php echo $retPlans[$a]['plan'];
                                  }  
                              ?>
                          </td>
                          <td> <?php 
                                  if ($retPlans[$a]['cancelled'] == 0){ ?>
                                    <strong class='btn-<?php echo ($retPlans[$a]['paid'] != 'PAGO' ? "danger" : "success"); ?>'>&nbsp;&nbsp;<?php echo $retPlans[$a]['paid']; ?>&nbsp;&nbsp;</strong></td>
                                  <?php }else{ ?>
                                    <strong class='btn-dark'>&nbsp;&nbsp;CANCELADO&nbsp;&nbsp;</strong>
                                  <?php } ?>
                          </td>
                          <?php if($_SESSION['sPersonType'] == 4){ ?>
                              <td>
                              <?php if ($retPlans[$a]['paid'] != 'PAGO' && $retPlans[$a]['cancelled'] == 0){ ?>
                                <a href="javascript:;" onclick="cancelPlan(<?php echo $retPlans[$a]['stpid']; ?>)" title="Cancelar Contrata&ccedil;&atilde;o"><li class="fa fa-times"></li></a> 
                              <?php }else if ($retPlans[$a]['paid'] != 'PAGO'){ ?>
                                | <a href="javascript:;" onclick="setAsPaid(<?php echo $retPlans[$a]['stpid']; ?>)" title="Confirmar Pagamento"><li class="fa fa-money"></li></a>  
                              <?php }else{ echo "-"; } ?>
                              </td>
                          <?php } ?>
                        </tr>
                      <?php } ?>
                                             
                      </tbody>
                    </table>

                    <?php if($_SESSION['sPersonType'] == 2){ ?>

                      <div class="ln_solid"></div>
                      <div class="col-md-12">
                      <a href="#" onclick="checkPlan(0);" class="btn btn-primary">Contratar Novo Plano</a>
                      </div>

                    <?php } ?>

                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>