<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
    $functions = new functions();
   
    $functions->fCheckLevelProfile(3);

    $retTax = $functions->fGetTaxLog();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Taxas & Abrang&ecirc;ncia</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-taxlog" class="form-horizontal form-label-left" data-parsley-validate>

                    <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/logistic" target="_blank"><strong>neste link!</strong></a>
                      </p>
                        <br>
                        
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="taxmode">Tipo de Cobran&ccedil;a <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="taxmode" id="taxmode" class="form-control" required="required">
                            <option selected value="">Selecione um Tipo de Cobran&ccedil;a</option>
                            <option <?php echo ($retTax[0]['taxmode'] == 1 ? "selected" : ""); ?> value="1">Taxa de Entrega Fixa</option>
                            <option <?php echo ($retTax[0]['taxmode'] == 2 ? "selected" : ""); ?> value="2">Percentual por dist&acirc;ncia percorrida (KM entre origem e destino)</option>
                            <option <?php echo ($retTax[0]['taxmode'] == 3 ? "selected" : ""); ?> value="3">Frete Peso</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vltax">Valor R$ <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="vltax" value="<?php echo ($retTax[0]['vltax']); ?>" class="form-control col-md-7 col-xs-12"  data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0', 'rightAlign': false" name="vltax" placeholder="<?php echo ("Digite o valor aplicado Ex: 15,90, 22,50"); ?>" <?php echo ('required="required"'); ?> type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="maxdist">&Aacute;rea m&aacute;xima de abrang&ecirc;ncia (km) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="maxdist" value="<?php echo $retTax[0]['maxdist'];?>" class="form-control col-md-7 col-xs-12"  name="maxdist" placeholder="Dist&acirc;ncia m&aacute;xima de entrega? Ex: 25, 50 km" required="required" type="number">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" onclick="location.href='<?php echo SIS_URL; ?>home'" class="btn btn-primary">Cancelar</button>
                            <button id="send" type="submit" class="btn btn-success">Atualizar Valores</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/taxlog/taxlog.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>