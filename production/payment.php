<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile("2,4");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
   <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>Pagamento</h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- Smart Wizard -->
                    <p>Siga os passos corretamente para poder efetuar o pagamento e contratar o plano desejado:</p>
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Etapa 1<br />
                                              <small>Confirmar plano</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Etapa 2<br />
                                              <small>Forma de Pagamento</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Etapa 3<br />
                                              <small>Processar Pagamento</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Etapa 4<br />
                                              <small>Confirma&ccedil;&atilde;o</small>
                                          </span>
                          </a>
                        </li>
                      </ul>
                      <div id="step-2" style="min-height: 300px !important; overflow:hidden">

                        <h2 class="StepTitle">Forma de Pagamento</h2>
                        <p>
                          Selecione a forma de pagamento desejada:
                        </p>

                        <form method="post" id="form-payment" data-parsley-validate class="form-horizontal form-label-left">
                           
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Forma de Pagamento</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <div id="type" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary active" onclick="fChangePayment(1)" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                <input type="radio" name="typec" value="CREDIT_CARD"><li class="fa fa-credit-card"></li> Cart&atilde;o
                                </label>
                                <label class="btn btn-primary" onclick="fChangePayment(2)" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                <input type="radio" name="typeb" value="BOLETO"><li class="fa fa-barcode"></li> Boleto
                                </label>
                                <?php if($_SESSION['sPersonType'] == 4){ ?>
                                  <label class="btn btn-primary" onclick="fChangePayment(3)" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                  <input type="radio" name="typed" value="MONEY"><li class="fa fa-money"></li> Dinheiro
                                  </label>
                                <?php } ?> 
                            </div>
                            </div>
                        </div>
                        
                        <div id="cc" style="display:block">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cardname">Titular do Cart&atilde;o <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="cardname" name="cardname" required="required" placeholder="Digite seu nome completo (como aparece no cart&atilde;o)" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="doc">CPF do Titular <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="doc" name="doc" data-inputmask="'mask' : '999.999.999-99'" placeholder="Digite seu CPF corretamente" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cardnum" class="control-label col-md-3 col-sm-3 col-xs-12">N&uacute;mero do Cart&atilde;o <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="cardnum" class="form-control col-md-7 col-xs-12" onblur="buscaBandeira(<?php echo $_SESSION['sPlanData'][0]['price']; ?>, <?php echo $_SESSION['sPlanData'][0]['periodmonth']; ?>, this.value)" data-inputmask="'mask' : '9999.9999.9999.9999'" placeholder="Digite o n&uacute;mero do seu cart&atilde;o" required="required" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="validated" class="control-label col-md-3 col-sm-3 col-xs-12">Validade <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="validated" data-inputmask="'mask' : '99/9999'" placeholder="Ex: 12/2027" class="form-control col-md-1 col-sm-1 col-xs-1" required="required" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cvv" class="control-label col-md-3 col-sm-3 col-xs-12">CVV <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="cvv" maxlength="3" data-inputmask="'mask' : '999'" placeholder="Ex: 123" class="form-control col-md-1 col-sm-1 col-xs-1" required="required" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Parcelamento <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <select disabled="disabled" name="installments" id="installments" class="form-control" required="required">
                                  <option selected value="">Parcelar o valor de R$ <?php echo number_format($_SESSION['sPlanData'][0]['price'], 2, ",", ".");?> em...</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div id="boleto" style="display:none">
                            <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p><br>Um boleto será gerado no valor de R$ <strong><?php echo number_format($_SESSION['sPlanData'][0]['price'], 2, ",", ".");?></strong> e 
                                    encaminhando ao seu e-mail de cadastro!</p>
                                    <p>A compensação ocorrerá dentro de 1 a 2 dias úteis.</p>
                                    <p>Após compensado o boleto, seu perfil estará liberado para cadastrar suas ofertas!</p>
                                    <p>
                                        Caso deseje alterar o plano selecionado por um outro, por favor, <a href="<?php echo SIS_URL; ?>aquire-plans"><strong>clique aqui!</strong></a>
                                    </p>
                                    <p>
                                        Clique em <strong>Avan&ccedil;ar</strong> para prosseguir.
                                    </p>
                                </div>
                            </div>
                          </div>
                          <div id="money" style="display:none">
                            <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p><br>Cobran&ccedil;a em DINHEIRO no valor de R$ <strong><?php echo number_format($_SESSION['sPlanData'][0]['price'], 2, ",", ".");?></strong>.</p>
                                    <p>M&eacute;todo exclusivo para uso de Perfil Administrador.</p>
                                    <p>O plano &eacute; habilitado instantaneamente ao prosseguir por este m&eacute;todo.</p>
                                    <p>
                                        Caso deseje alterar o plano selecionado por um outro, por favor, <a href="<?php echo SIS_URL; ?>aquire-plans"><strong>clique aqui!</strong></a>
                                    </p>
                                    <p>
                                        Clique em <strong>Avan&ccedil;ar</strong> para prosseguir.
                                    </p>
                                </div>
                            </div>
                          </div>
                        </form>

                      </div>
                      <div id="step-1" style="min-height: 300px !important; overflow:hidden">
                        <h2 class="StepTitle">Confirme o Plano Desejado</h2>
                        <p>
                          Aten&ccedil;&atilde;o <?php echo $_SESSION['sPersonPreName']; ?>, voc&ecirc; selecionou o seguinte plano para contratar:
                        </p>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <p>
                                <h2>Seu Plano &eacute; o <strong><?php echo $_SESSION['sPlanData'][0]['plan'];?></strong></h2>
                                <h2>Valor: <strong>R$ <?php echo number_format($_SESSION['sPlanData'][0]['price'], 2, ",", ".");?></strong> <small>em at&eacute; <?php echo $_SESSION['sPlanData'][0]['periodmonth'];?>x de R$ <?php echo number_format($_SESSION['sPlanData'][0]['price']/$_SESSION['sPlanData'][0]['periodmonth'], 2, ",", ".");?></small></h2>
                                <h2>V&aacute;lido por: <?php echo ($_SESSION['sPlanData'][0]['periodmonth'] == 1 ? "<strong>30</strong> dias" : "<strong>".$_SESSION['sPlanData'][0]['periodmonth']. "</strong> m&ecirc;ses");?></h2>
                                <h2>Cadastre <strong><?php echo ($_SESSION['sPlanData'][0]['numoffers'] == 9999 ? "quantas ofertas quiser!" : "at&eacute; ".$_SESSION['sPlanData'][0]['numoffers']." ofertas");?> </strong></h2>
                                <h2><i class="fa <?php echo ($_SESSION['sPlanData'][0]['canuseadmin'] == 1 ? "fa-check text-success" : "fa-times text-danger");?>"></i> Acesso ao portal gerenciador</h2>
                                <h2><i class="fa <?php echo ($_SESSION['sPlanData'][0]['canseller'] == 1 ? "fa-check text-success" : "fa-times text-danger");?>"></i> Vendas Online no App</h2>
                                </p>
                                <p>
                                Caso deseje alterar o plano selecionado por um outro, por favor, <a href="<?php echo SIS_URL; ?>aquire-plans"><strong>clique aqui!</strong></a>
                                </p>
                                <p>
                                Clique em <strong>Avan&ccedil;ar</strong> para prosseguir.
                                </p>
                                </div>
                          </div>
                      </div>
                      <div id="step-3" style="min-height: 300px !important; overflow:hidden">
                        <h2 class="StepTitle">Processar Pagamento</h2>
                        <p>
                          <div id="retpag">Aguarde! Estamos processando a sua solicita&ccedil;&atilde;o...</div>
                        </p>
                      </div>
                      <div id="step-4" style="min-height: 300px !important; overflow:hidden">
                        <h2 class="StepTitle">Confirma&ccedil;&atilde;o da Contrata&ccedil;&atilde;o</h2>
                        <p>Parab&eacute;ns <?php echo $_SESSION['sPersonPreName']; ?>! J&aacute; registramos a sua contrata&ccedil;&atilde;o do plano <strong><?php echo $_SESSION['sPlanData'][0]['plan'];?></strong> no sistema!
                        </p>
                        <p>
                         Assim que confirmarmos o pagamento do plano, voc&ecirc; poder&aacute; incluir novas ofertas/promo&ccedil;&otilde;es
                         <?php echo ($_SESSION['sPlanData'][0]['canuseadmin'] == 1 ? ',<br>bem como utilizar o painel administrativo' : '.');
						                   echo ($_SESSION['sPlanData'][0]['canseller'] == 1 ? ' e vender online atrav&eacute;s da plataforma.' : ''); ?>
                        </p>
                        <p>
                        Clique em <strong>Conclu&iacute;do</strong> para finalizar.
                        </p>
                      </div>

                    </div>
                    <!-- End SmartWizard Content -->



                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <script src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>

    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="<?php echo SIS_URL; ?>vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    
    <script src="<?php echo SIS_URL; ?>build/js/payment/payment.js"></script> 
    
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    
    <script> getSession();</script>

	
  </body>
</html>