<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
    $functions = new functions();
   
    $functions->fCheckLevelProfile(2);

    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $theid = $_REQUEST['id'];

    if ($theid != '0' && $theid != ''){
      if (!$retAddress = $functions->fGetAddress($theid)){
        header('Location: '.SIS_URL.'home');
			  exit;
      }
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
     <!-- Cropper.js -->
     <link href="<?php echo SIS_URL; ?>vendors/cropper/dist/cropper.min.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="<?php echo SIS_URL; ?>vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Endere&ccedil;o</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-address" class="form-horizontal form-label-left" data-parsley-validate>
                    <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/address" target="_blank"><strong>neste link!</strong></a>
                      </p>
                        <br>
                        
                      <?php
                         
                          if ($_SESSION['sPersonType'] == 4){
                            
                            $retStores = $functions->fGetStores();

                            if (is_array($retAddress))
                            {
                              foreach ($retAddress as $sanity => $address)
                                $addresses[] = $address['strid'];
                            }else{
                                $addresses[] = null;
                            }
                        ?> 

                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="strid">Endere&ccedil;o p/ Empresa <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="strid" id="strid" class="form-control">
                                <option selected value="">Selecione a Empresa</option>
                                <?php for ($s = 0; $s < count($retStores); $s++){ ?>
                                  <option <?php echo (in_array($retStores[$s]['strid'], $addresses) ? "selected" : ""); ?> value="<?php echo $retStores[$s]['strid']; ?>"><?php echo $retStores[$s]['storedata']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                      </div>

                      <?php } ?>

                      <?php 
                        
                        list($util, $sab, $dom) = explode("<br>", $retAddress[0]['open24h']); 

                        $utilI_sanity = explode(" ", $util);
                        $hUtil_sanity = explode(" - ", $util);
                        $hUtilT_sanity = explode(" ", $hUtil_sanity[1]);
                        $hSab_sanity = explode(" ", $sab);
                        $hDom_sanity = explode(" ", $dom);

                        $utilI = $utilI_sanity[0]." ".$utilI_sanity[1];
                        $utilF = $utilI_sanity[2]." ".$utilI_sanity[3];

                        $hUtilI = $hUtilT_sanity[0]." ".$hUtilT_sanity[1];
                        $hUtilF = $hUtilT_sanity[2]." ".$hUtilT_sanity[3];

                        $hSabI = $hSab_sanity[0]." ".$hSab_sanity[1]." ".$hSab_sanity[2];
                        $hSabF = $hSab_sanity[3]." ".$hSab_sanity[4];

                        $hDomI = $hDom_sanity[0]." ".$hDom_sanity[1]." ".$hDom_sanity[2]." ".$hDom_sanity[3];
                        $hDomF = $hDom_sanity[4]." ".$hDom_sanity[5];

                      ?>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store">Funcionamento (dias &uacute;teis)
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select name="deUI" id="deUF" class="form-control">
                            <option selected value="">Inicio</option>
                            <option <?php echo ($utilI == "De segunda" ? "selected" : ""); ?> value="De segunda">De Segunda</option>
                            <option <?php echo ($utilI == "De terça" ? "selected" : ""); ?> value="De ter&ccedil;a">De Ter&ccedil;a</option>
                            <option <?php echo ($utilI == "De quarta" ? "selected" : ""); ?> value="De quarta">De Quarta</option>
                            <option <?php echo ($utilI == "De quinta" ? "selected" : ""); ?> value="De quinta">De Quinta</option>
                            <option <?php echo ($utilI == "De sexta" ? "selected" : ""); ?> value="De sexta">De Sexta</option>
                            <option <?php echo ($utilI == "De sábado" ? "selected" : ""); ?> value="De s&aacute;bado">De S&aacute;bado</option>
                          </select>
                        </div><div class="col-md-2 col-sm-2 col-xs-12"> 
                          <select name="deUF" id="deUF" class="form-control">
                            <option selected value="">Fim</option>
                            <option <?php echo ($utilF == "à segunda" ? "selected" : ""); ?> value="&agrave; segunda">&agrave; Segunda</option>
                            <option <?php echo ($utilF == "à terça" ? "selected" : ""); ?> value="&agrave; ter&ccedil;a">&agrave; Ter&ccedil;a</option>
                            <option <?php echo ($utilF == "à quarta" ? "selected" : ""); ?> value="&agrave; quarta">&agrave; Quarta</option>
                            <option <?php echo ($utilF == "à quinta" ? "selected" : ""); ?> value="&agrave; quinta">&agrave; Quinta</option>
                            <option <?php echo ($utilF == "à sexta" ? "selected" : ""); ?> value="&agrave; sexta">&agrave; Sexta</option>
                            <option <?php echo ($utilF == "à sábado" ? "selected" : ""); ?> value="&agrave; s&aacute;bado">&agrave; S&aacute;bado</option>
                          </select>
                        </div>
                      </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store">Hor&aacute;rios (dia &uacute;til)
                        </label>  
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select name="hUI" id="hUI" class="form-control">
                            <option selected value="">Hor&aacute;rio Inicio</option>
                            <?php for($h1 = 0; $h1 < 24; $h1++){ ?>
                              <option <?php echo ($hUtilI == "Das ".$h1.":00" ? "selected" : ""); ?> value="Das <?php echo $h1;?>:00">Das <?php echo $h1;?>:00</option>
                              <option <?php echo ($hUtilI == "Das ".$h1.":30" ? "selected" : ""); ?> value="Das <?php echo $h1;?>:30">Das <?php echo $h1;?>:30</option>
                            <?php } ?>
                          </select>
                        </div><div class="col-md-2 col-sm-2 col-xs-12"> 
                          <select name="hUF" id="hUF" class="form-control">
                            <option selected value="">Hor&aacute;rio Fim</option>
                            <?php for($h1 = 0; $h1 < 24; $h1++){ ?>
                              <option <?php echo ($hUtilF == "Às ".$h1.":00" ? "selected" : ""); ?> value="&Agrave;s <?php echo $h1;?>:00">&agrave;s <?php echo $h1;?>:00</option>
                              <option <?php echo ($hUtilF == "Às ".$h1.":30" ? "selected" : ""); ?> value="&Agrave;s <?php echo $h1;?>:30">&agrave;s <?php echo $h1;?>:30</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store">Hor&aacute;rios (S&aacute;bados)
                        </label>  
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select name="sI" id="sI" class="form-control">
                            <option selected value="">Hor&aacute;rio Inicio</option>
                            <?php for($h1 = 0; $h1 < 24; $h1++){ ?>
                              <option <?php echo ($hSabI == "Sábados, das ".$h1.":00" ? "selected" : ""); ?> value="S&aacute;bados, das <?php echo $h1;?>:00">Das <?php echo $h1;?>:00</option>
                              <option <?php echo ($hSabI == "Sábados, das ".$h1.":30" ? "selected" : ""); ?> value="S&aacute;bados, das <?php echo $h1;?>:30">Das <?php echo $h1;?>:30</option>
                            <?php } ?>
                          </select>
                        </div><div class="col-md-2 col-sm-2 col-xs-12"> 
                          <select name="sF" id="sF" class="form-control">
                            <option selected value="">Hor&aacute;rio Fim</option>
                            <?php for($h1 = 0; $h1 < 24; $h1++){ ?>
                              <option <?php echo ($hSabF == "às ".$h1.":00" ? "selected" : ""); ?> value="&agrave;s <?php echo $h1;?>:00">&agrave;s <?php echo $h1;?>:00</option>
                              <option <?php echo ($hSabF == "às ".$h1.":30" ? "selected" : ""); ?> value="&agrave;s <?php echo $h1;?>:30">&agrave;s <?php echo $h1;?>:30</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store">Hor&aacute;rios (Domingos)
                        </label>  
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select name="dI" id="dF" class="form-control">
                            <option selected value="">Hor&aacute;rio Inicio</option>
                            <?php for($h1 = 0; $h1 < 24; $h1++){ ?>
                              <option <?php echo ($hDomI == "Aos domingos, das ".$h1.":00" ? "selected" : ""); ?> value="Aos domingos, das <?php echo $h1;?>:00">Das <?php echo $h1;?>:00</option>
                              <option <?php echo ($hDomI == "Aos domingos, das ".$h1.":30" ? "selected" : ""); ?> value="Aos domingos, das <?php echo $h1;?>:30">Das <?php echo $h1;?>:30</option>
                            <?php } ?>
                          </select>
                        </div><div class="col-md-2 col-sm-2 col-xs-12"> 
                          <select name="dF" id="dF" class="form-control">
                            <option selected value="">Hor&aacute;rio Fim</option>
                            <?php for($h1 = 0; $h1 < 24; $h1++){ ?>
                              <option <?php echo ($hDomF == "às ".$h1.":00" ? "selected" : ""); ?> value="&agrave;s <?php echo $h1;?>:00">&agrave;s <?php echo $h1;?>:00</option>
                              <option <?php echo ($hDomF == "às ".$h1.":30" ? "selected" : ""); ?> value="&agrave;s <?php echo $h1;?>:30">&agrave;s <?php echo $h1;?>:30</option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="neighboor">Imagens do Local <span class="required">*</span><br>M&aacute;x. 6 imagens&nbsp;&nbsp;
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 dropzone" id="myDropzone">
                          <div class="dz-default dz-message" data-dz-message><li class="fa fa-upload"></li> Arraste a imagem desejada aqui ou <strong>clique para upload!</strong><br>M&aacute;x. 6 imagens (JPG, GIF, PNG)
                          <br><br><li class="fa fa-warning"></li> Aten&ccedil;&atilde;o: As imagens podem estar sujeitas a modera&ccedil;&atilde;o!</div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Endere&ccedil;o <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="address" autocomplete="cc-number" value="<?php echo $retAddress[0]['address']; ?>" class="form-control col-md-7 col-xs-12" name="address" placeholder="Ex: Rua Adriana de Oliveira Silva" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">N&uacute;mero <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                          <input id="number" value="<?php echo $retAddress[0]['number']; ?>" class="form-control col-md-7 col-xs-12" name="number" placeholder="10, 100, 1000" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="complement">Complemento
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                          <input id="complement" value="<?php echo $retAddress[0]['complement']; ?>" class="form-control col-md-7 col-xs-12" name="complement" placeholder="Ex: CJ 10, Sala 156" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="neighboor">Bairro <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" value="<?php echo $retAddress[0]['neighboor']; ?>" id="neighboor" name="neighboor" required="required" placeholder="Vila Mariana" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zipcode">CEP <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                          <input id="zipcode" value="<?php echo $retAddress[0]['zipcode']; ?>" data-inputmask="'mask' : '99999-999'" class="form-control col-md-7 col-xs-12" name="zipcode" placeholder="01539-010" required="required" type="text">
                            <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm" target="_blank">N&atilde;o sei meu CEP</a>
                        </div>
                      </div>  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">Cidade <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="city" name="city"  value="<?php echo $retAddress[0]['city']; ?>" placeholder="S&atilde;o Paulo" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">UF <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <input type="text" id="state" name="state"  value="<?php echo $retAddress[0]['state']; ?>" required="required" placeholder="SP" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tel1">Telefone Fixo
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tel1" value="<?php echo $retAddress[0]['tel1']; ?>" class="form-control col-md-7 col-xs-12" name="tel1" placeholder="(11) 2345-0000" type="text">
                          <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">WhatsApp
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="whatsapp" value="<?php echo $retAddress[0]['whatsapp']; ?>" class="form-control col-md-7 col-xs-12" name="whatsapp" placeholder="(11) 98765-0000" type="text">
                          <span class="fa fa-whatsapp form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tel2">Celular <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tel2" value="<?php echo $retAddress[0]['tel2']; ?>" class="form-control col-md-7 col-xs-12" name="tel2" required="required" placeholder="(11) 98765-0000" type="text">
                          <span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">Website
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="url" value="<?php echo $retAddress[0]['url']; ?>" class="form-control col-md-7 col-xs-12" name="url" placeholder="Ex: http://www.sualoja.com.br" type="text">
                          <span class="fa fa-globe form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email da Loja
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="email" value="<?php echo $retAddress[0]['email']; ?>" class="form-control col-md-7 col-xs-12" name="email" placeholder="Ex: sualoja@email.com" type="text">
                          <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="twitter">Conta Twitter
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="twitter" value="<?php echo $retAddress[0]['twitter']; ?>" class="form-control col-md-7 col-xs-12" name="twitter" placeholder="Ex: em2dgroup" type="text">
                          <span class="fa fa-twitter form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="instagram">Conta Instagram
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="instagram" value="<?php echo $retAddress[0]['instagram']; ?>" class="form-control col-md-7 col-xs-12" name="instagram" placeholder="Ex: seligaiapp" type="text">
                          <span class="fa fa-instagram form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="id" id="id" value="<?php echo ($theid != '' ? $theid : '0'); ?>">
                            <input type="hidden" name="latitude" id="latitude" value="<?php echo $retAddress[0]['latitude']; ?>">
                            <input type="hidden" name="longitude" id="longitude" value="<?php echo $retAddress[0]['longitude']; ?>">
                            <button type="button" onclick="location.href='<?php echo SIS_URL; ?>address-list'" class="btn btn-primary">Cancelar</button>
                            <button id="send" type="submit" class="btn btn-success"><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Endere&ccedil;o</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
     <!-- Cropper.js -->
     <script src="<?php echo SIS_URL; ?>vendors/cropper/dist/cropper.min.js"></script>
    <!-- Dropzone.js -->
    <script src="<?php echo SIS_URL; ?>vendors/dropzone/dist/dropzone.js"></script>
    <!-- Custom Theme Scripts --> 
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script>
        <?php for ($p = 0; $p < count($retAddress); $p++){ 
          if ($retAddress[$p]['phoid'] != ''){?>
            $ADMIN_UPLOADED_FILES[<?php echo $p; ?>] = {"name": "<?php echo $retAddress[$p]['phoid']; ?>", "directory": 'stores', "url" : "<?php echo $retAddress[$p]['photo']; ?>"};
        <?php }} ?>
    </script>
    <script src="<?php echo SIS_URL; ?>build/js/upload_a.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/address/address.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGERYfSVlukcIsjXqXvfB86P9QmKW50TI&libraries=places&callback=initAutocomplete"></script>

	
  </body>
</html>