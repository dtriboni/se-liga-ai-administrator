<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
    $functions = new functions();
   
    $functions->fCheckLevelProfile(2);

    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $theid = $_REQUEST['id'];

    unset($_SESSION['sCaracteristicData']);

    list($numOffers, $dateRemaining, $dateExpires) = explode("|", $functions->fGetOffersRemaining());

    if ($theid != '0' && $theid != ''){
      if (!$retOffer = $functions->fGetOffer($theid)){
        header('Location: '.SIS_URL.'home');
        exit;
      }
    }else{
      if ($numOffers < 1 || $dateRemaining < 1){
        header('Location: '.SIS_URL.'home');
        exit;
      }
    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Cropper.js -->
    <link href="<?php echo SIS_URL; ?>vendors/cropper/dist/cropper.min.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="<?php echo SIS_URL; ?>vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="<?php echo SIS_URL; ?>vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Oferta/Promo&ccedil;&atilde;o</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-offer" class="form-horizontal form-label-left" enctype="multipart/form-data" data-parsley-validate>
                    <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/offer" target="_blank"><strong>neste link!</strong></a>

                      <?php if ($numOffers < 1 || $dateRemaining < 1){ ?>

                        <p>
                          <li class="fa fa-times text-danger"></li> Seu plano atual expirou. N&atilde;o &eacute; poss&iacute;vel cadastrar ofertas/promo&ccedil;&otilde;es! 
                          <br><a href='<?php echo SIS_URL;?>aquire-plans'><strong>Clique aqui</strong></a> para renovar seu contrato!
                        </p>

                      <?php }else{ ?>

                        <p>
                          <li class="fa fa-check text-success"></li> Voc&ecirc; pode cadastrar <code><?php echo ($numOffers >= 9999 ? "quantas ofertas quiser!" : "mais ".$numOffers.($numOffers < 2 ? " oferta ou promo&ccedil;&atilde;o.": " ofertas ou promo&ccedil;&otilde;es.")); ?></code> 
                          <?php echo ($dateRemaining < 10 ? "<br><li class='fa fa-warning text-danger'></li> <strong>ATEN&Ccedil;&Atilde;O:</strong> ".$dateExpires." <a href='".SIS_URL."aquire-plans'><strong>Clique aqui</strong></a> para renovar seu contrato!" : ""); ?>
                        </p>

                      <?php }?>

                        <br>
                        
                        <?php

                            $retAddress = $functions->fGetAddresses();

                            if (is_array($retOffer))
                            {
                              foreach ($retOffer as $sanity => $any)
                                $addresses[] = $any['addid'];
                            }else{
                                $addresses[] = null;
                            }
                            
                        ?> 

                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catid">Endere&ccedil;o da Oferta/Promo&ccedil;&atilde;o <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="addid" id="addid" class="form-control" required="required">
                              <option selected value="">Selecione o Endere&ccedil;o da Empresa</option>
                              <?php for ($u = 0; $u < count($retAddress); $u++){ 
                                        if ($retAddress[$u]['active'] == 1 ){ ?>
                                                              
                                  <?php if ($storeName != $retAddress[$u]['store']){
                                    if ($storeName != ""){ ?>
                                      </optgroup>
                                    <?php } ?>    
                                    <optgroup label="<?php echo $retAddress[$u]['store']; ?>">
                                  <?php } ?>

                                  <option <?php echo (in_array($retAddress[$u]['addid'], $addresses) ? "selected" : ""); ?> value="<?php echo $retAddress[$u]['addid']; ?>"><?php echo $retAddress[$u]['address']; ?>, <?php echo $retAddress[$u]['number']; ?> <?php echo $retAddress[$u]['complement']; ?> - <?php echo $retAddress[$u]['city']; ?>/<?php echo $retAddress[$u]['state']; ?></option>

                              <?php $storeName = $retAddress[$u]['store']; } } ?>
                          </select>
                        </div>
                      </div>
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catid">Tipo <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="type" onchange="changeTypeOffer(this.value)" id="type" class="form-control" required="required">
                              <option selected value="">Selecione o Tipo</option>
                                <option <?php echo (($retOffer[0]['type'] == 1) ? "selected" : ""); ?> value="1">PRODUTO</option>
                                <option <?php echo (($retOffer[0]['type'] == 2) ? "selected" : ""); ?> value="2">SERVI&Ccedil;O</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prodserv">T&iacute;tulo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="prodserv" value="<?php echo $retOffer[0]['prodserv']; ?>" class="form-control col-md-7 col-xs-12" name="prodserv" placeholder="Ex: Samsung Galaxy S10, Manuten&ccedil;&atilde;o de Notebook" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Descri&ccedil;&atilde;o <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="description" class="form-control col-md-7 col-xs-12" name="description" placeholder="Descreva com clareza, seu produto ou servi&ccedil;o" required="required"><?php echo $retOffer[0]['description']; ?></textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="modality">Modalidade <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="modality" onchange="changeModalityOffer(this.value)" id="modality" class="form-control" required="required">
                              <option selected value="">Selecione a Modalidade da Oferta/Promo&ccedil;&atilde;o</option>
                                <option <?php echo (($retOffer[0]['modality'] == 1) ? "selected" : ""); ?> value="1">OFERTA</option>
                                <option <?php echo (($retOffer[0]['modality'] == 2) ? "selected" : ""); ?> value="2">PROMO&Ccedil;&Atilde;O</option>
                                <option <?php echo (($retOffer[0]['modality'] == 3) ? "selected" : ""); ?> value="3">DESCONTO</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group stocks" style="<?php echo ($retOffer[0]['type'] == 2 ? "display:none" : "display:block");?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Caracter&iacute;sticas <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><li class="fa fa-sort-amount-asc"></li> Lan&ccedil;ar/Ver Caracter&iacute;sticas</button>
                          <ul class="parsley-errors-list" id="carac-error" style="display:none">
                            <li class="parsley-required"><br>Informe ao menos uma caracter&iacute;stica do Produto.</li>
                          </ul> 
                        </div>
                      </div>
                      <div class="item form-group prices" style="<?php echo ($retOffer[0]['modality'] == 3 ? "display:none" : "display:block");?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="oldprice">Valor Real <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="oldprice" value="<?php echo $retOffer[0]['oldprice']; ?>" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0', 'rightAlign': false" im-insert="true" class="form-control col-md-7 col-xs-12" name="oldprice" placeholder="Ex: 99,99 - 199,90" <?php echo ($retOffer[0]['modality'] == 1 || $retOffer[0]['modality'] == 2 ? 'required="required"' : "");?> type="text" >
                        </div>
                      </div>
                      <div class="item form-group promo" style="<?php echo ($retOffer[0]['modality'] == 1 || $retOffer[0]['modality'] == 3 ? "display:none" : "display:block");?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="oldprice">Valor Promocional <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="newprice" value="<?php echo $retOffer[0]['newprice']; ?>" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0', 'rightAlign': false" im-insert="true" class="form-control col-md-7 col-xs-12" name="newprice" placeholder="Ex: 59,99 - 159,90" <?php echo ($retOffer[0]['modality'] == 2 ? 'required="required"' : "");?> type="text">
                        </div>
                      </div>
                      <div class="item form-group desc" style="<?php echo ($retOffer[0]['modality'] == 3 ? "display:block" : "display:none");?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="oldprice">Percentual de Desconto <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="discount" value="<?php echo $retOffer[0]['discount']; ?>" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0', 'rightAlign': false" im-insert="true" class="form-control col-md-7 col-xs-12" name="discount" placeholder="Ex: 10,00 - 50,00" <?php echo ($retOffer[0]['modality'] == 3 ? 'required="required"' : "");?> type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="onlinesell">Vender Online? <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="onlinesell" id="onlinesell" class="form-control" required="required">
                              <option selected value="">Deseja vender este ítem online?</option>
                                <option <?php echo (($retOffer[0]['onlinesell'] == 1) ? "selected" : ""); ?> value="1">QUERO VENDER ONLINE!</option>
                                <option <?php echo (($retOffer[0]['onlinesell'] == 0) ? "selected" : ""); ?> value="2">QUERO APENAS DIVULGAR!</option>
                          </select>
                        </div>
                      </div>
                              <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                  <?php list($dm, $sz, $cl, $vo, $st, $cat) = explode("|", $functions->fGetCategoryCaracteristics()); ?>

                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                      </button>
                                      <h4 class="modal-title" id="myModalLabel">Caracter&iacute;sticas do Produto</h4>
                                    </div>
                                    <div class="modal-body">
                                      <h4>Regras de Lan&ccedil;amento para categoria <?php echo $cat;?></h4>
                                      <p>Alguns campos s&atilde;o <code>mandat&oacute;rios</code> baseado no tipo de produto &agrave; ser cadastrado.
                                        <br>Por exemplo, para o produto que estiver lan&ccedil;ando, voc&ecirc; <strong>DEVE</strong> informar peso, o(s) devidos tamanho(s) E/OU cor(es),
                                        <br>bem como informar a(s) voltagem(ns) e as dimens&otilde;es corretas, se aplic&aacute;veis ao tipo do produto.</p>
                                      <p>Clique no bot&atilde;o <strong>Adicionar</strong> quando estiver pronto.<br>Clique no &iacute;cone <strong class="text-danger">X</strong> para remover a caracter&iacute;stica desejada.</p>
                                                          
                                      <br>
                                      <div class="item form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="weight">Peso (kg) <span class="required">*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <input id="weight" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': true, 'digits': 3, 'digitsOptional': false, 'rightAlign': false" im-insert="true" class="form-control col-md-7 col-xs-12" name="weight" placeholder="Ex: 10,000">
                                          Informe o peso do produto para c&aacute;lculo do frete, em quilogramas.
                                        </div>
                                      </div>
                                      <div class="item form-group" style="<?php echo ($dm == 1 ? "display:block" : "display:none");?>">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="dimensions">Dimens&otilde;es (mm) <span class="required">*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <input id="dimensions" data-inputmask="'mask' : '#999 x #999 x #999'" im-insert="true" class="form-control col-md-7 col-xs-12" name="dimensions" placeholder="Ex: 1500 x 0430 x 0045">
                                          Informe as dimens&otilde;es na seguinte ordem: (Altura x Largura x Profundidade) em mil&iacute;metros.
                                        </div>
                                      </div>
                                      <div class="item form-group" style="<?php echo ($st == 1 ? "display:block" : "display:none");?>">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="stock">Qtde. Estoque <span class="required">*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <input id="stock" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': false, 'digits': 2, 'digitsOptional': true, 'placeholder': '0', 'rightAlign': false" im-insert="true" class="form-control col-md-7 col-xs-12" name="stock" placeholder="Ex: 100">
                                          Informe a quantidade REAL de estoque que deseja ser comercializada via App SeLigaA&iacute;
                                        </div>
                                      </div>
                                      <div class="item form-group" style="<?php echo ($st == 1 ? "display:block" : "display:none");?>">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="stockmin">Estoque M&iacute;nimo <span class="required">*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <input id="stockmin" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': false, 'digits': 2, 'digitsOptional': true, 'placeholder': '0', 'rightAlign': false" im-insert="true" class="form-control col-md-7 col-xs-12" name="stockmin" placeholder="Ex: 100">
                                          Informe a quantidade m&iacute;nima de estoque para ser alertado quando o estoque principal estiver acabando.
                                        </div>
                                      </div>
                                      <div class="item form-group" style="<?php echo ($sz == 1 ? "display:block" : "display:none");?>">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="size">Tamanho <span class="required">*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <input id="size" class="form-control col-md-7 col-xs-12" name="size" placeholder="Ex: 40, 41, 42, P, M, G, GG">
                                          Informe o tamanho de acordo com o tipo do produto. (Exemplo camisa: G, Tenis: 40).
                                        </div>
                                      </div>
                                      <div class="item form-group" style="<?php echo ($cl == 1 ? "display:block" : "display:none");?>">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="color">Cor 
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <div class="input-group demo2">
                                            <input type="text" id="color" name="color" class="form-control" />
                                            <span class="input-group-addon"><i></i></span>
                                            <span class="input-group-addon"><a href="javascript:;"><li class="fa fa-times"></li></a></span>
                                          </div>
                                          Selecione a cor desejada clicando no bot&atilde;o ao lado.
                                        </div>
                                      </div>
                                      <div class="item form-group" style="<?php echo ($vo == 1 ? "display:block" : "display:none");?>">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="voltage">Voltagem <span class="required">*</span>
                                        </label>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                          <input id="voltage" class="form-control col-md-7 col-xs-12" name="voltage" placeholder="Ex: 127, 220" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': false, 'digits': 2, 'digitsOptional': true, 'placeholder': '0', 'rightAlign': false" im-insert="true">
                                          Informe a tens&atilde;o ele&eacute;trica dispon&iacute;vel do produto.
                                        </div>
                                      </div>
                                      <div class="item form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12">
                                          </label>
                                          <div class="col-md-10 col-sm-10 col-xs-12">
                                          <button type="button" onclick="addCaracteristic('<?php echo $functions->fGetCategoryCaracteristics(); ?>');" class="btn btn-primary"><li class="fa fa-plus"></li> Adicionar</button>
                                          </div>
                                      </div>
                                      <br>
                                        <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                          <thead>
                                            <tr>
                                              <th>Peso(kg)</th>
                                              <th>Estoque</th>
                                              <th>Dimens&otilde;es(mm)</th>
                                              <th>Tamanho</th>                       
                                              <th>Cor</th>                        
                                              <th>Voltagem</th>                       
                                            </tr>
                                          </thead>
                                          <tbody>

                                            <?php 

                                              $retCaracteristics = $functions->fGetOfferCaracteristics($theid);

                                              for ($c = 0; $c < count($retCaracteristics); $c++){ 
                                            
                                            ?>

                                              <tr id="row_<?php echo $retCaracteristics[$c]['ctsid']; ?>">
                                                <td><?php echo number_format($retCaracteristics[$c]['weight'], 3, ",", "."); ?></td>
                                                <td><strong><?php echo $retCaracteristics[$c]['stock']; ?> / min <?php echo $retCaracteristics[$c]['stockmin']; ?></strong></td>
                                                <td><?php echo ($retCaracteristics[$c]['dimensions'] == '' ? '-' : $retCaracteristics[$c]['dimensions']); ?></td>
                                                <td><?php echo ($retCaracteristics[$c]['size'] == '' ? '-' : $retCaracteristics[$c]['size']); ?></td>
                                                <td><?php echo ($retCaracteristics[$c]['color'] == '' ? '-' : '<strong style="background-color:'.$retCaracteristics[$c]['color'].'">&nbsp;&nbsp;&nbsp;&nbsp;</strong>'); ?></td>
                                                <td><?php echo ($retCaracteristics[$c]['voltage'] == '' ? '-' : $retCaracteristics[$c]['voltage']); ?></td>
                                              </tr>
                                            <?php } 
                                            ?>
                                                                  
                                        </tbody>
                                      </table>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="start">In&iacute;cio da Vig&ecirc;ncia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="start" value="<?php echo $retOffer[0]['starts']; ?>" class="form-control col-md-7 col-xs-12" name="start" required="required" type="date">                        </div>
                      </div>
                      <!-- LEMBRANDO QUE EXPIRACAO NAO PODE EXCEDER A DATA FINAL DO PLANO CONTRATADO -->
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end">Expira&ccedil;&atilde;o <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="end" value="<?php echo $retOffer[0]['ends']; ?>" class="form-control col-md-7 col-xs-12" name="end" required="required" type="date">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="neighboor">Imagens <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12 dropzone" id="myDropzone">
                          <div class="dz-default dz-message" data-dz-message><li class="fa fa-upload"></li> Arraste as imagens desejadas aqui ou <strong>clique para upload!</strong><br>M&aacute;x. 10 imagens (JPG, GIF, PNG)
                          <br><br><li class="fa fa-warning"></li> Aten&ccedil;&atilde;o: As imagens podem estar sujeitas a modera&ccedil;&atilde;o!</div>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="id" id="id" value="<?php echo ($theid != '' ? $theid : '0'); ?>">
                            <button type="button" onclick="location.href='<?php echo SIS_URL; ?>offers-list'" class="btn btn-primary"><li class="fa fa-undo"></li> Cancelar</button>
<!--                             <button type="button" class="btn btn-warning"><li class="fa fa-mobile"></li> Ver no App</button>
 -->                            <button id="send" <?php echo ($dateRemaining < 0 && $theid == '0' ? "disabled='disabled'" : ""); ?> type="submit" class="btn btn-success"><li class="fa fa-check"></li> <?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Oferta/Promo&ccedil;&atilde;o</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
     
    <script src="<?php echo SIS_URL; ?>vendors/cropper/dist/cropper.min.js"></script>

    <!-- Dropzone.js -->
     <script src="<?php echo SIS_URL; ?>vendors/dropzone/dist/dropzone.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script>
        <?php for ($p = 0; $p < count($retOffer); $p++){ 
          if ($retOffer[$p]['phoid'] != ''){?>
            $ADMIN_UPLOADED_FILES[<?php echo $p; ?>] = {"name": "<?php echo $retOffer[$p]['phoid']; ?>", "directory": 'offers', "url" : "<?php echo $retOffer[$p]['photo']; ?>"};
        <?php }} ?>
    </script> 

     <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
       
    <script src="<?php echo SIS_URL; ?>build/js/upload_o.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/offer/offer.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo SIS_URL; ?>vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  </body>
</html>