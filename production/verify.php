<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
    
    $functions = new functions(false);

    $queries = new queries();
    
    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $_REQUEST['type'];

    $email = $functions->fDecrypt($_REQUEST['hash']);

    if (!$functions->fGetEmail($email, 'users')){
        header("location: ".SIS_URL."login");
    }
?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EM2D Admin</title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo SIS_URL; ?>vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="login">
  <div class="loading"></div>
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" id="form-retrieve" data-parsley-validate>
            <h1>Recuperar senha</h1>
              <p>Informe seu Email cadastrado para poder cadastrar uma nova senha:</p>
              <div>
                <input type="email" value="<?php echo trim($email); ?>" data-parsley-trigger="change" readonly id="usr" name="usr" class="form-control" placeholder="seu@email.com.br" required="" />
              </div>
              <div>
                <input type="password" id="pwd" name="pwd" class="form-control" placeholder="Digite sua nova Senha" required="required" />
              </div>
              <div>
                <input type="password"data-parsley-equalto="#pwd" id="cpwd" name="cpwd" class="form-control" placeholder="Confirme sua Senha" required="required" />
              </div>
              <div>
                <button type="submit" class="btn btn-primary">SALVAR SENHA</button>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
                <div>
                  <p>Copyright &copy; 2019 EM2D - Todos os direitos reservados.<br>
                  <a href="https://em2d.com.br/privacy-policy.html" target="_blank">Pol&iacute;tica de Privacidade & Termos de Uso</a></p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>

    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/auth/verify.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
  </body>
</html>