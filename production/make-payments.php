<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contas &agrave; Pagar</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Clique no &iacute;tem desejado para expandir campos ocultos e exibir mais a&ccedil;&otilde;es
                    </p>

                    <?php $retPayments = $functions->fGetReceivePayments(true); ?>
					
                    <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Per&iacute;odo</th>
                          <th>N&ordm; Pedidos</th>
                          <th>Valor Bruto</th>
                          <th>Repasse EM2D</th>
                          <?php if($_SESSION['sPersonType'] != 4){ ?>
                            <th>&Agrave; Receber</th>
                          <?php } ?>
                          <th>Status</th>
                          <th>A&ccedil;&atilde;o</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php for ($a = 0; $a < count($retPayments); $a++){ ?>

                        <tr>
                          <td><?php echo $retPayments[$a]['periodmonth']; ?></td>
                          <td><?php echo $retPayments[$a]['totorders']; ?></td>
                          <td><strong>

                          <?php if ($retPayments[$a]['type'] == 2){ ?>
                                R$ <?php echo number_format($retPayments[$a]['grossstore'], 2, ",", "."); ?>
                          <?php }elseif ($retPayments[$a]['type'] == 3){ ?>
                                R$ <?php echo number_format($retPayments[$a]['grosslogistic'], 2, ",", "."); ?>
                          <?php }else{ ?>
                                R$ <?php echo number_format($retPayments[$a]['grosslogistic'] + $retPayments[$a]['grossstore'], 2, ",", "."); ?>
                          <?php } ?>  

                          </strong></td>
                          <td><strong class="red">
                          <?php if ($retPayments[$a]['type'] == 2){ ?>
                                R$ <?php echo number_format(((SIS_EM2D_COMISSION_STORES_PERCENT * ($retPayments[$a]['grossstore'])) / 100), 2, ",", ".");?> (<?php echo number_format(SIS_EM2D_COMISSION_STORES_PERCENT, 1 ,",", "");?>%)
                          <?php }elseif ($retPayments[$a]['type'] == 3){ ?>
                                R$ <?php echo number_format(((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retPayments[$a]['grosslogistic'])) / 100), 2, ",", ".");?> (<?php echo number_format(SIS_EM2D_COMISSION_LOGISTIC_PERCENT, 1 ,",", "");?>%)
                          <?php }else{ 
                                  $totStoreComission = ((SIS_EM2D_COMISSION_STORES_PERCENT * ($retPayments[$a]['grossstore'])) / 100);
                                  $totLogisticComission = ((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retPayments[$a]['grosslogistic'])) / 100);
                                ?>
                                R$ <?php echo number_format($totStoreComission + $totLogisticComission, 2, ",", ".");?>
                          <?php } ?>  
                          </strong></td>

                          <?php if($_SESSION['sPersonType'] != 4){ ?>
                            <td><strong class="green">
                            <?php if ($retPayments[$a]['type'] == 2){ ?>
                                  R$ <?php echo number_format($retPayments[$a]['grossstore'] - ((SIS_EM2D_COMISSION_STORES_PERCENT * ($retPayments[$a]['grossstore'])) / 100), 2, ",", ".");?>
                            <?php }else{ ?>
                                  R$ <?php echo number_format($retPayments[$a]['grosslogistic'] - ((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retPayments[$a]['grosslogistic'])) / 100), 2, ",", ".");?>
                            <?php } ?>
                            </strong>
                            </td>
                          <?php } ?>

                          <td> <?php 
                                  if ($retPayments[$a]['status'] == 1){ ?>
                                    <strong class='btn-success'>&nbsp;&nbsp;REPASSE EFETUADO&nbsp;&nbsp;</strong>
                                  <?php }else{ ?>
                                    <strong class='btn-danger'>&nbsp;&nbsp;AGUARDANDO FECHAMENTO&nbsp;&nbsp;</strong>
                                  <?php } ?>
                                  </td>        
                          <td><a href="<?php echo SIS_URL;?>payment-manage/<?php echo $retPayments[$a]['periodlink']; ?>" title="Ver Pedidos"><li class="fa fa-eye"></li></a></td>
                        </tr>
                      <?php } ?>
                                             
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>