<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile("2,4");

   $strid = (isset($_SESSION['sStorePlanID']) ? $_SESSION['sStorePlanID'] : 0);

   $retPlan = $functions->fGetPlanPreviouslyAquired($strid);

if (count($retPlan) > 0)
{
   if ($retPlan[0]['paid'] == 0)
  {
    if ($retPlan[0]['cancelled'] == 1)
    {
      if ($retPlan[0]['diffdays'] > 15)
      {
        header('Location: '.SIS_URL.'home');
        exit;    
      }
      
    }else{

      header('Location: '.SIS_URL.'home');
      exit;    
    }

  }else{

    if ($retPlan[0]['diffdays'] > 15)
    {
      header('Location: '.SIS_URL.'home');
      exit;
    }
  }
}
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Planos de Contrata&ccedil;&atilde;o</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="height:600px;">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12">

                      <?php $retPlans = $functions->fGetPlans(true); 
                      
                          for ($a = 0; $a < count($retPlans); $a++){ 

                      ?>

                        <!-- price element -->
                        <div class="col-md-<?php echo (12 / count($retPlans)) ?> col-sm-6 col-xs-12">
                          <div class="pricing ui-ribbon-container">
                            <?php if ($retPlans[$a]['recomended'] == 1){ ?>
                              <div class="ui-ribbon-wrapper">
                                <div class="ui-ribbon">
                                  Essencial
                                </div>
                              </div>
                            <?php } ?>
                            <div class="title">
                              <h2>Plano <?php echo $retPlans[$a]['plan'];?></h2>
                              <h1><?php echo ($retPlans[$a]['periodmonth'] > 1 ? $retPlans[$a]['periodmonth']."x R$ ". number_format($retPlans[$a]['price']/$retPlans[$a]['periodmonth'], 2, ",", ".") : "R$ ". number_format($retPlans[$a]['price'], 2, ",", ".")." &agrave; vista");?></h1>
                              <?php if ($retPlans[$a]['periodmonth'] > 1){ ?> 
                                <span>ou R$ <?php echo number_format($retPlans[$a]['price'], 2, ",", ".");?> &agrave; vista no boleto</span>
                              <?php } ?>
                            </div>
                            <div class="x_content">
                              <div class="">
                                <div class="pricing_features">
                                  <ul class="list-unstyled text-left">
                                    <li><i class="fa fa-check text-success"></i> Per&iacute;odo de <strong> <?php echo ($retPlans[$a]['periodmonth'] == 1 ? "30 dias" : $retPlans[$a]['periodmonth']." m&ecirc;ses");?></strong></li>
                                    <li><i class="fa fa-check text-success"></i> Cadastre <strong><?php echo ($retPlans[$a]['numoffers'] == 9999 ? "quantas ofertas quiser" : "at&eacute; ".$retPlans[$a]['numoffers']." ofertas");?> </strong></li>
                                    <li><i class="fa <?php echo ($retPlans[$a]['canuseadmin'] == 1 ? "fa-check text-success" : "fa-times text-danger");?>"></i> Acesso ao <strong>portal gerenciador</strong></li>
                                    <li><i class="fa <?php echo ($retPlans[$a]['canseller'] == 1 ? "fa-check text-success" : "fa-times text-danger");?>"></i> <strong>Vendas Online no App</strong></li>
                                    <li><i class="fa fa-check text-success"></i> <strong>Push Notification</strong> a cada lan&ccedil;amento</li>
                                    <li><i class="fa <?php echo ($retPlans[$a]['plaid'] == 6 ? "fa-check text-success" : "fa-times text-danger");?>"></i> Visibilidade <strong>padr&atilde;o</strong></li>
                                    <li><i class="fa <?php echo ($retPlans[$a]['plaid'] == 1 || $retPlans[$a]['plaid'] == 2 ? "fa-check text-success" : "fa-times text-danger");?>"></i> Visiblidade <strong>destacada</strong></li>
                                    <li><i class="fa <?php echo ($retPlans[$a]['plaid'] == 3 ? "fa-check text-success" : "fa-times text-danger");?>"></i> <strong>TOP </strong> nas buscas por ofertas e promo&ccedil;&otilde;es</li>
                                  </ul>
                                </div>
                              </div>
                              <div class="pricing_footer">
                                <a href="javascript:void(0);" onclick="selectPlan(<?php echo $retPlans[$a]['plaid']; ?>)" class="btn btn-primary btn-block" role="button">Contratar Agora</a>
                                <p>
                                  <a href="<?php echo SIS_URL; ?>home">Voltar</a>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- price element -->

                          <?php } ?>  



                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
  </body>
</html>