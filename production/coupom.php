<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
    $functions = new functions();
   
    $functions->fCheckLevelProfile(2);

    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $theid = $_REQUEST['id'];

    if ($theid != '0' && $theid != ''){
      if (!$retCoupom = $functions->fGetCoupom($theid)){
        header('Location: '.SIS_URL.'home');
		    exit;
      }
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Cupom de Desconto</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-coupom" class="form-horizontal form-label-left" data-parsley-validate>

                    <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/coupons" target="_blank"><strong>neste link!</strong></a>
                      </p>
                        <br>
                        
                        <?php
                         
                          if ($_SESSION['sPersonType'] == 4){
                            
                            $retStores = $functions->fGetStores();

                            if (is_array($retCoupom))
                            {
                              foreach ($retCoupom as $sanity => $coupom)
                                $coupons[] = $coupom['strid'];
                            }else{
                                $coupons[] = null;
                            }
                        ?> 

                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="strid">Propriet&aacute;rio do Cupom <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="strid" id="strid" class="form-control">
                                <option selected value="">Selecione a Empresa</option>
                                <option <?php echo ($retStores[$s]['strid'] == 0 ? "selected" : ""); ?> value="0">Geral Se Liga A&iacute;</option>
                                <?php for ($s = 0; $s < count($retStores); $s++){ ?>
                                  <option <?php echo (in_array($retStores[$s]['strid'], $coupons) ? "selected" : ""); ?> value="<?php echo $retStores[$s]['strid']; ?>"><?php echo $retStores[$s]['storedata']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                      </div>

                      <?php } ?>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="value">Valor de Desconto R$ <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="value" value="<?php echo $retCoupom[0]['value']; ?>" class="form-control col-md-7 col-xs-12"  data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixPoint': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0', 'rightAlign': false" name="value" placeholder="Digite o valor aplicado Ex: 5,00 10,50 20,00" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="maxusagepershop">N&ordm;. M&aacute;ximo utiliza&ccedil;&atilde;o por compra *<br><small class="red">Quantas vezes o usu&aacute;rio poder&aacute; utilizar o mesmo cupom</small>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="maxusagepershop" value="<?php echo $retCoupom[0]['maxusagepershop'];?>" class="form-control col-md-7 col-xs-12"  name="maxusagepershop" placeholder="Ex: 1, 10, 100, 1000" required="required" type="number">
                        </div>
                      </div>
                      <?php
                        $randPass = strtoupper($functions->fRandomPassword(10));
                        $aux = SIS_URL.'qrcode/'.($retCoupom[0]['code'] == '' ? $randPass : $retCoupom[0]['code']);
                        ?>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">C&oacute;digo do Cupom <span class="required">*</span><br><small class="red">Utilize o c&oacute;digo gerado automaticamente<br>ou digite um personalizado, de sua prefer&ecirc;ncia.<br><br>Caso deseje validar o QR Code, utilize um<br>leitor de c&oacute;digo em seu celular.</small>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="code" onblur="fChangeCoupom(event);" value="<?php echo ($retCoupom[0]['code'] == '' ? $randPass : $retCoupom[0]['code']); ?>" <?php echo ($retCoupom[0]['code'] == '' ? "" : "readonly"); ?> class="form-control col-md-3 col-xs-12" name="code" required="required" type="text">
                          <br><img src="<?php echo $aux; ?>" />
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="expires">Expira&ccedil;&atilde;o <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="expires" value="<?php echo $retCoupom[0]['expires']; ?>" class="form-control col-md-7 col-xs-12" name="expires" required="required" type="date">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="id" id="id" value="<?php echo ($theid != '' ? $theid : '0'); ?>">
                            <button type="button" onclick="location.href='<?php echo SIS_URL; ?>coupons-list'" class="btn btn-primary">Cancelar</button>
                            <button id="send" type="submit" class="btn btn-success"><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Cupom</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
     <script src="<?php echo SIS_URL; ?>build/js/coupom/coupom.js"></script> 
     <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>