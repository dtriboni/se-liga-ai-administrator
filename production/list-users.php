<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   // Check if this page have permission to access
   //$functions->fCheckLevelProfile(2);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Usu&aacute;rios SeLigaA&iacute;</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Clique no usu&aacute;rio desejado para expandir campos ocultos e exibir mais a&ccedil;&otilde;es
                    </p>

                    <?php $retUsers = $functions->fGetUsers(); ?>
					
                    <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nome Completo</th>
                          <th>ATIVO?</th>
                          <th>Sexo</th>
                          <th>Origem</th>
                          <th>CPF/CNPJ</th>
                          <th>E-mail/Contato</th>
                          <th>Tipo</th>
                          <th>Data de Nascimento</th>
                          <th>Data de Cadastro</th>
                          <th>A&ccedil;&atilde;o</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php for ($a = 0; $a < count($retUsers); $a++){ ?>
                        <tr id="row_<?php echo $retUsers[$a]['usrid']; ?>">
                          
                          <?php if ($retUsers[$a]['password'] == '0000'){ ?>
                            <td><strong><li class="fa fa-star"></li> <?php echo strtoupper($retUsers[$a]['name']);?></strong></td>
                            <td><?php echo $retUsers[$a]['actived']; ?></td>
                            <td>-</td>
                            <td><?php echo ucfirst($retUsers[$a]['signup']); ?></td>
                            <td><strong class="btn-dark">&nbsp;NOVO USU&Aacute;RIO SE LIGA A&Iacute;&nbsp;</strong></td>
                            <td><strong><?php echo $retUsers[$a]['email']; ?> / <?php echo $retUsers[$a]['cellphone']; ?></strong></td>
                            <td><strong class="btn-<?php echo $arrayTypePersonColor[$retUsers[$a]['type']];?>">&nbsp;<?php echo $arrayTypePerson[$retUsers[$a]['type']]; ?>&nbsp;</strong></td>
                            <td>-</td>
                            <td><strong><?php echo $retUsers[$a]['cadastro']; ?></strong></td>
                          <?php }else{ ?>
                            <td><?php echo $retUsers[$a]['name']; ?></td>
                            <td><?php echo $retUsers[$a]['actived']; ?></td>
                            <td><?php echo $retUsers[$a]['sexo']; ?></td>
                            <td><?php echo ucfirst($retUsers[$a]['signup']); ?></td>
                            <td><?php echo $retUsers[$a]['cpfcnpj']; ?></td>
                            <td><?php echo $retUsers[$a]['email']; ?></td>
                            <td><strong class="btn-<?php echo $arrayTypePersonColor[$retUsers[$a]['type']];?>">&nbsp;<?php echo $arrayTypePerson[$retUsers[$a]['type']]; ?>&nbsp;</strong></td>
                            <td><?php echo $retUsers[$a]['nascimento']; ?></td>
                            <td><?php echo $retUsers[$a]['cadastro']; ?></td>
                          <?php } ?>
                          
                          <td>
                              <a href="<?php echo SIS_URL."user/".$retUsers[$a]['usrid'] ;?>" title="Alterar Registro"><li class="fa fa-edit"></li></a>
                             
                              <?php  if ($_SESSION['sPersonType'] == 4 && $_SESSION['sPersonID'] != $retUsers[$a]['usrid']){ //Only Admin Profile is granted to disable users
                                        if($retUsers[$a]['active'] == 1){ 
                                ?> | 
                                    <a href="javascript:;" onclick="changeRecord(<?php echo $retUsers[$a]['usrid']; ?>, 'usrid', 'u', 0)" title="Desativar"><li class="fa fa-eye-slash"></li></a>
                                <?php }else{ ?>
                                     | <a href="javascript:;" onclick="changeRecord(<?php echo $retUsers[$a]['usrid']; ?>, 'usrid', 'u', 1)" title="Ativar"><li class="fa fa-eye"></li></a>
                              <?php } } ?>  

                            </td>
                        </tr>
                      <?php } ?>
                                             
                      </tbody>
                    </table>

                        <?php if($_SESSION['sPersonType'] == 4){ ?>

                            <div class="ln_solid"></div>
                            <div class="col-md-12">
                            <a href="<?php echo SIS_URL;?>user" class="btn btn-primary">Cadastrar Novo Usu&aacute;rio</a>
                            </div>

                        <?php } ?>

                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>