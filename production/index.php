<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
  $functions = new functions();  

?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title><?php echo SIS_TITULO; ?> </title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo SIS_URL; ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo SIS_URL; ?>vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">

          <?php $retTotals = $functions->fGetTotals4Home(); ?>
            
            <?php if ($_SESSION['sPersonType'] == 2){ ?>
              <!-- DASHBOARD LOJISTA -->
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-shopping-cart"></i> Pedidos do M&ecirc;s</span>
                <div class="count green"><?php echo $retTotals[0]['numtotseller']; ?></div>
                <span class="count_bottom"><i class="green">realizados no m&ecirc;s atual</i></span>
              </div>

              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Total Mensal</span>
                <div class="count green"><?php echo number_format($retTotals[0]['totseller'], 2, ",", "."); ?></div>
                <?php
                    $percent = (100 / $retTotals[0]['totsellerprevious']) * $retTotals[0]['totseller'];
                    $sort = ($percent > 99 ? "asc" : "desc");
                    $color = ($percent > 99 ? "green" : "red");
                ?>
                <span class="count_bottom"><i class="<?php echo $color; ?>"><i class="fa fa-sort-<?php echo $sort; ?>"></i><?php echo number_format($percent, 0, ",", ""); ?>% </i> do &uacute;ltimo m&ecirc;s</span>
              </div>
              
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Repasse EM2D</span>
                <div class="count red"><?php echo number_format((SIS_EM2D_COMISSION_STORES_PERCENT * ($retTotals[0]['totseller'])) / 100, 2, ",", ".");?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-warning"></i> <?php echo number_format(SIS_EM2D_COMISSION_STORES_PERCENT, 1, ",", "."); ?>% do Mensal </i> <a href="https://em2d.com.br/privacy-policy.html#terms" target="_blank">Ver Termos</a></span>
              </div>

              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Total &agrave; Receber</span>
                <div class="count green"><?php echo number_format($retTotals[0]['totseller'] - ((SIS_EM2D_COMISSION_STORES_PERCENT * ($retTotals[0]['totseller'])) / 100), 2, ",", ".");?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-calendar"></i> Ocorre todo dia 15 </i></span>
              </div>
            <?php } ?>

            <?php if ($_SESSION['sPersonType'] == 3){ ?>
              <!-- DASHBOARD LOGISTICA -->
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-truck"></i> Entregas do M&ecirc;s</span>
                <div class="count green"><?php echo $retTotals[0]['numtotlogger']; ?></div>
                <span class="count_bottom"><i class="green">realizadas no m&ecirc;s atual</i></span>
              </div>

              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Total Mensal</span>
                <div class="count green"><?php echo number_format($retTotals[0]['totlogger'], 2, ",", "."); ?></div>
                <?php
                    $percent = (100 / $retTotals[0]['totloggerprevious']) * $retTotals[0]['totlogger'];
                    $sort = ($percent > 99 ? "asc" : "desc");
                    $color = ($percent > 99 ? "green" : "red");
                ?>
                <span class="count_bottom"><i class="<?php echo $color; ?>"><i class="fa fa-sort-<?php echo $sort; ?>"></i><?php echo number_format($percent, 0, ",", ""); ?>% </i> do &uacute;ltimo m&ecirc;s</span>
              </div>
              
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Repasse EM2D</span>
                <div class="count red"><?php echo number_format((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retTotals[0]['totlogger'])) / 100, 2, ",", ".");?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-warning"></i> <?php echo number_format(SIS_EM2D_COMISSION_LOGISTIC_PERCENT, 1, ",", "."); ?>% do Mensal </i> <a href="https://em2d.com.br/privacy-policy.html#terms" target="_blank">Ver Termos</a></span>
              </div>

              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> Total &agrave; Receber</span>
                <div class="count green"><?php echo number_format($retTotals[0]['totlogger'] - ((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retTotals[0]['totlogger'])) / 100), 2, ",", ".");?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-calendar"></i> Ocorre todo dia 15 </i></span>
              </div>
            <?php } ?>

            <?php if ($_SESSION['sPersonType'] == 4){ ?>
              <!-- DASHBOARD ADMIN -->
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total de Usu&aacute;rios</span>
                <div class="count"><?php echo $functions->fBeaultyNum($retTotals[0]['numtotusers'], 1); ?></div>
                <span class="count_bottom"><i class="green"><?php echo $functions->fBeaultyNum($retTotals[0]['numtotusers'], 0); ?></i></span>
              </div> 
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-tags"></i> Ofertas Ativas</span>
                <div class="count"><?php echo $retTotals[0]['numtotactiveoffers']; ?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i><?php echo $retTotals[0]['numtotinactiveoffers']; ?> </i> inativas</span>
              </div> 
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> &Agrave; receber Lojistas</span>
                <div class="count green"><?php echo number_format( ((SIS_EM2D_COMISSION_STORES_PERCENT * ($retTotals[0]['totseller_r'])) / 100), 2, ",", ".");?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-warning"></i> <?php echo number_format(SIS_EM2D_COMISSION_STORES_PERCENT, 1, ",", "."); ?>% do m&ecirc;s vigente total</i></span>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-money"></i> &Agrave; receber Entregas</span>
                <div class="count green"><?php echo number_format( ((SIS_EM2D_COMISSION_LOGISTIC_PERCENT * ($retTotals[0]['totlogger_r'])) / 100), 2, ",", ".");?></div>
                <span class="count_bottom"><i class="red"><i class="fa fa-warning"></i> <?php echo number_format(SIS_EM2D_COMISSION_LOGISTIC_PERCENT, 1, ",", "."); ?>% do m&ecirc;s vigente total</i></span>
              </div>
            <?php } ?>

          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
                <div class="row x_title">
                  <div class="col-md-12">
                    <h3>Evolu&ccedil;&atilde;o de Vendas via App <small>M&ecirc;s Atual</small></h3>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div id="chart_plot_01" class="demo-placeholder"></div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <br />

          <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Lan&ccedil;amentos</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        <p></p>
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          Tipo/Modalidade
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                          Quantidade
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                          <tr>
                            <td>
                              <p><i class="fa fa-square aero"></i>Produto </p>
                            </td>
                            <td id="tdprod">--</td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square purple"></i>Servi&ccedil;o </p>
                            </td>
                            <td id="tdserv">--</td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square red"></i>Oferta </p>
                            </td>
                            <td id="tdoffer">--</td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square green"></i>Promo&ccedil;&atilde;o </p>
                            </td>
                            <td id="tdpromo">--</td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square blue"></i>Desconto Percentual </p>
                            </td>
                            <td id="tddesc">--</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td id="numoffers"></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>


            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Meu Perfil</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
                    <ul class="quick-list">
                      <li><i class="fa fa-user"></i><a href="#">Dados do Usu&aacute;rio</a></li>
                      <li><i class="fa fa-building"></i><a href="#">Dados da Empresa</a></li>
                      <li><i class="fa fa-home"></i><a href="#">Endere&ccedil;os da Empresa</a> </li>
                      <li><i class="fa fa-tags"></i><a href="#">Ofertas Cadastradas</a></li>
                      <li><i class="fa fa-money"></i><a href="#">Vendas via App</a> </li>
                      <li><i class="fa fa-trophy"></i><a href="#">TOP Seller (50 vendas / m&ecirc;s)</a>
                      </li>
                    </ul>

                    <div class="sidebar-widget">
                        <h4>Evolu&ccedil;&atilde;o</h4>
                        <canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
                        <div class="goal-wrapper">
                          <span id="gauge-text" class="gauge-value pull-left">0</span>
                          <span class="gauge-value pull-left">%</span>
                          <span id="goal-text" class="goal-value pull-right">100%</span>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>


          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Novidades Se Liga A&iacute;</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                     
                    <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                              <a>Novo Portal Gerenciador do Se Liga A&iacute;</a>
                            </h2>
                            <div class="byline">
                              <span>13/04/2019</span>
                            </div>
                            <p class="excerpt">Com o portal gerenciador do Se Liga A&iacute;, ficou mais f&aacute;cil gerenciar suas ofertas, seus planos contratados, sua conta e muito mais. Consulte a documenta&ccedil;&atilde;o para maiores detalhes.
                            </p>
                          </div>
                        </div>
                      </li>

                      
                    </ul>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /page content -->

       <?php require("../_requires/footer.php"); ?>

      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo SIS_URL; ?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo SIS_URL; ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo SIS_URL; ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo SIS_URL; ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo SIS_URL; ?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo SIS_URL; ?>vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo SIS_URL; ?>vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo SIS_URL; ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo SIS_URL; ?>vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
	
  </body>
</html>
