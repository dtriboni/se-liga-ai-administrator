<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
    
    $functions = new functions(false);

    $queries = new queries();
?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EM2D Admin</title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo SIS_URL; ?>vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="login">
  <div class="loading"></div>
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" id="form-signup" data-parsley-validate>
            <img class="logo-login" src="<?php echo SIS_URL; ?>production/images/logo/logo-login.png">
              <h4>&Eacute; um prazer ter voc&ecirc; conosco!</h4><br>
              <p>Preencha o formul&aacute;rio abaixo para que nossa equipe possa entrar em contato com voc&ecirc;:</p> 
              <div>
                <input type="text" id="name" name="name" class="form-control" placeholder="Informe seu Nome Completo" required="required" />
              </div>
              <div>
                <input type="email" id="usr" name="usr" class="form-control" placeholder="seu@email.com.br" required="required" />
              </div>
              <div>
                <input type="text" id="tel" name="tel" class="form-control" placeholder="Celular, WhatsApp ou Telefone Fixo" required="required" />
              </div>
              <div>
                  <select name="type" id="type" class="form-control" required="required">
                        <option selected value="">O que deseja fazer no Se Liga A&iacute;?</option>
                        <option value="2">Quero Divulgar minha Empresa</option>
                        <option value="3">Quero ser um Entregador</option>
                  </select>
              </div><br>
              <div>
                <button type="submit" class="btn btn-primary">ENVIAR</button>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
                <div>
                  <p>Copyright &copy; 2019 EM2D - Todos os direitos reservados.<br>
                  <a href="https://em2d.com.br/privacy-policy.html" target="_blank">Pol&iacute;tica de Privacidade & Termos de Uso</a></p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>

    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/auth/signup.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
  </body>
</html>