<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile("2,3");

    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $theid = $_REQUEST['id'];

    if ($theid != '0' && $theid != '')
    {
        if(!$retUser = $functions->fGetUser($theid)){
          header('Location: '.SIS_URL.'home');
          exit;  
        }
    }else{
      if($_SESSION['sPersonType'] != 4){
        header('Location: '.SIS_URL.'home');
        exit;      
      }
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Usu&aacute;rio</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-user" class="form-horizontal form-label-left" data-parsley-validate>

                      <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/user" target="_blank"><strong>neste link!</strong></a>
                      </p>
                        <br>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome Completo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" value="<?php echo $retUser[0]['name']; ?>" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" required="required" type="text">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gender">Sexo <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="gender" id="gender" class="form-control" required="required">
                                <option selected value="">Selecione</option>
                                <option <?php echo ($retUser[0]['gender'] == 'male' ? "selected" : ""); ?> value="male">Masculino</option>
                                <option <?php echo ($retUser[0]['gender'] == 'female' ? "selected" : ""); ?> value="female">Feminino</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="borndate">Data de Nascimento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="borndate" value="<?php echo $retUser[0]['borndate']; ?>" class="form-control col-md-7 col-xs-12" name="borndate" required="required" type="date">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="signup">Origem Cadastro <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" value="<?php echo ($retUser[0]['signup'] != '' ? $retUser[0]['signup'] : 'email'); ?>" readonly id="signup" name="signup" required="required" class="form-control col-md-7 col-xs-12">
                          <span class="fa fa-globe form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input  type="text" value="<?php echo $retUser[0]['email']; ?>" <?php echo ($theid == 0 ? '' : 'readonly')?> id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                          <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
                          <ul class="parsley-errors-list" id="email-error" style="display:none">
                            <li class="parsley-required"><br>Email j&aacute; cadastrado!</li>
                          </ul>
                        </div>
                      </div>

                      <?php if($retUser[0]['signup'] == 'email' || $theid == 0){ ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pwd">Senha <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="pwd" name="pwd" data-validate-length="6,8" data-validate-words="1" required="required" class="form-control col-md-7 col-xs-12">
                            <span class="fa fa-lock form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpwd">Confirme a Senha <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="cpwd" name="cpwd" data-parsley-equalto="#pwd" data-validate-length="6,8" data-validate-words="1" required="required" class="form-control col-md-7 col-xs-12">
                            <span class="fa fa-lock form-control-feedback right" aria-hidden="true"> 2</span>
                            </div>
                        </div>
                    <?php } ?>

                        <?php if($_SESSION['sPersonType'] == 4){ ?>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Tipo do Usu&aacute;rio <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select onchange="changeType(this.value);" name="type" id="type" class="form-control" required="required">
                                      <option selected value="">Selecione</option>
                                      <option <?php echo ($retUser[0]['type'] == '1' ? "selected" : ""); ?> value="1">USU&Aacute;RIO COMPRADOR</option>
                                      <option <?php echo ($retUser[0]['type'] == '2' ? "selected" : ""); ?> value="2">USU&Aacute;RIO LOJISTA</option>
                                      <option <?php echo ($retUser[0]['type'] == '3' ? "selected" : ""); ?> value="3">USU&Aacute;RIO LOG&Iacute;STICO</option>
                                </select>
                              </div>
                            </div>  
                            <div class="item form-group cua" <?php echo ($retUser[0]['type'] != '1' ? "style='display:block'" : "style='display:none'"); ?>>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="canuseadmin">Acesso ao Portal Admin? <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="canuseadmin" id="canuseadmin" class="form-control" required="required">
                                      <option <?php echo ($retUser[0]['canuseadmin'] == '0' ? "selected" : ""); ?> value="0">N&Atilde;O</option>
                                      <option <?php echo ($retUser[0]['canuseadmin'] == '1' ? "selected" : ""); ?> value="1">SIM</option>
                                </select>
                            </div>
                            </div>
                            <div class="item form-group cus" <?php echo ($retUser[0]['type'] != '1' && $retUser[0]['type'] != '3' ? "style='display:block'" : "style='display:none'"); ?>>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="canseller">Vendas pelo App? <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="canseller" id="canseller" class="form-control" required="required">
                                      <option <?php echo ($retUser[0]['canseller'] == '0' ? "selected" : ""); ?> value="0">N&Atilde;O</option>
                                      <option <?php echo ($retUser[0]['canseller'] == '1' ? "selected" : ""); ?> value="1">SIM</option>
                                </select>
                            </div>
                      </div>
                      <?php } ?>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="hidden" name="id" id="id" value="<?php echo ($theid != '' ? $theid : '0'); ?>">
                          <button type="button" onclick="location.href='<?php echo SIS_URL; ?>user-list'" class="btn btn-primary">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success"><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Usu&aacute;rio</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/user/user.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>