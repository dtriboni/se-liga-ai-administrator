<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   // Check if this page have permission to access
   $functions->fCheckLevelProfile();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Categorias SeLigaA&iacute;</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Clique na categoria desejada para expandir campos ocultos e exibir mais a&ccedil;&otilde;es
                    </p>

                    <?php $retCategories = $functions->fGetCategories(); ?>
					
                    <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Logo</th>
                          <th>ATIVA?</th>
                          <th>N&ordm; Estabelecimentos</th>
                          <th>A&ccedil;&atilde;o</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php for ($a = 0; $a < count($retCategories); $a++){ 
                        
                        $lblActives = ($retCategories[$a]['active'] == 1 ? "<strong class='btn-success'>EST&Atilde;O</strong>" : "<strong class='btn-danger'>EST&Aacute;VAM</strong>");

                        ?>
                        <tr id="row_<?php echo $retCategories[$a]['catid']; ?>">
                          <td width="200px !important;">
                            <div class="thumbnail" style="width: 200px !important; height: 172px !important;">
                            <div class="image view view-first">
                              <img style="width: 100%; display: block;" src="<?php echo $retCategories[$a]['logo']; ?>" alt="image" />
                              
                            </div>
                            <div class="caption">
                              <p><strong><?php echo $retCategories[$a]['category']; ?></strong></p>
                            </div>
                          </div>
                          </td>
                          <td><?php echo $retCategories[$a]['actived']; ?></td>
                          <td><strong class='btn-<?php echo ($retCategories[$a]['numstores'] == 0 ? "danger" : "warning"); ?>'>&nbsp;&nbsp;<?php echo $retCategories[$a]['numstores']; ?>&nbsp;&nbsp;</strong> estabelecimentos ATIVOS <?php echo $lblActives; ?>  utilizando <?php echo $retCategories[$a]['category']; ?></td>
                          
                          <td>
                              <a href="<?php echo SIS_URL."category/".$retCategories[$a]['catid'] ;?>" title="Alterar Registro"><li class="fa fa-edit"></li></a>
                             
                              <?php  if ($_SESSION['sPersonType'] == 4){ //Only Admin Profile is granted to disable users
                                        if($retCategories[$a]['active'] == 1){ 
                                ?> | 
                                    <a href="javascript:;" onclick="changeRecord(<?php echo $retCategories[$a]['catid']; ?>, 'catid', 'c', 0)" title="Desativar"><li class="fa fa-eye-slash"></li></a>
                                <?php }else{ ?>
                                     | <a href="javascript:;" onclick="changeRecord(<?php echo $retCategories[$a]['catid']; ?>, 'catid', 'c', 1)" title="Ativar"><li class="fa fa-eye"></li></a>
                              <?php } } ?>  

                            </td>
                        </tr>
                      <?php } ?>
                                             
                      </tbody>
                    </table>

                        <?php if($_SESSION['sPersonType'] == 4){ ?>

                            <div class="ln_solid"></div>
                            <div class="col-md-12">
                            <a href="<?php echo SIS_URL;?>category" class="btn btn-primary">Cadastrar Nova Categoria</a>
                            </div>

                        <?php } ?>

                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
	
  </body>
</html>