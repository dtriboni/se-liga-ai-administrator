<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
    $functions = new functions();
   
    $functions->fCheckLevelProfile(3);

    $_REQUEST = $functions->fSanitizeRequest($_GET);

    $theid = $_REQUEST['id'];

    if ($theid != '0' && $theid != ''){
      if (!$retLogistic = $functions->fGetLogistic($theid)){
        header('Location: '.SIS_URL.'home');
		    exit;
      }
    }else{
        if($_SESSION['sPersonType'] != 4){
            header('Location: '.SIS_URL.'home');
            exit;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Entregador</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form method="post" id="form-logistic" class="form-horizontal form-label-left" enctype="multipart/form-data" data-parsley-validate>

                    <p>D&uacute;vidas no preenchimento? Consulte a nossa <code>Documenta&ccedil;&atilde;o T&eacute;cnica</code> dispon&iacute;vel <a href="<?php echo SIS_URL;?>documentation/logistic" target="_blank"><strong>neste link!</strong></a>
                      </p>
                        <br>
                        
                        <?php

                          if ($_SESSION['sPersonType'] == 4){
                            
                            $retUser = $functions->fGetUsers(3);

                            if (is_array($retLogistic))
                            {
                              foreach ($retLogistic as $sanity => $logistic)
                                $logistics[] = $logistic['usrid'];
                            }else{
                                $logistics[] = null;
                            }
                            
                        ?> 

                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usrid">Respons&aacute;vel <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="usrid" id="usrid" class="form-control" required="required">
                              <option selected value="">Selecione o Respons&aacute;vel pelo Estabelecimento</option>
                              <?php for ($u = 0; $u < count($retUser); $u++){ 
                                        if ($retUser[$u]['active'] == 1 ){ ?>
                                <option <?php echo (in_array($retUser[$u]['usrid'], $logistics) ? "selected" : ""); ?> value="<?php echo $retUser[$u]['usrid']; ?>"><?php echo $retUser[$u]['name']; ?> (<?php echo $retUser[$u]['email']; ?>)</option>
                              <?php } } ?>
                          </select>
                        </div>
                      </div>

                      <?php } ?>  

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome Fantasia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" value="<?php echo $retLogistic[0]['name']; ?>" class="form-control col-md-7 col-xs-12" name="name" placeholder="Ex: Empresa Teste" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpfcnpj">CPF/CNPJ <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="cpfcnpj" value="<?php echo $retLogistic[0]['cpfcnpj']; ?>" onblur="formatDoc()" <?php echo ($retLogistic[0]['cpfcnpj'] == '' ? '' : 'readonly'); ?> class="form-control col-md-7 col-xs-12" name="cpfcnpj" placeholder="CPF ou CNPJ" required="required" type="text">
                          <ul class="parsley-errors-list" id="carac-error" style="display:none">
                            <li class="parsley-required"><br>Informe o CPF/CNPJ Corretamente.</li>
                          </ul> 
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Endere&ccedil;o <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="address" autocomplete="cc-number" value="<?php echo $retLogistic[0]['address']; ?>" class="form-control col-md-7 col-xs-12" name="address" placeholder="Ex: Rua Adriana de Oliveira Silva" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">N&uacute;mero <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="number" value="<?php echo $retLogistic[0]['number']; ?>" class="form-control col-md-7 col-xs-12" name="number" placeholder="10, 100, 1000" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="complement">Complemento
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="complement" value="<?php echo $retLogistic[0]['complement']; ?>" class="form-control col-md-7 col-xs-12" name="complement" placeholder="Ex: CJ 10, Sala 156" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="neighboor">Bairro <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" value="<?php echo $retLogistic[0]['neighboor']; ?>" id="neighboor" name="neighboor" required="required" placeholder="Vila Mariana" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zipcode">CEP <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="zipcode" value="<?php echo $retLogistic[0]['zipcode']; ?>" data-inputmask="'mask' : '99999-999'" class="form-control col-md-7 col-xs-12" name="zipcode" placeholder="01539-010" required="required" type="text">
                            <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm" target="_blank">N&atilde;o sei meu CEP</a>
                        </div>
                      </div>  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">Cidade <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="city" name="city"  value="<?php echo $retLogistic[0]['city']; ?>" placeholder="S&atilde;o Paulo" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">UF <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <input type="text" id="state" name="state"  value="<?php echo $retLogistic[0]['state']; ?>" required="required" placeholder="SP" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tel">Telefone Fixo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tel" value="<?php echo $retLogistic[0]['tel']; ?>" class="form-control col-md-7 col-xs-12" name="tel" placeholder="(11) 2345-0000" type="text" required="required">
                          <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">WhatsApp <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="whatsapp" value="<?php echo $retLogistic[0]['whatsapp']; ?>" class="form-control col-md-7 col-xs-12" name="whatsapp" placeholder="(11) 98765-0000" required="required" type="text">
                          <span class="fa fa-whatsapp form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone">Celular <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="cellphone" value="<?php echo $retLogistic[0]['cellphone']; ?>" class="form-control col-md-7 col-xs-12" name="cellphone" required="required" placeholder="(11) 98765-0000" type="text">
                          <span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >&nbsp;</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <h5>Dados Banc&aacute;rios para Repasse</h5>
                          Informe os dados banc&aacute;rios do <code>titular do CPF/CNPJ</code> para que a EM2D efetue os repasses mensais.
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bank">Banco <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                        <select name="bank" id="bank" class="form-control" required="required">
                              <option selected value="">Selecione o Banco</option>
                              <?php 
                              $retBank = $functions->fGetBanks();
                              for ($b = 0; $b < count($retBank); $b++){ ?>
                                <option <?php echo ($retLogistic[0]['bank'] == $retBank[$b]['bankname'] ? "selected" : ""); ?> value="<?php echo $retBank[$b]['bankname']; ?>"><?php echo $retBank[$b]['bankname']; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="agency">Ag&ecirc;ncia <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="agency" value="<?php echo $retLogistic[0]['agency']; ?>" data-inputmask="'mask' : '9999-9'" class="form-control col-md-7 col-xs-12" name="agency" placeholder="Ex: 9999-1" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="account">Conta <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="account" value="<?php echo $retLogistic[0]['account']; ?>"  class="form-control col-md-7 col-xs-12" name="account" placeholder="Ex: 12345-8" required="required" type="number">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="owner">Nome do Titular (Correntista) <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input id="owner" value="<?php echo $retLogistic[0]['owner']; ?>" class="form-control col-md-7 col-xs-12" name="owner" placeholder="Digite seu Nome Completo" required="required" type="text">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="hidden" name="latitude" id="latitude" value="<?php echo $retLogistic[0]['latitude']; ?>">
                            <input type="hidden" name="longitude" id="longitude" value="<?php echo $retLogistic[0]['longitude']; ?>">
                            <input type="hidden" name="id" id="id" value="<?php echo ($theid != '' ? $theid : '0'); ?>">
                            <button type="button" onclick="location.href='<?php echo SIS_URL; ?>logistic-list'" class="btn btn-primary">Cancelar</button>
                            <button id="send" type="submit" class="btn btn-success"><?php echo ($theid != '' ? 'Alterar' : 'Incluir'); ?> Entregador</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>build/js/logistic/logistic.js"></script> 
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGERYfSVlukcIsjXqXvfB86P9QmKW50TI&libraries=places&callback=initAutocomplete"></script>

  </body>
</html>