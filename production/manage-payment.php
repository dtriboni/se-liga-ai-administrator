<?php

	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";
	
	session_start2();
    
   $functions = new functions();  

   $functions->fCheckLevelProfile("2,3");

   $_REQUEST = $functions->fSanitizeRequest($_GET);

   $retOrders = $functions->fGetOrders(true, $_GET['period']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SIS_TITULO; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SIS_URL; ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SIS_URL; ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo SIS_URL; ?>build/css/custom.css" rel="stylesheet">

    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    
    <link href="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="loading"></div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="clearfix"></div>

            <?php require("../_requires/menu.php"); ?>

          
          </div>
        </div>

        <?php require("../_requires/header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Listar <?php echo ($_SESSION['sPersonType'] == 3 ? "Entregas": "Pedidos"); ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Clique no pedido desejado para expandir campos ocultos e exibir mais a&ccedil;&otilde;es
                    </p>
					
                    <table id="table-list-records" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Pedido</th>
                          <th>Data</th>
                          <th>Comprador</th>
                          <th>Valor Total</th>
                          <th>Frete</th>
                          <th>Pagamento</th>
                          <th>Status</th>
                          <th>Repasse</th>
                          <th>A&ccedil;&atilde;o</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php for ($a = 0; $a < count($retOrders); $a++){ ?>

                        <tr id="row_<?php echo $retOrders[$a]['ordid']; ?>">
                          <td><?php echo $retOrders[$a]['ord_alias']; ?></td>
                          <td><?php echo $retOrders[$a]['date']; ?></td>
                          <td><?php echo $retOrders[$a]['buyer']; ?></td>
                          <td><strong>R$ <?php echo number_format($retOrders[$a]['total'], 2, ",", "."); ?></strong></td>
                          <td><strong>R$ <?php echo number_format($retOrders[$a]['shipping'], 2, ",", "."); ?></strong></td>
                          <td><strong class='btn-<?php echo ($retOrders[$a]['paid'] != 'PAGO' ? "danger" : "success"); ?>'>&nbsp;&nbsp;<?php echo $retOrders[$a]['paid']; ?>&nbsp;&nbsp;</strong></td>
                          <td><strong class='btn-<?php echo ($retOrders[$a]['delivery'] != 'ENTREGUE' ? "danger" : "success"); ?>'>&nbsp;&nbsp;<?php echo $retOrders[$a]['delivery']; ?>&nbsp;&nbsp;</strong></td>
                          <td> <?php 
                                  if ($retOrders[$a]['refunded'] == 1){ ?>
                                    <strong class='btn-success'>&nbsp;&nbsp;REPASSE EFETUADO&nbsp;&nbsp;</strong>
                                  <?php }else{ ?>
                                    <strong class='btn-danger'>&nbsp;&nbsp;AGUARDANDO FECHAMENTO&nbsp;&nbsp;</strong>
                                  <?php } ?>
                          </td>      
                          <td>
                            <a href="<?php echo SIS_URL."order/".$retOrders[$a]['ordid'] ;?>" title="Detalhes do Pedido"><li class="fa fa-list-ul"></li></a> 
                            | <a href="javascript:;" onclick="changeRecord(<?php echo $retOrders[$a]['plaid']; ?>, 'plaid', 'p', 0)" title="Consultar Log&iacute;stica"><li class="fa fa-truck"></li></a>
                           
                          </td>
                        </tr>
                      <?php } ?>
                                             
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php require("../_requires/footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo SIS_URL; ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SIS_URL; ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo SIS_URL; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SIS_URL; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SIS_URL; ?>build/js/custom.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo SIS_URL; ?>vendors/pnotify/dist/pnotify.nonblock.js"></script>
                        <script>check4NewOrders();</script>
  </body>
</html>