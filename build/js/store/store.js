$(function () {
	
	$('#form-store').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		var validCPFCNPJ = formatDoc();
		if (ok && validCPFCNPJ){
			$('.loading').show();
			var myForm = document.getElementById('form-store');
			var formData = new FormData(myForm);
			var id = formData.getAll('id');
			$.ajax({
				url: (id == 0 ? "" : "../") + "store-save",
				type: 'POST',
				data: formData,
	        success: function (data) {
						var json = $.parseJSON(data);
						$('.loading').hide();
						if(json.ret == true){
							$DROP_ZONE.on('sending', function(file, xhr, formData){
								formData.append('strid', json.strid);
							});
							$DROP_ZONE.processQueue(); 
							$("#send").attr("disabled", true);
							setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "store-list"; }, 3000);
						}							
						return new PNotify({
							title: 'Aten&ccedil;&atilde;o',
							type: json.type,
							text: json.msg,
							nonblock: {
									nonblock: true
							},
							styling: 'bootstrap3',
							addclass: json.type
						}); 
								
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
    }); 
	
	formatDoc = function(){
		var doc = $('#cpfcnpj').val();
		if(!consiste_cpfcgc_sem_mascara(doc.replace(/\D/g,''))){
			$('#cpfcnpj').addClass('parsley-error');
			$('#carac-error').show();
			return false;
		}else{
			$('#cpfcnpj').removeClass('parsley-error');
			$('#carac-error').hide();
			return true;
		}
	}

	consiste_cpfcgc_sem_mascara = function(Param) {
		tmp = Param;
		if (tmp.length <= 11) {
			x = verifica_cpf(tmp);
			if (!x) {
				return false;
			}
		}
		else {
			x = verifica_cgc(tmp);
			if (!x) {
				return false;
			}
		}
		return true;
	}
	
	//Verifica se o número de CPF informado é válido
	 verifica_cpf = function(sequencia) {
		if (Procura_Str(1,sequencia,'00000000000,11111111111,22222222222,33333333333,44444444444,55555555555,66666666666,77777777777,88888888888,99999999999,00000000191,19100000000') > 0 ) {
			return false;
		}
		seq = sequencia;
		soma = 0;
		multiplicador = 2;
		for (f = seq.length - 3;f >= 0;f--) {
			soma += seq.substring(f,f + 1) * multiplicador;
			multiplicador++;
		}
		resto = soma % 11;
		if (resto == 1 || resto == 0) {
			digito = 0;
		}
		else {
			digito = 11 - resto;
		}
		if (digito != seq.substring(seq.length - 2,seq.length - 1)) {
			return false;
		}
		soma = 0;
		multiplicador = 2;
		for (f = seq.length - 2;f >= 0;f--) {
			soma += seq.substring(f,f + 1) * multiplicador;
			multiplicador++;
		}
		resto = soma % 11;
		if (resto == 1 || resto == 0) {
			digito = 0;
		}
		else {
			digito = 11 - resto;
		}
		if (digito != seq.substring(seq.length - 1,seq.length)) {
			return false;
		}
		return true;
	}
	
	//Verifica se o número de CGC informado é válido
	verifica_cgc = function(sequencia) {
		seq = sequencia;
		soma = 0;
		multiplicador = 2;
		for (f = seq.length - 3;f >= 0;f-- ) {
			soma += seq.substring(f,f + 1) * multiplicador;
			if ( multiplicador < 9 ) {
				multiplicador++;
			}
			else {
				multiplicador = 2;
			}
		}
		resto = soma % 11;
		if (resto == 1 || resto == 0) {
			digito = 0;
		}
		else {
			digito = 11 - resto;
		}
		if (digito != seq.substring(seq.length - 2,seq.length - 1)) {
			return false;
		}
	
		soma = 0;
		multiplicador = 2;
		for (f = seq.length - 2;f >= 0;f--) {
			soma += seq.substring(f,f + 1) * multiplicador;
			if (multiplicador < 9) {
				multiplicador++;
			}
			else {
				multiplicador = 2;
			}
		}
		resto = soma % 11;
		if (resto == 1 || resto == 0) {
			digito = 0;
		}
		else {
			digito = 11 - resto;
		}
		if (digito != seq.substring(seq.length - 1,seq.length)) {
			return false;
		}
		return true;
	}


	Procura_Str = function(param0,param1,param2) {
		for (a = param0 - 1;a < param1.length;a++) {
			for (b = 1;b < param1.length;b++) {
				if (param2 == param1.substring(b - 1,b + param2.length - 1)) {
					return a;
				}
			}
		}
		return 0;
	}
   
});

$("input[id*='cpfcnpj']").inputmask({
  mask: ['999.999.999-99', '99.999.999/9999-99'],
  keepStatic: true
});


