$(function () {

	$('#form-taxlog').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-taxlog');
			var formData = new FormData(myForm);
			$.ajax({
				url: "taxlog-save",
				type: 'POST',
				data: formData,
	        success: function (data) {
						var json = $.parseJSON(data);
						$('.loading').hide();
						if(json.ret == true){
							$("#send").attr("disabled", true);
							setTimeout(function(){ location.href = "home"; }, 3000);
						}
						return new PNotify({
							title: 'Aten&ccedil;&atilde;o',
							type: json.type,
							text: json.msg,
							nonblock: {
									nonblock: true
							},
							styling: 'bootstrap3',
							addclass: json.type
						}); 
								
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
    }); 
   
});



