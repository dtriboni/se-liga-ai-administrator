$(function () {

	$('#form-payment').parsley().on('form:validated', function() {
    var ok = $('.parsley-error').length === 0;
    if ($PAYMENT_METHOD == 2 || $PAYMENT_METHOD == 3){
        ok = true
    }
		if (ok){

			var myForm = document.getElementById('form-payment');
      var formData = new FormData(myForm);

      formData.append("type", $PAYMENT_METHOD);
      formData.append("sh", PagSeguroDirectPayment.getSenderHash());

      var cardnum = $('#cardnum').val();
      var cvv       = $('#cvv').val();
      var validated  = $('#validated').val();
      var valids    = validated;
      var vs = valids.split('/', 2);

      if ($PAYMENT_METHOD == 1){
        PagSeguroDirectPayment.createCardToken({
          
          cardNumber:       cardnum.replace(/\./g,''),
          cvv:              cvv,
          expirationMonth:  vs[0],
          expirationYear:   vs[1],
          brand:            $PS_BRAND,

          success: response => {

            $PAYMENT_PROCCESSING = true;
            $('.loading').show();
            $('#wizard').smartWizard("goToStep", 3); 
            $('.buttonPrevious').addClass('disabled'); 
            $('.buttonNext').addClass('disabled');

            formData.append("ct", response.card.token);
            
            $.ajax({
              url: "payment-proccess",
              type: 'POST',
              data: formData,
                success: function (data) {
                  var json = $.parseJSON(data);

                  if (json.ret == true){
                    $PAYMENT_FINISHED = true;
                    $PAYMENT_PROCCESSING = false;
                    $('.buttonPrevious').addClass('disabled'); 
                    $('.buttonNext').removeClass('disabled');
                    $('#retpag').html('<li class="fa fa-check text-success"></li> ' + json.msg);
                    $('.loading').hide();
                  }else{
                    $PAYMENT_FINISHED = false;
                    $PAYMENT_PROCCESSING = false;
                    $('#cardname').val('');
                    $('#doc').val('');
                    $('#cardnum').val('');
                    $('#cvv').val('');
                    $('#validated').val('');
                    $('#installments').val('');
                    $('#installments').attr('disabled', true);
                    $('.buttonNext').addClass('disabled');
                    $('.buttonFinish').addClass('disabled');
                    $('.buttonPrevious').removeClass('disabled'); 
                    $('#retpag').html('<li class="fa fa-times text-danger"></li> ' + json.msg);
                    $('.loading').hide();
                  }
                  			
                },
                  cache: false,
                  contentType: false,
                  processData: false
            });	

          },error: response => { 
        
              alert("Não foi possível processar seu pagamento. Por favor, tente mais tarde!\r\n\r\nCOD: CT NULL");
          }
        });
      }else{

        $PAYMENT_PROCCESSING = true;
        $('.loading').show();
        $('#wizard').smartWizard("goToStep", 3); 
        $('.buttonPrevious').addClass('disabled'); 
        $('.buttonNext').addClass('disabled');

        $.ajax({
          url: "payment-proccess",
          type: 'POST',
          data: formData,
            success: function (data) {
              var json = $.parseJSON(data);

              if (json.ret == true){
                $PAYMENT_FINISHED = true;
                $PAYMENT_PROCCESSING = false;
                $('.buttonPrevious').addClass('disabled'); 
                $('.buttonNext').removeClass('disabled');
                $('#retpag').html('<li class="fa fa-check text-success"></li> ' + json.msg);
                $('.loading').hide();
              }else{
                $PAYMENT_FINISHED = false;
                $PAYMENT_PROCCESSING = false;
                $('#cardname').val('');
                $('#doc').val('');
                $('#cardnum').val('');
                $('#cvv').val('');
                $('#validated').val('');
                $('#installments').val('');
                $('#installments').attr('disabled', true);
                $('.buttonNext').addClass('disabled');
                //$('.buttonFinish').addClass('disabled');
                $('.buttonPrevious').removeClass('disabled'); 
                $('#retpag').html('<li class="fa fa-times text-danger"></li> ' + json.msg);
                $('.loading').hide();
              }
                    
            },
              cache: false,
              contentType: false,
              processData: false
        });	
      }
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
    }); 
});

function getSession(){
	$.ajax({
		url: "session",
		type: 'GET',
		data: null,
		success: function (data) {
			var json = $.parseJSON(data);
			$PS_SESSION = json.session_id;
		},
		cache: false,
		contentType: false,
		processData: false
	});	
}

// RETORNA OS MEIOS DE PAGAMENTO DISPONÍVEIS NA CONTA PARA EXIBIÇÃO NO CHECKOUT
  // BUSCA A BANDEIRA DO CARTÃO (EX: VISA, MASTERCARD ETC...) E DEPOIS BUSCA AS PARCELAS;
  // ESTA FUNÇÃO É CHAMADA QUANDO O INPUT QUE RECEBE O NÚMERO DO CARTÃO PERDE O FOCO;
 function buscaBandeira(value, parcelas, numcard) { 
	console.log($PS_SESSION);
    PagSeguroDirectPayment.setSessionId($PS_SESSION);
    PagSeguroDirectPayment.getBrand({
      cardBin: numcard.replace(/\./g,''),
      success: response => {
        $PS_BRAND = response.brand.name;
        buscaParcelas(value, parcelas); 
      },
      error: response => { 
        
	   alert(JSON.stringify(response));
	   
      }
    });
  }

  // VERIFICA QUAL BANDEIRA FOI INFORMADA PELO CLIENTE AO DIGITAR OS DADOS DO CARTÃO E RETORNA AS 
  // PARCELAS DISPONPIVEIS E VAI BUSCAR AS PARCELAS NA API DO PAGSEGURO PARA O CLIENTE ESCOLHER  
  function buscaParcelas(value, parcelas) { 
    PagSeguroDirectPayment.getInstallments({
      amount: value,                //valor total da compra (deve ser informado)
      brand: $PS_BRAND,   //bandeira do cartão (capturado na função buscaBandeira)
      maxInstallmentNoInterest: (parcelas == 1 ? 2 : parcelas),  //quantidade de parcelas sem juros
      success: response => {
        var numParc = [];
        for (var p = 0; p < response.installments[$PS_BRAND].length; p++){
          if (parcelas >= response.installments[$PS_BRAND][p].quantity){
            numParc[p] = response.installments[$PS_BRAND][p];
          }
        }
		var dynamicSelect = document.getElementById("installments");
        numParc.forEach(function(item){ 
                var newOption = document.createElement("option");
				newOption.text = item.quantity + "x de R$ " + currencyFormat(item.installmentAmount).replace(".", ",");
				newOption.value = item.quantity + "|" + item.installmentAmount;
                dynamicSelect.add(newOption);
		});
		dynamicSelect.removeAttribute("disabled");
      },
      error: response => { 
		alert(JSON.stringify(response));
	  }
    });
  }

  function currencyFormat(num) {
	return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  //const fullNumber = '2034399002125581';
//const last4Digits = fullNumber.slice(-4);
//const maskedNumber = last4Digits.padStart(fullNumber.length, '*');

//console.log(maskedNumber);
// expected output: "************5581"