$(function () {

	$('#form-offer').parsley().on('form:validated', function() {
        var ok = $('.parsley-error').length === 0;
        var table = $('#table-list-records').DataTable();
        if ($('#type').val() == 1 || $('#type').val() == ''){
            if (!table.data().count()){
                $('#carac-error').show();
                ok = false;
            }else{
                $('#carac-error').hide();
            }
        }
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-offer');
			var formData = new FormData(myForm);
			var id = formData.getAll('id');
			$.ajax({
				url: (id == 0 ? "" : "../") + "offer-save",
				type: 'POST',
				data: formData,
	        success: function (data) {
                var json = $.parseJSON(data);
                var $isUploadStarted = false;
                //$('.loading').hide();
                if(json.ret == true){

                    $DROP_ZONE.on('sending', function(file, xhr, formData){
                        $isUploadStarted = true;
                        formData.append('ofeid', json.ofeid);
                    });

                    $DROP_ZONE.processQueue(); 
                    $("#send").attr("disabled", true);

                    $DROP_ZONE.on('complete', function(file, xhr, formData){
                        $('.loading').hide();
                        setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "offers-list"; }, 3000);
                    });

                    if (!$isUploadStarted){
                        $('.loading').hide();
                        setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "offers-list"; }, 3000);
                    }
                    
                }else{
                    if (json.date == 'incorrect'){
                        $("#ends").val("");
                    }
                }   
                return new PNotify({
                    title: 'Aten&ccedil;&atilde;o',
                    type: json.type,
                    text: json.msg,
                    nonblock: {
                            nonblock: true
                    },
                    styling: 'bootstrap3',
                    addclass: json.type
                }); 
								
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
    }); 
    
   
});