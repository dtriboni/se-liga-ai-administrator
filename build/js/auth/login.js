$(function () {
	
	$('#form-signin-page').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-signin-page');
			var formData = new FormData(myForm);
			$.ajax({
	        url: "signin",
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	        	var json = $.parseJSON(data);
	        	if(json.ret == true){
	        		location.href = json.url;
				}else{
					$('.loading').hide();
					return new PNotify({
						title: 'Aten&ccedil;&atilde;o',
						type: 'error',
						text: json.msg,
						nonblock: {
								nonblock: true
						},
						styling: 'bootstrap3',
						addclass: 'error'
					});       	
	        	}
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
	});

	$('#form-password').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-password');
			var formData = new FormData(myForm);
			$.ajax({
	        url: "remember",
	        type: 'POST',
	        data: formData,
	        success: function (data) {

				$('.loading').hide();
	        	var json = $.parseJSON(data);
					return new PNotify({
							title: 'Aten&ccedil;&atilde;o',
							type: json.status,
							text: json.msg,
							nonblock: {
									nonblock: true
							},
							styling: 'bootstrap3',
							addclass: json.status
					});       		
	        },
				cache: false,
				contentType: false,
				processData: false
			});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
	});
	
});
