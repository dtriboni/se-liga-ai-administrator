$(function () {

	$('#form-retrieve').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-retrieve');
			var formData = new FormData(myForm);
			$.ajax({
	        url: "../../_actions/retrieve.php",
	        type: 'POST',
	        data: formData,
	        success: function (data) {
				
				$('.loading').hide();
				var json = $.parseJSON(data);
				
				if(json.ret == true){
					setTimeout(function(){ 
						location.href = json.url;
					}, 4000);
				}

				return new PNotify({
					title: 'Aten&ccedil;&atilde;o',
					type: json.type,
					text: json.msg,
					nonblock: {
							nonblock: true
					},
					styling: 'bootstrap3',
					addclass: json.type
				}); 
					
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
	});

});
