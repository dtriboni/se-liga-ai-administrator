var $DROP_ZONE;
var myForm = document.getElementById('form-offer');
var formData = new FormData(myForm);
var id = formData.getAll('id');

var modalTemplate = '' + 
			'<div class="modal fade" tabindex="-1" role="dialog">' + 
				'<div class="modal-dialog" role="document">' + 
					'<div class="modal-content">' + 
						'<div class="modal-header">' + 
							'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + 
							'<h4 class="modal-title">Ajustar Imagem</h4>' + 
						'</div>' + 						
						'<div class="modal-body">' + 
							'<div class="image-container"></div>' + 
						'</div>' + 						
						'<div class="modal-footer">' + 
							'<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>' + 
							'<button type="button" class="btn btn-primary crop-upload">OK</button>' + 
						'</div>' + 
					'</div>' + 
				'</div>' + 
			'</div>' + 
    '';
    
    function dataURItoBlob(dataURI) {
			var byteString = atob(dataURI.split(',')[1]);
			var ab = new ArrayBuffer(byteString.length);
			var ia = new Uint8Array(ab);
			for (var i = 0; i < byteString.length; i++) {
				ia[i] = byteString.charCodeAt(i);
			}
			return new Blob([ab], { type: 'image/jpeg' });
		}

$(document).ready(function () 
{
    Dropzone.autoDiscover = false;
    $("#myDropzone").dropzone({
      url: (id == 0 ? "../" : "../../") + "upload_a",
      dictInvalidFileType: "Formato de arquivo incorreto.",
      dictRemoveFileConfirmation: "Deseja remover esta imagem?",
      dictResponseError : "Arquivo muito grande",
      autoProcessQueue: false,
      uploadMultiple: true,
      parallelUploads: 6,
      maxFiles: 6,
      maxFilesize: 2,
      acceptedFiles: 'image/*',
      addRemoveLinks: true,
      init: function () {
          thisDropzone = this;
          $DROP_ZONE = thisDropzone;
          var child = 1;
          $.each($ADMIN_UPLOADED_FILES, function (key, value) {
              var mockFile = { name: value.name, 
                                url: value.url, 
                                directory: value.directory };
              thisDropzone.emit("addedfile", mockFile);
              var _refS;
              _refS = thisDropzone.previewsContainer;
              _refS.children[child].children[0].children[0].src = value.url;
              thisDropzone.emit("complete", mockFile);
              child++;
          });
      },
      thumbnail: function (file) {
        if (file.cropped) {
            return;
        }
        var cachedFilename = file.name;
        var $cropperModal = $(modalTemplate);
        var $uploadCrop = $cropperModal.find('.crop-upload');
        var $img = $('<img />');
        var reader = new FileReader();
        reader.onloadend = function () {
            $cropperModal.find('.image-container').html($img);
            $img.attr('src', reader.result);
            $img.attr('width', '570px');
            $img.cropper({
                preview: '.image-preview',
                aspectRatio: 16 / 9,
                autoCropArea: 1,
                movable: false,
                cropBoxResizable: true,
                minContainerWidth: 570,
                minContainerHeight: 320
            });
        };
        reader.readAsDataURL(file);
        $cropperModal.modal('show');
        $uploadCrop.on('click', function() {
            $DROP_ZONE.removeFile(file);    
            var blob = $img.cropper('getCroppedCanvas').toDataURL();
            var newFile = dataURItoBlob(blob);
            newFile.cropped = true;
            newFile.name = cachedFilename;
            $DROP_ZONE.addFile(newFile);
            var _ref;
            _ref = newFile.previewElement;
            _ref.childNodes[1].children[0].src = blob;
            $cropperModal.modal('hide');
        });
      },
      removedfile: function(file) {
        var phoid = file.name;
        var url = file.url;
        var dir = file.directory;
        $.ajax({
          type: 'POST',
          url: (id == 0 ? "../" : "../../") + 'remove',
          data: {phoid: phoid, url: url, dir: dir},
          sucess: function(data){
            console.log('success to removed file');
          }
        });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      },
      success: function (file, response) {
          var imgName = response;
          file.previewElement.classList.add("dz-success");
      },
      error: function (file, response) {
          file.previewElement.classList.add("dz-error");
          $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
      }
  });
});