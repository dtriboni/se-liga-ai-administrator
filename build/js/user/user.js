$(function () {

	$('#form-user').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			postForm();	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
	});

	postForm = function(){
		var myForm = document.getElementById('form-user');
			var formData = new FormData(myForm);
			var id = formData.getAll('id');
			$.ajax({
				url: (id == 0 ? "" : "../") + "user-save",
				type: 'POST',
				data: formData,
	        success: function (data) {

						var json = $.parseJSON(data);
						$('.loading').hide();

						if(json.ret == true){
							$("#send").attr("disabled", true);
							$('#email').removeClass('parsley-error');
							$('#email-error').hide();
							setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "user-list"; }, 3000);
						}else{
							$('#email').addClass('parsley-error');
							$('#email-error').show();
						}
							
						return new PNotify({
							title: 'Aten&ccedil;&atilde;o',
							type: json.type,
							text: json.msg,
							nonblock: {
									nonblock: true
							},
							styling: 'bootstrap3',
							addclass: json.type
						}); 
								
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});
		} 
});


function changeType(id){
	if (id != 1 && id != 3){
		$('.cua').show();
		$('.cus').show();
	}else{
		if (id == 3){
			$('.cua').show();
			$('.cus').hide();
		}else{
			$('.cua').hide();
			$('.cus').hide();
		}	
	}
	alert("ATENÇÃO: Alternar entre tipos de usuário irá impactar nas seguintes consequências:\r\n\r\n" +
				"USUÁRIO LOJISTA > COMPRADOR ou LOGÍSTICA\r\n" + 
				"Será excluído o perfil para gerenciamento da loja existente, bem como das ofertas, promoções, pedidos e planos contratados!\r\n\r\n" + 
				"USUÁRIO LOGÍSTICA > COMPRADOR ou LOJISTA\r\n" + 
				"Será excluído o perfil para gerenciamento da transportadora existente, bem como das taxas aplicadas, pedidos e planos contratados.");
}