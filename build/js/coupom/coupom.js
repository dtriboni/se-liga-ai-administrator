$(function () {

	$('#form-coupom').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-coupom');
            var formData = new FormData(myForm);
            var id = formData.getAll('id');
			$.ajax({
				url: (id == 0 ? "" : "../") + "coupom-save",
				type: 'POST',
				data: formData,
	        success: function (data) {
						var json = $.parseJSON(data);
						$('.loading').hide();
						if(json.ret == true){
							$("#send").attr("disabled", true);
							setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "coupons-list"; }, 3000);
						}
						return new PNotify({
							title: 'Aten&ccedil;&atilde;o',
							type: json.type,
							text: json.msg,
							nonblock: {
									nonblock: true
							},
							styling: 'bootstrap3',
							addclass: json.type
						}); 
								
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
    }); 

    fChangeCoupom = function(e){
        e.preventDefault();
        var texto = $('#code').val(); 
        var myForm = document.getElementById('form-coupom');
        var formData = new FormData(myForm);
        var id = formData.getAll('id');
        if(texto.length == 0){
            alert('Informe um código para gerar um QR Code.');
            return(false);
        }
        $('img').attr('src', (id == 0 ? "" : "../") + 'qrcode/'+texto);
    }
   
});



