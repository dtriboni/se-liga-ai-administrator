var placeSearch, autocomplete;

$(function () {

	$('#form-address').parsley().on('form:validated', function() {
		var ok = $('.parsley-error').length === 0;
		if (ok){
			$('.loading').show();
			var myForm = document.getElementById('form-address');
			var formData = new FormData(myForm);
			var id = formData.getAll('id');
			$.ajax({
				url: (id == 0 ? "" : "../") + "address-save",
				type: 'POST',
				data: formData,
	        success: function (data) {

						var json = $.parseJSON(data);
						var $isUploadStarted = false;

						if(json.ret == true){

							$DROP_ZONE.on('sending', function(file, xhr, formData){
								$isUploadStarted = true;
								formData.append('addid', json.addid);
							});

							$DROP_ZONE.processQueue(); 
							$("#send").attr("disabled", true);

							$DROP_ZONE.on('complete', function(file, xhr, formData){
									$('.loading').hide();
									setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "address-list"; }, 3000);
							});

							if (!$isUploadStarted){
									$('.loading').hide();
									setTimeout(function(){ location.href = (id == 0 ? "" : "../") + "address-list"; }, 3000);
							}

						}
							
						return new PNotify({
							title: 'Aten&ccedil;&atilde;o',
							type: json.type,
							text: json.msg,
							nonblock: {
									nonblock: true
							},
							styling: 'bootstrap3',
							addclass: json.type
						}); 
								
	        },
	        cache: false,
	        contentType: false,
	        processData: false
				});	
		}	
	})
	.on('form:submit', function() {
	  return false; // Don't submit form for this demo
    }); 
    
   
});

 function initAutocomplete() {
	autocomplete = new google.maps.places.Autocomplete(
			document.getElementById('address'), {types: ['address'], 
			componentRestrictions: {country: 'BR'}});
	autocomplete.addListener('place_changed', fillInAddress);
}

 function fillInAddress() {
	// Get the place details from the autocomplete object.
	var place = autocomplete.getPlace();
	$("#address").val('');
	$("#number").val('');
	$("#complement").val('');
	$("#neighboor").val('');
	$("#city").val('');
	$("#state").val('');
	$("#zipcode").val('');
	for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (addressType == "postal_code") {
			$("#zipcode").val(place.address_components[i].long_name);
		}
		if (addressType == "street_number") {
			$("#number").val(place.address_components[i].long_name);
		}
		if (addressType == "administrative_area_level_1") {
			$("#state").val(place.address_components[i].short_name);
		}
		if (addressType == "administrative_area_level_2") {
			$("#city").val(place.address_components[i].long_name);
		}
		if (addressType == "sublocality_level_1") {
			$("#neighboor").val(place.address_components[i].long_name);
		}
		if (addressType == "route") {
			$("#address").val(place.address_components[i].long_name);
    }
  }
	$("#latitude").val(place.geometry.location.lat());
	$("#longitude").val(place.geometry.location.lng());
	$("#number").focus();
}

$("input[id*='whatsapp']").inputmask({
  mask: ['(99) 9999-9999', '(99) 99999-9999'],
  keepStatic: true
});
$("input[id*='tel1']").inputmask({
  mask: ['(99) 9999-9999', '(99) 99999-9999'],
  keepStatic: true
});
$("input[id*='tel2']").inputmask({
  mask: ['(99) 9999-9999', '(99) 99999-9999'],
  keepStatic: true
});
