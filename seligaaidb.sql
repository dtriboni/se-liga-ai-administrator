-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 02-Abr-2019 às 17:48
-- Versão do servidor: 8.0.15
-- versão do PHP: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seligaaidb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banks`
--

DROP TABLE IF EXISTS `banks`;
CREATE TABLE IF NOT EXISTS `banks` (
  `banid` int(11) NOT NULL,
  `bankname` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE KEY `PK_BANID` (`banid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `banks`
--

INSERT INTO `banks` (`banid`, `bankname`) VALUES
(1, '001 - BANCO DO BRASIL S/A'),
(2, '002 - BANCO CENTRAL DO BRASIL'),
(3, '003 - BANCO DA AMAZONIA S.A'),
(4, '004 - BANCO DO NORDESTE DO BRASIL S.A'),
(7, '007 - BANCO NAC DESENV. ECO. SOCIAL S.A'),
(8, '008 - BANCO MERIDIONAL DO BRASIL'),
(20, '020 - BANCO DO ESTADO DE ALAGOAS S.A'),
(21, '021 - BANCO DO ESTADO DO ESPIRITO SANTO S.A'),
(22, '022 - BANCO DE CREDITO REAL DE MINAS GERAIS SA'),
(24, '024 - BANCO DO ESTADO DE PERNAMBUCO'),
(25, '025 - BANCO ALFA S/A'),
(26, '026 - BANCO DO ESTADO DO ACRE S.A'),
(27, '027 - BANCO DO ESTADO DE SANTA CATARINA S.A'),
(28, '028 - BANCO DO ESTADO DA BAHIA S.A'),
(29, '029 - BANCO DO ESTADO DO RIO DE JANEIRO S.A'),
(30, '030 - BANCO DO ESTADO DA PARAIBA S.A'),
(31, '031 - BANCO DO ESTADO DE GOIAS S.A'),
(32, '032 - BANCO DO ESTADO DO MATO GROSSO S.A.'),
(33, '033 - BANCO DO ESTADO DE SAO PAULO S.A'),
(34, '034 - BANCO DO ESADO DO AMAZONAS S.A'),
(35, '035 - BANCO DO ESTADO DO CEARA S.A'),
(36, '036 - BANCO DO ESTADO DO MARANHAO S.A'),
(37, '037 - BANCO DO ESTADO DO PARA S.A'),
(38, '038 - BANCO DO ESTADO DO PARANA S.A'),
(39, '039 - BANCO DO ESTADO DO PIAUI S.A'),
(41, '041 - BANCO DO ESTADO DO RIO GRANDE DO SUL S.A'),
(47, '047 - BANCO DO ESTADO DE SERGIPE S.A'),
(48, '048 - BANCO DO ESTADO DE MINAS GERAIS S.A'),
(59, '059 - BANCO DO ESTADO DE RONDONIA S.A'),
(70, '070 - BANCO DE BRASILIA S.A'),
(77, '077 - BANCO INTER S.A.'),
(104, '104 - CAIXA ECONOMICA FEDERAL'),
(106, '106 - BANCO ITABANCO S.A.'),
(107, '107 - BANCO BBM S.A'),
(109, '109 - BANCO CREDIBANCO S.A'),
(116, '116 - BANCO B.N.L DO BRASIL S.A'),
(121, '121 - BANCO AGIBANK S.A.'),
(148, '148 - MULTI BANCO S.A'),
(151, '151 - CAIXA ECONOMICA DO ESTADO DE SAO PAULO'),
(153, '153 - CAIXA ECONOMICA DO ESTADO DO R.G.SUL'),
(165, '165 - BANCO NORCHEM S.A'),
(166, '166 - BANCO INTER-ATLANTICO S.A'),
(168, '168 - BANCO C.C.F. BRASIL S.A'),
(175, '175 - CONTINENTAL BANCO S.A'),
(184, '184 - BBA - CREDITANSTALT S.A'),
(199, '199 - BANCO FINANCIAL PORTUGUES'),
(200, '200 - BANCO FRICRISA AXELRUD S.A'),
(201, '201 - BANCO AUGUSTA INDUSTRIA E COMERCIAL S.A'),
(204, '204 - BANCO S.R.L S.A'),
(205, '205 - BANCO SUL AMERICA S.A'),
(206, '206 - BANCO MARTINELLI S.A'),
(208, '208 - BANCO PACTUAL S.A'),
(210, '210 - DEUTSCH SUDAMERIKANICHE BANK AG'),
(211, '211 - BANCO SISTEMA S.A'),
(212, '212 - BANCO ORIGINAL S.A'),
(213, '213 - BANCO ARBI S.A'),
(214, '214 - BANCO DIBENS S.A'),
(215, '215 - BANCO AMERICA DO SUL S.A'),
(216, '216 - BANCO REGIONAL MALCON S.A'),
(217, '217 - BANCO AGROINVEST S.A'),
(218, '218 - BBS - BANCO BONSUCESSO S.A.'),
(219, '219 - BANCO DE CREDITO DE SAO PAULO S.A'),
(220, '220 - BANCO CREFISUL'),
(221, '221 - BANCO GRAPHUS S.A'),
(222, '222 - BANCO AGF BRASIL S. A.'),
(223, '223 - BANCO INTERUNION S.A'),
(224, '224 - BANCO FIBRA S.A'),
(225, '225 - BANCO BRASCAN S.A'),
(228, '228 - BANCO ICATU S.A'),
(229, '229 - BANCO CRUZEIRO S.A'),
(230, '230 - BANCO BANDEIRANTES S.A'),
(231, '231 - BANCO BOAVISTA S.A'),
(232, '232 - BANCO INTERPART S.A'),
(233, '233 - BANCO MAPPIN S.A'),
(234, '234 - BANCO LAVRA S.A.'),
(235, '235 - BANCO LIBERAL S.A'),
(236, '236 - BANCO CAMBIAL S.A'),
(237, '237 - BANCO BRADESCO S.A'),
(239, '239 - BANCO BANCRED S.A'),
(240, '240 - BANCO DE CREDITO REAL DE MINAS GERAIS S.'),
(241, '241 - BANCO CLASSICO S.A'),
(242, '242 - BANCO EUROINVEST S.A'),
(243, '243 - BANCO STOCK S.A'),
(244, '244 - BANCO CIDADE S.A'),
(245, '245 - BANCO EMPRESARIAL S.A'),
(246, '246 - BANCO ABC ROMA S.A'),
(247, '247 - BANCO OMEGA S.A'),
(249, '249 - BANCO INVESTCRED S.A'),
(250, '250 - BANCO SCHAHIN CURY S.A'),
(251, '251 - BANCO SAO JORGE S.A.'),
(252, '252 - BANCO FININVEST S.A'),
(254, '254 - BANCO PARANA BANCO S.A'),
(255, '255 - MILBANCO S.A.'),
(256, '256 - BANCO GULVINVEST S.A'),
(258, '258 - BANCO INDUSCRED S.A'),
(260, '260 - NU PAGAMENTOS S.A. NUBANK'),
(261, '261 - BANCO VARIG S.A'),
(262, '262 - BANCO BOREAL S.A'),
(263, '263 - BANCO CACIQUE'),
(264, '264 - BANCO PERFORMANCE S.A'),
(265, '265 - BANCO FATOR S.A'),
(266, '266 - BANCO CEDULA S.A'),
(267, '267 - BANCO BBM-COM.C.IMOB.CFI S.A.'),
(275, '275 - BANCO REAL S.A'),
(277, '277 - BANCO PLANIBANC S.A'),
(282, '282 - BANCO BRASILEIRO COMERCIAL'),
(291, '291 - BANCO DE CREDITO NACIONAL S.A'),
(294, '294 - BCR - BANCO DE CREDITO REAL S.A'),
(295, '295 - BANCO CREDIPLAN S.A'),
(300, '300 - BANCO DE LA NACION ARGENTINA S.A'),
(302, '302 - BANCO DO PROGRESSO S.A'),
(303, '303 - BANCO HNF S.A.'),
(304, '304 - BANCO PONTUAL S.A'),
(308, '308 - BANCO COMERCIAL BANCESA S.A.'),
(318, '318 - BANCO B.M.G. S.A'),
(320, '320 - BANCO INDUSTRIAL E COMERCIAL'),
(341, '341 - BANCO ITAU S.A'),
(346, '346 - BANCO FRANCES E BRASILEIRO S.A'),
(347, '347 - BANCO SUDAMERIS BRASIL S.A'),
(351, '351 - BANCO BOZANO SIMONSEN S.A'),
(353, '353 - BANCO GERAL DO COMERCIO S.A'),
(356, '356 - ABN AMRO S.A'),
(366, '366 - BANCO SOGERAL S.A'),
(369, '369 - PONTUAL'),
(370, '370 - BEAL - BANCO EUROPEU PARA AMERICA LATINA'),
(372, '372 - BANCO ITAMARATI S.A'),
(375, '375 - BANCO FENICIA S.A'),
(376, '376 - CHASE MANHATTAN BANK S.A'),
(388, '388 - BANCO MERCANTIL DE DESCONTOS S/A'),
(389, '389 - BANCO MERCANTIL DO BRASIL S.A'),
(392, '392 - BANCO MERCANTIL DE SAO PAULO S.A'),
(394, '394 - BANCO B.M.C. S.A'),
(399, '399 - BANCO BAMERINDUS DO BRASIL S.A'),
(409, '409 - UNIBANCO - UNIAO DOS BANCOS BRASILEIROS'),
(412, '412 - BANCO NACIONAL DA BAHIA S.A'),
(415, '415 - BANCO NACIONAL S.A'),
(420, '420 - BANCO NACIONAL DO NORTE S.A'),
(422, '422 - BANCO SAFRA S.A'),
(424, '424 - BANCO NOROESTE S.A'),
(434, '434 - BANCO FORTALEZA S.A'),
(453, '453 - BANCO RURAL S.A'),
(456, '456 - BANCO TOKIO S.A'),
(464, '464 - BANCO SUMITOMO BRASILEIRO S.A'),
(466, '466 - BANCO MITSUBISHI BRASILEIRO S.A'),
(472, '472 - LLOYDS BANK PLC'),
(473, '473 - BANCO FINANCIAL PORTUGUES S.A'),
(477, '477 - CITIBANK N.A'),
(479, '479 - BANCO DE BOSTON S.A'),
(480, '480 - BANCO PORTUGUES DO ATLANTICO-BRASIL S.A'),
(483, '483 - BANCO AGRIMISA S.A.'),
(487, '487 - DEUTSCHE BANK S.A - BANCO ALEMAO'),
(488, '488 - BANCO J. P. MORGAN S.A'),
(489, '489 - BANESTO BANCO URUGAUAY S.A'),
(492, '492 - INTERNATIONALE NEDERLANDEN BANK N.V.'),
(493, '493 - BANCO UNION S.A.C.A'),
(494, '494 - BANCO LA REP. ORIENTAL DEL URUGUAY'),
(495, '495 - BANCO LA PROVINCIA DE BUENOS AIRES'),
(496, '496 - BANCO EXTERIOR DE ESPANA S.A'),
(498, '498 - CENTRO HISPANO BANCO'),
(499, '499 - BANCO IOCHPE S.A'),
(501, '501 - BANCO BRASILEIRO IRAQUIANO S.A.'),
(502, '502 - BANCO SANTANDER S.A'),
(504, '504 - BANCO MULTIPLIC S.A'),
(505, '505 - BANCO GARANTIA S.A'),
(600, '600 - BANCO LUSO BRASILEIRO S.A'),
(601, '601 - BFC BANCO S.A.'),
(602, '602 - BANCO PATENTE S.A'),
(604, '604 - BANCO INDUSTRIAL DO BRASIL S.A'),
(607, '607 - BANCO SANTOS NEVES S.A'),
(608, '608 - BANCO OPEN S.A'),
(610, '610 - BANCO V.R. S.A'),
(611, '611 - BANCO PAULISTA S.A'),
(612, '612 - BANCO GUANABARA S.A'),
(613, '613 - BANCO PECUNIA S.A'),
(616, '616 - BANCO INTERPACIFICO S.A'),
(617, '617 - BANCO INVESTOR S.A.'),
(618, '618 - BANCO TENDENCIA S.A'),
(621, '621 - BANCO APLICAP S.A.'),
(622, '622 - BANCO DRACMA S.A'),
(623, '623 - BANCO PANAMERICANO S.A'),
(624, '624 - BANCO GENERAL MOTORS S.A'),
(625, '625 - BANCO ARAUCARIA S.A'),
(626, '626 - BANCO FICSA S.A'),
(627, '627 - BANCO DESTAK S.A'),
(628, '628 - BANCO CRITERIUM S.A'),
(629, '629 - BANCORP BANCO COML. E. DE INVESTMENTO'),
(630, '630 - BANCO INTERCAP S.A'),
(633, '633 - BANCO REDIMENTO S.A'),
(634, '634 - BANCO TRIANGULO S.A'),
(635, '635 - BANCO DO ESTADO DO AMAPA S.A'),
(637, '637 - BANCO SOFISA S.A'),
(638, '638 - BANCO PROSPER S.A'),
(639, '639 - BIG S.A. - BANCO IRMAOS GUIMARAES'),
(640, '640 - BANCO DE CREDITO METROPOLITANO S.A'),
(641, '641 - BANCO EXCEL ECONOMICO S/A'),
(643, '643 - BANCO SEGMENTO S.A'),
(645, '645 - BANCO DO ESTADO DE RORAIMA S.A'),
(647, '647 - BANCO MARKA S.A'),
(648, '648 - BANCO ATLANTIS S.A'),
(649, '649 - BANCO DIMENSAO S.A'),
(650, '650 - BANCO PEBB S.A'),
(652, '652 - BANCO FRANCES E BRASILEIRO SA'),
(653, '653 - BANCO INDUSVAL S.A'),
(654, '654 - BANCO A. J. RENNER S.A'),
(655, '655 - BANCO VOTORANTIM S.A.'),
(656, '656 - BANCO MATRIX S.A'),
(657, '657 - BANCO TECNICORP S.A'),
(658, '658 - BANCO PORTO REAL S.A'),
(702, '702 - BANCO SANTOS S.A'),
(705, '705 - BANCO INVESTCORP S.A.'),
(707, '707 - BANCO DAYCOVAL S.A'),
(711, '711 - BANCO VETOR S.A.'),
(713, '713 - BANCO CINDAM S.A'),
(715, '715 - BANCO VEGA S.A'),
(718, '718 - BANCO OPERADOR S.A'),
(719, '719 - BANCO PRIMUS S.A'),
(720, '720 - BANCO MAXINVEST S.A'),
(721, '721 - BANCO CREDIBEL S.A'),
(722, '722 - BANCO INTERIOR DE SAO PAULO S.A'),
(724, '724 - BANCO PORTO SEGURO S.A'),
(725, '725 - BANCO FINABANCO S.A'),
(726, '726 - BANCO UNIVERSAL S.A'),
(728, '728 - BANCO FITAL S.A'),
(729, '729 - BANCO FONTE S.A'),
(730, '730 - BANCO COMERCIAL PARAGUAYO S.A'),
(731, '731 - BANCO GNPP S.A.'),
(732, '732 - BANCO PREMIER S.A.'),
(733, '733 - BANCO NACOES S.A.'),
(734, '734 - BANCO GERDAU S.A'),
(735, '735 - BANCO NEON S.A.'),
(736, '736 - BANCO UNITED S.A'),
(737, '737 - THECA'),
(738, '738 - MARADA'),
(739, '739 - BGN'),
(740, '740 - BCN BARCLAYS'),
(741, '741 - BRP'),
(742, '742 - EQUATORIAL'),
(743, '743 - BANCO EMBLEMA S.A'),
(744, '744 - THE FIRST NATIONAL BANK OF BOSTON'),
(745, '745 - CITIBAN N.A.'),
(746, '746 - MODAL SA'),
(747, '747 - RAIBOBANK DO BRASIL'),
(748, '748 - SICREDI'),
(749, '749 - BRMSANTIL SA'),
(750, '750 - BANCO REPUBLIC NATIONAL OF NEW YORK (BRA'),
(751, '751 - DRESDNER BANK LATEINAMERIKA-BRASIL S/A'),
(752, '752 - BANCO BANQUE NATIONALE DE PARIS BRASIL S'),
(753, '753 - BANCO COMERCIAL URUGUAI S.A.'),
(755, '755 - BANCO MERRILL LYNCH S.A'),
(756, '756 - BANCO COOPERATIVO DO BRASIL S.A.'),
(757, '757 - BANCO KEB DO BRASIL S.A.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `buy`
--

DROP TABLE IF EXISTS `buy`;
CREATE TABLE IF NOT EXISTS `buy` (
  `buyid` int(11) NOT NULL AUTO_INCREMENT,
  `ordid` int(11) NOT NULL,
  `strid` int(11) NOT NULL,
  `usrid` int(11) NOT NULL,
  `addid` int(11) NOT NULL,
  `ofeid` int(11) NOT NULL,
  `qtd` int(4) NOT NULL,
  `vlunit` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`buyid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `buy`
--

INSERT INTO `buy` (`buyid`, `ordid`, `strid`, `usrid`, `addid`, `ofeid`, `qtd`, `vlunit`, `discount`) VALUES
(1, 1, 64, 134, 97, 60, 1, '59.90', '2.50'),
(2, 2, 64, 134, 97, 60, 2, '299.90', '0.00'),
(3, 1, 29, 134, 97, 60, 1, '59.90', '2.50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `caracteristics`
--

DROP TABLE IF EXISTS `caracteristics`;
CREATE TABLE IF NOT EXISTS `caracteristics` (
  `ctsid` int(11) NOT NULL AUTO_INCREMENT,
  `ofeid` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `dimensions` varchar(20) DEFAULT '0x0x0',
  `stock` int(10) DEFAULT '0',
  `stockmin` int(10) DEFAULT '0',
  `size` varchar(10) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `voltage` varchar(3) DEFAULT '220',
  PRIMARY KEY (`ctsid`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `caracteristics`
--

INSERT INTO `caracteristics` (`ctsid`, `ofeid`, `weight`, `dimensions`, `stock`, `stockmin`, `size`, `color`, `voltage`) VALUES
(8, 155, '9.856', '0695x6852x9460', 1000, 12, '', '', '127'),
(9, 155, '10.001', '', 100, 10, '', '', '220'),
(10, 155, '11.220', '4543x8957x4395', 4792, 37339, '', '', '55'),
(63, 154, '43.000', '4324x2365x4644', 53, 5345, '', '', '534');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cartid` int(11) NOT NULL AUTO_INCREMENT,
  `usrid` int(11) NOT NULL,
  `strid` int(11) NOT NULL,
  `ofeid` int(11) NOT NULL,
  `qtd` int(4) NOT NULL DEFAULT '0',
  `vlunit` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`cartid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `catid` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `category` varchar(50) NOT NULL,
  `dimension` int(1) DEFAULT '0',
  `size` int(1) DEFAULT '0',
  `color` int(1) DEFAULT '0',
  `voltage` int(1) DEFAULT '0',
  `stocks` int(1) DEFAULT '0',
  `logo` varchar(100) DEFAULT NULL,
  `icon` varchar(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`catid`, `active`, `category`, `dimension`, `size`, `color`, `voltage`, `stocks`, `logo`, `icon`) VALUES
(1, 1, 'Baladas', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/baladas.jpg', 'wine'),
(2, 1, 'Supermercados', 1, 1, 1, 1, 1, 'https://em2d.com.br/seligaai/categories/supermercados.jpg', 'cart'),
(3, 1, 'Moda', 0, 1, 1, 0, 1, 'https://em2d.com.br/seligaai/categories/moda.jpg', 'shirt'),
(4, 1, 'Bares & Restaurantes', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/restaurantes.jpg', 'restaurant'),
(5, 1, 'Viagens & Lazer', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/viagens.jpg', 'jet'),
(6, 1, 'Automóveis', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/automoveis.jpg', 'car'),
(7, 1, 'Farmácias', 1, 0, 1, 1, 1, 'https://em2d.com.br/seligaai/categories/farmacias.jpg', 'medkit'),
(8, 1, 'Serviços em Geral', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/servicos.jpg', 'construct'),
(9, 0, 'Dentistas', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/dentistas.jpg', 'egg'),
(10, 1, 'Pets', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/pets.jpg', 'paw'),
(11, 1, 'Eletro Eletrônicos', 1, 0, 0, 1, 1, 'https://em2d.com.br/seligaai/categories/eletronicos.jpg', 'laptop'),
(12, 1, 'Esportes', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/sports.jpg', 'bicycle'),
(13, 1, 'Eletro Domésticos', 1, 0, 1, 1, 1, 'https://em2d.com.br/seligaai/categories/eletrodomesticos.jpg', 'radio'),
(14, 1, 'Fitness & Academias', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/academias.jpg', 'body'),
(15, 1, 'Decoração', 0, 1, 1, 0, 1, 'https://em2d.com.br/seligaai/categories/decoracao.jpg', 'images'),
(16, 1, 'Infantil', 0, 1, 1, 0, 1, 'https://em2d.com.br/seligaai/categories/infantil.jpg', 'happy'),
(17, 1, 'Importados', 1, 0, 0, 0, 1, 'https://em2d.com.br/seligaai/categories/importados.jpg', 'globe'),
(18, 1, 'Escolar & Escritório', 0, 1, 0, 0, 1, 'https://em2d.com.br/seligaai/categories/escolar.jpg', 'school'),
(19, 1, 'Bebidas', 0, 0, 0, 0, 1, 'https://em2d.com.br/seligaai/categories/bebidas.jpg', 'beer'),
(20, 1, 'Conveniências & Lanchonetes', 0, 0, 0, 0, 0, 'https://em2d.com.br/seligaai/categories/conveniencia.jpg', 'pizza'),
(21, 1, 'Outros', 1, 1, 1, 1, 1, 'https://em2d.com.br/seligaai/categories/outros.jpg', 'help'),
(22, 1, 'Beleza & Cosméticos', 1, 0, 1, 0, 1, 'https://em2d.com.br/seligaai/categories/beleza.jpg', 'glasses'),
(23, 1, 'Informática', 1, 0, 1, 1, 1, 'https://em2d.com.br/seligaai/categories/informatica.jpg', 'desktop');

-- --------------------------------------------------------

--
-- Estrutura da tabela `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `couid` int(11) NOT NULL AUTO_INCREMENT,
  `strid` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires` datetime NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `maxusagepershop` int(2) NOT NULL DEFAULT '1',
  `code` varchar(70) NOT NULL,
  PRIMARY KEY (`couid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `logistics`
--

DROP TABLE IF EXISTS `logistics`;
CREATE TABLE IF NOT EXISTS `logistics` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(80) NOT NULL,
  `cpfcnpj` varchar(18) NOT NULL,
  `bank` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `agency` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `account` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `owner` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `usrid` int(11) NOT NULL,
  `address` varchar(80) NOT NULL,
  `number` varchar(10) NOT NULL,
  `complement` varchar(30) DEFAULT NULL,
  `neighboor` varchar(30) NOT NULL,
  `zipcode` varchar(9) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `latitude` varchar(16) NOT NULL,
  `longitude` varchar(16) NOT NULL,
  `tel` varchar(16) NOT NULL,
  `cellphone` varchar(16) NOT NULL,
  `whatsapp` varchar(16) NOT NULL,
  `vltax` decimal(5,2) NOT NULL,
  `taxmode` int(1) NOT NULL DEFAULT '1',
  `maxdist` int(3) NOT NULL DEFAULT '30',
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `logistics`
--

INSERT INTO `logistics` (`logid`, `active`, `name`, `cpfcnpj`, `bank`, `agency`, `account`, `owner`, `usrid`, `address`, `number`, `complement`, `neighboor`, `zipcode`, `city`, `state`, `latitude`, `longitude`, `tel`, `cellphone`, `whatsapp`, `vltax`, `taxmode`, `maxdist`) VALUES
(1, 1, 'Logística Triboni', '08.401.414/0001-34', '', '', '', '', 136, 'Rua Dona Ana Neri', '581', NULL, 'Cambuci', '01522-000', 'São Paulo', 'SP', '-9.3916327', '-40.5470297', '(11) 2361-0767', '', '(11) 98858-4338', '15.90', 1, 30);

-- --------------------------------------------------------

--
-- Estrutura da tabela `lotteries`
--

DROP TABLE IF EXISTS `lotteries`;
CREATE TABLE IF NOT EXISTS `lotteries` (
  `lotid` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`lotid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lotteries`
--

INSERT INTO `lotteries` (`lotid`, `start`, `end`, `content`) VALUES
(1, '2018-12-12 00:00:00', '2018-12-13 00:00:00', '<p>ESTE É UM TESTE DE SORTEIO -- SEM VALIDADE -- O App Se Liga Aí é um serviço online que permite ao usuário obter descontos e promoções comerciais ativas em uma determinada área geográfica, subdivididas por ramo de marketing de mercado.</p>\r\n\r\n        <img src=\"https://em2d.com.br/seligaai/sorteios/instagram2.png\">\r\n\r\n        <p>\r\n            EM2D Empreendimentos Publicitários Ltda.<br>\r\n            Rua Padeiro João Luiz, 137, SALA A - <br>\r\n            CEP 56330-470, Petrolina (PE)<br>\r\n            CNPJ: 31.318.426/0001-11<br>\r\n            Sociedade registrada junto à JUCEPE PE<br>\r\n            <a href=\"mailto:seligaai@em2d.com.br\">seligaai@em2d.com.br</a>\r\n        </p>\r\n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `ofeid` int(11) NOT NULL AUTO_INCREMENT,
  `proid` int(11) NOT NULL,
  `addid` int(11) NOT NULL,
  `plaid` int(11) DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `removed` int(1) NOT NULL DEFAULT '0',
  `stock` int(6) NOT NULL DEFAULT '0',
  `stockmin` int(6) NOT NULL DEFAULT '50',
  `weight` decimal(10,3) NOT NULL DEFAULT '0.000',
  `dimensions` varchar(20) NOT NULL DEFAULT '0x0x0',
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `modality` int(1) NOT NULL DEFAULT '1',
  `oldprice` decimal(15,2) DEFAULT '0.00',
  `newprice` decimal(15,2) DEFAULT '0.00',
  `discount` decimal(15,2) DEFAULT '0.00',
  `description` varchar(150) NOT NULL,
  `prodserv` varchar(80) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `numviews` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ofeid`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `offers`
--

INSERT INTO `offers` (`ofeid`, `proid`, `addid`, `plaid`, `active`, `removed`, `stock`, `stockmin`, `weight`, `dimensions`, `start`, `end`, `modality`, `oldprice`, `newprice`, `discount`, `description`, `prodserv`, `type`, `numviews`) VALUES
(60, 0, 97, 5, 0, 0, 49, 55, '0.000', '0x0x0', '2018-10-29 00:00:00', '2019-08-01 23:59:59', 2, '40.00', '20.00', '50.00', 'Oferecemos um serviço com barba com toalh quente!', 'Combo', 2, 17),
(61, 0, 52, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-10-29 00:00:00', '2019-07-01 23:59:59', 1, '150.00', '100.00', '33.33', 'Tattoo com promoção imperdível', 'Tattoo', 1, 82),
(62, 0, 53, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-10-30 00:00:00', '2019-07-01 23:59:59', 1, '150.00', '120.00', '20.00', 'Manutenção completa de ar condicionado', 'Manutenção de Ar Condicionado 9 a 18mil btus', 1, 13),
(63, 0, 53, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-10-30 00:00:00', '2019-07-01 23:59:59', 1, '1499.00', '1399.00', '6.67', 'Ar condicionado Split Gree Class A', 'Ar condicionado Gree 9mil Btus', 1, 17),
(66, 0, 55, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-05 00:00:00', '2019-07-01 23:59:59', 1, '200.00', '180.00', '10.00', 'Valor para térreo. A partir de 1 andar há aumento.', 'Instalação de ar condicionado para residência', 2, 13),
(67, 0, 55, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-05 00:00:00', '2019-07-01 23:59:59', 1, '80.00', '70.00', '12.50', 'Manutenção preventiva de ar condicionado ', 'Manutenção preventiva (limpeza) de ar condicionado ', 1, 13),
(69, 0, 56, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-05 00:00:00', '2019-07-01 23:59:59', 1, '36.00', '29.88', '17.00', 'Promoção de Inauguração!!!!!!!', 'Cerveja Devassa - fardo', 1, 40),
(70, 0, 61, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-08 00:00:00', '2019-07-01 23:59:59', 1, '76.70', '69.00', '10.04', 'R$69,00 à vista ou R$76,70 em até 4x no cartão!!!', 'Macacões e Vestidos ', 1, 43),
(71, 0, 64, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-10 00:00:00', '2019-07-01 23:59:59', 1, '28.00', '25.00', '10.71', 'Shortinhos moleton do P ao GG', 'Short moleton', 1, 72),
(72, 0, 65, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-10 00:00:00', '2019-07-01 23:59:59', 1, '55.00', '30.00', '45.45', 'Vestidos longos lindos com precinho imperdível!', 'Vestido longo', 1, 85),
(73, 0, 66, 0, 0, 0, 0, 50, '0.000', '0x0x0', '2018-11-10 00:00:00', '2019-07-01 23:59:59', 1, '90.00', '59.90', '33.44', 'whey black code concentrada da marca red séries ', 'Whey black code', 1, 0),
(74, 0, 67, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-10 00:00:00', '2019-07-01 23:59:59', 1, '90.00', '59.90', '33.44', 'Whey black code concentrada a vista!!', 'Whey black code concentrada ', 1, 72),
(75, 0, 68, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-10 00:00:00', '2019-07-01 23:59:59', 1, '700.00', '599.00', '14.43', 'Ensaio Newborn para bebês até 13 dias de vida.', 'Ensaio Newborn (recém nascido)', 1, 29),
(76, 0, 61, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-12 00:00:00', '2019-07-01 23:59:59', 1, '69.50', '62.50', '10.07', 'R$62,50 à vista ou R$69,50 no cartão em até 4x.', 'Calça Pantalona, saia longa e short panta.', 1, 51),
(77, 0, 56, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-12 00:00:00', '2019-07-01 23:59:59', 1, '17.00', '15.00', '11.76', 'Combo 1 = 10,00 ; Combo 2 = 12,00 ; Combo 3 = 15,0', 'Combos Hambúrguer + batata + refri', 1, 63),
(78, 0, 71, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-17 00:00:00', '2019-07-01 23:59:59', 1, '100.00', '70.00', '30.00', 'Aceitamos cartões! Oferta valida para as TERÇAS-FEIRAS!', 'Cabelo, hidratação e manicure', 1, 38),
(79, 0, 72, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-17 00:00:00', '2019-07-01 23:59:59', 1, '30.00', '25.00', '16.67', 'Design de sobrancelhas com Henna por apenas R$25,00. Venha nos fazer uma visita e transformar seu olhar!', 'Design de Sobrancelha ', 1, 28),
(80, 0, 72, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-17 00:00:00', '2019-07-01 23:59:59', 1, '20.00', '15.00', '25.00', 'Design de sobrancelha simples por apenas R$15,00. Nos faça uma visita e transforme seu olhar!', 'Design de sobrancelha', 1, 41),
(81, 0, 73, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-17 00:00:00', '2019-07-01 23:59:59', 1, '89.90', '39.90', '55.62', 'Black friday fim de ano! Aceitamos  cartões!!', 'Shorts, saias e conjuntos', 1, 68),
(83, 0, 64, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-19 00:00:00', '2019-07-01 23:59:59', 1, '25.00', '15.00', '40.00', 'Todas as peças com descontos de 10% a 40% - de 19/11 a 23/11', 'Black Week', 1, 28),
(84, 0, 73, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-20 00:00:00', '2019-07-01 23:59:59', 1, '89.90', '35.00', '61.07', 'Black friday TATTY MODAS  venham conferir', 'Sandálias e sapatilha ', 1, 42),
(85, 0, 75, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-21 00:00:00', '2019-07-01 23:59:59', 1, '400.00', '49.00', '87.75', 'Cursos técnicos de \nELETRÔTECNICA\nEDIFICAÇÕES\nENFERMAGEM\nRADIOLOGIA\nADMINISTRAÇÃO. ', 'CURSOS TÉCNICOS', 1, 33),
(86, 0, 76, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-21 00:00:00', '2019-07-01 23:59:59', 1, '30.00', '25.00', '16.67', 'Somos uma empresa especializada em serviços de manicure, pedicure e design de sombrancelhas. Nossa esterelização tem tecnologia hospitalar.', 'Manicure e pedicure ', 1, 17),
(87, 0, 70, 0, 0, 0, 0, 50, '0.000', '0x0x0', '2018-11-22 00:00:00', '2019-07-01 23:59:59', 1, '12.90', '11.00', '14.73', 'teste de descrição de uma empresa ', 'produto de qualidade ', 1, 0),
(88, 0, 70, 0, 0, 0, 0, 50, '0.000', '0x0x0', '2018-11-22 00:00:00', '2019-07-01 23:59:59', 1, '99.90', '89.90', '10.01', 'teste de descrico de produtos de teste', 'teste de qualidade de ', 1, 0),
(90, 0, 82, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-25 00:00:00', '2019-07-01 23:59:59', 1, '68.90', '62.00', '10.01', 'Disponíveis nos tamanhos P, M e G', 'Look para meninas', 1, 21),
(91, 0, 82, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-25 00:00:00', '2019-07-01 23:59:59', 1, '122.00', '110.00', '10.00', 'Disponíveis nos tamanhos P, M e G', 'Look social para meninos', 1, 16),
(107, 0, 85, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-26 00:00:00', '2019-07-01 23:59:59', 1, '40.00', '20.00', '50.00', '15 hot filadelfia e 15 haru hot', 'Combinado Hot', 1, 42),
(108, 0, 85, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-26 00:00:00', '2019-07-01 23:59:59', 1, '55.00', '30.00', '45.45', 'Combinado com 20 peças hot + 20 peças cruas + Temaki salmão skin - A escolha do cliente se deseja o temaki cru ou empanado, sem adicional no valor.', 'Combo 40 peças + Temaki skin', 1, 49),
(109, 0, 86, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-26 00:00:00', '2019-07-01 23:59:59', 1, '140.00', '100.00', '28.57', 'Unha em Gel.  Promoção válida para o mês de Dezembro ', 'Unha em Gel ', 1, 30),
(110, 0, 89, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-27 00:00:00', '2019-07-01 23:59:59', 1, '11.99', '10.00', '16.60', 'Bolos simples com sabores inesqueciveis !!', 'Bolo Simples sabores diversos', 1, 14),
(111, 0, 89, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-27 00:00:00', '2019-07-01 23:59:59', 1, '18.00', '15.00', '16.67', 'Deliciosas tortas de chocolate com recheio servem 15 pessoas . ', 'Torta de Chocolate Recheada ', 1, 20),
(112, 0, 88, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-27 00:00:00', '2019-07-01 23:59:59', 1, '29.90', '26.90', '10.03', 'Disponível tamanhos: 2, 4 e 6. Masculina e Feminina! Estampas exclusivas', 'Camisa Polo, mallha da Marisol', 1, 22),
(113, 0, 90, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-29 00:00:00', '2019-07-01 23:59:59', 1, '26.00', '18.00', '30.77', 'Granola s adição de açúcar 1kg', 'Granola sem açúcar', 1, 15),
(114, 0, 91, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-29 00:00:00', '2019-07-01 23:59:59', 1, '12.99', '9.99', '23.09', 'Pastel + Batata Frita + Refri', 'Pastel + Batata Frita + Refri', 1, 15),
(115, 0, 91, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-29 00:00:00', '2019-07-01 23:59:59', 1, '19.99', '17.99', '10.01', 'Combo Bueno Bacon + 1 Refri Lata + Fritas', 'Combo Bueno Bacon', 1, 12),
(116, 0, 92, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-29 00:00:00', '2019-07-01 23:59:59', 1, '700.00', '350.00', '50.00', 'Cuidar da pele nao é custo é investimento!', 'Tratamento corporal 50% ', 1, 16),
(117, 0, 92, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-29 00:00:00', '2019-07-01 23:59:59', 1, '130.00', '120.00', '7.69', 'Faca sua Limpeza de pele e ganhe um Peeling de Diamante gratuito !!', 'Limpeza de Pele ', 1, 11),
(118, 0, 94, 0, 0, 0, 0, 50, '0.000', '0x0x0', '2018-12-30 00:00:00', '2019-07-01 23:59:59', 1, '12.00', '11.00', '8.33', 'máximo de informações possíveis ', 'produto de qualidade ', 1, 1),
(119, 0, 0, 0, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-30 00:00:00', '2019-07-01 23:59:59', 1, '49.99', '38.00', '23.98', 'Maiô infantil todo forrado! Escolha sua estampa... tamanhos disponíveis: 1.2.4.6.8.10 e 12 anos. Vendemos no varejo e atacado. Vem conhecer a gente! ', 'MAIÔ INFANTIL', 1, 0),
(120, 0, 95, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-30 00:00:00', '2019-07-01 23:59:59', 1, '299.90', '269.00', '10.30', 'Macacão Feminino', 'Macacão Feminino', 1, 18),
(121, 0, 95, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-11-30 00:00:00', '2019-07-01 23:59:59', 1, '90.00', '49.90', '44.56', 'Camisa Masculina ', 'Camisa Masculina', 1, 27),
(122, 0, 96, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-01 00:00:00', '2019-07-01 23:59:59', 1, '129.90', '90.00', '30.72', 'Lindos vestidos com vários modelos e estamlas \n\nLindos vestidos com modelos e estampas diferentes, estilo boneca!!', 'Vestidos ', 1, 30),
(123, 0, 97, 5, 1, 1, 0, 50, '0.000', '0x0x0', '2018-12-01 00:00:00', '2019-07-01 23:59:59', 1, '60.00', '40.00', '33.33', 'Remoção de vírus e limpeza física', 'Manutenção de Notebooks ou PC', 2, 17),
(124, 0, 98, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-01 00:00:00', '2019-07-01 23:59:59', 1, '200.00', '180.00', '10.00', 'Manutenção e instalação de  ar condicionado', 'Serviços de  refrigeração ', 1, 15),
(125, 0, 98, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-01 00:00:00', '2019-07-01 23:59:59', 1, '100.00', '80.00', '20.00', 'Manutenção de ar condicionado', 'Serviços de refrigeração ', 1, 19),
(126, 0, 100, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '75.00', '60.00', '20.00', 'Lindos vestidos Bonecas a vista por 60,00. Visite-nos e conconfira as nossas promoções! !', 'Vestidos Bonecas ', 1, 20),
(127, 0, 100, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '149.90', '100.00', '33.29', 'Lindos Vestidos de Gripy a vista por 100,00', 'Vestidos de Gripy', 1, 16),
(128, 0, 101, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '75.00', '55.00', '26.67', 'Lindos Vestidos Pra voce ficar na Moda', 'Vestidos Femininos', 1, 20),
(129, 0, 101, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '50.00', '39.90', '20.20', 'Lindas Camisas Masculinas . Venha Conferir!!', ' Camisas Masculinas', 1, 13),
(130, 0, 102, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '15.00', '10.00', '33.33', 'Depilaçao de Axilas', 'Depilação ', 1, 19),
(131, 0, 103, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '28.00', '25.00', '10.71', 'Lindas Sandálias Havaianas.  Venha conferir as ofertas!! ', 'Sandálias Havaianas', 1, 9),
(132, 0, 104, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '188.90', '94.00', '50.24', 'Sapatos femininos cores e tamanhos diversos.', 'Calçados femininos', 1, 26),
(133, 0, 104, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '292.90', '234.00', '20.11', 'Lindos vestidos e macaquinhos.', 'Vestido feminino', 1, 27),
(134, 0, 105, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '89.90', '70.00', '22.14', 'Shorts PCA com  cintos cores e tamanhos variados. ', 'Shorts Masculinos ', 1, 21),
(135, 0, 105, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '239.00', '190.00', '20.50', 'Lindos Vestidos Kesses ores e tamanhos diversos.', 'Vestidos Kesses', 1, 14),
(137, 0, 106, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '12.00', '10.00', '16.67', 'Combo de pastel sabores:carne,frango com  catupiry ou pizza', 'Combo de Pastel ', 1, 11),
(138, 0, 107, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '12.00', '10.00', '16.67', 'Cuscuz deicioso recheado com carne do sol.', ' Cuscuz sabor: Carne do Sol', 1, 11),
(139, 0, 108, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '80.00', '70.00', '12.50', 'Película promocional. Aproveite e vemha conferir!', 'Película', 1, 8),
(140, 0, 109, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '65.00', '60.00', '7.69', 'Promoção válida semente nos dias de :QUINTA E SEXTA .', 'Combo Whisky +energético +gelo', 1, 11),
(141, 0, 110, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '30.00', '25.00', '16.67', 'Carregador ', 'Carregadores para Celular', 1, 4),
(142, 0, 110, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '15.00', '10.00', '33.33', 'Películas para Celulares ', 'Películas  Para Celulares', 1, 5),
(143, 0, 111, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '45.00', '38.00', '15.56', 'Lindas camisas Masculinas Cores, estampas e tamanhos Variados. ', 'Camisas Masculinas ', 1, 15),
(144, 0, 111, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-08 00:00:00', '2019-07-01 23:59:59', 1, '60.00', '50.00', '16.67', 'Lindos Vestidos!  Venha conferir nossas ofertas! ', 'Vestidos!! ', 1, 17),
(145, 0, 114, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-09 00:00:00', '2019-07-01 23:59:59', 1, '120.00', '100.00', '16.67', 'Mais opções aqui na loja venha conferir será um prazer lhe atender ?', 'Vestidos Moda evangélica.', 1, 17),
(146, 0, 120, 5, 0, 0, 0, 50, '0.000', '0x0x0', '2018-12-20 00:00:00', '2019-07-01 23:59:59', 1, '10.90', '9.90', '9.17', 'descricao da oferta de produtos ', 'teste ', 1, 0),
(147, 0, 93, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-21 00:00:00', '2019-07-01 23:59:59', 1, '49.90', '39.90', '20.04', 'Macaquinho em viscose. Disponível nos tamanhos 2.4.6 e 8 anos. \nFazemos entrega. Consultar valor. ', 'Lindo macacão em viscose', 1, 17),
(148, 0, 93, 5, 1, 0, 0, 50, '0.000', '0x0x0', '2018-12-21 00:00:00', '2019-07-01 23:59:59', 1, '89.90', '65.00', '27.70', 'Vestido da LHAMA com descontão! ?\nSegue nosso insta ATACADO INFANTIL BY ARELI', 'Vestido Infantil', 1, 19),
(149, 0, 97, 6, 1, 0, 6544, 654, '0.000', '0x0x0', '2019-03-14 00:00:00', '2019-03-08 23:59:59', 2, '6757545.00', '54353.00', '99.00', 'fdsfsd', 'csfds', 1, 0),
(150, 0, 97, 6, 0, 1, 65, 864, '0.000', '0x0x0', '2019-03-22 00:00:00', '2019-03-29 23:59:59', 2, '564.00', '345.00', '39.00', '87686', '54353', 1, 0),
(151, 0, 97, 6, 1, 0, 4545, 545454, '0.000', '0x0x0', '2019-03-25 00:00:00', '2019-03-26 23:59:59', 2, '54.00', '44.00', '19.00', 'teste', 'teste', 1, 0),
(153, 0, 97, 6, 1, 0, 0, 0, '0.000', '0x0x0', '2019-03-27 00:00:00', '2019-03-28 23:59:59', 1, '49.90', '0.00', '0.00', 'teste', 'teste', 2, 0),
(154, 0, 97, 1, 1, 0, 10, 1, '4.000', '2000x0500x0100', '2019-03-27 00:00:00', '2019-03-28 23:59:59', 2, '44.00', '33.00', '25.00', 'teste', 'teste', 1, 0),
(155, 0, 97, 1, 1, 0, 10000, 100, '25.800', '0403x3296x8349', '2019-03-28 00:00:00', '2019-03-29 23:59:59', 1, '50.00', '0.00', '0.00', 'teste', 'Produto teste', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `ordid` int(11) NOT NULL AUTO_INCREMENT,
  `logid` int(11) NOT NULL,
  `ord_alias` char(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) DEFAULT '0.00',
  `shipping` decimal(10,2) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `payment_date` datetime DEFAULT NULL,
  `payment_method` int(1) DEFAULT '0',
  `delivery_status` int(1) DEFAULT '0',
  PRIMARY KEY (`ordid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `orders`
--

INSERT INTO `orders` (`ordid`, `logid`, `ord_alias`, `total`, `discount`, `shipping`, `paid`, `payment_date`, `payment_method`, `delivery_status`) VALUES
(1, 1, 'PED123', '657.20', '0.00', '13.90', 1, NULL, 2, 0),
(2, 1, 'PED124', '657.20', '0.00', '13.90', 1, '2019-03-18 00:00:00', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `phoid` int(11) NOT NULL AUTO_INCREMENT,
  `ofeid` int(11) NOT NULL,
  `photo` text NOT NULL,
  `orders` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`phoid`)
) ENGINE=MyISAM AUTO_INCREMENT=379 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `photos`
--

INSERT INTO `photos` (`phoid`, `ofeid`, `photo`, `orders`) VALUES
(133, 86, 'https://em2d.com.br/seligaai/offers/str_221435.jpeg', 2),
(132, 86, 'https://em2d.com.br/seligaai/offers/str_172824.jpeg', 1),
(131, 85, 'https://em2d.com.br/seligaai/offers/str_666933.jpeg', 1),
(130, 85, 'https://em2d.com.br/seligaai/offers/str_661756.jpeg', 1),
(129, 85, 'https://em2d.com.br/seligaai/offers/str_740221.jpeg', 1),
(128, 84, 'https://em2d.com.br/seligaai/offers/str_226327.jpeg', 1),
(127, 83, 'https://em2d.com.br/seligaai/offers/str_662383.jpeg', 1),
(126, 83, 'https://em2d.com.br/seligaai/offers/str_297307.jpeg', 1),
(125, 83, 'https://em2d.com.br/seligaai/offers/str_975709.jpeg', 1),
(124, 83, 'https://em2d.com.br/seligaai/offers/str_997515.jpeg', 1),
(123, 83, 'https://em2d.com.br/seligaai/offers/str_999379.jpeg', 1),
(122, 82, 'https://em2d.com.br/seligaai/offers/str_431874.jpeg', 1),
(121, 82, 'https://em2d.com.br/seligaai/offers/str_015045.jpeg', 1),
(120, 82, 'https://em2d.com.br/seligaai/offers/str_802130.jpeg', 1),
(119, 82, 'https://em2d.com.br/seligaai/offers/str_963664.jpeg', 1),
(118, 82, 'https://em2d.com.br/seligaai/offers/str_274095.jpeg', 1),
(117, 81, 'https://em2d.com.br/seligaai/offers/str_501488.jpeg', 1),
(116, 81, 'https://em2d.com.br/seligaai/offers/str_096035.jpeg', 1),
(115, 81, 'https://em2d.com.br/seligaai/offers/str_737386.jpeg', 1),
(114, 81, 'https://em2d.com.br/seligaai/offers/str_104569.jpeg', 1),
(113, 81, 'https://em2d.com.br/seligaai/offers/str_370833.jpeg', 1),
(111, 80, 'https://em2d.com.br/seligaai/stores/str_638617.jpeg', 1),
(112, 79, 'https://em2d.com.br/seligaai/stores/str_638617.jpeg', 1),
(109, 78, 'https://em2d.com.br/seligaai/offers/str_109788.jpeg', 1),
(108, 78, 'https://em2d.com.br/seligaai/offers/str_860065.jpeg', 1),
(107, 78, 'https://em2d.com.br/seligaai/offers/str_900817.jpeg', 1),
(106, 77, 'https://em2d.com.br/seligaai/offers/str_664212.jpeg', 1),
(105, 77, 'https://em2d.com.br/seligaai/offers/str_553113.jpeg', 1),
(104, 77, 'https://em2d.com.br/seligaai/offers/str_767810.jpeg', 1),
(103, 76, 'https://em2d.com.br/seligaai/offers/str_835659.jpeg', 1),
(102, 76, 'https://em2d.com.br/seligaai/offers/str_966199.jpeg', 1),
(101, 76, 'https://em2d.com.br/seligaai/offers/str_759217.jpeg', 1),
(100, 75, 'https://em2d.com.br/seligaai/offers/str_265708.jpeg', 1),
(99, 75, 'https://em2d.com.br/seligaai/offers/str_913631.jpeg', 1),
(98, 75, 'https://em2d.com.br/seligaai/offers/str_413334.jpeg', 1),
(97, 75, 'https://em2d.com.br/seligaai/offers/str_192953.jpeg', 1),
(96, 75, 'https://em2d.com.br/seligaai/offers/str_021309.jpeg', 1),
(95, 74, 'https://em2d.com.br/seligaai/offers/str_836187.jpeg', 1),
(94, 73, 'https://em2d.com.br/seligaai/offers/str_206436.jpeg', 1),
(93, 73, 'https://em2d.com.br/seligaai/offers/str_326968.jpeg', 1),
(92, 72, 'https://em2d.com.br/seligaai/offers/str_480994.jpeg', 1),
(91, 72, 'https://em2d.com.br/seligaai/offers/str_416491.jpeg', 1),
(312, 71, 'https://em2d.com.br/seligaai/offers/str_384339.jpeg', 1),
(311, 71, 'https://em2d.com.br/seligaai/offers/str_414013.jpeg', 1),
(310, 71, 'https://em2d.com.br/seligaai/offers/str_908612.jpeg', 1),
(309, 71, 'https://em2d.com.br/seligaai/offers/str_688160.jpeg', 1),
(86, 70, 'https://em2d.com.br/seligaai/offers/str_286997.jpeg', 1),
(85, 70, 'https://em2d.com.br/seligaai/offers/str_833572.jpeg', 1),
(84, 70, 'https://em2d.com.br/seligaai/offers/str_753614.jpeg', 1),
(83, 70, 'https://em2d.com.br/seligaai/offers/str_499188.jpeg', 1),
(82, 69, 'https://em2d.com.br/seligaai/offers/str_072313.jpeg', 1),
(81, 69, 'https://em2d.com.br/seligaai/offers/str_153074.jpeg', 1),
(80, 69, 'https://em2d.com.br/seligaai/offers/str_046496.jpeg', 1),
(79, 69, 'https://em2d.com.br/seligaai/offers/str_833921.jpeg', 1),
(78, 67, 'https://em2d.com.br/seligaai/offers/str_118156.jpeg', 1),
(77, 66, 'https://em2d.com.br/seligaai/offers/str_169913.jpeg', 1),
(76, 65, 'https://em2d.com.br/seligaai/offers/str_054442.jpeg', 1),
(75, 64, 'https://em2d.com.br/seligaai/offers/str_818525.jpeg', 1),
(74, 64, 'https://em2d.com.br/seligaai/offers/str_005492.jpeg', 1),
(73, 63, 'https://em2d.com.br/seligaai/offers/str_331223.jpeg', 1),
(72, 61, 'https://em2d.com.br/seligaai/offers/str_221963.jpeg', 1),
(71, 60, 'https://em2d.com.br/seligaai/offers/str_960414.jpeg', 1),
(151, 87, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAA0JCgsKCA0LCgsODg0PEyAVExISEyccHhcgLikxMC4pLSwzOko+MzZGNywtQFdBRkxOUlNSMj5aYVpQYEpRUk//2wBDAQ4ODhMREyYVFSZPNS01T09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0//wAARCAHgAQ4DASIAAhEBAxEB/8QAGwABAAIDAQEAAAAAAAAAAAAAAAMFAgQGAQf/xABMEAACAQMDAQQGBQoFAwIDCQABAgMABBEFEiExEyJBUQYUYXGBkRUXMlKhI0JkZaOksdHh4jNTksHwJGJyFnM0gvEHQ1RVk5Si0tP/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIEAwUG/8QALhEBAAICAAQCCQQDAAAAAAAAAAECAxEEEiExIlEFEzJBYXGRofAUwdHhM0Kx/9oADAMBAAIRAxEAPwD6LVXHJf3jo0c6WjCNZGgaHecNnGTkeR4HSrSqO3W6uLSKS5s72O7MYRpoWRDjOehb+I8TjFBZ2VyZ+1jlULNA+xwPHxDAeAI5/DwqGTVbeG+9VnyhaTs4zydzYU44HH2x+NSWNq0D3E0zBpp33MR0CjhR8B+Oa8u7ITYaIKshljdySeQrA/PA/AeVBPbyNLFueMxtuZSp9hI+Rxmte+1O3sLm2iuTsWfcBIeikY6+znrVJ6L6XqWn38r3cRjieIj/ABAQWyMcA+Wattd0xdTsezCKZl5jYtt2n34PHsoNQ+katrI0+2tluFZgBKkwIIwCT0xxz4+FXcjrGhZs4HkCSfcBya57QPRyTTrw3V3JE7qpCBMnGfHJx4ZHxq8u4GnjXspOylRtyPt3bT0PHjwSPjQeC+tWUFZQxJI2qCWyMZG0c5GRkY48aliljmjEkMiyIejKcg1UrpN+k5nj1ONJCXLMLbru256t/wBq/L31aWsC20AiVi2CSWPUkkkn5k0GhDrUU8TiKNmuUiExhGclSAeDjGcMOPP51aVR63pMs1oyWEa8QlFTdg5LoeM8Ywp8fIVP6PWtzZ2MkN0hTExMYJH2cDnAJAycnGfGgx1PXotN1FLaeFzG0YcyKc7ckjp8POt2wuzeQNKUEeHK7N4Zlx97H2W9nhVfqWl31zrEd3Z3MUCCIRsWXceGJ+yRg848fCt7TtMtNNjZbWMqXxvYsSWx5/PwoJ7qYW1pNcFdwiRnIHjgZqp03XpLy4t45rEwpdBjC4kDbtvXjAx0NXE0STQvFKNySKVYZxkHg1Q6J6PNp2qTXMrq8aZWDxOD4ny44+JoLue6gt2jWaVEaRgqKTyxJA4Hj1FV9xrJtbC0vZ4EWG4ZASJCSgYZz9nnArcvNPgvJIJZVPaW7h42BPHIJ49uMVV3ul3F9pNjp5Ts+xaPtHJGMKpBx4+7iguZJ0SB5lDShcjEQ3EkHBAA8c1VjW5DGX9XtQfum9TJq2iiSJSsYwCzN18SST+JNaJsCLlYkU+qYyV3KFGPzSm3nPXOc0G1Z3HrVqk3ZvHuGdrDp/Me2tVry7F0yepyCJZgm/bncpC8jB6ZJOegAx16blrEYLaOFpDIUULuIAz8BwKloFQS3cccyQghpGYAqDyufE15fPcpbN6nF2kx4XkAD2nJql0zT9QS8ee7iwzurFtynPPPQ0HRVGJQZQgGQVJ3Z8iOPxpIX3AKrY8SMc+zrXigmZCIyiqhHOPZ5e6gje+ijLh1mGzjJiYAnIAAOMEkkYpDf207FY3PHBLArg5xtOfHPhXkln2vrBkcM0pXZkEBAvK9CM4bJzwefYK110rs4IESYmSLcS7DlyxyTu+0OrdD4+NA1LVobCZYXaNWKhu83hkj/atm1vI7ixW6XlCCe4C2cHHAHJ6VQek+jX2o6jHNaRB0WEKSXA5yT4++rjQbWay0eC3uFCyJuyAc9WJ/3oMjqtsNPjvSJeykIC4Qk8nAz5fGpby+t7O2knmYlIjhgg3EHwBx06jr5itIaZP/AOnY9P3x9qu3JydvDhvLyFT6taT3+mXNqnZqzkBCScYBB5448fwoJLm/jt7iOFopm3sql1TKoScDJ9prbrRuoLie8iLRxPbRsrKDKykMDncQF5x4DOOufDG9QS7F8qbF8q9rVv76KxiDOryyMcRwxAGSQ+O0Z5x1PkOaDZ2L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7UM11bW7ok9xFEzhiodwpYKMsRnrgcnyoJdi+VNi+VeRyJLGskTq6OAyspyGB6EGsqDzYvlTYvlXtRwzwz7+wmjk7Nyj7GB2sOqnHQ+ygz2L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7Sg82L5U2L5V7UV1cw2drJc3D7IYlLO2CcAewUEmxfKmxfKsLeeO5toriBt0UqB0bBGVIyDzUlArlzHfRelDes3FqLmcP6nLJaM4EQ57MEOoDDknjJHieg6ioby0gvrV7a6TfE+Ny5I6HI5HI5AoNXsdZ/8Ax9h/+yf/AP1rctxMsKi5kjkl53NGhRT7gScfOpKUCq/XLiS308GGZYZHljRWbIHLgEEhW25GRnHBIqwrwgMCGAIPBB8aDl7vUpookjF9Jaj6TFvK0xQ7FMW7YHAPBOO8eRnnGKrLqe8TWNOlm1GC8kie/wCxkhZW7NRCNqthQNw8ePGu6WNFj7NUUJjG0DjHur0AKAAAAOABQceuq3R0+WWW+NvLBo8VxAgKASuyMWbbjnkAY6Dy5FaF76R3yaRPcQ6nIt3DBasVdY1XdIik7Rgl85JOcY8K7DU9KGpB0kvLiKN4zGyRiMgg5BOWUkEg44NbcFvDbwxRQxhUhQRoOu1RjjPwFBz2p6rLDPPJb3WDDewwdnI4XGdm5VTHeBDE5JBHOOBzoRa7cG+ktp714y+qXEMYCxojIgTCs55Xr1ALHPurtNi79+0b8Y3Y5x5UIDDBAI9tBy1lqdzONHSfUsJcvdLJIpT8rschMNtHh4gDNU+iekGp6hqNpbajqTQ20sErGVVSMybS2GzjjGP/AOPPjXc3FjDcXlvdSFi0CuqrxtIYAHI+FQT6Pa3GsxapKXaWKIxBDgoRknkY68+dA0C5uLzQ7S5uuZZI8ltu3ePBseGRg/GrClKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBWnq7XC6VctaXMNrMEJWab7Ce0/8APnW5WEsUc0TRTRrJG4wyOMgjyIoItPaRtOtWnmjnlMKF5YyCrtgZYY8D1rYrGONIo1jiRURAFVVGAoHQAVlQebh5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQR7h5N8jTcPJvkakpQKUpQKVHPMlvC0spwqjJ9vsHtqtWG51KQSXYeKAoStsVIHXgucjdnnu9PP2xtetNxuZ1CVtYgdJDZJJdmMkEoMICPN2wv41prdekVyzGC0sLYD82dncn3MvBq4ht4oIkjjQBU5XgDBxjgdB8POpaleMla+zX6/mlFJN6T269pJDpky5+xF2u4/yqZNcENs02q2ktmqnBZSJkBzjBZM4PvArfub60tADdXMMOenaSBc/Oud1X0x0yAslrG13Iy7SVOwY8t3X5VFsla+07YseTPOq49/Lp/TqIpI5ollidXjcZVlOQRWVcJpWuRSXe60WPTXQFvVg/5K68l5wqNwBuA5z8K7DTb9b+1WQxtBMAO1gf7cR8iOoz1GcZHNTE1tHNWeinEcLfBOrQ26UpRmKUpQKVhMJGhcQuqSFSFZl3AHwJGRn51zF1qWr2nodd3MT+tX0E8sRmEYGFWQqX2DjgD4dTnBoOqpXO+i1w3rF/YDUm1KG3Mbx3LNuJDg5Xd0OCvX2keFdFQKUpQKVVa/JewQ289pdLDGtxEsqdkGMitIq4BJ7v2j4Z91VOtT3+o+kj6PaX8tjHbWZug8XV3yAA3/bg9P6YDq6Vwuq6jq1+0MttqDWQi0ddSZIkBDvzlTnw9+fcalurzUtTsTdxahJZtp+mRXhEI/xpHQt3vDaNnTHiaDtaVq6Xcve6VZ3ciqrzwJIwXoCygnHzraoFKVzvpXcSiXTrP159OtrmR+2vFbbs2rkJu6Ddz8qDoqV88g9INU1fTdMiu7ptOt55JI7jUFITLKu5eeAuT188HGORUup6rqlzo+iCC7ukNwZ45mtI900pjOFYJkNztJODgZ58KDvqVUeil3dX3o3Z3V9Kss8isWdSpz3jjpxnGM+3rzVvQKUpQKUqC+uPVbKWcDJVePf0H41EzERuU1ibTEQ0wV1C/JeNuytXwgOQDIDy3Hlxj3mti6vbPTYA1zOkKdAXbkn+JNfPde1u+F1JYQTNDFABCwj4LsB3sn/y3VUWtlcXlyIYlaRyASE7xxx5e+s0Zp3qsbl7lfRcTSMma/LX9ncXfp3YIjC1t55nH2SwCqfjnP4Vzt36X6zcuwimECNwEjQfxIzmtRdCv2u2tUtWEka5k3uoAz0Oc4Hh4mtfU7a7sbhrW7QRsDu2KRtGR4Y9lUyeu5d26Q28Lh9H+simPVpnznf9IZhNKzTXMxaQgEmRiWYHp+FSRxxom4qzOQevAU+Hv4pa2+R2rjjwFTSCs/xepM/6xP06Ne4C7VdOM5BUA4BHka7LS7sJp1vrMGZp4Y9l6qsN7xDjvDxYcMDx3QQTXHlVZJAdoYcgk+Xh+P4Ve+hVzZW11cm4ljiuCo7F5CAPaMnpnj4V34e/Jf5sHpHHzYObvNfvE+788n0alVPo328VjLZXOS1lO0CMerIACp+TCrat8xqXyd68tpgpUE9yIZFTs3clSx244Axk8n21Hc3qxQzFAS8ZZRkcbgm75YqFG3WtBZrbW00NvI6mSSSTecEqzsWOOMcE8VC1+wEACbXd0Vgw/NbPIwfYamgu1mRCqu2Qu5guACQCOM58R548TQYadp0Vh27rJLNNcPvlmlILucYHQAYA4AAwK3K10vI5GlRFZnj6oMZP48fHBrGa5ZVV1Uqq96UMmSi/MeR6ZoNqlK1Wv4FMm7cFjbazY4z5eY+PFBne2kd7biGUsFEiSZU85Vgw/FRWjqWhx31565Fcz2lw0Jt5Hi2kvETkr3gcHPiOa2hqEJjLIGYjJZVwdoGMnOcHqOhPWvVvoWVCA+HdUHHiVDD8DQV956NWlwUME9xahbT1JhEVO6D7veB+fWsrr0fgliWK2uJ7SM24tZVjIYSwgEBTuB5AJww55Nbpu2MUbIhbcUDPgADcR4Zz0PtqVLlWfbscZXch4w445HPtHXHWgyt4IrW2it4F2xRIERck4UDAHNSVri8j9YWBgVkI6EgkHGcYBz0+HtqS3mE8XaKpAJ4yQc/IkUElQXkEtxEqQ3ctqQ2S8SqSRg8d4ED5eFeyXUcc3ZNnIXcx8FHn/wDT41Cmp20jBIyzu32FXBLD/bp44oI10mKDS007TpprGBM4MJBYA5JALA45Oc9a15vR63K6f6lPNZvp6ssDRBTgMAGyGBBzj5nNb3r8P5Xh/wAkjO3HgCQfxU16btVjLsGYAvkgY2hWwT18Px8vCgx0nToNJ02GwtS5ihB2lzknJJOfiTW3WtJdbZUGwiMsQ0hxjhSTjnPUeVSxTdo7IUZHUAlWx0OcHgnyNBJSlKBUF6A0ABGQZI8g/wDmKnrW1B+ztGkIzsZGx7mBqmSdUn5L4/bh8u16OW19JL3eCresNIuD4E7gfxq+9F5ktrWcCG3yzgmV7oRsFOCASBn34rR9M9Mktb835uROl1I2OMbCPzfgOPhVAQfV1ZS2ASrccA9Rz7eflWbHkjDktuH1Gbh/1/CY4pbXbrryfQIdYs7vXX02xXes6gtcCQ8MoJ48T0HiK1dV0OTU9RtnebHEm5tmQqoQPMljk/Gud9FpDa6xFeMjNFEG3Y9qkf710P01Ml5FKIwI4nlOA3LBiTg/h8qtOb1keL87PC4rgc3CcRH6fc6jvH3/AHbGn6LZpJbyvumSSJ2xMmwAgjHd+J4JrT1awtdQ1u0hsjFFDLF3mgUMoYbiR3ep6Vr6jqc2p3UXrG2OFXGFxuC5PJPiav5tesYGCY7cRNlGhwRjbjknAzyeBmu2LHSa82ujLP6rDljczzTHzny/tUazZ2kHozayR24jcS9nIwUBzjcrZPmcVzeltb2utQHUY1eBHIkUjcOhGfaM4NWupayLiKS1WNOwklaRmAy3eYtjnp1xxVRZ24vtUtrdyFWSRUJHHHT54rlxVq2vHJ1e/wCicOXHw+T18TET9ez6XpT5urjDiQSxQzlwchiy7cj4IKs6q9FWESXqwfYglW2X2KiKMfAlqtK0vAy9LfT/AI1rm1aedH7Vo1CMjbDgnJHs9lYzWEUzszPIA+SVUjGSu0np1xWyWO4qoBIAPJxXmZPuJ/qP8qObXXT4gVJklYq6sCzZwFzge7k+2vY7GOIpsklAQKCofAbAwCfh86nzJ9xP9R/lTMn3E/1H+VBqDTuxif1eeUP2YRMkYXByOg86l9U3xoJZXLbdsmMYk8wcjp16dM1NmT7if6j/ACpmT7if6j/Kg8ki7R1btJF2+CnAPIPPy/E1ryadBLM0sjSMWBABb7Pu8eozjoPCtnMn3E/1H+VMyfcT/Uf5UGpNpwMISKVw/eBct1DDB4xjwHAx06ivU06NTEe1k/JlW2jGCyrtz08vbW1mT7if6j/KmZPuJ/qP8qCH1JO4BLIFTb3RjDFehPHu+QrOO2WNy29m8FBxhBnOBx06fIVnmT7if6j/ACpmT7if6j/Kgw9WT1kz7nyfzc8A4xnz6fCvbeAW6sodmLNuJYAEnAHgB5VnmT7q/wCr+lN+cbRksMjPH/OtBDcWUVxKjy7mCEHacEHH/PDr41F9GxRoeyaQMvMZ342ewHHtxznitrMn3E/1H+VMyfcT/Uf5UGmNLjMW15ZQSrKxVvtAsTg58s9fH8K2GtI2ieMlsOHB5++cmpMyfcT/AFH+Ve7+BgZJJHx/4KCE2iNNvZ3K5LdmcbckYPGPafnWcEAhyTI8jEAbnxnA6Dge/wCdZZk+4n+o/wAqZk+4n+o/yoM6V4pyM/CvaBWMiCSNkb7LAg1lSncch6TaUL7TmkFyVksEY9mRw2Byfiqgj41xNlayXkphiPON3JwOv9a+r6ladvBKFztljaOVUHeZSMZHtHhXNQej4063VrZvWd5P5RUwfYP+eNYb455tPoOA4+MeGaTPXfT4ebRitFtoAkS4QeJPU1hIEHUlj5Lx+Jq7h0a7uW3SDsgepfqfhW9b+jdsj7riRpvIY2irxjme0ItxuOvtW3PwccWbO2NAWPHAyT/z2Vtw+j2q3TAtAUDdXlYD5+P4V3UFnbW5zBbxRnGMqgB+dT10jBHvlnv6VtH+Ouvm5a29C7baDeXErv4iPCj3cgn+FWUttpmhWDyJbxpGAQ3GWceWTnOTgf8A0q0llSJCzsowCeTjjz93PJ8Kr4YzqF4LqVYzbQtmHu8yN4P7hkge0k+VdIpWvaGS3EZc3XLaeX86JtIsRp+nRQE7pPtStnO5zyTnx/kBW7SlXZbWm1ptPvYD/Gb/AMR/vWdYD/Gb2qMfjWdFSq3XdRl0y2guI0Rou3UXGQSViwSzDHiAM+4GrKo57eG4ULPGsijPdYZByCpyPEYJHxoKGH0lHbapPLGrWFqYhbuhUGQM7RsSWYLjep544Ga24/SC2ls/W4IJpYFUtM6FCIgCQxPe5xtP2d2fDOanGi6etqbaKAxR9nHGOzkZSAjFlwQcggknPU55zUy6fbize1cSSxyRdk5klZmdeerE5/ONBrfTdt69e2mxg9nGZHLSRrkAA8AsCB3h3iAvtrVg9KbO4VBBb3EsryOgjjaNvsqrM24PtwAw8c5PSt650eyu555bpJJu3iMTI8rFApxnaucKTtHI54rVufR22kiCwO8cgZn7d5ZXk3FAud+8HGFXIzggeHWg1z6Y6WJoosS5kjifnYCpkXcq4LZJwRnaCBnk14/pfZxRdpLZXqr2CXPIjP5JjgPw/ngY689K27X0es4IrcMZTLHBHDI0crxLNsUKCyqcHjzzxxUkvo9pUsfZyWuV9WW1x2jf4SnIXr5jr1oLOlKUCoo//uv/AG/5VKSAMkgD21EgKmIHjuY+PFBqT61YQ3/qDSsbwsFWBY2LMSCRjjBGAct0HiRVVqFla3GrXr69ZXNzAyrHZ7YnlRU2gvgRglW3fnHBIxjgGugmgDlpYxGlz2ZRJigYrn+IyAcZ8Kj0+0NnYQW8kz3DxoA8shLM7eJOSTyfDPHSg9071n6NtfXv/iuxTtun28Dd04656Vmv5n/uN/vXkUJinmftSY5MHYxJ2tzkgk8AjHAwBgnxr0cBD4byc+/NBq6zqJ0rTnvfVZblIzmRYsZVfFufAVs2c0k9qks1u9u7DJicglffjipiARg8inSgwi+yf/Jv4ms6xj+x7yT+NZUClKUCtWe1Yt2lsVWTduIbO0npn2H2itqlVtWLd01tMdmmLxY5hFcAxEgnMhAHwI4/h4VOZ0CqSHIfptQt/DNS1ry2NrLkvAhJ5JAwT8qjxx8fst4Z+CRZlZygD5AzyhA+ZGK1JtUhEkcUH5WViMpH3iB8OPx461Kmm2UfS2jP/kN38a2gABgDAFPHPw+/8J8ET75+38q5LOe5kY37fkeCsIb7WPv/AMhxViBgYHSlKtEaVtebFKUqVXjKGGGAI9orHso/8tPkKzpQYdlH/lp8hTso/wDLT5Cs6UGHZR/5afIU7KP/AC0+QrOlBh2Uf+WnyFOyj/y0+QrOlBh2Uf8Alp8hTso/8tPkKzpQYdlH/lp8hTso/wDLT5Cs6UGIjQHIRQfYKyIBGCAR5GlKDDso/wDLT5CnZR/5afIVnSgx7KP/AC1+VZHkYNKUGHZR/wCWnyFe9lH/AJa/KsqUClKUClK1L7UI7KS2iZGZ7qTs48YA3YJ5J6dPiaDbpVa2u2MUlrDeM9rcXblIoJU75IbbzjIAJ6HOD4VN9JQ/Tf0Ttk7f1b1ndgbdu7bjOc5z7KDcpVJ/6q0ZFPrN9FEwkkTbyT3CeeniB8+Bk1tWuu6VeXxsra+ikuNu7YD18ePMjxHUeNBY0qtm1u0h16DR3EhuJk3qwA2D7RwTng90+FTadqEWoesNAVMcMxhyCckgDOQRxyfb/tQblKhvblLKymupQxSFC5C9TgZwPbVavpPpX0TBqdxO1tBPu2CVTuypwRgZ5zQXFKr21zS1v4bE3sXrE2OzQZO7IyOenI6efhUer67Z6VJFBLIjXU5URw5wWBYAnODjqevXBoLSlaUGrWFxqc2mw3KvdwLukjAPdHHjjHiK1YvSG2llkUW9yEjvTZNKQu0SZA+9nBJHOPGgt6VTj0p0LsYpfpGIRyydkjEMAWwCfDgAMuSeBnmpbv0h0eyupba7v4opogS6NnI4B+PBGB4+HQ0FnSq467pQubS3N7F2t4oeAc98Hpz0Gegz1PFY/TunO1/FBdRPPYIzTIxK7cZzzg8DHJAOKCzpVSfSTSU0mLU5LxFtZH7MOFY97njGM+B6itj6X076UXTBdIbx13CMZPGM9emcc4645oN6lKUClKUClKUClKUClKUClKUClKUCtLUrF71FCTKu0EGOWMSRSA+DrwTjwwRz59K3aguZJVMKQlA0j7cspIA2k9MjyoKI+iNq0tnI7pIbdQrCRXxgOz4Ta42jLEDO7AC+Rzu3Wk3T6+NWs7yGKT1X1YpLAZBjfuzw689KsEncW7ySJhkBJHTOCefZnGfjWt61cC2uGZQAva7JNwzlScDGPIfhQcjqXone2nrh01JryW8jdHOI0QKzhgOXBBBHXBBGBgdRbad6ItZ63bam+oGVoSx2tGSxDJt27t32R4DHGcVeNdyLPJCkXayByFUHb3Qqk5Pvb8axW+cyFEgd3Ld1PslV2qTnPj3sUFNqXogt/fXGoDUriC7kmSWMxqOzQoAFJX84gZ5yOp91WOm6FBaWV5aXRS7iurl52V4xjkg4xk5wR1qWDUSUgV03bljDuGGQzDjjyrAalL6xvaIiJoQ6JkZbLAAk+HWg81fR21GztrK3u2sraGRWYQLhiF+yqnPdwQD0PQeVaEOhX9hdWy2V6Zoo5JZFkuk7Ro94Gd2GUvltx4xgnnNXS3ZkEQjQCRnKsDyAB9o+0eR9orUGpyvprTRRq06E7hg7VABOevTAx76DUtdDlhungEm2xj9W2lgGeTsgCvIPHKjOV5HTHWtzUdIa8uHkjuBEsyxrMDHuJEbll2nIxyTnOcjHSpZtTETSZhJVd4DBxyyjJBHhUa6hIt5cRyocIA5Ukfk1CgseOvJoILD0dt7LWp9RTs2aVncEq+8M5ycndtI5OBtHhz4nPStAtdPu7u7dYZ7me6kuEmMIDxh/zQ3Jx18up4qc6jIdgS3w7SKoDPwQQT1Hjx0rCHVHNukksQI2qzupwBuYqOPh/Gg15dBZ7C0tFuwqw2TWUrGLJkjYIGK891u5weQM9DUkGhJbvD2U57OG5WdVK5OFgEIXOfZnPwx41vx3EjXXYvDsUhirbsk7SB0xx1rYoOZg9ERHpy6fJe77ZokjnxFtZ9kjOpU7iF5bB4OQPCtiPS79mvbOWZY7MSCezkRQSXaQyneD12sBjGAQfE9L6lBzuo+iy6ltluriN7ntWkcmJhE2UVMBQ4YcIv53XPngbEPo7DbazBf2sixJDGI9gU7mUIVClg2CBweVLZA72MAXVKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBWLIrsjMMlDuX2HBH8CayrXunuklthbQpIjS4nLNjYm08j252/P4gJpI1kUq4OCMHBIyKw9XiKMm07W3ZG487utUeryFNSvZBcSrNb2cL2sQuGRXlLS4XaCA24qowevSt7Vr+ezkVYeyCmCWVnkGez2be8RkZUbjnGT0wOtButbQtIZCpDkk7gxB5AB/AD5ViLK3UgqhBXoQ5BHdC9c+QHyrnn9I7xBM49VkMd3dQCBVO9lijZlOd3XKjPH5w6eOcmvXi31rbxTWM4me1DPHG2AJFkLfnf9gI9jcg9aC+WxtlAAQgKysBvOMqMDx9grz1C2wo2N3VCDvtkAHI8fOuei1W5S8kvLmCBza4iuJUDDZEZpkZlBJxzGjH2A+QxvnUtSilt4Z4YO2aO3acAqojZ32tyXz7BgHJHtxQXCxqrlhknnqc4z1x+HHsqFbG2RSqx4BQxkbjypJJHXzJrDVbqWztopIQhZ7iGLvAkAPIqk9fI1z2pek93ZWk8ga0eaESZj2Y4Wdow3LjghMYG45Pl1DpHsLZ87kY5LHG9sZbrxn2mvZLK2kkaR4gXbqcnnjH8K5/WXuJPSlbaOO+nT6PLiG2ujAN+/AYncvu8evQ1pWd7caVql/HcTxNdkWyyMy7hIywZbBLKByM5YjgHxoOv9Vh7mQx7Mgrl2OCMgePtrEWNsIjEIu4QARuPQEkfiTVLo2uX+oy2sctvChmjS43LnBiMZ3Ec9RIAPcw99bc2oXPrs9upiRElMaNtyWPY79o73DePTGPHNBvRQSLdGRym1QwQLnJ3EE5z7v8A6Vs1zmkavdzW8CTSRR7baycyTKSZjLkNjB6nGB7c1mus3zRptgR2kN1hlUYjEUmxc7nUHPU8jp08QHQUrQ1KcR6ZqDPeCEIjL2sUZZoMoOSBkkjO7w4I99c9a6pPp5ECRSPcPNBHIDd+sRFX3kPGWbILbcYYjqvXxDsKVzF9qN5JpWkXEsqxSSanHHKYJAFZQ7DB2sRggDIyfKrGa9K6ldLFLAgWK2xJIWZe/LIpGAcZ4wOnJGcgDAW1K5+81y6ttLluxHFI8frQKKAP8OQqrcuDjgZxk5Ix5He1mOS8EOnQXklq8xZ3eIlXCKOqn/zaMHzBNBZUqq0y5tNRv2v7eNCXtItshHfALSZU+WCOQPHrnAxa0ClKUClKUClKUClKUClKUClK17pbtpbY2skaIsuZw4zuTaeB7c7T8PgQ9ks7SW6S6ktYXuEGElaMF1HkD1HU17c2ttdoqXdvFOituVZEDAHz58ea53W/VDrd2jrCdRayhFgXQM4l3y4K8HGG25PTz4qz1y4vLZYRZORLc7reJQgOJWwVkJweFCuT50E1tp1hpjXN2FRHdpJZJ5MZUMdzDd4LnnFTpYWUYAjtLdQJO1AWMDD9N3Tr7etclq2sXD2GoWE8zdv215G8ZjxiEQyMmTjgHCkHqcHrg1tS6vdpetC13c+sLdXCtax26kiIRO0ZXjJJ2qRyMksPDgOl9Utdsq+rQ4mBWQbB3wSSQfMZZj/8x868khtGu45pY4Dcxg9m7KN6g8cHqM5x8a5Oz166kvS02o409JbcmdVUrsaOTJZigABdVB44ORkeEs1+YrrTJJZLd7+QW67p4lim2tKVJVNpPKlsjcMeQzgh1kkccqhZUVwGDAMM4IOQfeCM1DJYWUoAls7d8b8bolON/wBrw8cnPnWjp1+9zfpClwk2yKT1tAVJglDqAvABx9sDPUKD7TEL+4aG/SKcyajAtwUtQoIGGPZFsDIyu3GTzuPXHAXHYQ+sesdknbbdnabRu25zjPXGfCopLGzlZ2ltIHZ2VmLRgliv2SfMjw8qqvpOWT0c1S+gu4ZZIY5GjeGQSqhEYI52KDzzgg9fgKtNYvZ7izS21KV4pZrWKWTsk7rskhkQHbjPCEjqCccdKDpLXS7a0umnhBXudnHGMBIV4yEUcDJAJ8zU7Wts10t01vEbhF2rKUG8DyB645PzrnJNXvE0u9aS7Kajb29y7wBFIiIf8mTxx3cYz9oHNXFg1xLfX6yXcrJb3IRE2oBtMSNg4XPVyfgPbkNr1GzzAfVYM2/+CezH5P8A8fL4VhcWVg9qY7q1tmt0Yy7ZI1KKeSWweAeSc+01TxarcPYq63SNPFHCb5cri1YuA/hxhe0zknGwefOvqs9pcvDLcTRTaf6vOYbp4I5SZt4AVMrgnAO0D7QHjjNB1CxojOyIqlzucgY3HAGT5nAA+AqBNPsY7eS2js7dIJDl41iUKx9oxg1QXWpaqNQu7ft44HSSUwRAb5JIxASpVAuSN4zncOcr5Cs7DU7y60bU5be6lup4bYGKRIlKdr2ZOEIA3HOCQVGCcUF09vp11GbGSG1mS325gZVYR8d3u+HHSop9F06Wxms0tYoIpgA/YoqZwcjoOec8Hjk+dctFqbR6xfXljeiW2kntRLcOQqlOyf7TBCFG8KCcDB448Os0aW5m0i1kvJEknaMFnRSof24IBGRz0FBHFomlxWMFo9lbzRQAiPto1bBY5J5HGT5VvdjF24n7NO1C7BJtG4LnOM+WQOK50alJcWVuDPHLOr2ou48KwglM6ArjHB+2OTkbVPB5Olda7fvNcx21y8PZQysd0YZ4ysyDLKEO3Cs3i3dwxHmHXRQQwljFFGhbqVUDPJP8WJ95PnXsU0U2/sZUk7Nij7WB2sOoPkfZVB9JzPoFzeR3U5SO7VfWBEpxF2ih2TAIZQpbvY8M44qp07UfV5L2QX8kNrNd3hWaONXEkmIzGASCCSCxUDG7HjQdxSqW9ur+30Sx1KVNstuEmvYgOSm3EgHuzux/21UXF1N6OrFbG7jF1MYZrkuVVZXeX8oyd3LHGQRldq4I56B2COrjKMGAJGQc8g4I+Yrx3SONpJGVEUEszHAAHiTXOtfz2hWKOW2j7WW47ITS7e0ft24ACMWP2eAQTuOM9Rl6UXc8CzQyS9hZy2EwVgATJNlQqcg84zgDrz5cB0VeZGQMjJ6Cqa2v5zrMlpd3Bil9absbdVU74eyBDHjO3dnvfe7tV2oR2i+lQlmEMkrzQII7tWDjByrW7DPAJ7w6ZByQDyHVF1DhCw3EEhc8kDGT+I+de1w5v5I9Su306JIZuylaZkt0My4nQMWAXqFZiBubICkjJ56bRrprwXsi3DXFuLkrbylQAybVztIADANuGfZ1NBZUpSgVDdXdrZRiS8uYbeMnaGlcICfLJ8eDU1Vet29xcy6attLJCy3RYyxoGMY7KQZ5BGOQOfOgso5EljWSJ1eNwGVlOQwPQg+VZVzDrqdvdapIfXbgJLbLG5L7doVdzqi8N3uWVcZ5HFV8MWsTW16ZTqasunzNEA0qHte2kKgDce9tCcZJxgZIoOzuJ4bWFprmVIolxud2CgZOByfbUEemWEd817HaxLctnMgXnJ6n3nxPU1yl6msrYzRxrqpvopHMskcjdm4aVSuwc57vguNoBB8jYwHUFuL1ZBeukbTsTtkXuGRSFXkhyUBClcbcY6mg6KaJJ4Xik3bHGDtYqce8c1nVFPcOClyY9RaxdZl7JEk7Usdu3ugBlHEmCemRyOMXUIkEEYnZWlCjeVGATjnFBnSlKCOeGO5t5IJl3xSoUdfMEYIrKONYo1jjGEQBVHkBWVKBSlKDGRBJGyMWAYEHaxU/Ajke8V5DFHBCkMKBI41Coo6AAYArOlApSlApSlBDc2sF0IhOhYRSLKg3EYZeh4648jU1KUClKUClKUClKUClKUClKUClKUClK17qzjupbaR2kU20vapsbGTtK4PswxoMZdRtor1bNu2adgrYSB3ChiQCWAIUZB6kdK2qpbq1n/8AUq3Is7qWFooUEkNz2aoVdyd67xuGGBxg+NQ6fo7LdQPeWcTIFvFk3BWz2kwZAfPK7vdyKC5uLuO3uLaFwxa5kMaEDgEKzc/BTU9VWtwzyS2E0Nm12sE7NJEpQEgxuv5xA6sKrhpF52MpWEQuLO0ROyZScxszPGpPHIwOeDnnjNB01K52XRTdw2Ze3kiLs8d0HMYYxFzId2zAJLKo48Hbqeai0XRbuy1jtbhZD2bzE3AkjCzCRgwyoXcT0zuOAV4JHFB09K5WXR7tLiOaKxVwTqBnUMg7XtGzGGz1yAOuceNbunR6naadqkotpWupJS9vHO0W5vyaKM7CFAyD0xxQWllf2uoLM1nMJVhlaFyARhx1HPXr1HFbNU0ttcWc0Vvb2CXOnJCgSPcoYOhwPtdTtwQSeqdRmoJbG5OnG1FmWmuElVZcpus+0csvj0XI+yT9gYHTAdBUcU8UzSrE4YxPscD81sA4+RHzqr1+0nuDHJBam6xFLEYtygMW24DbuNnd5IO4cY6mtOTSruK/lultzNDHetOIFKjtlaAR8ZIGQc9ccZ68UHSUrm30m7KztHbiBvVLREETKcGN2Z41LezA73ByM+NXOlxtFYIskUkTbnYrJs3csTk7O7k5zx50G3SlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBWLSIjIruqlztUE43HBOB58An4VlUNxawXLwvPGHaCTtIyfzWwRn5E0Gpc61a2lxPDch4uxaEFmwFYSEgEHPQYbPkFNZS6rFBqK2s4EKGOR+2kcKp2dnnHmO+eeMbTUepaPFqN7G86qYOxdJQDhmYgqvwCyS/Fh8INJ0u+tjZSahPFNNHDcLOy57zSSK/AwOO6R4eHFBZS31pE5SS5hVwSNhcbshdxGPPBBx5GsIdTsZYbeX1mJBcIjRq7hWO/7PB8Tg49xqsl0e+m1xLmSeNrZJXcd9gwVomQLsHdJBJ7xyxBxkYwYbfQtQt1tkjmt1It7eCWTlioi57qkbWz4E4K84zxgLgatphUMNRtCpKqCJlwS2do6+ODjzwa8OsaWEDnUrMKV3gmdcFc7c9emeM+dcrPoeuQ6jY3ber3DiaEHa8hA2CTkgjCKd35vC54B8ZPoLVhqF1Apg/62yZLiZlbs0Mkrs4TzIDcA4z1OOlB1cl7bR2txcCVZI7YN2vZncVKjJGB4+yq1fSFI7qK0u7SWKcmJZdrKyRPITsUnIJPHJUEDzrbkgvZNLvLY9gJGV47c7mI27cKXJHXPXGaq5dJur/V49SUrEpkjcNKGWe2CZDRqB3SG5yc4wT9oYoNubW5omkT6Hve0jj7Z1LRHCA4J7rnnGcD84qQOhIzOvWvriwxxTywlo0a6jUGJXkAKDOcnIK8gEDcuTzUhW9t0vrlooprmSQLbrH4JwEDHA4BLMeuNxxmq2L0Z2zaaS6IlrHCZ2jZwbh4hhcrnbgYByQT4cUEq+ldh2VxK8VykUUTzIxUHtUV9hIAOR3uO8B/HGx9PW66Xd30sFwnqbmOaEKGcMACQMEg8HOc9M5xg1Vx+jV+sV2nrUEbywmNpUL7romTdukGRg4yvGThjggYFSn0akk0m5gaRIJXMzW8ELnsIC6bMcjkck5AGN7YAoLiHU4Jr6O0WOcSSQGcM0ZC7Rt43dCe+OmceNRarrEel96a2uXiVd8syKNka5xySRk58Bk+zpn2PT5Bc2Tu4KQWckD4YhiWMfIx/4HnjwrT1bRru5hW1tZozbYG1rh3aW3kDE9qj53McEjBI6DnGQQ2ItetpdWOnrFMD2rQrKQArSKu5lxndwD1xiobf0lt7mwS7gtLlxLIsUKK0bNK7DO3uudpAGTuxgVEmhXY1qS8M8Shnlb1hcmVlddqxkHu7VOCOv2RwOSYYfR+8jkF0vqUVxC0BjWFdiS9mrqS2F7pYSEcA7cDrQX1jdpfWq3EaOgJZSkgwysrFWB9oIIrYrT0q1ls7BYrh0eZneWQxghdzuXIGecAtj4VuUClKUClKUClKUClKUClKUClKUClKUCopGlWWLYFMZJEmeo44I+PHxqWo7iFbiBomZlBxypwRg5GKDXa4lMUbq6jtCSCIHkBXPdPB44x1pJcut8sMbxyZYBowp3IMfaJzjy8PGppINyoscskKoMAR4HHxBrOWISbO8ylGDDacZ9h9lBrXM1yl9DDG8QWYNjdGSVwB/wBwzUcN7I932ZaJlDurqqkGMKTgk5xzj2da25IEkuIpiW3RbtoHTnzqOSyikQKWcDLkkHqGJJB9nP4CghW+eSByihH3oE3AkFHYBW+WePZUoluEkijmEZLyFdygjI2k5xnjkYo+n25/wl7E46xAL0IIPTzH4msvVEEYUSS7w28SFssDjHjx04xjFBgJbmZT2DQqys4IdSc4YhfH2HNRSzubu3DNJGML2qqwIRie6Dxzkgj5efM0toxijSCV0KnDENgsCe9n29efM0fT7Vw+Yl3OMb8AsvAAwT06UGMNxNKe2zELcyFANp3EZ2g59/hjpW5UC2qLKHV5AuS3Z57uT44/4M81PQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKUpQKqde1SfT/VoLO3aa5umYR93cBtXccjIz8/M+GDbVHcQQ3MLQ3MMc0TfaSRQynx5BqYmInqie3Rzv03q8lxMFtrSFYZLeJ0kLF1aZVx04O1n5HGR5eOx6zqc9msqygXD9tapHDH3BMrsolLEnCgL0OevicCrgWlqu7bbQjcVZsIOSuNpPuwMeWBUU+l6dc49YsLWXaSR2kKtgk5PUeJOatzR5K8s+atXUbm71mJIgsdpBevbOpyHdhCzZ8tvI49gPjgTXOqXMd1dNGlt6pZOEnDuRI5KBu54fnKAD9o55GObI2tu1yLkwRG4VdolKDcB5Z645PzrFbO0WSKRbaEPCmyJhGMxr0wp8B7BUbjyTqVAdf1BYLZmgs+1vljlt1V2IVGkRcPx1/KDke3jjmbTNY1G4u4kuobURG5ls3MZbd2iKzbxn80hSMdRwcnoLlbK0UuVtYAZJBK+Ix3nByGPmc85obK0KlTawbWdnI7MYLMCGPvIJBPjk1PNXyRy281EdTvbPUbxnMUlmb9LcBnPaBnjjxtHTaCckeOSeMc7umajPew6YZ2SK4uIDdOkQ3KY8AAEnkcup/+UjPnvx6fZRSiWKzt0kUYDLEoIGAMZx5AD3AVlBaW9u7PBEqFkVO7wAq52qB0AGTwPOk2jXYiJTUpSqLlKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFKUoFa168iIgicqSeQu3eRg/Zzwf5ZrZrGSNJU2SorqfBhkUGjb6mJnCKhPB75BUHCg5PHAOfM+HnUl1ferMQYw2E3Z3de8Fx09tbJhiJJMSEsu0naOR5e6sTbwFVUwx7U+yNgwPdQYWszSwQsSDvjVjlu9yPID/nPlUa32beSbsxhIFmA3dcgnHTjpWyIIVdXESBkG1SFGQPIeyvDBEY5I+zULLneAMbs9elBCl2WaVdgBjGd27ufZB+1j/u+QzWvqOqmx0y+vew3i0JGzfjfjHjjjr7a3jbwMgRoYyoO4KVGM+dJLeCWJ4pYY3jk+2jKCG948aDnW9K3ju3gmso0CS3ERftyQOyjD5wEzgg+GSPbWVx6WIlubi3snmjFtLcYDENhJAmCMcdck+AB61dtp1i7FnsrZixZiTEpyWGGPTxHB8xXsNhZW5BgtLeLClBsjC904yOB04HHsoOeb0oMes3kboHt7eNcCCdZVYGUJuwF3Bhn7OfDGOc1s2/pRDPrdrpqJEzXCq25JHyoMXaZIZB7B1zzk46Va/RWmmPszp9ps2dnt7Fcbc7tuMdM848+a8m0nTZ0Cy2NuQI+zU9mAVXBAAI5GATjHTNBPcTCC3mlCNI0SF+zQZZsA8AeZxVDfekyaeGkMYuY5JWVXhcuibYVfBIU9T7OmT4Yq5sNNtNNjMdlF2anqNxbz8yfM0XTbBbU2q2NsLcncYhEuwnzxjGeKDkofSua+uNO1Awvb2qpdtJCku7tBHGrc8D4VYr6WPJPshsFaM3EUKu0xG5ZHdVcDb0IQH3NV5JptjIGzaQgtvyyqFYF+HIYcgnxI5pHptjEkaJZwYjCBSUBI2fY5PPHgfCgpbH0shuo7bfbiOW4ieRY95ONpkHXaBz2fHx445il9L2Syef1CNXSCGcJLciPtBIrNhCV7xG0Djqc+XN8ulaarIy6faAou1SIV7oyTgccDJJ95qVbO1V4XW2hDQLsiIQZjXGML5DHgKCif0rji7Uy2ymONo07WKUyRktEz8FVJIBXBwOhB9lXGpajBp1mbqY5jVgp28k844A6n+vlXraZp72wtnsLVoA28RGFSobzxjGeTzUklpbS27W80EcsLnLJIu5Sc55B9vNByenek07a3cCSFmhvFg7ANL3IpGhDrHnH5xJGcdR0rZHpe7ajp1ounr/ANc7KGM/2NsjIeNvP2c/HHtq9k0qwkZGNrGpSRJBsGzLJwhOMZx4A8VkNNsBJHILG23xEmNuyXKEnJwccckn40FCnplE9qsgspFlNuZ9jFgCBEz8NtweV2+Hjxxg5/8Aqe4Ac/RgcqVVVjnLMzNAZgANnsC/HpV0dL04lSbC1ykZiX8ivdQ5yo46cnj2mpBZWincttCrAhgVQAghdoOfMLx7uKCil9Lrf8pJbxxNapb9t28kjKD+WMWMKjHquQfaKzsPSb16ATx2iiMmIZ7XJ/KTtCPzfJd3xx7atodLsIYFgjs4ezWEQYKA5jH5pJ5I69fOsotPsoQwhs7eMMVYhYlGSp3A9PA8j20HOP6VJAXv5GhEL21qexMzHYziViO6p73AHIHAByK6TTrr17TbW82bPWIUk25zt3AHGfHrWP0bYflf+htvy7B5fyS/lGByC3HJySeait9F022vvXLe1WKbbtBQkKBjGAucDj2UCHUhLazTCI/koe1xkjP2uMkf9vWqKf007G37ZrBFXFsctOQAJkLZOEJ7u3HAOevsrqeyiCuojTEmd42jvZ6586g+jbDj/obbu7CPyS8bOF8PDw8qCpt/SVLrS7vUIIQVtg2FaXYshCo2csoIHfIGRzjOOcDK51xJNCl1FJjDbxzFJJYO9Iig4ztdOGzjII4BJ8qtbfTrG1jeO2sraFJBh1jiVQ3vAHNZCytBai1FrCLcdIuzGwc56dOvNBhpUz3GkWU8shkeSBHZyANxKgk4HA+FbVKUClKUClCQASSAB1JrBpVXH2jn7qk/woM6VgsqscAOPehH8RWSkMAVIIPiKD2lKUClKUCleFlDBSwBPQZ61gZkBxiT/wDTb+VBJSsO1TaGJ2gnA3Db/Gs6BSlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBXjNtGcE84wK9qIFWdn547g8vb/z2UFbLrelxzCK4uQZg7ALsY7SpII6eGD/AMIqGb0s0OIgPfr8EYgcZ6geVad/aX3rzyxXFmjySbI+0vZFLBjgrjGAcKOADyPPk1eoJqEEnYT3cME5kaT8g1wAGcbmUEKdy93OPf44IDp4vSHS5o5pIrolIAWkPZOAuCAeo68jjrzUUXpFo8twIYLsCeVwijs2G9jnA6ezk1TWcV3dTR28t0kgcgbC9wFZNuZAcgAnvDAbOO8PAgbtlpWuQ3cbT3ELQh0ZgLudjwctjdxj+WPbQdMrbgfAg4Iz0r2omIV0fHjtJ9h6fjj51LQK1NQvoLG2lnnmWGKJd0kjchR7vEnwHj/HaY7VJAJwM4HjXK+lNpc6lLb2yXKxCA9oQSwDufMryABuHHnUxG5HEekP/wBoWrXN8w0UTWFvtIO9FaR/aSQdvHgDxzz5cjdXOp3ciy3k95O4+y8rsx+BNfSXsE0bSjIjxXCBY9rguJFkBPK93CDPPzAxxWIt9R1CP/oY4CHB7VoXx+apOQxwPtfmgYOeBVb2rSdS0YeGnJHNMxEb1uZcbp/pP6U6euy3vrtkbjbKvajA8BuBx8K+o+ifpfba7GITE9teRpmS3bJyM/aQnqOnHh8OabSZL6K7uILx7kyertJHGOXkY4wM4LA8546ZJ8K1C+rXer2s6xR2VzE8ajs4SpILADcc5C4wPEEcHipx+Ou1eIw+pvy73HnHvfTgQwBByDyDStTTbkXdnFcowaOZBIpBzgnqPga26OJSlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBSlKBWozOlmHji7V8btmQMknPU1t1rSrMltKIcbwTsOM9Tnp8TQQpGstxulsY1IAYSMFJ3f88a2jyCCetaMTagdgk2DJXJEfh4+NSldQCkh7ZmHQFWAP4nH40GyowoGScDqaxmkaOIuqFyMcD317EHEY7XaX8SvQ1nSCUc3+BL7ENT1FIpZNgH2ztPu8fwqWgxkJVQR94D8RXH6pfeo9vJMrzS+ssiqH2nBYkeB8MAV2L52EgZIHA8zXzv00spH1UXTQrPaSKpWItglgCSfLBHGevzq+PuMjezX8iaTLpslq020Ayy4Pjg7SmSDjGRXkFnJDaiOKSGJxgI0cm8sx/POMYBwCOcnjp4URvdRt3TU7m9Kx2/dRTh32hshcsOeT1NdFbanpEpglu723kW43PJGbhBIpbnc4U+4dcgeGBS+Otp3Z2x8Rkx1mtZ6SmsIDazevicXt4AQ+5ScdOg+0MjA3EnGSPZWhJf3jXHrhuZi4k3xR7zsByfDpj34yKt7O80yfs49MvbMTcuvZXALnK+I6gew8jHUVk5luIZPWLNXVlXZcspkk5CcAIC33zz5jwq1dV9zla02ncyvdImM1tbvs7NWjJVM5Cg7SAPPGasq09NtobW2jgh7QiNSdzk5O47jnPOa3K5z3QUpSoClKUClKUClKUClKUClKUClKUClKUClKUCsHXvdoqksBjAOMis6wlkEULyEZCKW+VBTajqd1bTmOH1WQc8ndkeQI/r8qppbq/uJQbiRCh6gE8H2DpXsxMsryk4d2LEjzrEqceBPvIoFOgyelSW9s0uSCB9rPJbGFzyPbj8DXhjTjYzSNt3MCmNvGePZ7aDADncevQCsqmig3GTtS0YjTe3dycceHHmKjKOpIdHXBI5U9QM4oMa1ryFpYWVGVG6qxGQD7fZW2scjY2xud3Iwp55x/HivCrDGVYZ6ZHXnH8eKDgrqa8srh47q2jWRgDhhu49nPSohqk4YNtiyvQ7cEdeR5da79rFLsqs0IeP7Xfi3gDnkD4H5VXP6P6c0jH1EZI3kB3HHXOB0GKnY5NNVmjIKRQqw6MAc/xrptJW/k/LX0aRnACRgEMfa3PA/H5VZrodrZpb9jaIsj5IBjy6nOOCea2EicxhlUkEhQMEls5xjz+yajYwAwK9r0q4AJRgDjBwec9P4Gs5oxDGrFmOcfm8cqG6/GgjpXqo7EBY3YkZGFJyM4/jxRVZxlEZhkDgZ5PSg8pQFckMWB8MDPNeqjsoZUcgkAEKecnA/EGg8pWYj/JO5yCrKMEddwJB/D8akhgWRYyzH8pL2Qx4Hjn29elBBStprMqiPuJVlU91STkqW6fDrWpnnGD8vjQe0r0o6jLIwHmR7M/wIrygUpSgUpSg27bUbm2PdfeuPsvkiuhsruO8h7ROCOGU9Qa5Ot7R5THqEY3EK+QR58cfjQdNSlKBXN2Wq3c/odYXEqi4vL2HaWY7FzsZiSQDjhT0HXA4zkdJUF6u+xnUruzG3HwoODbUezeTtFiSOFQZHaU9Sue6Md4e3jx4qN9QlNlcSvC0MkKtuQMCc4UjBI8m8RVngZz414AAMAAD2UGta6ldQi5dLeN3JMCxrNxIcc8leMf7EUh1gPb9qkCxJ6uUdu0UOWaMN3cJzgHOTk8HjzsYIEkVmkIO3aQvn3gPLHn45qeewU3JkRZMDcoCnC57ULjy6edBTHV5ooZd9piVoG3I8w7iJt/OC8nBXwHjVnNdCRSFj27naR+9nLEY48h1+dYmCNb6yRD3XI6jO3vlccjwA8q8aLaUVu4xG47mzgHGOgzn59RQaMmrXNmJZbVZJGtMhgxURbRhyp43bvIjjmpF1gpPHA9mFhVuwKrKHO5hv/OTBXGPI8fOza0VJQqRyv3iAcrjiTbg5GOn8Rxio0gSWVHdt5d0XKDGQ27OOB90eFBqx6jPawrJNYQkQo+5knLNjDdAUHXPTOPnWtB6QuJYIDaQmVoSzMZu6yFcDJK8EBRxgjjOeasbfAlyUcAwuTk9e43Tj+deRwDc6iTZ+S38Dk93cR0Psz04zQal/qcjSpdTIZLSKMkBZAd4A3dAgxwCM58vhiuvuskAfTxDskTOZlG3DHBwF6NvI6DBHWtrum2OI0DB17+O8eG4zU8MEcyW8bAriMYIxgZlKn8GoHrcWR/0+diqqEspPdz4lfHPhjpSO9VURXg3FCCpD4wQqqD0P3c/GvZLWIWfbYkWQkARswypyw549ns8awu7ZYAhTd3t24E52kNjrigytruO2A2W+WzySwyQGB67c+AHl7KitplhwHj3gMrjDYOVzj4cmoaUHpdjL2vG/du+NbaXqRuSsPd7pHCAqQSQR3ceJ8M+2tOlBNJMjQBEjZWOzcS2R3VK8DHtrKC4WJFV4y3ZyCRCGxzx14PHArXpQb0eoBUCmHO1VC4fxCleePbVft5z/wBu38MVlSg2JblZLZIUhEYU5JB6nAHl7PEmtelKBSlKBSlKBU1l/wDHW/8A7i/xqGtjT0L6hAF6hwflzQdZSlKBSlKDl9Ts2tblsL+SckoQOPdWnXYyxJNGY5VDKeoNV0+hwOcwu0Xs+0KCh3uIzGHbYTkrng/ChlkKspkYhjlhnqfM/IVcfQH6V+z/AK0+gP0r9n/WgqHlkeRZHkdnXoxYkj416biclSZpCV+yS5491W30B+lfs/60+gP0r9n/AFoKjtZNpXe2C4fr+d5/jXpnmLFjLIWK7SSxyR5e6rb6A/Sv2f8AWn0B+lfs/wCtBTmRzjLtwu0c9B5e6glkAYCRgGADAHqBVx9AfpX7P+tPoD9K/Z/1oKXJxjPHlXu5sY3HGNuM+Gc4+fNXP0B+lfs/60+gP0r9n/Wgp2kkfO92bdjOTnOOlevNLIMPK7cY5Yn/AJ0Hyq3+gP0r9n/Wn0B+lfs/60FLSrr6A/Sv2f8AWn0B+lfs/wCtBS0q6+gP0r9n/Wn0B+lfs/60FLSrr6A/Sv2f9afQH6V+z/rQUtKuvoD9K/Z/1p9AfpX7P+tBS0q6+gP0r9n/AFp9AfpX7P8ArQUtKuvoD9K/Z/1p9AfpX7P+tBS0q6+gP0r9n/Wn0B+lfs/60FLVzoliwf1qQFccICMZyOvu5rbg0a1iwXDSt/3Hj5CrCgUpSgj7eL76/OnbxffX517Sg87eL76/OnbxffX517Sg87eL76/OnbxffX517SgjnvIIIJJmfKxqWIHXAGa9e6iQoNwO9sdRxwT4+7wrOlShANRsS7oLuHcmdw3jK465oNSsTH2gvINmdu7eMZ8qnpTodXnbxffX507eL76/OvaVCXnbxffX507eL76/OvaUHnbxffX507eL76/OvaUEcs8W0d9ftL4+0Vn28X31+de0oPO3i++vzp28X31+de0oPO3i++vzp28X31+de0oPO3i++vzrVbVIvWZYI4ZZWiALFSoHPTqwrbpUxMR3hExM9kct5bQrulnjRScZZgBWCalYyOqR3cLM4yoDg5H/AAGp6U6HVANRtHVjFPHKVYKwVxkEnHiaygu4pULbguHZcZ8mI/2qWlJmCNvO3i++vzp28X31+de0qEvO3i++vzp28X31+de0oPO3i++vzp28X31+de0oFKUoK7VtUFiYLaGMy3t2WS3jCkjIHLMR0QZGT1x0BrTW31fTYY717mTUZdp9cgGAH8miXoCOBt4DDk97k6+hszenPpQCxIUWgAJ6Dsyf96sdDudQvtOsb27NqFuLbtXSNGBDNgrgknjacH2/Kg37W5gvLWO5tZVlhlXcjqcgipaqtHRU1DWwihR68DgDHJghJPzq1oFKUoFUWmK2vWH0jdTXsVvdN2ltbrKYTEg4BLRkFi32sEkDIA6ZN7VFpjNoNh9HXUN7Lb2rdnbXCxGYyoeQCsYJUr9nJABwCOuAE6w3un6pax2ztPpsymORJZGeSFwGYPvYksDjaQTx3ceNW1c/d6rcHVbCbs5LbR1JMtzNGULSHKLGythkXJzuIwTtHFdBQKUpQKrfSG5u7XRpX08ot1I0cMLP0VpHVAx4PTdnp4VZVUelEyW+im4lJEUFzbSyEAnCrOhY4HkATQY32nWFrGJ5pdTIeaOPCajOO87hBxvAAyw+FSPpUluI20y8u45BNGZBNcvMrxhhvXEhbGVzyMHOOai13RVv0EkZu2mNxAzKt5KihVkQsQoYKCFBPAznkc1uRwWmkRtJ21ztleOPM1zLN3mYKoG9jjJYdPj0oN6lKUClKUFZqs9y13aabZyvBLc7pHnVVbs40K7sBsjJLKoyDjJPhg609jrq3McNrrEj20pBlnmii7SEKeQgVACXyBlgQoUnnIFbWr6dJdG3vLMxLqFkXa2aYEpllKlWA5wQfDoQDzjBjg9ILJrdzdsba6hKJPbMCXSRhwi8d/JzgrnOOKDOxg1W11OSO5vPXbF490ckiqssbjA2naACDkkHGRjFWdVljb3NzejVL5ZLeRUkhitCUIjQsp3MRnLHYDwcAHGMgk2dApSlAqq9J9Qk0r0bv72AP2sUJ7MqoJVjwGweMAkE+wVa1ynpv6WWGhQCwuLRr2S7iYPCH2DszlTluTzyBgeB6UHzWy9OvSG21IXcuoS3CsymSF8bGA6gDGFz5gCvutfCdK1zQtK1VdQh0CeV0JaKOa+DJGc8EDs85Hhknz64NfY/R/XLP0h0316w3hA5R1dcMjAA4Ph0I6edBZ0pSgUr5l9bf6i/e/7KfW3+ov3v+yg7qXQNOlvp73bcx3Fxt7V4buWLftGFyFYDgVO2mWpsILFe3it4FVYxDcSRkBRgDcrAkY8zXz762/1F+9/2U+tv9Rfvf9lB9DsbC309JVthJ+VftHaSZ5GZsBclmJPRQPhW1XzL62/1F+9/2U+tv9Rfvf8AZQfTaV8y+tv9Rfvf9lPrb/UX73/ZQfTaV8y+tv8AUX73/ZT62/1F+9/2UH0meCG5haG5ijmicYZJFDK3vBr2GJIIUhiXbHGoVRnOABgV81+tv9Rfvf8AZT62/wBRfvf9lB9NpXzL62/1F+9/2U+tv9Rfvf8AZQfTaxljjmieKZFkjdSrowyGB6gjxFfNPrb/AFF+9/2U+tv9Rfvf9lB3baHZsxYzaiCTnA1G4A+QfisotFsI3V2SWcowdPWriScIw6ModjtI8xzXBfW3+ov3v+yn1t/qL97/ALKD6bSvmX1t/qL97/sp9bf6i/e/7KD6bSvmX1t/qL97/sp9bf6i/e/7KD6bUEtlaT3MVzNawSTxf4crxgsnuJ5FfOfrb/UX73/ZT62/1F+9/wBlB9NpXzL62/1F+9/2U+tv9Rfvf9lB9NpXzL62/wBRfvf9lPrb/UX73/ZQfTap9X9F9F1u6W61Oy7eZEEYbtXXCgk4wpA6k1xX1t/qL97/ALKfW3+ov3v+yg6f6v8A0V//ACv94l//ALVcaRo+n6Jata6Zb9hC7mQrvZssQBnLEnoBXAfW3+ov3v8Asp9bf6i/e/7KD6bSvmX1t/qL97/sp9bf6i/e/wCyg+ZUpSgUpSgUpSgUrc0nTLvWNRisLFA88pOAzBQABkkk+Qq5uH0f0bu7m2toU1i8TKC4uYysMLdGHZHO8jnknGcYHHIc1Srb0nO/We02orS21vIwRAgLNAjMcDAGSSfjVTQKUpQKUpQKUpQSQQTXEqw28TyyN0RFLE+PQVHXUajc2voz61pGknttQSXEuqbTHJERwUiGcr4gtnnJGMYNZPDbellte38KJZ6pY2vrFyuMpebftyDAARvMc5LDpyaDlaUpQKUpQKUpQKUrr/SW6+iotEjsbTT0WbSYJpC1jDIXc5yxLKTk4FByFKu5rtr/ANG7qWeC0WSK8gVHhtIoiAyTZGUUZB2r18qpKBSlKBSlKBSlKBSlKBSlKBSlKDJHaN1eNirqcqynBB8xV/8AT9rq8kh9KbZ7iZ12pfW2Emix0GwYRxnjnBwTz0xz1KCx165t7rVDJZu7wJDDErum0tsiVM4ycZK561XUpQKUpQKUpQKUpQdDrVtp2oa3fXsOv6esdxcPKgeO4BAZiRn8l15rY0SbTdEt9XeXV7W5a706W2ijgjm3F2xjO5FAHHnXLUoFKUoFKUoFKUoFdjd6r6Jara6b9KJra3FnZRWreriIIdg694k9Sa46lB0Gq3fo6mivZaENU7Sa4jlkN4I8AIsg42+Pfrn6UoFKUoFKUoFKUoP/2Q==', 1),
(149, 87, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAA0JCgsKCA0LCgsODg0PEyAVExISEyccHhcgLikxMC4pLSwzOko+MzZGNywtQFdBRkxOUlNSMj5aYVpQYEpRUk//2wBDAQ4ODhMREyYVFSZPNS01T09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0//wAARCAHgAKoDASIAAhEBAxEB/8QAGwABAAIDAQEAAAAAAAAAAAAAAAMEAgUGAQf/xABDEAACAQMCAwUFBwEGBQMFAAABAgMABBESIQUxQQYTIlFhMnGBkaEUI0KxwdHw4RUWUmKS8QckM1NyNDVzQ0RjgqL/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAMhEAAgIBAwIEBQMEAgMAAAAAAAECEQMSITEEQRMiUfAFFGGhsTKBkUJx0eEjUiRD8f/aAAwDAQACEQMRAD8A4SlTW1tLdSGOEZYKW+AqLOk7YPrXTauiul1qfAwcZwcedACxwoJPPavMnzrNHIcEsw2xleeKEbHqxEpKzOiGPHhY4ZjnGAKjqe8kjklV0WUSFcytI+ou5JJPLbp51BQgUpSpApW/tbWzksLOFrEPNcwTSGdXfWrKWwcZ048Izt8arvwiG3jmluruRY4ppofu4QxJQoM4LDY9557Y61Fg1FK378HsYYr5heNcLFbFlcIAUkEsa4wGIIw+M565xsK8n7PJFdG1W9EkyCQyKiq2nR7XssSM8hkAk7HFLQo0NKt2dotxIRqym48iNtiR8qqVNFVNNuK7ClKULClKkghkuJkhiGXY4GTgD1J6D1oCOrNp9kQmW6DylSNMC7B/e3QcuQyfTnVuBEhnig4aPtXEO8/6oUNGMZ9kMN/MsQMY286wimSyOLICa8Rmb7SoJCgA+wD6ZOojbpjGTAILyF0PfSiKN5Xb7hCMx4PVfw+gO+1VavQFZgkcCGW9mYmSSYDCDfkScciSWbljbGMmN7WFXZft9scHGQJMH/8AmgN9wSyFvaiV1IllGTnoOlanjdkLa67yNSI5N/QHqK16Z1YAzq2xXZ8O4dBw+3litwsty6hkvFj1SkjmlvEd35f9TYc/KueOKUcjm5cnfk6qE8CxKFV3v/Xc5u34LxCdygtmU933niwulcZy2fZBHU4FWrbgfHb2yiEEDSW0z/dqrqVZtgSBnA5YLbDbGc7V2PDjZNPBHeLGI5Svf/e4UMATi4kOBNIWI8GNsHpnOF4k3D3MMMxmuIJAJGgcJPImxwx5QQjYY3BwDyOa11HFRwdxYTWzy29yum5jbSI9YPIkNyyDgjGM1uTa3kXCla3gikjt7XLiTSGVZUYs3+Y4IIwSRgZA5V0/GjbcZtLlUswi93GsdzFF3kkhDFtEK7FgcE6uWM7bE1zFpwC9njvTeGRFj0siKPBrcc9gQNI2IG+cDapXmKZJxxq5M5msljZkdxyXGa357Lt3blb1S4GVUxkBjkbZzttn5VpLi3ntJDHPG0Z8jyPu860cWuTDF1OLNtjke/fmGKHvmMchyses4G+OVevHKzsskwbu0ByWyNO2APmKr5O2525V6GYcmIztzqDapepK0s7L3rTO23d5LkkDy91SabmSXuDOWMWFGXJA3AAHxNV9MmjOltPPltVu24de3OXRGUbHU5xn96q5RirZeOLLN1BblTSSjSahscEdd/8AavZYmhYK+MkZ2PKpLm0uLRtM0bKM+10PxryKB5t84z1xknfH8+XOpUk1aJ8LJr01v6ENKsG1YLnV1HMY/P3fH4Go5IXjGSMr5jcUtFpYpx3aMltZmtGutAEKto1FgMt5AHcnccuWatQM0lkschW3sxJ99IpOqU8xsT4iByxgDO+M5qtbyoJEN0kk0MfKMPpB9M9Bnfb6c6me4la9QXMBdI2JW1BIVM76QOYGeY5nzzvQoei0jmbv1DWtjr0CWXLnO5xsPEceQA5ZxzqxEHms2VWaz4T3uHckMztjIBxgucYwAABnO2SajvUulkhlvkUnUf8AlACndrnJGkAaATnYb9eoJzbiTycOktJrVmLkkMDpCHORpUDAGeg8z50TXqUm5pLSrKlxdhofstsndW4ctzOqTyL74JA8sAZPnVWh2ODShY3HZ2zS4kuppoLd4YYc95dSMkUbZGCdO7HmAvX4V1eXl4nbd6LhpbmAhklxHcSpuSSRtDFz2U5ODzzWi7IssLCYt3bmQokmnvWDEA4ijG+s49s+ECtrEoi4Y4ZI0htrjVNGzmS3U7eKdhvLJy8KbDbI5iqS5LLYlgCyCxT7TFGsV0IYJTH4F8WMWygHvD5yOM8jzqzxBbSGW/s7Vw0SIslx9qYrFHJzzJKuWkflhNxz8sVAyP3vEEJmZ7mLWfEBJOmCcueUEP8AlOGxtnalvIqyWN0jKkbIYoZIoi8asOa20J8WrbeRts58xUCyzbFrji0CzrKWubdtaXA0XEq4O5KnEMfkF3ODkHOav3qI1hGqOqJExRfwoRnlGBzA6k+QrX8DHdW8kehUjhlPeRh+8hVtt5X/APqSHyXYYGwrbzBlS7GqTVJHqyMF3GOZ/wACenPpWkNtzyevbk3B8V7/AB7fOjIAYgEEA8x1rl+O8Vt7pDbRRa9J2lJ5H0rorp2jtJnU4ZUYj34rgK2yOtjk+F9PGcnkl24FWILpYCpFrA7Ac3BOfhnFQDGd690jbrtnnWLSapn0MW09SNp/eC6/7UPyP70/vBd/9qH5H961RADc8j0r3SM7Hz61j8vi/wCp1fO9R/3Lt5xWa8g7qWOIAHIKg5B+dQLc7aXQY8xzG2KgIAxg15WkYRiqSMZZ8kpapO2Xox91nBAOcZHmNuh54+nTr7IusNgFhvy36k+Q6b+75VDb57hsNjxfpv8Ap6efSrCDOdbAnPX4+frv/Xaoex2Y/OkvUraCgKMvXfOf5/PfXWdkrL7PC9/ImZ5G+7kJyVXzA8yeu/IYx156C3FzeBMeDI1HyHw9a6+xKwAhiApIXl1zpH5Y/mT5fxPPKOPRF7s3wdPHVqa2XBsKV4c42514uF8I5dK+Zo9A5Ltbw5YJUvYUIWUkSY5avP47/Kucr6bdQJc20sEnsyKVJ8s9a4h+znFFdlW3DAEgMHXf1519J8M66DxaMskmvV9jx+s6aSnqguTHgF2ba8KhpV7zAHcyiFmOfZMh9hT1PoK6x1B75IShyA9u9vDpweotoPxn/wDL5Z8q5eLh6mx0OgL82dT7B3xk4PyyK2fCuOPxDiKrfHVPKhjkfWEafc4XWSO6QbZC7nfnmvTjkWRvT2OKUHCr7m2XQXiBWPRIumddeUV+eZ2/+4lzn7vzyOtWobCXuImaSdJlbxsp7q5dOgY5xBHy8I57cq84ZFHJFbzM2v7LKY7ZlTCKM7i3H4+g1vvyNbSRRm5hYRhEIdlkY6Ebzdxu7f5dx8qxy5WnSOzp+mjJap+/fvsU1sO7ZzBKgbIeAwwhVQdRDD1J5GQ74z5U4pcBIFjSTDyDxpG3Lz7xvxt+W9W7meOB1numkHeR4KNtNKPhtGvoN+daCeYzSaiAqgYVRyUdBXNlzSqrPQ6bocTnr08e/f8A9RDdyhbCcSMB922CTzOK46ytxdXSQtKsStnLtyGBmtx2ilZo0txDJ7WrXpOk7HketaOCGS4mWGJSzucAV39NOUsa1bnidd02LBnyPD5U/wCE+5sG4bBJazzWd2ZTb+2DHjI8wc+hrCLh8AsY7u7ujEsrEIqpqO3XnUl1InD7Z7G2YtK+1zJ02/CPma94iC/BuHSqPAoZD78/0NdVI8tTyOlq2b2e11X9u74+hXveHSWt6ttG3fGRQyFR7QP+1Z8Q4Z9htoZTMHZyVZQNlI5jOd99qm48Gils42yJI7dQSPPf9qxuyT2fsSdz3j/mahpbkwy5JRxyvl0/rz/ggt7KNrE3lzM0cWvQoVNRY499Y8Qs1tHi7uXvY5Yw6tp08/Sr8Oi17OrLKguFmmwI2OAh3323zt9aj45iVLO6Ve7WSLAj/wAIH6b0aVCGabzVe1tdu33KlrnuT7yPp7/516VNLJjIHPfz23z1/n51jYWb3EDujHIbGnGx/n6jzq4OF/c5MqiUH2c7H0Hr+u1ZOLbPYh1ePHCm9yHhdz9nvRrbEbnS2+Pcc10CvpAZdIYLqBAXI8IPny+mNuW9cn03xn4V0PCLkTWfdOxV4tI3xgjz332APPy3yNq8zrsX/sX7npdNk/pZ0UMgddTA6uWMHasywI2zn/xNauGSSMkqxXIwQc/qTWQlkByHbPvrxX0jbtM9KOJtWbJ5FjhaWQ6VVdTE9BXLt2vfUdFkpXO2ZN8fKpe0d5df2aI0wEY4lYcyOgrk69L4f8Nxyg5ZlfoeR13UZMWTRHY6UEEFlACJs69W93l8xWu4hbJ3i3JOIXzkLzGBsPjyzvWxJbXH3me+IHcYOwHr8MeVGRZe9jlUs5B7/f0O488bmtMWTw5avfv6/sYzjrVE3Zu9kWweOJ9GCVZVGMqR18+tbaWeV44syHSmQqg4C+6uR4fcNw2/MTFWjkIUnUBtnY+nurru6zEoVlZ2PhRd2bIHTp+udqv1MGp2uGep8PzY54lF/qRGI3MbSBfApwSfPyrCrK/duilo5JCPCMjQhPn0P+3urOK1+915JRTs2nAY+npXJkmscdTO7xEuTDisTNwiVV5pGSR7ga4K1uZ7SXvbd9D4xnAO3xr6JxD/ANuuv/hf8jXzUEg5FdHwWbeOS+p898TSk4qXG5sJeL8SkjeKSfKsCrDQo2O3lUNpf3dmD9mlKq3MYBHyNVtR86ajgDPKvatnk+BhScVFV/ZGZllM3fs5MgbOo7nNW5OMcRljaJ58q6lSNCjI69Ko5OCPOmo550tkyxYpVqV19CxaXt1ZFjbylA2xGMg/A1Hc3E1zL3lxIXfGMnyqPJPM+teE5OTS2T4cFLUlv6nSdmIo5LOYuise8xuPT+tXeNO1pw55LdEyTpbIzgEY/ao+zUDw8OLuCO9bUAR0xzq9f2iX1sYHdlUkHK+lbJeU+dz566ttvypnJQR20kal7xozjcEcv5+voamgW2hcSR3jqwPMMu38wBn48qlu+E/2Y4mkH2i3Iwx0boem2f1qp31jo/8ASvn3+n83+PpXLODWzPrul6nFmxqcWvv7/B1gIIyORpWq4fxa2cCFtUekbM+wx5czV83lqBk3MP8ArFePPFKLpo+nx9RjnHUpIw4jp/s641cu7PzxtXHVt+M8TFx/y9uT3YPib/F/StRXpdJjlCHm7nz/AMTzwy5Vo7GwXiriORe6UlzkMTuvurOG/nnaGCC31yA/h3L+/wCVayvVYrkZODswBxkVf5fH6HF40/U2pkkuJ5XjtokmgILTa8pCF6jGxyR656ZJrPhcN9LdmSzupo0uCEaZ18Tk4LAbnONznPLnjODTEstw1rGiQssZ8MHIdMsx2543OenQAVPDxaayvWuomimmkjCuWTCr/lXBGwAA+GBtuU4yjjcca37CMrmnJl9eKTWXEYri9iZIsHFp+IjGAzEjrz9ccgCDXUWnEoOJQLJBIGVQBo5FPQiuIV5ONcUE08SKMfeGMEA45ZyT6Cql/ata3LIRhCcofMVxdR0MepSvyy+h348ubHB5X5o3W59B4h/7ddf/AAv+Rr5rW14FdpA13BK6otxCUUnlq6b9OZrPhfALu6ugLqGWGBSQ7EaT8M1TpMcehU1klts/SzPPN9TpcUVOG8Oe+kJzpiUjWevuHrW3/u/af9yf/UP2rc3clpaC1sVkCuF0onXA8/5vWFU+cyZVrWyPV6TocHh1JW+5qf7v2n/cn/1D9qf3ftP+5P8A6h+1basZHWONnc4VQST5Cnj5fU6X0XTLdwRzMnDUk4gbWzkzpzq1ncY67D1qe54Ulgn2k6biJPbR8rnO2xFXp47Sykj4lbzzSLcMQ2tMDBI39OWRkg7g9CKq8e4ijSTWVq8c0IbHer+LB5j5V6H/ADa41x3PC/8AEliyav1O65/aqL3C+NLeXSWotxCNPhw2eXTl5Vua+exSNFKkqHDIwYH1FdBb9phoxc2/j84zsfga74z9T5LrPhslLVhW3v1NnxzT/Y9xr5aR88jH1rnbO4vZYe7ghVxHjJzj9f5z51jxPi83EAIyojiBzpBzn3mqUErwyBo5DGTsWHlWeXzcHrfCIT6SNTbV817o3aTcVRdK2cQHvH71qr28a7kVpAuVGNs451YvXuYIlBvu8EozpHVT1rXHHSsoR7nsdZnlXhpv63X7cAgYBHI15XpxyByBXlXPPYpSlSQZpLJGHEcjIHGlgpxqHkfMVhSlAegkciRQknmSa8pQWKsrxG+VQq3tyAOQErbfWq1KrKMZcqyVJrgzMsjS960jGTOrWTk5881vbHjsbKEuxoYD2xuD8Olc/SqZMMMipm/T9VkwSuDOvfilii6jcIfRdzWnveMC5jdEEsQHsFWHiORz8hjOwrUUrPH0sIO+Toz/ABLNmjp4X0LcnEJpoO6n+9wulC5J7sbeyOQ2UD3fDFaRgzllGkE7DOcDoKxpXQeeWZLC6SBJzCxiddQdRkAevlVarkfFb6KFYUnIjUYC6QdvlVR2LsWbGT5AD6CpddjLH4m+uv2PKUpQ1BJOMknFKUoBSlKAUpSgFK9VWdgqKWZjgADJJrpeGdiOK3sInuglhAeTT5DH/wDXn88VDklySk3wczSu1PYFCp0cXGemu3wPidRxWk4r2W4twwGR7Yz2+C3fwAugA5k49n44qkcsJcMtLHKPKNLSlK0KFhrOQW7TKyOE06wp3TPLP9M0u7KezWMzgKZASFByRjzqxO9mOHrFbTyB+cilP+o3qc8hvivLl7WS0tIY7hi0WVYtHgYJJJ+FWpHLHJO16W+z9P8APvkhisZ5ZYo0Ua5ULoCcZG/7VE0LopLDBDlCDzBqdrzXdyznKAoyoF/AMYAHuqY38LfZpDEVlR8ysN9e3te+opE68qe6KBikBIMbAgZII5DzrwgjGQRkZHrW1PE44mgUM9yELCSSQYLq3Neu1Ur+4W4udUa6YkASNfJR/M/GjSLY8mSTpxpFalKVBuKUpQClKUApSlAKUrbdlrOO/wC0llbTDMbOWYeekFv0qG6VkpW6O47EdlobO2i4nfIktzKFkhG/3QIyPjv8MV0PH7kxW6Bd2GW04O+Ph61eyANq0nabiNtw82stzKqqSyY674OcfCuHI3JM6cdaka1uKyjhpna2w2cBcnf15Zre9nrgXVi4kA8QBaMjYZz59K4F+0F93qyBIRG3jEQbcjlnNdp2UvUvrSe5jBEZcKCeuBv+dYY01I6M1OJyHb3svDwxhxLh6hLaRwrwjPgY53Hpt8D9OLr7bxuSBuF3aXKl4TE2sDmRg8vWviVejilqVHBNUKVIJNlBUFV5CvNYwQVHT6VoRS9TClZmQEk6F+VNYB9nO+d6Cl6mFKyDgKw0jxU17sdI8QxihFL1MaVlq8OMDOeeKM+rHhAx++aCl6mNKz7wZPgXB9OVYu2p2blk5oS0q2Z5SlKkqKUpQCtr2Zvl4bx2C7dNYjD+HOM5QitVWUbmNww6dPMVElaomPJ3E3b+770hLSAJnYaiSK5TjfEbzjF3Nd3Cu4AGAucRjlt5DP51Tl1LOBvpbdc+VdJ2Pv8Ah9ncXa8RTaaEorFsD1HPG+1czSNN09zlVuH5MrHA2HQ1u+C8d4jYRCC3lkSI76QdW/oOlah0w5AIIBxkdasQTKhABKn3UUUg5NnSXHam5uLGeyuokkkZSEd/Dz9PjXJVdvHV4lOSWzt4s1SrXHGtyknZOZ10FRCo8IXOBttueVeTTLIoAiVMHOQBXssyvbpF4mKkEEjGnbcCpFu1V86SVyuRgAbDB2+Va0irz5Gqr3/BEZlMZXugWOPExydif3HyqR7qNs6bZF3BwMem3L3/ADqT7bbhNK2q4CuozjqdqiaW2NsE7pu8AwGJ/m3Pp1ppQXVZeK/BhFOkZc90rajlc/h516s8YGDAh8Gn4+dZRXIjcHcrlCy6QAcf7CpFu7dNltRpBfGTnYkY5+gx8aaUR8xljsl+COO5jVDqhXUAoB0jpnNefaF1ZMCE6wx2HIdOVV6VFI28adJEk0glk1BAgxyAA/Ko6UoZybk7YpSlSQKUpQClKUBkrADSwyCefUe6ve7DMQjBhjOWOPzrClUcEyyl2ZL3Eq7mJwPM7D507+RTp0pgeuaipUKHqS5Lsj1m1MSQBnyrylK0KClKUApSlAKUpQClKVDdbsHqIztpQZNTTWdxBHG8selZM6Dkb4rG2jdpRpUs3QAZzXWzWy354fw2eMHuwEWWLnjYEkH3c653nfY2jjTW5yBifTqwCOexBIrCuo4/we1sEiggeZrmE7ukZ3B35iudmQJKYuen2WI05HqP50q0M18iWJf0kNKEEHBGCKVuYilKUApSlAK9GNJzz6V5SgRme7xtknH1oO770ZyU617H3WB3gbOoZx5UYxaWCK2dXhJ8qg17XsYkJ4cE7+1tyr3EW3iY/wAP9KlElse6V42CqPEwxlif2NYF4e4VQh1jO/vx+xqLJcVza90YAJjcnOPrXoEZPiJAz08qlDWmrdJMavpj3+dYsYMxaA4x7eevu+tLGhLujAiLGxb3fCh7vSNznHzOT/SpU+y4wxfIzvjnz9fd9ajJh0jAfOnf/wAv2oHGlexHWETgyv1A2FZ1UineGR8ANnIINZ5uKM48na9koIH41aALvhmbPXCn9SPlXeNw6z+1LfGLMsIJVs8s1857KEntLaoTg4ZTv10Ma725ubT/AKb3DQSDwkEbEiuSV2dmKOqOyKd3w573vZUkZZHBwQcYPQ1z3Eez9z/ZE/8AaEsc0yAtHIBgj0/OuuilW3iJ1Bgeorlu1vFccPeFThpRpHuzvVFLsWcFTZxjbokgydQ3z/i6/v8AGsaRkm2Unlrb8lpXowdxOKfIpSlXKClK2NrwS/uoVmWIJE3su5wD7utVclHklJvg11K21x2evrfHe92MjI3PLzxitdcW0tu2JF26EcjRSUuA01yRUpSrEFtbi3QgiIMCiBgUHMcyD/OfpWUc9muVMJA6HSGPTzqlSpsyeGLJs2/cDZ+9wc+QORj6ZqzFcWSoNcRZtKgjQMZGc/pVClLJliUlTbNhHcWKIAUbOxx3SnTsc7nnz61VieJYXEilnIIUYG3rn9KhpSyFiSvfkVLZ8Ma5kacaTHGcuOo+FRVLBPJbuWjONQ0n1FZzi5KkbwaT3LNpPLY8UiuUBLRyq2M8+W1fT7hbG7hWbKYddWG25+dfLY5UkvocAhWcZz7xX0+G3Q2qAgEaRXBktM7MTreLNdecQtbC2KwxhgvIINq+ecQv5uJ3RlkH+VEG+K6btdeLB/yVsPvWI1YHIVysYECnBDSZ2YHYDH51bDBydkZX6s8OyqgGAgx8eteUpXelSo427dilKnslRruMSDUuckeeN6N0rC5Ol4FwOKG1S/vgCz4KKfwgjY++sLu4N04hhOLdPZPU1O8wkiV0kfAGCuo7D3VqWuDFlYwCATv51x6tci8Jp7FmRAv3jTMG8ya8lErBTPHjWoOTycedVpIZGKuXDahzY4xUtwyC3jiRgcL+E9asnTLyVmsvrUQkPH7B5jyNVKuuWKlGJIPmapV0QlZi1QpWYdeqA8voK8LAoBpAPnViaXqY0pSpKilKUApSlAKsRX97CmiG8uEXyWQgVXpUVYsykkeVy8js7nmzHJNY0pUgUpSgFTWhxcL8ahrOGQxShwM46VD4JXJvLWcpMg6McEVhdiCGdl0Dw8vjuKiiP3kZXcagQfSrvEYxNAsyjLJsdun+/wCdcMpVNfUn9MijEUkjZ7h2OOQFVC2JBoOd9q9kjb8KkgelIIyx1tsByq/BpdloNayYM6NGANzHvqPxrUnnU0rkMwB61DW+JbWUmyctb6DhWDaRjbkce/rWMrQlfukKnPM+XzrKWRGt0QaSwxghcEbbg+e/WpluLUHJhzupxoHQD19D781rX1M5Z5V+kxMto51tEwY48KjAHLPX3/OsI5LZd2iJOMEefP126VKtxZqhXuS3hcAlRncjH5H51gz2ZtgAknfBcZwAM/P3/So0/Ut81Ju3D7fcglKGQmIEJ0BrCrUU8KMNUSspKE5TcY543/3zXqS2qSE90zL4gAQM4ztnfnjb0qUjKWRtt6SpSlKGgpSlAKUpQClKUApSlAWrK8+zv94hdPfuvurecPuYbiTSJkj2yCz4z6VzNKwyYIzdstaapo6Xi05WdbeJJWgXdzknVWg1tFqCkYJIxmocnGMnFKssVclnNdkekljk15SlaGYpSlSBSlKAUpSgFKUoBSlKAUpSgFKUoBSlKAUpSgFKUoBSlKAUqzJZOkaOskUmpgrBGzoJ5A/X5V7dWMlvCsveRSxltJaNtQDeRqaZmssG0r5KtKtrw+d20IA0gi70oOYGeWPPcGoWt5FijkxnvGZAo55GP3qKZKyQfDIqVm0UiMyvGylRlgRjHvrFkZMa1K5GRkYyPOha0zylKUJFKUoBSlKAUpSgFKUoDJEaR1RAWZjgAdTW0j4FNgG4ljiB6ZyaucLhezgJihZpZANTMNgfL3Vs4rKO5cyyRrK/Itjl6Vy5c+ng6MOB5DTPwJAvhmbPqK11xw+aAFsq6jy512U3C+7spZLaPDINRjBABHX44rWC2iRBLIxjjnICAAnc/wA+lUhnbJy4NDpnK0rd8Y4UsUcksIYvGQXHTSfxfUfOtJXXGWpWczRcne2MccUDlYwRr8G7Hq3P34FZzy28hit4p2S1VgdLJuNt2OOZqiQQdxjrXmDjPSr2Y+D9XsW1vX764nZiJZVGkjodSn8hVmTiEDi2YI6yL3jSN/nYAah8RmtZpIGcbV4AScCliXTwb491Rs0v4QYUmDSL3TRTOebAnIx7tqp3lwbq5eUjCnZV/wAKjkPlUGKUbsmGGMJalyKUpUGopSlAKUpQClKUAqSEuj97HpzFh/EMjmOlR1YszmR48ZMi4+oP6VDV7ESdK0djwPtLPxG2vIr1rGP2EiiRVQvkkHA64FbDhls0V0d1CNknCBdvKuEh0297FMwKmOVD8M5P0rtYXM8vext3g5AAnb5HlXDmhTPQ6TIpxNrBw8xwXWZFdpA2ltAGMjFcff8AE1laQ4UTRSOEAGkHSRp0j35P74zXT8VvprbhF0LYvJMCNHdruNxy91fO5bhZABpIePcE+frTBCyvVzSOyhgjn7OQ8djLOJpGjnhzqCLkjBPwHPzFcFW04Bxi6tUksFLPb3AKPEADn199asnJJrrxrY4296Mw7qQwzsNtulNTgBcHlgVK0lykILagjgL8MbfQ0ke5kw7q5wxYEr1O/wClXpl9cK2l7/kiYvjSykDY4x6bfSsRlTnH0qdp7hgC2cDxDbbljP0rxrqfVlm3I6jof9qbkt4271P+P9mAeUR5UEL5gfzz+tYl2wQfLHyqVTcLDpVG0BtWdPXY/oKxEsrNI43LA6tulKI1xa/UeNI7SF8HJ3rDS2cYOfdUy3cyoyB9mJJ+IxT7ZPv4+ZJ+ef3qNyz0PmT/AI/2QUpSrGIpSlAKUpQCs4ZGhlWRcZHn1rClCGrVM+g9m+CcIu7eO8XVcO+CRIQQjY3GK2TcLRbiTU8kb6iDpI5Z2+mK+e8F41d8Gu1mtmJTUC8RPhcfzrXe8P7XcF4kQ12zWFxjxa/ErfGscsHJbE4Z+G/Nwbm0toYUIUE6uZO5NWb/ALPcN4jAq3MGH0BS8ezHAxg+f6dKqR8a4BbDvX4vavp3AVwfoK0fGf8AiLBGkkHCoGlc5Anc6Qp6EDG/0rPHjktzTNmjPyrc5jtFwf8AurxaVLS8SVZ4iqqd5EU887YHXGN652prq6nvbl7i6meWZz4nc5JqGulKkZJd2SNMzRd3gBduWd8DFSi8kDFlVFJAGRnoukdfI1WpU2VcIvlFr7dN3ZTCYK6eX+XT+W1YveSPbiBlTSABnG+38+tV6UsjwoehYF24fWFQNgAEZ2wunzrIX0wDDCEMpX2cYB91VaUtkvFB8oUpShcUpUsdtcSDMcErjzVCaC6IqV6yspwylT5EYrygFKUoBSlKAUpSgFKUoBSlKAUpSgFKlaJQSFbr1+P7ViY8A5deRIwfWosu4MwpWZjxsGBOcV4UwCdQ2258+f7UI0sxqeytJr65W3t1y7fIDzNQV3XZLhyQcMW7IPe3Gc56KDt+9RKWlWUdvZEL8CtuEcIluQnf3igBGIyNRIA25czWlj4PfzuZp+IyCU8+72ArseMMU4c2OZOw860PDb1ZQ6OunA2xnf5iuHJkn2Z1YcON/qW5rTbXtvPbw3k6XNk8yIe8G6Anc+nlnPWrPaLsy3D42vLMg2oxqUkllOcfKpJZjed9F3YCaSvXPL3YrubFBLassgyDsQetXxZZdymbDGLuGx8epWx7QcPPDeM3FtpwmotH/wCJ5Vrq7DBO0KUpUkilKUAr1RqYLkDJxknavKUBMbcZA71N2xvkY9TQwAS6DKmNJOoctgdvmKhpUUzTVH0LC266WLTKCF1Ac87E4+n1rz7Me9Kd7HswGc7HIP7VmtnkDXJpDBCDp2Orp/PI1mnDizKGmTxY9kFufLbpU6WZvqcK5X5IFt8hT3sY1Lnny9KxWMGMMXAyxBB6etZ/ZZPswnyoVgxAOcnBAP5/nU9vw1p9P3mnUARlT1z+1NLIlnxRVsrCEd5ErSIBJjcHOnPnXhiwmrUvs6sfHGKtLw4+AvIVDkaSEJByMjH8FV4olkidi5VgCQCNjjnv50phZ8TWy/JDX1ewjVOHWyoBpES4x7hXyiu74b2n4fZ9nrQ3s+qdV0GKMZbAJA+gFY5VsXjybniSA2MmVzjB+taEtbq/hwcAhtAzilt2lPHeLwcM4fA0cc58ckuM6RksMDPMAj41SxJakpcwSuQ50tEoYkZ5NnrXHkh3Ovp8i3RsYxE6pp0lyQpx0/grqrLwWu/U1x9lA7pxG6kDRRxWrSQoNmRxgKT9duW5zXvDO21lJogulkgY82IBQH38/pU44UrK5p3KiH/iGqm7spR7TIwPwIx+dchW67VcTXiXFPuW1QwroQjkT1P88q0td8P0o40qFKUqxIpW14DwG745c6IBohQ/eTMNl/c+lTccNpwS5k4VZW8c1xHgS3Uqh9WRnCqchcZ586pKaRKi2aSlXLVbq0K3S2qXEYySJIdS+W4IrqLTgPD+0/DftfDsWN0m0qDxIzY8s+EfzFVjlTLSxtHF0qxf2Nzw67e1u4jHKnQ8iPMeYqvWpQUpSgFKUoBSlKAViy6hWVKgG27FOIe11gzbbsPmjD9a+tyWdvINV3bIWJwGxjO+2SK+IwyyQTJNC7JIhyrKcEGuih7c8eijCNcRy4GMvGM/SqOFiNx4Ol7bzRcN7OXMcUax/aSIlAHMk5P0Br5ZFCzNqb2fzrc8W43xDjJH2+fvFU5VAoCg+6tfRQG4pSlaAVPZWkt9ew2kABkmcKueW/U+lQV0XYNA/au2J/Arkf6SP1qHwLo+k8MsIeFcOhs4FAEajUQPabqx95rgOFWgm7TcUnuUWQrM4DPyzqr6VINjXHXnDLaS/vYp2ZFMnfAhsZDAfqD8q4sz8p19OrkXriS3ht2eZ4o41G5PKoOxKJFd8VWJg0EjpKmxGM6sjf3Vqu74XbcPNrJcKVjk1aGbxZrouzk9pICtpjBQHbpj/escTqR0dRHyGHbTgg4rwkzQqv2q2BdT1ZQN1/X3ivlNfeFr4Q40uy+RxXoY32POkZ914NQYY/XyrEIS5TIGM7+6sxbylS2k4ABPyz+QrxoHVgrYGWKc+RHP86tZdxfOkxZCoJyCA2KSJ3b6Sc1L9kkbJjVio/xDSfl8aweF40DMMZJGPdSxLHJK9JHSlKsZClKUApSlAKUpQClKUAredjLlLXtRZvK4RGLISTgbqQPritHXoJUgg4I3BqCHxsfdn5V8/wC2F+0fFwLVHfTARMVO2M+nUZ+tUP7zcTvbMRG5cKqBZAFAJ2xnI33qG0vo4LlHZWbClWGOYPvrnyY3pZtimtSZvbNuEPZd5B9gEbDeRwpkT0yf96k7H3VpJxbiDoyRKGWOBNQAcEncDrnArXS9nuELA14F+4xkhZGwfrWoSRRI5hBCHAQD8IGw+lYYY3K0dOeTUaZ9XurqKzs5bmZgscSliScV8N5nJrecW43ezWP9mSXEjorAsDg/DPM8/pWjrtjGjgvU77Fh4StuJe8bfA0kY2xn5VM3D2HOUHOSCRgexq61RycYzsK9LMc5Y7+tX2KNZH/UWltZGiaTvCMLrIYEev6c/PFRyW0qwiUjKFQ2c+dQ5PmaamIwSce+mxNZL5Lh4eQcCVTy6Y/Dq6/zY1HHaNIHOoDRGXOQenSq+onmTTJ8zU7FVHJX6vseUpSoNRSlKAUpSgFKUoBSlKA9VijBlOCDkGrS3xJ+9jVhj8Phz+lVKUIaRsBfxKhRI5NB3K6gBn5VA97KyMigIrZzjmR5ZqtSg0+opSlCRSlKAUpSgFKUoBSlKAUpSgFKUoBSlKAUpW24WVgtlnGVZ5HjaUMAUwoKgE7LqORk9BWWXJ4cbqy8IanRqaVf4nIZwsjAEhygk2y+MHcjYkZxnrmks1zDGjTQR6XOtdQzzA9eWAKiOVtJ1u/qS4JN7lClWjdPLc973EJOCSunY9TXk08hDrJDGpDYPhwQcEY+vzxVtUuK+5GlepWpWUkbRtpcYOAcehGR9DWc1tPAiPNE6K/slhjNW1LbfkrTIqV6FYgkKSFGSQOVepG7q7KuQi6m9BkD8yKm0RRjSs4onmfREpZiCcD0GTWZtLgRrIYiFZS4PoOv1FQ5RTpslRb4RDSszDKIBOUYRM2kNjYnyrxY3dHdVyqDLHyGcfmanUvUUzGlKVJApSlAKUpQClKUAqe1ue4Yh17yF9niLYDf1HQ1BSqyipKmSm07RNc3DTsABoiTaOMHZB+/metWTfxltRtySYBCw1jfCgA7DPTPOqFKq8UGkqLLJJOy69+CqhI3GC5JaTUTkAAchyxU1xxZJu8YWoWR2Y6tXLJY55c/EN/JRWspVH0+N1t+SfFn6l664gk9uIxbKhCKhIPMjHixjnsd/I4qQ8TiNzJcfZT3so8ZLhgNxnSCpxyI67GtbSp+Xx1VfdjxZ3dmytOK/Z4hG0JcBgTh8ZGVOOXLwn/UawXiQHDJbRodTuAO9Lbgas+XoOvSqFKj5fHd13v9x4s+LLVtetDdLMyBgqFCqAJkEEdB61hJcAqBF3qnTpYtJqyMDbkNtuXuqClX8OOrVRXXKqL0PETHDFC8IeOPcDURk78+n4j0qaPisaNMTanEi40iTABznVy3OQK1dKo+nxvlfkss013LN7dC6dCsfdhQRjOcksTn6gfCq1KVrGKiqRSUnJ2xSpI42mkjigjd5XOkKoyWPTArGWOSGQxyoyOvNWGCKsQY0pSgFKUoC7JZG3JSdGDhA+2CDnluDgqRjeozbpvjvdtPNPMb5/Tzq9PE0KhtReE4VJFUBDtk4HpkZTmTvVheHBroCC47+2ZVbvVbOByww/xenTpnasnPSrZ6McEZNRS39+/9Gmnt3hILKQjE6CdicelQ11HF7YS2WQoPd7nbcDrv9fhVDhHDIp5JJZT3kSNpUEY1Hz/pWceojo1snN8OlHOscO5pqV03EuF272zyRRCORBkaBjPpitTwq1W9usMgEceGPrvyPvqYdRGUHL0MsvQ5MeVY+74KcVvNNnuonfHPSpNYOjIxV1KsOYIwa7dESNAkaqqjkFGAKhurOC7QrMgJxgMB4h7jXOuu8262O6Xwd6PLLc42ldInALUZ1PK3luB+lRy9noycwzsvowz+1bLq8V8nI/hfUJXX3OfpW3Ts5xHLiSEqyEfcrgyuv+JU5sOW/wCxrbz8E4fcQwsZzGjoqrdRYeKE9Fm6odwMnGPXfGzyK0l3ORYvK23TXY5GldPc9nYZQEg/5W7ABSGSUGK7G28Mp2J3Hh+uxqpN2buGt+94e73UkYxPamIpcRH1jySRuNx+lXtGVGss+H3d8s7WkDSLbxmWUggaVHXeoZopIJDHNG0brzVhgitvwC2vZxexW16lvoTW1ux8dwy5IRV/EdjtVu24Fe3rQMXWKKGMOrzxoGPLYrnl0AY788b4orbKZMkMauTo0dgbP7Sq36yfZ2IDtGfGozuR0+dVq6X+6bHJW7JAzgiE7j51W4pwua3giVLeJ4YyzF4kIc5A2bJJxtt5ZNWcWjDH1uDJJRUt/wBzSYOnVjbOM0wufaPwFeupVsMADzwDnnXsMgilVyiPpOdLjINVZ1qrNvYcI12ks0oDNJGe5B9RsT9K0pBBIOxHOt4naEhQGtRt5PgflULcRsHYu1iSzHJ3HOuWEsyk3KJ6WaHSzhFY51XNp/4JV1QgzjEkbuqlJt3OMHcA5DHbBGNs71EJRDNN/Z0ki27LjByfDjr0IBxk/EYNT2KTNifhzqs8aEiBfEcZ6Z2ZSTuNyMeVBi4fvLF5YrksMwKp6DIKnpgZABOTjmc4GwbTp+/z7+xIn2me1na1lkbQuXjxkhSDkrncrgbHn5+dV7HiS2VxNHP3hjZi2SPED6/0okC3issT4uzKwSMj/q6h+HybzBPXbHWmIoHWWGZjBdRthNbeAjkU5bHPUnG2+Kq8cZJpjJ1OTHJSi9179+62vEONQfZ3jtXZpGGAwXYfP0qr2enVbqWJ9mlAK+WRnb61p69VmRgykhlOQR0NR8tFY3BdzJ9fklmjll27Hc0qtw2d7mwimkxrYHOPQkfpVmvIlFxbTPqITU4qS4Yq/wAKtPtE+8ccrAahC7Fe8HXSR1/nSqUaNJIsaY1OQoycbmuh7kmL7AsLSY8Utk7eKL/Mj8jz5b86tjjbsw6nJpWlcv374/uiK4RLq2Nz3szJbnEdwNri1bpkcmXfHuJrUzLPazoskkFvecRJX7So1WvEAdisq/gbfcgfiPLnW9Lh3W7NwzxQ+FL5VOqM/wCF15kb88da1F6ptr6YxCOG54muDbyZNtxEHI8LfgffqR7Q5ZNd+CW9HhdZjVKXv3/D9V3ddYBLLJwpbFpIbX7yfg0sh1IdsvBIDk884JHtHzrxLsNAvGZLiS4WJhHBxRV+9teX3c0YwGG5BO/te6sobJLuNeDCOWS3t27yWzkbTdWJ38SNjDqc+vtD31vEgj+1R8SaQPOi90nEANm9JFxsd8ZwOYrrUbPIy9RHFs+ffv8AwapuFxSRrxK5t44eIEiX7TBM3d3QI9tRyBOckbdfOpLRcv3rQ95FFhpFzjIzj9a3EaqokiMapJcDJgY5il9Yz0Plv19K10wtrdJEgMomclGSUY0rz39cgVtDZUeF1jeSaySf7e/uTS3yQ2wgg0yRSIGIJI0ncFTjnt+lUYYHu3m7vQO7jMrdNhzwKi8I9oNnG388sVsbLi01tay26IDrGI8Egpny+dXacV5TmjOOWX/K6SXY4XtPw17a5+17hZSNQJ3BIyPgRWirtO0Sd7wudnwWVg+fXP8AU1xdZzVM9z4dmeTDv22FKUqp3n//2Q==', 1);
INSERT INTO `photos` (`phoid`, `ofeid`, `photo`, `orders`) VALUES
(150, 87, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAA0JCgsKCA0LCgsODg0PEyAVExISEyccHhcgLikxMC4pLSwzOko+MzZGNywtQFdBRkxOUlNSMj5aYVpQYEpRUk//2wBDAQ4ODhMREyYVFSZPNS01T09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0//wAARCAHgAQ4DASIAAhEBAxEB/8QAGwAAAQUBAQAAAAAAAAAAAAAAAAIDBAUGBwH/xABYEAABAwIDBAQHDAUJBgQGAwABAgMRAAQFEiETMUFRBhQiYTJSVHGRkqEVFiNVYnKBlLHB0eElM0KT0wckJjRTc6LS8DU2Y4KjwkNWpbJEZHSDpLPD4vH/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAMxEAAgIBAgMFBgYDAQEAAAAAAAECEQMSITFBUQQTFGGhIlJxgcHwBTKRsdHhI0LxM2L/2gAMAwEAAhEDEQA/AK2tI30VQlhBvcSbt31JC1M5MxQCYk6jTmYga1m6246UYde4aUXinWXSlIcQlBIXqMwTrxEjWImvpO1SzR09386PJwqDvWZPE7B3Db9y1dk5T2VRGZPA1PwzCrNxll7EHnx1hC1Mtso1UUkCMx0kmQB5tRIljpBiScUxRVw3m2WRKUBQAIEag/STVtgWMst2lram7TaLYS4CXWitteZQUNyhBHf30ySy9zF8+f6fB8xBQ7xrlyK3GMIas0G4snnHbcLyLDjZQttUSAQQN/mqHh+G3eJOluzZKymMx3BM8zVpjuLNvMOWrFwLjauhx1xLRQnQaAAknvrzoxjbGEG4Fyl1aHcpAbAMETzI50jPMsFpXLl97BxxvJV0h666IXVthy7gvpceQJLTaJkdxJH2VQ2jAuH9mV5AELWSBOiUlR0+itJinTFT7bjFjbBLaxlK3gFEgjXs7vtrMMurYdDjSsqh3T3ERxHdU9n8Q4PvePIZe6Ulo4Ey8wp6xU83eKS26hAcQmQQ4nNl0M/THIHdUAAkwNTWt6OY5YWrZF46ptSWUIlSCrMQtxRiJ8YVnLp9CMWeuLIhKEvqWyQmABmlOh+jSrYsmRylGS4c+T+/mVnCKSkmedQu4SUsqcSvQKbhYmJgkTBgTG+Kbet3mAkuIhK5yrBlKo3wRofoq2YxiwYQW28LcDalKUpHWtDKSmPBmIUrjx81V97dtvpbat7cW7DZJSjOVmTEkk+YVaE8jlTjS+/MiUYJbMkYJhKcVcfDl0m3QyjOpakyI9IivMdwr3HvUW+322ZsLzZcu8kREnlUjo5eWdt11q+dDSLhgthRQpQ17hrx7q96V39riOJtvWbm0bSyEE5SNZJ4+esteXxGn/X4fUvph3V8ykooorrMB5i1uLlLimGVrS0kqWoDRIAJ1PDQGrK2wQXWIXljb3C1PWyVkAtgBZSYic2kmoVjiNxZN3DTShsrhsocSQNdCBr3TNWthi1vYYxf4iF7Tbpc2TYSZlSgRPDz61zZpZVen5enE1goOrKVi2W7coYUpDJXBzPKyJAIkEk8I1q8V0aZS8G+tXpHFYw5zKPbNURd2z6FXK1ZQEoJSkSEgACBpwFWYxUG0ceWub6YSspVtDP7QXm7MDSAI7jJicve7aXQho3tEDELQ2N67bbVDuQxnQZB/A8xUlNlZm0S511tTymC5kzZcqgVaGRvgAZd5Jndvh3b4ubt18ICNooqy5iqJ7zqaZrTTJxVvcpaTewU80wpxta9yUgmY314whpSiXnUoSnWCDKu4QPtqau5t+rqbQv9kgCDyqZSa2SCS5lbSy3DRWTBBAyxzoQEwSpSZ4Azp30pRAaUM4UpSgePf+NWbIHBZPKDOTZrLxCUhLiTBO4HXQnv5GhyxuG0FakblKTAMns7z5hG+nReWyDaqZtChbCkqUdoCFkRJ3SJjnA5a0prEcrlyVtkt3CyopzSE6k7j2TrG8cOFZ3k6ff6lqj1GbW1D6CtSyADEAUkWpVei2S4hMnw3DlSBEyTU/CVWAtVdbviwvOYSGSuRA1kfTUd1ViMYzLcW/aAglSUQVaboJHHTfUa5amt/wBCdKpM9RhLirl1k3VolDYSdspyG1FQkAGN5E+g01Z4dc3l23bNoyuOSU7Q5ZAJBOu+IO7kalOXlm+46i4dfUy4ptZUhhKCMgKQkJzQBlO+eG6kYZfs2mMsX720VBUp0JSN5zDTXdqPbVdWTS3zr6fyTULQzbYe5c2zr6HWU7NKlBC1wtYSJOUcYFQ6n2txbW9k6EuvIunEqQohlKklJEZQSrSeJid0cZgVrFybdlGlSon9XZ8T2mjq7Pie007RVLZakNdXZ8T2mjq7Pie007RS2KQ11dnxPaaOrs+J7TTtFLYpDXV2fE9po6uz4ntNO0UtikNdXZ8T2mjq7Pie007RS2KQ11dnxPaaOrs+J7TTtFLYpDXV2fE9po6uz4ntNO0UtikNdXZ8T2mjq7Pie007RS2KQ11dnxPaaOrs+J7TTtFLYpDXV2fE9po6uz4ntNO0UtikNdXZ8T2mjq7Pie007RS2KQ11dnxPaaOrs+J7TTtFLYpDXV2fE9po6uz4ntNO0UtikNdXZ8T2mjq7Pie007Uu2w67uWlONMLKEpKgcphUGIGmp/A1Ep6VbZKjfBFf1dnxPaaOrs+J7TUhxtbSyh1CkLG9KhBFIqdT6kUNdXZ8T2mjq7Pie007UldheoQVrtLhKUiSS2QAPRUOdcWSo3yIPV2fE9po6uz4ntNO0VNsikFFFFQCzaKtij+eYansjRbAKh5zkOtDpVsV/wA8w1XZOiGAFHzHINa9Zxt5plDQtLNQQkJBU3JMc9aHsbedZW0bSzSFpKSUtwRPLWuXTO/y/t/BvqjXH9yrooorqMCThyUrxK1QtIUlTyAQRIIkVc3ti4hq+Xe2jLVugEsONpSlUzCR2d4M8aorZ7q90y/lzbNaVxMTBmi4d29y69ly7RZVEzEmawnjlKaaexrGSUaJnuaku7ND+YoY273YjImAYGup1+zXk+rD27gW1yX0tWr52SFBkDKoaQoTxg6ye+oqMRUi7LyWwUraDLiCfCTlCTrw3TTzWLBoMNi3zMWxK20FepXwKjGsSdwFVks21fQlOHMS5h5tLlLLi0KeLDi1oy5gjsKIG/fp9Gm+ljCms7Darspcft9skbLQaEwTPdypp7E1PuNOuty6hhTKlZvCBSQDu3668+6hWJzcWzux/UW+wjN4XZInd37qVmr5eXHcXjHkYKq4atXbR8KRcKUkbROQpIk6wT4pqM5YjqBvbdxTjKXNmrOjKoGJ3AnTXnT7GMuW9lasMtJC7dwrC1GQqZ0j/m50w9fA2Zs7drZMFzaEFWYk7t+mm7SOG+ke+1b8L8uF/wAUH3dbHthYdcgJU6VKUE/BslYR3qOkDzTxpNvZBzFOovPBs5y3nCcwzDTu3mnLbEUtItkvMBzqq87UKy7zJnfPD86i3Lxfu3bgAoLjhXE7pM76uu8cmuCK+ykiQxYJdTblb4RtgtZ7M5EJmVH0HTupTeHIuFW5t7gqbdUpKlLRlKCkSZEnhrvpd3jC7nE03nV2wEoLezUSQUkEGd3jGmxiRaWx1RhLSGVKUEqJVmKhBn6BFV/zP7Xn/Rb2BdvhiLtTXU7pCytRSUOpyKTAJkgE6ab+ZoxGzeQwm6UhSUFWzhbAZVMT4I0jvrxnFBarQqztGW8iiqVSpRkEQTppru7hUV9xlYCWGNmJmVLzHzTpp9E99IrJrt8PkQ3DTtxGKKKK6DIKKKKAKKKKAlYaw3c37LTqkJQVpkKJGYSNBHE1fYfYN4y27dXjjkBZbbaQYDIEaARv3VmW1qacS42cq0EKSeRFWRucPvnHX79DrTxTPwBGVZ8xBg/TXNnhN7xf3/ZtjlFbMLnNeYQm9uHgp5p3YgnetMAj6RJM1V1JvLvrOzQhtLTLQIbbSSYkzJnee/uqNWuOLityk3bJeFm3TiVubsAs5+1O7unumKsLBrE/fAhTwdS7ml1Sxpk490QIHDdFUlPC6uA2lsXDoQkylIWYB81VyY3K65qiYyS4hdbLrb2wENbRWT5s6UzUy+vE3oacW0E3ABDqxuc3QY575qHWkL0qysqvYKK0PvWd8pT6n50e9Z3ylPqfnXP43B73ozXw+ToZ6itD71nfKU+p+dHvWd8pT6n508bg970Y8Pk6GeorQ+9Z3ylPqfnR71nfKU+p+dPG4Pe9GPD5OhnqK0PvWd8pT6n50e9Z3ylPqfnTxuD3vRjw+ToZ6itD71nfKU+p+dHvWd8pT6n508bg970Y8Pk6GeorQ+9Z3ylPqfnR71nfKU+p+dPG4Pe9GPD5OhnqK0PvWd8pT6n50e9Z3ylPqfnTxuD3vRjw+ToZ6itD71nfKU+p+dHvWd8pT6n508bg970Y8Pk6GeorQ+9Z3ylPqfnR71nfKU+p+dPG4Pe9GPD5OhnqK0PvWd8pT6n50e9Z3ylPqfnTxuD3vRjw+ToZ6itD71nfKU+p+dHvWd8pT6n508bg970Y8Pk6GeorQ+9Z3ylPqfnR71nfKU+p+dPG4Pe9GPD5OhnqK0PvWd8pT6n50e9Z3ylPqfnTxuD3vRjw+ToZ6itD71nfKU+p+dHvWd8pT6n508bg970Y8Pk6GeorQ+9Z3ylPqfnR71nfKU+p+dPG4Pe9GPD5OhnqK0PvWd8pT6n50e9Z3ylPqfnTxuD3vRjw+ToZ6itD71nfKU+p+dHvWd8pT6n508bg970Y8Pk6GpcXkQTVb1fHPjLD/qC/41Trr9SaerwT0ir6tjnxnh/1Bf8AGo6tjnxnh/1Bf8aovSLpLb4KxI2bruoy7SIIG6BJnUdwG8iROId6TdIsWSHLdt1trOlJdazpQ2owIkQI1HhSdd9Sb4+zymr4I6D1bHPjPD/qC/41HVsc+M8P+oL/AI1c+ucc6QYM+yq/cuytYOYOFScpCiCEyShWkGYPhVqej3TS2xINs3iCw+TGYkZVb479w1MQO7ShM+zTitS3RcdWxz4zw/6gv+NR1bHPjPD/AKgv+NVoKqcfuv0HdLtLj4RtaUFTS9UqzpBGm467qWc4rq2OfGeH/UF/xqOrY58Z4f8AUF/xqjrxe6Q8lxSbfq6r42mQTnG8ZiqY3iYjdxptrHrhLK7i5bbTDbiwxs1pV2TAheqVjmREb6WCZ1bHPjPD/qC/41HVsc+M8P8AqC/41VOM4lfqwy5bAUkBtKzcJYcZGqwCgZtZ1meIkRxp0XK8Lu1WNmxatjbW7RUG1a50qkxm7ufnmlgserY58Z4f9QX/ABqOrY58Z4f9QX/GppzErptx23UtgPNvpbzhhaswKM2iEkmfpioL+MP3/Q+5vUzbvoUlBU2ojXMnUcRvpYLPq2OfGeH/AFBf8ajq2OfGeH/UF/xqgYfc3jGFC7ctH9opDaUKXdKe2ubeoInfxjQ6xIqZ7oXSnbVhtduVvOLQpZaWMsIzCUEgpPcTqIPGlgX1bHPjPD/qC/41HVsc+M8P+oL/AI1QcOxy8um7R95Nq206pxC94gpSVTM6DTv3d+i7nEXLiyvGV5VpdsnXEKSytAEAaAq8MdreI3btaWCX1bHPjPD/AKgv+NR1bHPjPD/qC/41VDmLXWGXGJurVt0oDKG2yDlSVIkHfoIGvMxuqeMTxAuBlLKMyn0todcYcaSoFCieydZBTz17t9LBI6tjnxnh/wBQX/Go6tjnxnh/1Bf8aqu0xV9u4umUosWnTdKQpSGVlTkTJCEyVHTmI+mrrBb1eI4UxdupSlbgMhO7QkfdSwM9Wxz4zw/6gv8AjUdWxz4zw/6gv+NVpRSwVfVsc+M8P+oL/jUdWxz4zw/6gv8AjVaUUsFX1bHPjPD/AKgv+NWK9+2OcsP/AHC/89dJrj2IWT2HXztq+IW2qJjRQ4Edxrp7NCM21I5e1ZJwScTRXvSXpTh+z65bWTO1TmTmYXr/ANTf3b6Q10r6SPWr9y21h5Zt8u1VsVdnMYGm0115UwnHbYKRmS+s51LLkZCkmOCCnMrTw5STJqPb4laG2xZi5LyBfrQpK2mgrLCyrUFXfzNbLFGt4mDzyvaX7EpXTTHUhMjDu0JEMqP/APJpSmemHSB5ZQ0jD1KCVLI2KxokFR/8TkDSMCety+2lNwhhDDrai86tLaloStSimJ3HQmJ1A0O8JdxjDxhLNta27qHkZhmcTnhKmlIUArNOqjOgA7tNTxwulELNkq3L9iZhnTPE3MStmb1Fopl51LR2TSkqBUQkGSo8TyrfDUVxyw/2rYf/AFjH/wCxNdiT4Irn7TCMJJROns2SU4XIauv1JpGI3XU7Fx4AFcpQgHcVqISmeQkil3X6k1D6RNF3BX0/sgoUs8kBYKiO8JBNc51RVySZx+7eXiuIrdceddGZxRIbSFJbErKsoIExmJGms6610boEcPXgLa7LIl06PoCyohUxJBPZJAB00rndspTd91Ny3Qh4NO2vYgFS1BQGYkx4RAndFXH8nt3izGLOYbast7FSgu52qSC0E6GNRqdBGv21GRWj0e0puNLgdHxSwaxPDbiyejK8gpmJyngfoMH6K5LdIuWGbVlb6EN2q3AlDqEy28kJLiTAMyYidCI3a103pJ0gt+j9kl95CnXXDDTQMZo368IrluJupW8Lt1tO1ubhy6VblRISheVSQSI368jEbqrisy7JqvyOqdDsTGKYA04A5maOyWV8SADpzGsa66a1OXhGHrvOtqt/hs4cnOoDMNxiYn6Kov5O7J63wPbvoWguwEJKhBQJUFAcJzn0Crxq0LeOXF0luG3mEAqnesE8PNlq5y5klNpHgwTDet9a6tL20LslaiMxMzExSmMIsLfRtjshKkBKlqUkJV4QAJIE1SXWHYu8ppxIfW62826A88kthfazFIB8EEIgHWCe+n7LD8WfU2nEbu8QgJUpRQtKDmJT2ZSoyPCjQR9MUMyyTgmHJt3GNgotuJSlSS6s6JMgCTpryp5zDbN24L7jMuFaHM2Y+Eicp38JNVdixi7WKsFW3TZJTkUhxaVQMgj9oyc08J743X1ARH8Ms31qccaVnUsOFSXFJOYDKDIOmlIRg+Ht2DlihiLZxWZSM6tTpxmeAqdRQFenBMPQjIlpzKAIBeWcsEERrodBqKdaw20ZcQ4htWdCy4FKcUo5iMpJJOummtS6KAhM4TYMNIabtwG2ypSUlRIlQg7zrIJpLeC4e1mysr7TRZOZ1auwf2RJ0FT6KAguYRYOOPrct8yn0BDkrVCgIjSeEDXfTjOHWjISENq7Lm1BU4pRzZcsyTy0qVRQFa5gOGOuqdXbHOpZcKg4sHMd50NTLS1ZsrZFvbIyNInKmSYkzx89PUUAUUUUAUUUh8ui3cLASp0JOQK3FUaT9NALqJf4bZYk2lF7bodCfBJ0I8xGooZ64CoFKQkqJGcyfCVy4Rlrxh6+WhZftUtqDwSEhYVKNJUDPOTrBjhO+FJrdFW01TRB96eBeQ/9Vf8Amo96eBeQ/wDVX/mqbaOYioAXTDSDk1KTpmyo7zpmKx9A+lTDt6VfDWyAgrA8OFJTkBmNQTmzDfu51fvp9WU0Y/d9CB708C8h/wCqv/NR708C8h/6q/8ANU5vrpeWpSEJBQAMx0kFWoAPKOVOFy7Lr6QwhKUp+CUVTnMceWtO+n1Y7vH7voUGLdHsJsbZi5tbTZvIvLbKraLMS+gHQnka0yfBFVONlxWENl5ISo3lqco4fDt6VbJ3Cjk5bsvGKS2VDV1+pNPHUQaYuf1Jp+qljCdIOj4snnLhi0s1sO51LKrY/Bp3kdncIA13wFZYUe1Hw55TWKtNWt5s3bnKlaGm1JcHYmZcnMnRUZxmAIAgGTo+lGCX+LC2Xh+Im2XbrC0oUOyVeNI1BAJ5jzVn7nDMdt71165w5m9bKuwgthxlEkyQhJBKt3ajiqjVnbCSlGmyHjl43dhnrTqrhba3UKauVsbRqCoEAJ0zEJ0lChJEGYqTY4QnFEBm0tlhCHsqnrllJT2T2jBMyoBEjQ5piApRDabLGr5tty1wO3s3tZU1bbAjWIUFmFpIkxFX2C4FjVtiq7y9xQIZISEW7ABSE78kEQkDQdnfzFEqJlJQjs0jR2ds3Z2rduyCENpgTvPeax990xxBGNO2Npb2qUoeLKS7mMkKiSQRArbCuZ41g+L2vSJ+5t7R9zM+X2nGWysCVSOG8cj9lYZ5SSWk6PwrHhy5JLLXDazTOYrjbePIwtSsPOZou7QMuTAmezm1Mg1DV0jxxNveubPDyqyf2TgyqCf2pIJWOKYA3maolL6TKuusqs7sulstE9T0KCSSCnLB1J4U2G+kYbu2+o3ZTeKzPg2k5zMz4Omp4RWDyvlZ6cew41V6OX77+hdv9J+kVvbdZdsbHZCJUmVZZJAkBZI1BGvEEb6uuiuPuY4w+H2UNusZcxQTlVmmIB3buZrELZ6RLtnLddlelpwAKT1U6wpSx+z4ylH6a1nQbCbzDra5evG9l1jJkQfCAE7xw37t9WxTm5rjRj2/s/Z8fZpOo6r2r5f2amiiiuw+cCiiigCiiigCiiigCiiigCiiigCoOL4j7mWZfFq/cq1hDSSY0mSeA76nVFxT/ZV5/cL/APaaiV1sVldOiG1it3dYLb39jhu3ceOrO3Cco11zEa7h6aiM9I7kX1xa3uGC3VbW6rhwJuUuEJA3QBvOmhPGnejj6LXofb3Dk5GWVrVG+ASTSOiyBdtXGNvJSX75xUHihtJyhP8Ah+nSsk5PTTMU5PTT3ZIaxuG3TdW4C20tKCLdzbFYcmAIAk6HTlrrVvWRubZNo9trIKwu3buNihbqIQhZCsz0HRQIhCZMandVw29dX/Rd1adblxhxKFI7OdQkJUndGaAR56mE3wZaE3umTWsRsXn9g1e2y3pI2aXUlWm/SZqhV0pCsRu9m5YixtEmc7vwj5yk/B6wdQB//umbZNs9g+GWOHNpTjaLqVEMkLTBUZKo3Dsnujuq1t7S1KOlU2zJ2WfZ9gdjsq3cqy72Uqr72Me+nOq+9rJRxO4xTB1PvrsshvLRTbTDhUtsG4Ro536D21qk+CKzCGGWuh1gttpCFuO2RWpKQCo7dvfzrTJ3CuiF6FZ0Y70qxt8FTRAoTdtFCSrMgkSUlOo7tNKVXmVPIVctYdaZ8Y+qaOtM+MfVNGVPIUZU8hShYdaZ8Y+qaOtM+OfVNGVPiijKnxRShYdaZ8c+qaOtM+MfVNGVPIUZU8hSibDrTPjH1TR1pnxj6poyp5CjKnxRSiLDrTPjH1TR1pnxj6poyp5CjKnkKULDrTPjH1TR1pnxj6poyp5CjKnkKULDrTPjH1TR1pnxj6poyp5CjKnkKULDrTPjH1TR1pnxj6poyp5CjKnxRShYdaZ8Y+qaOtM+MfVNGVPIUZU8hShYdaZ8Y+qaOtM+OfVNGVPIUZU8hShYdaZ8c+qaOtM+MfVNGVPIUZU8hShYdaZ8Y+qaOtM+OfVNGVPIUZU+KKULDrTPjn1TR1pnxz6poyp5CjKnkKULDrTPjH1TR1pnxz6poyp5CjKnkPRShYdaZ8Y+qaOtM+OfVNGVPIUZU8hShZBxeb22Zt7cFRNyytSogIShxKyTPPLGk6kcJNWA3UkJA3AV7NBZ5NE0maJqStipomkzRNBYqaJpM0TQWKmiaTNE0FipomkzRNAKmiaTNE0FipomkzRNAKmiaTNE0FipomkzRNAKomkzRNBYqaJpM0TQCpomkzRNBYqaJpM0TQCpomkzRNBYqaJpM0TQWKmikzRNAKmiaTNE0B5NE0maJqStipomkzRNBYqa8mvJomgs9mia8miaCz2aJryaJoLPZomvJomgs9mia8miaCz2aJryaJoLPZomvJomgs9mia8mvJoLFTRNeTRNBZ7NE0ma9mgs9mia8miaCz2aJrya8mgsVNE0maJoLFTRNJmiaCxU0TSZomgs8miaTNE1YrYqaJpM0TQWKmiaTNE1AsVNE0maJqRYqaJpM0TQWKmiaTNE0FipomkzRNBYqaJpM0TQWKmiaTNE0FipomkzRNBYqaJpM0TQWKmiaTNE0FipomkzRNBYqaJpM0TQWKmiaTNE0FipomkzRNBYqaJpM0TQWJmiaTNE1JWxU0TSZomgsVNE0ypZzhJ0B40pJ0pRFjk0TSZomgsVNE0ma8mgsXNE0ma8mgsXNE0iaJoLFzRNImiaCxc0TSJrxRMaUoWOTRNIB01omgsXNE0iaJoLFzRNImiaCxc0TSJomgsXNE0iaJoLFzRNImiaCxc0TSJomgsXNE0iaJoLEzRNJmiakrYqaJpM0TQWKMEa0TSZomgsVNE0maJoLFTRNJJ00rwEka0Fi5omkzRNBYqaJpBUAJJpKVEpzgggndNKFjs0TSQdOVE0FipomkzRNBYqaJpM0TQWKmiaTNE0FnhKsx081LmkzRNBYqaJpM14TAJpQsXNE1WYfdOv3LqVrlAEpEDTWrGavODg6ZlhzRyx1RFTRNJmiaoa2KmiaaU4EkBUidx4UZzmEmARu76mhY7NE0maJqBYmaJpE0TVilgV6kcRz40qaTRNBYuaJpE0TQWLmvJpM0lbiW0lS1JSkbyTApQsdmiarV43hjbmzVfMZuWcUtOLYeoSm9Y/eCp0voRrXUnzXk000828jOy4hxO6UqkUuaiibFTScozSPpHOiaJoLFTRNJmiaCxU0TSZomgsVNE0maJoLFTRNUTTi/fAsFao2hEZjEZJiruatKNFYz1WKmiaTNE1UtYqaStYCFEkaCvFKhJPIVyJnEr53GAlV6+Gi92gXTlCZ491WjGw7adckdIwpQTdGeKTVxM8awHSK+QcMcNjepDoWCNi52onXdT/APJ7e3Fyq5RcPuulIkZ1k8q37RFOVnD+HqUez2+puJomkzRNcx3WDmqd5EcuNBzEiVHQb6JomgsVNE0maJoLEzXhUBvIFQrF/wDRrTr6x4OqlGs30mfZdxqw0D7JBC0JfDQVoo+ESAOBq+nj5GUZ6qS5mw2ifGHpo2ifGHprEZMO+Kv/AFhv/NRkw74q/wDWG/8ANVbib91LqbfaJ8YemvZrnWLm0bw1xVtYlh0FOVwYkh3L2hPZCiTppW3sbq36pbt7ZGfZpGWdZipStWjOfsNKT4nuMYiMMw9d0UFeXgDXNsZ6W3+JtlkENNHgNDW36Yn9AO+f7jXJ6twWxfBBTk9XIdStQkg6nUzrNKDqxxpTCWlJ+FDpOngAVIUzaZuyH4+cPwrQ6ZTgnTRMsOlGK4fbpYt3UBtPApBre9EsUusVw9b92oFQVAgAVzfqtupsrSLgJGhVAIBrddAdMIcHy/xqJLZnLlcNnBVv9GauaJpM0TWJSxU0TSZomgsVNE0maJoLFTUa5xC1tZD7yUkcONeXl7b2LG2unMjcxMVzDpbdsYpjgcsXA6lSQkEaa/TWkIXxCuTpG2tr62cxdVyl5OyLvhE/IitA0+08JacSsfJM1xRVhdp3sK+iDWp6B3CLF+5VeKLYKQE5gedaTx2Q4LHG1JM6LNE1Ft762uiQw6lZG8bjUiawaa4hST3RhsV6W4jb469YNBGQOBAMDcforEyFOKUVZZJO6rrFRm6avdzoPsFUzUwSGgvzg6VsdHZ0kr8l97hCP7U+g1Z4LjNxgzT71sAVqKUkHlr+AqB2vJk+g0hKSWHx4oCvaB99QazSlGn5fex1Xopir+L4Uq5uYz7QpEcoFXU1lf5Pj/R5X98r7BWoms3xON7NoVNE0maJqosVNR7i72SsoTJ76emqPpPKLNt1tRSsuBMg8INXhG3REpUrI+KEjojoYOX8awVpb2l5a577FnWVBRGQsKcEc5mt3ih/okfm/jXPra3xByyW5boUWBOYgDlrWj/kjszSjd1wJL+FYc22hbeKOLClpBJs1JASTqqZ1gaxxpD2G4ei4ZQ1ia3G1khxzqihsxzidaaRf4leBiwQ6XAVIQ03lTvEBI3eapOIWON4Ywl6+aW02peQKIGpgmPYar7J1J5FtJq/vyG04dhvXVNe6qw0EBQd6oqSqd2WZ+mncIcWnpJbtIunXmkvgJUokZhO+DuquRcXK3pQslxQy7hrUvA83vhtdp4e2GbzzRCael6uh0Tpgf0C75/uNcorqvS4/oF7/XA1yqofBFOycZFhhz9wwFG3uks5oCgf2qfWy2FQLhBnWYNMYctCM20sE3QVAEqIy+inlWr4UQWjv3VouAy0sj5fpv8AXbzH0uON2Ttql5ktuKCla6yK13QM/olz5/41iCw8ne0v0Vtegp/RLnz/AMaiXAwkkla6/RmpmiaRNE1kRYuaVnGTLGvOmpomlCxc0TSJomlCzDYvd4ni+IXuFMpbU23rKlRA/wBGqa46N31hbru1usgMjNAMmlYxcbDpFekOPIKnEg7MxI0mq1bwdYVtDcLVlVqpRImRHHlXUmki0I5VWl0nV7X9TQYbY3V9Yt3CHG+1vCtDNPOYXfo/8JK/mrqss1oTZMwpxLkakEgRH21YdcQmdle3SeWaDw8/Orpo48iyKb08Ph/BKsH8QwpanRYqUI11mBWuwy8N9Yt3BQEFYmJmsMq/ulMqSb0mQQUlAM/TFa3o6YwZj5tY5Uqsvicl+avlf1MLihCelt+s/sJUr/DVOyOyYuNnruk61a4ycvSHFlcm49OUVUNqQEwtrOZ3yRVWejgVwvyX7D2vlp9Jry3Epu0zm+BVrzgg0jO15P8A4jT2H5VXTiAnKHG1Jj6KLiXkmoN/x9Dd/wAn5/o+f75X2CtPNZboCf6Pn++V9grTTWbOOb9pi5omm1K3b440BU+bhUURY5NVPSKFWjQIBG04+Y1ZKWEpKlGABJJ4VUYld2tzkaS6lWUyRNaY17SZjnmlBkbFD/RQ/N/GubthWxMXKUDXsFRE/RXRsTP9Fj838a54yXOrqy2u0TrK8kx9NTI6Ox/lfyGm0wtBS6lBkQZjL31LcZW6nK7ijKwDMKdUR9lWlv0lQzatNHAcPXs20pzqZBKoESdONQ8XxtvE7VDCMMs7UpWF52UBJOhEbt2vsrPc7Lk3wK1xoNOZUvtK0nMgmKmYD/t601n4Ua868wfEk4a+4tVjb3edOUJeQFAa7xT2FvdY6TsvhpLW0uM2zQICZO4d1WXEjJemXwN70sP6Ce/1wNcsrqPSs/oN7/XA1y6olwRj2P8A2LDDUYcvP7ouvtwBs9kAZ5zp5qcZDRuUB8kM5+0Rvy0jDLXrIX/PLa3yQRtlAZvNNPtKW/cobcuChKlZSudB31dcCcz9qW/9fDYeebwwWhUw84biEwkgxMmeHKK1XQc/opfz/wAayKWc1k8+bqFNrCA2d6xz31rehJjCl/P/ABqXwOaW0eN7/Q000TVbi+KN4Za7VUKUTATOprHvdM71ZORvKO5X5VRRIjGc/wAqs6FNE1y57pNijng3CkDuJNPYL0iu7bE0uXly460sZVBa9B31OlGr7PkUdTR0uaJptKwpIUkgg7iK9mq0YWczxlZT0gvIeS38Mjwvt+iq4r/mxHWU+Arsxv7Q0+nfUzHnW2sfu87KXPhEnXkN4+mq7rDexKOrpzFJGbjqQZ3cN1bnZji3GLrp06FzZsOu2LWUgoCZgqA3DWplujEBakMiWYVy+mqy2J6s180U8FrG5ah9NTaOPJFtvh+g+51hwuvqbB2gIUYEDdu5cK2nR8/odjzVhNo5ljOqOU1uMAP6IY81VnwK1VGC6QEjG8TPAlKfsP3VWNqWE9l7IJ3TVr0iH6SxBX/HQP8ACaqkFsJ7TSlHmFR91UfE9LBviXy/ZCs7nlP+I07YK/STWZWaVQTTOZr+wX6/5VJbtgh1t1LzDZBSrItwAj00XEtNpRae1ryNr0CP6AP96r7BWlmsr0NuLe2wlTLtwyle1UY2g3aVpEPtOeA6hXmUDVaZ505LUx+a8mkZq9mooiylu7++uA81bWzZZzKaK1KgyN/Hv5VQDo5dGSXWBJnj+FJvb24YxK4bS+8lkPKJQ2vKdd8HhTb776LNu5TijqlOKI2G3WVoGup4cPbXWoJJWUUcvGDRoMTP9GD838aw9li+K2mHOWdo6U2q82dOzSZkQdSJ3VtsTP8ARo/NP31h7N7CUYe4i8tX3Ls5sjiFgJGmmnnrCZv2PeL2vgRU3Fw4EsIUVBQCAkJEnhFK9z7zabPqzufLmy5dY3TVmzddG027Yew68U6EALUHBBVGpH01GxN/BnbZKcNs7hl4LlSnFggpg6emKpZ227qKr5f2Qyzc2zwCmlocEEAp110qXg4cT0htw6CHA92gRBBmmsLcw5p1ZxO3debKeyG1AEGn8LUyrpGyq2QpDJeltKjJCZ0BouJGS9Mr6G86Un9CPf64GuX107pOf0I9/rga5jSfBGPYuEidYWz1wFbGzVcZIKspPZ9FPLWgq0YQOBEk/fUW1KU+E+41u8DjU5SbYrCW9usnkBvqy4F8169/r/wecsXWrUXS7ZrZ5UqBCzMKmPsrS9DlhvB3FqMJSok+2ssXszeyNw/k0GRWo03Ve4J/upefNV99WOSerR7XX6MqOkmKqv71QQr4JJhI++qOlufrNO6pLqiEfBOvKVyIqvE9DGliiormQ6KkNLdLqA6txLZUMxA1A41YY2jDmXWxgt5d3DZSdoXkZYPCNBUWa6n9s1HQ3EXLuwWy8SpbSoCuY0rRzWO6Cfq7nz/hWumpZ4+Wo5JJHNMdt1P45iCkqSA1CjPHcNPTUJeHuIKwXW+wpxJ1OpQJPp4VI6RgnHbsgaBQk/RVYEKOaEk5RJjhWh6GJS0R35fQvLJTaWGy63nGzgCYgxoal3jlm4omzt1sjMICl5oEDT0yabwy5DNohBZYXLR1dTO8Rp38qfOQrWvZ2xBTGUL0Gg1Gu/8AOlbnDOVNkUbLZahe013RHCPvra4Cf0Sz5vurJXJbW+8U27TUCcjapSnzGda1WBH9FM+b7qiXAzm90Y3pKIuro8V3IPoR+dVje3DacrzYEaAkVZ9KtL4jxlqV9g+6qYG3gSl2fnD8KrLieh2dXhiSJuP7dr0imMQnrXaIJyIkj5oom38V31h+FeX0dZ7MxkRE/NFTE1UaktvT4EcEjUGKkNX12z+ruHBHfNWGE4ndWdmWmMPZuElZVnW2VGYGk0u8xW7fetVuYaw2WnQtKUtEZzyPOr7GUsknJxcE18V+w3a9IcWQ8hIvFFMgQUiumtKKmkE7ykE1yLOpy/zqQEKU5JSBAGu6utMn4FHzRVJnN2mMYuNKjE4k4gXl22WEFZdJDuYykTuiYpL9utGD29wq0bQhayBcBwlS9+hTMDdy4VLur1ab5y2TbWiiHyoLdQJOu4qJ3VUK8JRgbzu3VurYi0oo1WJH+jh+afvrnzbqEMlBYSsme0d4roGJH+jyvmn76yGFs4c5ZHrmKqtVlRGzDObTTWawmR2J+w/kVKDC0nLmgjTnUh9W0QEotNmQZkD8qm3DOHWVzaO4diheUHQVKLUbOCIOu+rX3cd/8wK+rJ/Cs1udk8iTTr9/4Mw3LboKmiqNSkjfUzBjOOW6gnLLkxyqTd4m6rES8MTLpLYTtdkkcd0RTGFLLmPsrUrOVOyVRE99SuKInJyhJ1y8/wCDddJTOCveY/Ya5nXSukZnBnvMfsNc1qMnIy7BwkT8PuHrfNsSwM8BW1CT6JqY0g29wh5D7BUhWYdqRNVKASUwCatGG1tXLa3rdakJUCpOXeKvE07RFJ319RtTZzE7RoySdFitDgv+6t381X31VXD9mqyLbdktt6EgLKRGhM8eIj0VaYMf6L3XzVffUpbnHlk3jtqtzIufrPRVmxc2KbsLfs7ldvlgtpehRVzmqxz9Z6KdcQ2pMIQ0g89pNVPSlFPTY8XkbQHZu5M0lObWOU1446gqWUNupQR2QVTGlRgzrqpuPnCluoSogoDaAOAXM1Nsr3cLNT0FPwVz878K0t46tDQ2ZhSlATWZ6DmGrj533CtHcguOMtpBKlL0A7hUnl9pf+WS8zn+NLK7+9UXAnMUEo8bQfZVe1uuPhcnZOnj6jSp2Lg9cujswQNnKjvToKhMhRFzlQlQCDmJ4CRqKs+J6OH/AMl8voX+HsqucNaQHLZsISVyshKjA3TvPcOdTG3VNuOpNth7hKCJKBAkDUHn+dQbVKm8Jt3lWzS215m0rKjObKOR4bxUrq7lnaF27w5K0KJbS4pxQhUSNx4Cq2qOWcXr47hepUhK2l21qlQUrtNEGJA3EcB95rR4If0Wz5h9lZm6SG3HnFWBYbclDSM6iG1CJgnf5jzrSYMYwxnzD7Knkc+baqZjOlKpxhSfFH31XS9zb/w1Z4wi3f6R3KLq56u2keHkKtYkCBVVFtzd9UfjVG92epgVYYbchUvc2/8ADSL+es6xORG75or2Lbm76o/GvL6Os9mYyIifmirRL/7L+PgWeDu46iyIwtKixnMwkHtQJ+6l3z3SBT9obtK9oHQWJSPD4V5guHYnd2Jcsr9DDW0IyKcUnWBroPNS7/DMVZfs0v4i24p14JaIcUciueo0rTkefKWPvmvZvfluU5LpxAl/9aXe3551rqzR+CR80VylSFt4iUOqC1pdhSgZkzvrqbR+CR80VWQ7Y/y/Ax12M2NOguhmXT8IZ7PfpTNqy0++pD122wkAnOsEhRnuH01YurwlN1ci+Zu1vbVUFpaQmOG8Uiwu12rSVXjLnUTmDSksNyTPjKSZ41o5Otl9+RbHFUiyxE/oBQ+SfvrAti32R2hXtNYjdW8xA/oNY+QfvrM4Zk9xnptMPcMq+EeMODQbvurKaMuwzUYSb6opmdmXmw+VBrMM+XfHGKmX6cKSynqC7hTubXaRGWD3eakWzBbdZeVsHEpKVlC1aKG+DVq9i1s0kKOB4UZMQkE/fWXLgehJpyTTZXWKcJLE3y7gO5jo3ER6KVheyGPs7AqLW17BVvjhNTWcYtnSQMCwtMD9oEffUPD1hfSJtaW0NhTxIQ34Ke4d1SnujOa9mbbfBm06QmcHe8x+w1ziui4+Zwh75p+w1zqmXkY/hruMviWOGN2Lm069du28AZMiCrNzn2UK1JkzB0J5UYZbLuc+R21byQfh1JE+aafaK7m6baU4lvMrLmCRA131K4G2VpTbv+vQG02vUnVqdUm5CgG0A6FPE7vvq7wj/di58yvvqufw/Y2RuRdNrgJOTIJ1JH3VYYT/ALs3HmV99XitzizTUsdp3uZRz9Z6Kt7bG37S/TepbtVOJQUBK2VFEHu51UO/rDPdV1i95jDzabPFGnWphYQq0DaiNwPOKzt8D1JRi9LZAN4S8HZakLzxlMTM+ipbGOvW6bsIRaK62nKvO0Tl0js8qgIs3Vqb2bT6i4oJRDROZXADmaVdWz7bymrpt9t1vwkLZylMidRwqzb4FFDHdr79DQdDFpbt7lSjACtT9Aq2GOO216HrZtpZQClO0BO/zEVmMIFwcJueqqSF7QSVbo0qYnC+kKkhSWWSCJBq+1bo83PSzSk5Jb82QcbtV3Lrt60DnWczradw7x3VRk9pUHea1Dthj1ukLdQw2CYBPOqG4dt0X76mm0raUFJSNwBIiR5jrR8Tr7Ll1LSmpV0La1ZZOHMObcbU9kt5ToI3z91TDZWQccCcSaISmUq2ahnMDTdpx38qrLYE2zWn7IpyO6hlLiyU7bMIceS3eNuJQmULAIDncJH21pMIMYY180fZWRg8q1eGKy4Y2eSPuqUjl7Q6SMVjRLmN3BTqSsR6BTf85/sG/QKRfqC8ReKiQM51GtNQz/ar9T86yfFnrwj/AI4ryXIkfzn+wb9ApjEJ612gAciJA+aK8hn+1X6n50X0dY0MjIj/ANoq0SUqkv4+BMw20wh62K7/ABBdu7mICAidNNd3npV1Z4M27bptsRW4hbgS6ooIyJ57qRhzuDItiMRYfW9mMFswMukffSrp7BFO25tbe4SgOAvBR3p5Dvq/I5nr71/mr5V/JBCW032VpedsOQlRESJ0NdRbPwafMK5cC2b4FkENlzsg7wJ0rpqFfBp8woc/4g6cTJX6VLxN9KElSi4YAEk1FiD31KvVrbxN5ba1IWlZIUkwR9NNKQ0phK23XHLgk52y3AA55p14cONb3RMFcUX1+f0M4PkGsIhlK2isupSR+yd5rc33+yXfmGsQ0yFslezcVE6pIiubKtyv4a/Yl8RFs0l+6aaW4lpLiwkrVuSCd581WWK4Ta2Fsl1jFLe7UV5Shs6gQTPs9tRB1BSW0pbui6SkK7aYPONPRUnq1j5Lf/vEfhWVHoSyU+foVraAtYSVBIPE8Km4OAnGmEgggORI40ttvDk3mS4t77ZqQMiUOICs084iKMNDacfaDKVpbDvZCyCoDvjjUriRklqxy+Br8bVOFP8AzT9lc+rfYz/st/5p+ysDVs3FHH+Eu8cviSGFMiNq2pe6IVFWI2bz6GmLZMrOUAqOp9NVzCGlRtHg3u/ZJn0VYgsMvJWy85mSZCgncahcDr7Rx2u/nQBsllx4WnwbasqlBZgGrfCj/Rx8fJX99VIeAYcYTcKDbisyklG81a4V/u6981f31pHicHab7vfqupl3P1noqwvb26uHEv3rlw4tKcgWsgkDlVe5+s9FSEWjjjmzTkmJ1XArNLoenPTS1DjN47tGUsrezoWC0ARoqdI75qfiNliaLtZxVLzLzqCsrfIGYCBv9AqqTaqK0gZASrKO1xqY2w+01eOurSrZICT8JmMkgiO7Sp3uzOWiqiS8LcYsrJ9l64aKnDIymeVWCcffSkJTfgACB2U/hSPf1ceQN+uaPfzccLFr1zV1NI8zJ2btGSTlPDd//S/g9dxlVwEouLxK0BUwQBr9ArNjDVFUC6tZJ07Z/CtF7+bnyFr1zTN30tfxC2VZrtG0JehJUFEka1Dkma4MfacLqOOk+O6/aiIyVsspbIEoGU+cVMGJ3IaLeS3ymdSymde+KUhzKwpLjz6EbR2EpHZk/jx7qflbeFpYbevQ6lZK2chyJSRv3b91H5lk7baW5Aeu3XlKUpLYzb8qAB9FX1mrLhSf7v7qrblTDYfYs37osJCoStMaGJnlMewVNbVkwUqPBkn2VeBy9r4KupilFTlwtSU5yVExHfSod8nHqmmmwlSu2vIOcTTmzZ8o/wAJrA917bfRhDvk49U0m/nrOog5Eaf8opWzZ8o/wmk30C5gGRkRr/yirxK/7L+/ImYbiFpa2xbfw1FyvMTnJjTTSlXWJWbztupvC0NBtwKWkH9YOVS8BuscZsCjDLRLrG0JKime1Akbx3UvEbvH13Fiq7s0ocQ+CwMvhL5b605HA2u+ey5/7/QoQtK77OhAQlTkhI4a7q6QlfZHmrnS1OqxJSn05XS7KxyM610JJ7I81Sjn/FHWivvgUzl1YNPvousMFy4XCdpt1I05QK8w55/CQMTNspVu+FNoIdKePMa/smoV/wD1575xpkpAbCw4gqO9AnMPPwqzgmq6/H7RvilUV8C/viPcp0SPANZWywjErrDnLu2A6ugqzfCAbhJ0qYSRIzEgGN9TxhYgRiFsJ4ZjWUk5mWGS7NFpviZa2ZdfuWmWR8I4sJRrGpMDWrLEsIxbDLdL92opQpeQFLubWCeB7qt/cpPl9p61RLpnYO7PbJeETmSZFV7to6V2yM5UkUbCH7q5bbbUouqMJJV99SsMQtrHWkO+GhyFGZ189XGEdHX8VtGrhm4bbDl11YBQMg5QqdOGtOK6MXKXGU9Zal3rEGDpsd/p4VnqSZvNuSa5NFjjGuFv/NP2Vga2Nzd268C2YfbKy14ObWYrH5F+KfRV829HN+GRcIST6k/DLpVqVQxauhcA7dIMead2+pbSDb3SHs7Csi82XPoe6qlKVadk1PYU0m4Qp9JU0FArSN5FEdGeCttcyfcXm2szb7G3BhIzhwToSeXGamYV/u8981f31X2jNu6y626w4HVLzNucAnlvqwt/5vZqtUaoUCCTv1rSN8Tzsyio6I9UzLufrPRT7pyIkLYX3AVZHCWCqStz2V47h/gpYC3HFGAkIBJ081U0tHf4nHKkip26vER6KW1eLaJIbaIUnKQUyCKsPcq+8juP3P5VEeRslrbdORaRqlSBM+io3NdcJbUaCxwZOIN3rrXVWU2paGVTGYqzwN8iImmmsJLnS53AJswWyRt+r6GEz4M/fT1leotGsSS5eXFuFbAw2wFpUBBkkgxFMWt4FdNHMVReXZtnFKCbtNuCpRyx4MR7KpKUtTSM8ajoTZSu3WzulsdVtDlcKM2y3wYnfV10gw4YDcPsFNq+41kIcDGQazwk8qzym3H755bSXHAlwrUoJ1AneRwrQdLLkXbt26m5fuJLYzvNBtXHgAKmDk5InJGCpLqhu2W29h7RfvHUqKlLLQTKUkjwhrvO6p5fZ2zx9274hST28qpc0Gh7W7h9AqntEoNq1mVByincrfjVrpOWUqkOqWnZqWLl0uuSlwEnVOkSePm7qsXTHR1w/wDAP2VUkNwe1VjeKy9HVd7UeyrLmc2XdwXmjKMSFGGtppujdT0nyL2GmG5BMOBHfrr6KXKvKh6VVie3NW/+jknyL2Gmr/8ArXg5ewjTl2RXsq8qHpVSb/8ArO+ewjXn2RVolEqkv78izweyxa4siuwvSy1nIKdtk1gax6KVe2OMNP2ibi+Li1uhLR2+bIrn3eeouHYbb3dsXXcSZtlZinIs68NaXdYZbMu26EYoy6HXAlSgf1Y5mtN6ONyXevdc/wDX6kEpWjECl1WZwOwpUzJnXWuhTCAe6udhAbvghKwsJcgKG4676374BtlAmARvqYnL+Jq3D76ERNqRdh02as4dzF3biMvLJH317eX+C2b2yuLVKVkZtGpp1nDbNy5Q2vEEoQoKJWY0jdxplfR3CbzErhu5xxthDKUhCjl+EkSePDdSeTRwK4cK7Q05vbhtsUZEEg7xoa0IGKQP0bZ7hxH41nqe/nP/ABvbVYui2bHrrh8/+ovIxT4ts/SPxqoxPbdb/nDLbK8o7Le6Kam5/wCN7abczz8JmzR+1vqW7RXFi0yvb5X/ACy16O4nf2eHsNWuGOXSE3+1StJ8JeQDJu3xrT68WxEvWyjhDoKOuZRJ7Wc9vh+zxp/ojiNlaYTbN3N2yytOKbVSVrAIRswM3mnjUhzE7A3FmRfMQj3QzHaDTOex6eHOuPnwPVrY5wBDgAPGrN+5bdWypNq20GkgKCdzneaq06LHnrVOqSu0KDidmQq3SkpCBO/wd+8V04+Zl2mai42v3+hCOJMFU+5luO2VRrujd5qbN8zsS31BmSjJm1kHn56mW9pbWtylxrFrUkLKJUkERHhb6i9fu1NONIQhaEoKCUtzCZ3zWm5zRUG/YXq1+43h39ZMb8tX76rQ3ILP6rI3Oh8KO17ar7ax2LKLjrCFZm0nIEgETPH6PbVo4XLh9LrVi4E5UIypTMlMA8OMj0iqxVGWaam9hV6vDTdvG0HwJeQW+yrwIGbf3zVXigaW8sMoUtkunIlI1KeG+ra+uuti5U3h5aDjyFyBOzCUkFO7jqfoqovVOsKBBdZdSsjsylSdKciIfn2IXV0eSXHoH4VCfUlDriCp1uB2U/jVj1+88uvv3h/Gqm+UpdypS1KUo6kqMk+es2ejhjcqZduNFdhi7gdcSENt9lJGVWg36U7gFsp3DbRQubhGa4UIQoADTeNKCwXMLxp0Wa3Q221LwfyBrQb0/tVCKHGeh7N03bOtnrJSLpNxAOng5OHnqHkSkYLs8p4qT40/SgwRkuXOJgPPIyNLMoIBVrx0qbjtk+u5ubZpTr5OQ53FCRE+apljhw9z0OowRwqcsVOKcTeBOfUdsifZTOO25Yxh5s4ddMEJSdmm42sd5VrU45p+yWyYZLJ3qf3t5roV7VpcoaQhTCiUiNFJ/GhTa0OobW0tKl6JGYa0R/8AKXvrH8KGgOv2wU08hOcSHSTIkbq1dJFUnJ7/AH6ssDgOJgKmyV2Zn4VGkb+PfUi9wrFnMMNp1EpKUqBUXUR2Izca1DjVhNzFuYAcj4NemqaauG7PM8AwrddR2FckxXGu0SbotLDFVJ8Uc6fwa8tL/qVwwNvmCMuYQFESBI01BpLmGPoaQ51YlLgTkgntEiYH0Ca3i7Sxd6WOtODYoW4hKQttQCuwNO1oTyiCK0uMdG2Ly3Qi02bWXSMxSAO6N3IxvGm6kstM7oXJWziRABILGo7zXl9/WN0dhGn/ACit1cfydYyXlFu/tsp3BKlADuimrj+TbF3nc/WrXwQNVHgIrWOSPUpftf8ATL4Y1ga7UnE37hD+cwGxpl0jge+lXbWApethaXFypsuAPlQ1SjmOzvrSO9HR0dtkNYnZ2l244VLSsK4ADTd/qabvcDccbwm7RhNqwzcvoyJS6Dtc25J00FbWtNnDqk8zrV+qr9OJjwGxfQwSWg52Cd5E6VrWmbq4bJaStaJg61RY1hL+D42Le5QhtalBYQhUhIJ0E1oLQ24aO2uLttWbcyiRHPeNatFriO2W1GuPmJ9z7vydXsqsxZgtICXWwlwKG8axBq8zWXlmI/u//wC1U+NlES0txxOYQp0QrcaltNHNhU1NW0RyZJJ3nU1O92cQAA6yNPkJ/CoSwQtQUIMmasjitwlOtnaAJgH4P86on5lssbq4p/Ea92cR8pHqJ/CotxcO3Tm1fXnXETAGn0VO92XfJLT92fxqFdXCrp3aqQ2gxEIECpb8yMcKlehL9C76K4Rh9/hds9d2yXXF4lsVEqIlGQGNDzNSHMEwwP2iRaJhfX8wzK12Z7HHhVd0dwu+vcPYdtsTctUKvtiEJG5eQHP540p9eD4gHrZJxdwlfXMpg9nIe361cnPienyMMDLgPfVxdsNgNG0ZuI2YLhcRx7u6qgGXAdwmtU2L9Fi8u+LwSlhKkZFojJPGujGuJj2qbg4tfuUa2XWxLjS0AGJUmNeVPMLvLdp0MpcS28iFnJIKavFNoxJhJi7dYcfMStAmB9tJ2bow5UdZyi1P7aIifTHtrTScj7SmqklZU2Ckl6A2Acu/MTNbvCv1bP8Ae/8AczWCw7+sH5tb3Cv1bP8Ae/8AczWT/KRn2yfIiN/1S6/vP+1dVGOPuW2M3DzSyhabhUKABI0I3Grdv+qXXz/+1dVONuqYxq4dSpKSm4XBUjMBoeFWMcP5/vzIXu5feXO/ukVR4g4p67W4tRUpWpJETV/7qv8AlNt9VT/lqgxFZcvFrKgoq1kJyg/RVGj1Oyv2y2fxlVja4nhoYCxfNtgrKoKIA4cagLxlS+jTeDbABKHy9tc2p03RTOMf7QV81P2CoNUklqZthX+OPwNTbdMnGLJu2FihQRaKts20OoJBnd3UX+PjGr92/ebdtysBORlWYacyay1W2EOpbYXmuVMyrcEzNTjSUiMy9h7ffqTOtM/2t56B+NeNuJev7YNqeV2wPhNOI3U71lv4xc/d/lTK3Su7ZUzcl1aSMpUmMpkRW8uBywVP8rX6/wAI6Ipp0G6EHQO//EK5p7qbfadzvacLr/x1ck91UirrpH8KovWfbzhXbRzE/dSHbjpDmXnetJ+GzQtPEDP90V56x1JJNGkmmiJ0lvTh/TK5D4cJORKyhwqOQoTKQTunTXQ1qLe8cXgSbl+xXcNtnIp3rKkSoqHAHmRWA6RXN+cccXeuINysp2hbIKT2RER3RWqZYuTgK3ksOlgOAFwXEJnMNMno1rqjFVv5FM8mqa6Pz+hem2eFyi3OCr2i0laR7oubhE8e+mFHLZvXZwdexZUUrPug5oRv40hVjiIxBpBsboOFtRCeviSJGubh5qhuWt4MKu3DavhpDiwtfWgUpIOsp4+fjUJJ8/X+zKUpLk/0X8DfSe3dtw0HbI2uZKoBuFO5vSdKjO2lyMNwRZwxSUuushC+urO2J3CJ7E8xEU90jt7q3DQurd1kqSrLtLgOz5uVIdwN5GF4E+cUuVC8eZQlB3M5uKdeFaWlFWy+JPVLbp98DPdL2nGOkLbb1qbZYCTsy+XY78xJNXGGodVbqLeJWtqM57Du86DXcdPwqp6Z2C8N6SN2rl07dKSEnaueEZ4VbYct5NuoN4daXIzntvbxoNN40/GrRbcfZI7VoVd5w34kvZv/AB7h/wDr/lrP9IEkLIW+3cHMn4RvwToe4VoNrc/EmHe3/NWf6QFSlkrYbt1Zk/BteCND3mp9vmc+Luda7ur8hnGrlhi8WEpOWYBGtQVYuhQIKVaxOlC7nCnekaX129yrC86czSz8IUwJ3EcZ41CxE2q8QfVh7S27Ur+CQqZCe/U/bXFDLJVFnrPs+NuyV7os+Kuj3RZ8VfopnZWDirFtpx9C1kJuVOJASgkjVOuoid8UvHbG1w/FnbWwuxeW6AkpeSNFSATu5HSrd6x4eBb4J0tGE2bVsLIvbO761m2mWeyE5Yg8t9WKMdxC56tcM4FcLbQLkpKVEhW2PPLw9vdWRtFWguLY3bayyknbZd5HdVgyxcu3RXbG/bw5SlbLZkkhPDSaJXuZ5HWy283w/fiRxg+KBQPuddQD/Zml3S762yt3bd02HBlSlcjMOVLYs8ZW8hL7l422T2lhSlZR5pqReYQ6Wto3eXV08gjIhbKhx11NW1SWyTKKSk/blGvvzK3rrjR2YW8gpUeyFEQavW8CxZxlKkvIyLSIBf4HWIqqu8Guylt5CVuvPSp5AbI2Znd3/RSXMKv0sslpNypwg7RJSQEGdADOulWU5riiuWMZJaJpPnz+qLbor0fvsVacv7Us7ILLRC3AkzAP3irbEcaX0bvEWV5ahbgAdltwEEEp4/8A2/bWbt7J5rCLhhWHXXXVrSWnkrISgcQRMGaL2wVciyDWG3LCkNhNy4VFZcPFQBOnHSqJ5OFGmSPZ8ktTkr+JPT0rYS0631Zfwipmd2ih/wB3sqJe9IWbi9XdptwVKcK9m6kLTrzHGkXeFNpsSzZ2N25cB+RcLGUKbjdlnQzUJ3BL5GTIypzMnMco8E8j31KlN8iixdljK0/UvcFurnG73qlnYYWHMpV8JbpSIHfNMdKOj2IYU2L+9FqlDrgbCWFggGCd3AaGqo4VftlJYYuc2XtHLlg8hrqKSvDMVWIWw+oclEmofeWbwngjupL9SV0hsH7VdrdPZMl40FthKpMADfyqtsbZd9fMWjJAcfcDacxgSTGtTHcJu1WrWVi6U+CQtKk9kDSIM+emRhOJAgptXQRuIFR7b5ExyYYpJSX6icTsXcLxF6xuCkuskBRQZG4Hf9NPWFtdPsFVvbOupCoJQmQDTV/Yu2rbLjxWVuglYWkjKQd08dIpi2MuZFXCmUEE5hO+O6pUmuJaTuNwf1/Ym3SXrMpF1butFXg5hE1Lfs3rTCLXFHQg290SlsJVKpBO8cNxqnbG3z7V8JyIKhnJMnkO804pVqcNQA7cG7DhlBA2YRG8GZme6jyS5EqNqpcSytWXbtnasNJKJjtLSn7TT3ufd/2Tf71H41S2HU+uN+6IeNtrn2IGfcYidN8VNwl3BW13vupbvvIU0oWuUkFK+BMEffUvNS4GD7PK9n6f2TDhLzqkqcyoy7gHEGfbV17vtW9qMHctF7d1aVJc6zCR2gdROXhvrFWdyqzuUvobacUmey6jMk/RUxjD7NxhC14qw2pQkoUlUj2UeRvgiksWmnkla8k/7NsOmFmvGCg2S0KYQ4hQVfqykjUwv6NI3yKjW3SvDbqwvLZxDluVZnEqW8ozJ8GNxPeayvuXYfHNt6qvwqDeMtMP5GbhFwmJzoBA82tVUmvv+w8eLL7MW0/hX0NXjHS6yxIN7O0fZyJI7TxcmfPuqe90utVYJgQ6ndBuwuGip0pGVZQBIBnfWEWq3No0lDag+FHOsnRQ4V4bl5Vqm1LiiylZWlHAKIgmjk6S6G8MUU21z+htbnEML6VdLetFblugpQlIcQDu3kmdKm9KrbD8FWgN3ClBSc0JExr3Vzxl5xleZtUGra2xllVjfNYnbm6febyW7hP6k86ybyxmpRey5EzwQmvaH/di18d30Uzc4jaXDYTncGszkn76qLdxDVw2440l5CFAqbUSAocjFJWoKcUpKQkEkhI3Duro76Rmux407RM69eeVv/vDR1688rf/AHhpiivP7yXU7+5x+6v0H+vXnlb/AO8NHXrzyt/94aYrSdC+jCOkd8tNxdoZYZGZSEqBcXqJhMyBr4URuHma59SHixL/AFX6FA5dXDzZbduHVoO9Klkg0xkTyrrnTToTYXeHqvcMbtrG4tkElICWmnEjWDuAPJX0HmOSnQ1DlLmyYRhXsoTkTyoyJ5Uqio1PqXpCcieVGRPKlUU1PqKQnInlRkTypVLZS2t9tDrmybUoBS8ubKJ1McYpqfUUhrInlRkTyq79zcD/APMP/wCEv8ah4jbWNvs+o4h1zNOf4FTeTdG/fOvopb6kUuhAyJ5UZE8qVRTU+pNITkTyoyJ5Uqimp9RSE5E8qMieVKoqdT6jShORPKjInlVhh+E3l88wG7d/YurCS8GipKRMEzu0qVjmB3FliVyi1tLldojtJc2aikCATr3bvopql1IpFLkTyoyJ5UqimqXUnShORPKjInlSqKapdRpQnInlRkTypVFNUuo0oTkTyoyJ5VZ4Gxbv3+a5KihlO12aUBRcykSmCddJJ7ga6T7kYX8W2f7hP4UUpdSrpcjkeRPKjInlVv0htba2v81qslD5U6E5AkJSVkCI4ECR3EVVU1S6lqXQTkTyoyJ5UqimqXUaUFFFFVJCtT/J7i9hguOv3OJP7FlVspsKyKVKipJiADwBrNMMO3DhbZTmUEKWRIHZSkqUfQCaeTht6XWm12rzZdMIK21Cecaa6cqEPc29p00bvehmKWuOYhnxF4OIZTsYlJQIEpTG/Nvrn1OotbhwIKGXFBebKQkwqBJjnFJU06lpDqm1pbXORZSQFRvg8akJJCKKKKgkKKKKAKdtXSxdMvBakFtaVBSUhRTBmQDofNS7mxurRCV3DKmwoxrwMTB5GCDB1pgoUEBZScpJAVGhIiR7R6aA03vmf+Ob36gz/mqrxrE14jsc94/c7PNG1YQ3lmN2Umd3GqyilkUFFKdbUy6tpwALQopUAQdR3ik0JCiiigCilstOPvtstJzOOKCEiYkkwKVcMLt1hDimySJ+DdSselJIoC/6IYknDRevXVwEWqEA7Ke0tZOmUeYH2T3SulGN2+K4KyqyulIG1yu26oCjoSCeYEcNNRxFZGlJQpYUUicok694H31NkVvYmiiioJCiiigCilNNreeQ02My1qCUiYknQU7d2dxZqCbhvLmmFBQUkxoYI0MceVAeWd0/ZXSLm1XkdROVUAxIjj3GukN3ONJwafctsXqYQlAeTlIjw+UT+zNcyQhTi0obSVLUQEpSJJPIV5U2Q1ZJxJ26exB5V+ZuQrI5oBqnSNNOFRqKKgkKKKKAKKZ23yfbRtvk+2ppldaLDDblFpcrdcCilTDzYy75W2pA+iVCtOjpdbe6yrhwXarYOsrbbMHLlYW2qBmgEqUD5hWI23yfbRtvk+2lMhyizXJ6S25vUXK1XqWgyWxZoI2TRDRbBSJ1kkncmJO+oT+MtOYO7ak3Dy3mmWgh0jJb7OO0jUyTCuCYCjvms9tvk+2jbfJ9tKYuI9RTO2+T7aNt8n20pk60PUUztvk+2jbfJ9tKY1ou039sMQVclVyA66XlpBICFHNG5QKiCoaynjprTrGNIQpCXg682h1a0hZPZKwO0BmmUkEjWTmOoOtZ/bfJ9tG2+T7aUyNUTUOX7T2GXbktspUyGwwhQAdUHAc5TmKs2XeTO49rhUA4iEouUi5unlu2+yDrmih20kp8I9mEkfTuqm23yfbRtvk+2lMaomkexmyVtlNMOBblwXgpYzQraZpjNA7OkCN28zpVYi+m6vnHkFZSqNVzJ0A4qUfaagbb5Pto23yfbSmNUR6imdt8n20bb5PtpTJ1onYe+m1xG2uHAShp1C1BO8gEHSp1tibGwuBdtDaONqQNk0lKTKTllKSkSCZkhR3RETVHtvk+2jbfJ9tKY1RL04qx1UMNoebWoZ13MhTm0I7Z37lQBv4cZivFYwQXer7W3SpJyNtqhLai7n0iP2dJiqPbfJ9tG2+T7aUyNUS5XiLfV0oC3XFJUSQ4kFLis+baKkntEdkiDpx4VGxJ9m5vFPMpcAUBmK1EknidSSPNJ89V+2+T7aNt8n20pk6oj1FM7b5Pto23yfbSmNaJlk8m3vrd9YJS06lZA3wDNWzWMWrDriEMrcbW6t1Lq0wtsqy+CEqB/ZI0UJnug53bfJ9tG2+T7aUxqiX9pjLbBbcLakLRdbZKGJQhImSN8HQlMETEdqBFJfxC0XYpZQq6DweDm2UJVMnMrwtCZBgRuEk76ott8n20bb5PtpTI1RLyxxNm3u1OPOXD46y0/tFJGdWTMCCCrko8Tu79HnMat3Lphxu2TbobUpXwaVZkSCMoIWNNZgZdeHPO7b5Pto23yfbSmNUSVcLQ5curaCkoUslIUZIE6SabpnbfJ9tG2+T7aUydaGqKKKuYhRRRQBRRRQBRUzCcMu8YxFqwsUBb7pMBSgkAASSSeQq5uF4P0bu7m2tmUYxeIlAuLlspZZVuUNkZzka6kxMQNNQM1RVt0nOfGdplQlTttbuKCEBAKlMIUowIAkkn6aqaAKKKKAKKKKAKKKKAcYYeuHUs27S3XFbkISVE8dwputRiNza9GetYRhJ22IIdh3FMpbcaI0KGhMp4gqnWSIiDSls23Sy2vb9lCLPFLG16xcpiUXmXw3BAAQrmNZKhu1NAZWiiigCiiigCiiigCiitf0luvcprBG7G0w9CXsJYecKrFlwrWZlRKkkyYFAZCirt67Vf9G7p19i0S41eMJQtm0aaICkPSJQkSDlTv5VSUAUUUUAUUUUAUUUUAUUUUAUUUUAUUUUApC1NrSttRStJlKkmCDzFX/u/a4u44elNsu4eWnKi+toQ81G4ZBCFidNYME67oz1FAWOPXNvdYoXLNa1sIZZaStaMpVkaSiYkxJTO+q6iigCiiigCiiigCiiigNDjVth2IY3fXrOP4elu4uFuoC27gEBSiRPwW/WpGCPYbglvi63cXtblV3hzts02w29mK1RE5kJAGnOstRQBRRRQBRRRQBRRRQBWxu8V6JYra4b7qIxtNxZ2TVqrq4aCDkG/tEneTWOooDQYrd9HUYKuywIYptHrht1w3gbgBCXBpl49us/RRQBRRRQBRRRQBRRRQH//2Q==', 1),
(153, 88, 'https://em2d.com.br/seligaai/offers/str_654193.jpeg', 1),
(163, 89, 'https://em2d.com.br/seligaai/offers/str_666503.jpeg', 1),
(162, 89, 'https://em2d.com.br/seligaai/offers/str_724414.jpeg', 1),
(161, 89, 'https://em2d.com.br/seligaai/offers/str_749042.jpeg', 1),
(160, 89, 'https://em2d.com.br/seligaai/offers/str_281014.jpeg', 1),
(159, 89, 'https://em2d.com.br/seligaai/offers/str_211794.jpeg', 1),
(178, 107, 'https://em2d.com.br/seligaai/offers/str_597569.jpeg', 1),
(167, 90, 'https://em2d.com.br/seligaai/offers/menina1.jpeg', 1),
(168, 90, 'https://em2d.com.br/seligaai/offers/menina2.jpeg', 1),
(169, 90, 'https://em2d.com.br/seligaai/offers/menina3.jpeg', 1),
(170, 90, 'https://em2d.com.br/seligaai/offers/menina4.jpeg', 1),
(171, 90, 'https://em2d.com.br/seligaai/offers/menina5.jpeg', 1),
(172, 91, 'https://em2d.com.br/seligaai/offers/menino1.jpeg', 1),
(173, 91, 'https://em2d.com.br/seligaai/offers/menino2.jpeg', 1),
(174, 91, 'https://em2d.com.br/seligaai/offers/menino3.jpeg', 1),
(175, 91, 'https://em2d.com.br/seligaai/offers/menino4.jpeg', 1),
(176, 91, 'https://em2d.com.br/seligaai/offers/menino5.jpeg', 1),
(179, 108, 'https://em2d.com.br/seligaai/offers/str_314452.jpeg', 1),
(180, 109, 'https://em2d.com.br/seligaai/offers/str_224258.jpeg', 1),
(181, 110, 'https://em2d.com.br/seligaai/offers/str_396104.jpeg', 1),
(182, 110, 'https://em2d.com.br/seligaai/offers/str_078276.jpeg', 1),
(183, 111, 'https://em2d.com.br/seligaai/offers/str_353766.jpeg', 1),
(184, 111, 'https://em2d.com.br/seligaai/offers/str_810297.jpeg', 1),
(185, 111, 'https://em2d.com.br/seligaai/offers/str_780251.jpeg', 1),
(186, 111, 'https://em2d.com.br/seligaai/offers/str_456205.jpeg', 1),
(187, 112, 'https://em2d.com.br/seligaai/offers/str_460609.jpeg', 1),
(188, 112, 'https://em2d.com.br/seligaai/offers/str_056698.jpeg', 1),
(189, 112, 'https://em2d.com.br/seligaai/offers/str_266708.jpeg', 1),
(190, 112, 'https://em2d.com.br/seligaai/offers/str_947246.jpeg', 1),
(191, 112, 'https://em2d.com.br/seligaai/offers/str_371008.jpeg', 1),
(193, 113, 'https://em2d.com.br/seligaai/offers/str_555271.jpeg', 1),
(194, 114, 'https://em2d.com.br/seligaai/offers/str_038260.jpeg', 1),
(195, 115, 'https://em2d.com.br/seligaai/offers/str_394904.jpeg', 1),
(196, 116, 'https://em2d.com.br/seligaai/offers/str_521422.jpeg', 1),
(197, 117, 'https://em2d.com.br/seligaai/offers/str_355557.jpeg', 1),
(198, 117, 'https://em2d.com.br/seligaai/offers/str_880631.jpeg', 1),
(317, 118, 'https://em2d.com.br/seligaai/offers/str_656682.jpeg', 1),
(316, 118, 'https://em2d.com.br/seligaai/offers/str_679539.jpeg', 1),
(315, 118, 'https://em2d.com.br/seligaai/offers/str_509222.jpeg', 1),
(314, 118, 'https://em2d.com.br/seligaai/offers/str_925523.jpeg', 1),
(313, 118, 'https://em2d.com.br/seligaai/offers/str_273221.jpeg', 1),
(213, 119, 'https://em2d.com.br/seligaai/offers/str_861876.jpeg', 1),
(212, 119, 'https://em2d.com.br/seligaai/offers/str_910310.jpeg', 1),
(211, 119, 'https://em2d.com.br/seligaai/offers/str_712805.jpeg', 1),
(210, 119, 'https://em2d.com.br/seligaai/offers/str_427765.jpeg', 1),
(209, 119, 'https://em2d.com.br/seligaai/offers/str_013991.jpeg', 1),
(231, 120, 'https://em2d.com.br/seligaai/offers/str_759313.jpeg', 1),
(230, 120, 'https://em2d.com.br/seligaai/offers/str_798757.jpeg', 1),
(229, 120, 'https://em2d.com.br/seligaai/offers/str_548131.jpeg', 1),
(228, 120, 'https://em2d.com.br/seligaai/offers/str_152944.jpeg', 1),
(218, 121, 'https://em2d.com.br/seligaai/offers/str_335064.jpeg', 1),
(219, 121, 'https://em2d.com.br/seligaai/offers/str_203638.jpeg', 1),
(220, 121, 'https://em2d.com.br/seligaai/offers/str_543764.jpeg', 1),
(221, 121, 'https://em2d.com.br/seligaai/offers/str_353257.jpeg', 1),
(227, 120, 'https://em2d.com.br/seligaai/offers/str_457314.jpeg', 1),
(232, 122, 'https://em2d.com.br/seligaai/offers/str_637345.jpeg', 1),
(233, 122, 'https://em2d.com.br/seligaai/offers/str_225651.jpeg', 1),
(234, 122, 'https://em2d.com.br/seligaai/offers/str_019971.jpeg', 1),
(235, 122, 'https://em2d.com.br/seligaai/offers/str_626871.jpeg', 1),
(236, 123, 'https://em2d.com.br/seligaai/offers/str_672974.jpeg', 1),
(237, 124, 'https://em2d.com.br/seligaai/offers/str_140027.jpeg', 1),
(238, 125, 'https://em2d.com.br/seligaai/offers/str_095525.jpeg', 1),
(239, 126, 'https://em2d.com.br/seligaai/offers/str_185446.jpeg', 1),
(240, 126, 'https://em2d.com.br/seligaai/offers/str_807321.jpeg', 1),
(241, 126, 'https://em2d.com.br/seligaai/offers/str_113641.jpeg', 1),
(242, 126, 'https://em2d.com.br/seligaai/offers/str_952893.jpeg', 1),
(243, 126, 'https://em2d.com.br/seligaai/offers/str_080899.jpeg', 1),
(244, 127, 'https://em2d.com.br/seligaai/offers/str_493587.jpeg', 1),
(245, 127, 'https://em2d.com.br/seligaai/offers/str_781556.jpeg', 1),
(246, 127, 'https://em2d.com.br/seligaai/offers/str_604454.jpeg', 1),
(247, 127, 'https://em2d.com.br/seligaai/offers/str_235657.jpeg', 1),
(248, 127, 'https://em2d.com.br/seligaai/offers/str_983058.jpeg', 1),
(249, 128, 'https://em2d.com.br/seligaai/offers/str_159954.jpeg', 1),
(250, 128, 'https://em2d.com.br/seligaai/offers/str_723982.jpeg', 1),
(251, 128, 'https://em2d.com.br/seligaai/offers/str_470400.jpeg', 1),
(252, 128, 'https://em2d.com.br/seligaai/offers/str_005529.jpeg', 1),
(253, 128, 'https://em2d.com.br/seligaai/offers/str_656746.jpeg', 1),
(254, 129, 'https://em2d.com.br/seligaai/offers/str_834858.jpeg', 1),
(255, 129, 'https://em2d.com.br/seligaai/offers/str_402696.jpeg', 1),
(256, 129, 'https://em2d.com.br/seligaai/offers/str_857197.jpeg', 1),
(257, 129, 'https://em2d.com.br/seligaai/offers/str_126929.jpeg', 1),
(258, 129, 'https://em2d.com.br/seligaai/offers/str_146073.jpeg', 1),
(259, 130, 'https://em2d.com.br/seligaai/offers/str_674976.jpeg', 1),
(260, 131, 'https://em2d.com.br/seligaai/offers/str_799778.jpeg', 1),
(261, 131, 'https://em2d.com.br/seligaai/offers/str_980244.jpeg', 1),
(262, 131, 'https://em2d.com.br/seligaai/offers/str_349888.jpeg', 1),
(263, 131, 'https://em2d.com.br/seligaai/offers/str_863976.jpeg', 1),
(264, 131, 'https://em2d.com.br/seligaai/offers/str_515513.jpeg', 1),
(265, 132, 'https://em2d.com.br/seligaai/offers/str_440089.jpeg', 1),
(266, 132, 'https://em2d.com.br/seligaai/offers/str_335748.jpeg', 1),
(267, 132, 'https://em2d.com.br/seligaai/offers/str_116252.jpeg', 1),
(268, 132, 'https://em2d.com.br/seligaai/offers/str_378521.jpeg', 1),
(269, 132, 'https://em2d.com.br/seligaai/offers/str_108939.jpeg', 1),
(270, 133, 'https://em2d.com.br/seligaai/offers/str_585914.jpeg', 1),
(271, 133, 'https://em2d.com.br/seligaai/offers/str_883482.jpeg', 1),
(272, 133, 'https://em2d.com.br/seligaai/offers/str_104236.jpeg', 1),
(273, 133, 'https://em2d.com.br/seligaai/offers/str_744851.jpeg', 1),
(274, 133, 'https://em2d.com.br/seligaai/offers/str_124713.jpeg', 1),
(275, 134, 'https://em2d.com.br/seligaai/offers/str_303438.jpeg', 1),
(276, 134, 'https://em2d.com.br/seligaai/offers/str_184722.jpeg', 1),
(277, 134, 'https://em2d.com.br/seligaai/offers/str_153419.jpeg', 1),
(278, 134, 'https://em2d.com.br/seligaai/offers/str_281260.jpeg', 1),
(279, 134, 'https://em2d.com.br/seligaai/offers/str_977422.jpeg', 1),
(280, 135, 'https://em2d.com.br/seligaai/offers/str_105529.jpeg', 1),
(281, 135, 'https://em2d.com.br/seligaai/offers/str_131366.jpeg', 1),
(282, 135, 'https://em2d.com.br/seligaai/offers/str_077780.jpeg', 1),
(283, 135, 'https://em2d.com.br/seligaai/offers/str_724244.jpeg', 1),
(284, 135, 'https://em2d.com.br/seligaai/offers/str_013020.jpeg', 1),
(285, 136, 'https://em2d.com.br/seligaai/offers/str_760524.jpeg', 1),
(286, 136, 'https://em2d.com.br/seligaai/offers/str_395956.jpeg', 1),
(287, 136, 'https://em2d.com.br/seligaai/offers/str_200605.jpeg', 1),
(288, 136, 'https://em2d.com.br/seligaai/offers/str_844953.jpeg', 1),
(289, 136, 'https://em2d.com.br/seligaai/offers/str_644167.jpeg', 1),
(290, 137, 'https://em2d.com.br/seligaai/offers/str_638417.jpeg', 1),
(291, 138, 'https://em2d.com.br/seligaai/offers/str_188531.jpeg', 1),
(292, 138, 'https://em2d.com.br/seligaai/offers/str_908320.jpeg', 1),
(293, 139, 'https://em2d.com.br/seligaai/offers/str_152540.jpeg', 1),
(294, 139, 'https://em2d.com.br/seligaai/offers/str_869677.jpeg', 1),
(295, 139, 'https://em2d.com.br/seligaai/offers/str_143705.jpeg', 1),
(296, 140, 'https://em2d.com.br/seligaai/offers/str_091738.jpeg', 1),
(297, 141, 'https://em2d.com.br/seligaai/offers/str_232243.jpeg', 1),
(298, 142, 'https://em2d.com.br/seligaai/offers/str_214202.jpeg', 1),
(299, 143, 'https://em2d.com.br/seligaai/offers/str_905454.jpeg', 1),
(300, 144, 'https://em2d.com.br/seligaai/offers/str_315458.jpeg', 1),
(301, 144, 'https://em2d.com.br/seligaai/offers/str_828577.jpeg', 1),
(302, 144, 'https://em2d.com.br/seligaai/offers/str_997114.jpeg', 1),
(303, 144, 'https://em2d.com.br/seligaai/offers/str_078501.jpeg', 1),
(304, 145, 'https://em2d.com.br/seligaai/offers/str_400315.jpeg', 1),
(305, 145, 'https://em2d.com.br/seligaai/offers/str_748620.jpeg', 1),
(306, 145, 'https://em2d.com.br/seligaai/offers/str_437636.jpeg', 1),
(307, 145, 'https://em2d.com.br/seligaai/offers/str_581864.jpeg', 1),
(308, 145, 'https://em2d.com.br/seligaai/offers/str_284087.jpeg', 1),
(318, 146, 'https://em2d.com.br/seligaai/offers/str_844115.jpeg', 1),
(319, 147, 'https://em2d.com.br/seligaai/offers/str_516103.jpeg', 1),
(320, 147, 'https://em2d.com.br/seligaai/offers/str_210476.jpeg', 1),
(321, 147, 'https://em2d.com.br/seligaai/offers/str_898669.jpeg', 1),
(322, 148, 'https://em2d.com.br/seligaai/offers/str_974687.jpeg', 1),
(323, 148, 'https://em2d.com.br/seligaai/offers/str_681279.jpeg', 1),
(324, 148, 'https://em2d.com.br/seligaai/offers/str_356551.jpeg', 1),
(378, 154, 'https://em2d.com.br/seligaai/offers/str_547269.jpeg', 1),
(377, 154, 'https://em2d.com.br/seligaai/offers/str_707577.jpeg', 1),
(376, 154, 'https://em2d.com.br/seligaai/offers/str_962415.jpeg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `plans`
--

DROP TABLE IF EXISTS `plans`;
CREATE TABLE IF NOT EXISTS `plans` (
  `plaid` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `plan` varchar(30) NOT NULL,
  `approvalcode` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `maxinstallments` int(2) NOT NULL DEFAULT '1',
  `numoffers` int(4) NOT NULL,
  `canuseadmin` int(1) NOT NULL DEFAULT '0',
  `canseller` int(1) NOT NULL DEFAULT '0',
  `periodmonth` int(2) NOT NULL,
  `background` varchar(100) NOT NULL,
  `recomended` int(1) NOT NULL DEFAULT '0',
  `order` int(2) NOT NULL,
  PRIMARY KEY (`plaid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `plans`
--

INSERT INTO `plans` (`plaid`, `active`, `plan`, `approvalcode`, `description`, `price`, `maxinstallments`, `numoffers`, `canuseadmin`, `canseller`, `periodmonth`, `background`, `recomended`, `order`) VALUES
(1, 1, 'Start', '', 'Pacote mensal para quem possui ofertas ocasionais.', '179.80', 1, 50, 1, 0, 2, 'https://em2d.com.br/seligaai/icons/plans/plan1.jpg', 0, 2),
(2, 1, 'Start+', '', '', '319.60', 1, 100, 1, 1, 4, 'https://em2d.com.br/seligaai/icons/plans/plan2.jpg', 0, 3),
(3, 1, 'Advanced', '', '', '359.40', 1, 9999, 1, 1, 6, 'https://em2d.com.br/seligaai/icons/plans/plan3.jpg', 1, 4),
(5, 0, 'Experience', '', '30 dias grátis para você conhecer nossa plataforma.', '0.00', 1, 2, 0, 0, 1, 'https://em2d.com.br/seligaai/icons/plans/plan4.jpg', 0, 1),
(6, 1, 'Light', '', '', '49.90', 1, 5, 0, 0, 1, 'https://em2d.com.br/seligaai/icons/plans/plan4.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `proid` int(11) NOT NULL AUTO_INCREMENT,
  `ean` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `photo` varchar(255) NOT NULL,
  PRIMARY KEY (`proid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`proid`, `ean`, `name`, `photo`) VALUES
(1, '7892509091831', 'Smartphone Samsung Galaxy S8', 'https://www.extra-imagens.com.br/TelefoneseCelulares/Smartphones/Android/11482022/811971657/smartphone-samsung-galaxy-s8-dual-chip-ametista-com-64gb-tela-5-8-android-7-0-4g-camera-12mp-e-octa-core-11482022.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `storeaddresses`
--

DROP TABLE IF EXISTS `storeaddresses`;
CREATE TABLE IF NOT EXISTS `storeaddresses` (
  `addid` int(11) NOT NULL AUTO_INCREMENT,
  `strid` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `branchname` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `number` varchar(10) NOT NULL,
  `complement` varchar(30) DEFAULT NULL,
  `neighboor` varchar(40) NOT NULL,
  `zipcode` char(9) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` char(2) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `tel1` varchar(15) DEFAULT NULL,
  `tel2` varchar(15) DEFAULT NULL,
  `url` varchar(60) DEFAULT NULL,
  `twitter` varchar(130) DEFAULT NULL,
  `instagram` varchar(130) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `open24h` text,
  `cnpj` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`addid`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `storeaddresses`
--

INSERT INTO `storeaddresses` (`addid`, `strid`, `active`, `branchname`, `address`, `number`, `complement`, `neighboor`, `zipcode`, `city`, `state`, `whatsapp`, `tel1`, `tel2`, `url`, `twitter`, `instagram`, `email`, `latitude`, `longitude`, `open24h`, `cnpj`) VALUES
(49, 23, 1, 'Loja Valentina', 'Rua da Piedade', '463', '', 'Gercino Coelho', '56306-240', 'Petrolina', 'PE', '', '(87) 9629-6771', '', '', NULL, NULL, 'marcos_junior_@hotmail.com', '-9.389723', '-40.509628', NULL, ''),
(52, 26, 1, 'Brutus Barbearia', 'Avenida Nilo Coelho', '207', '', 'Gercino Coelho', '56306-000', 'Petrolina', 'PE', '(87) 99810-3968', '(87) 9886-8997', '(87) 9886-8997', 'https://www.facebook.com/ribeirocabeloebarba', NULL, NULL, 'ribeirocabeloebarba@gmail.com', '-9.3903729', '-40.5104685', NULL, ''),
(53, 27, 1, 'Climax', 'Avenida gaudencio barros', '124', 'Loja', 'Joao XXIII', '48900-335', 'Juazeiro', 'Ba', '(87) 99962-3655', '(74) 3611-9002', '', '', NULL, NULL, '', '-9.424286', '-40.505123', NULL, ''),
(54, 28, 1, 'Unidade praça 21 de setembro', 'Avenida Guararapes', '', '', 'Centro', '56302-000', 'Petrolina', 'PE', '(87) 98812-4121', '(87) 8852-5040', '', '', NULL, NULL, '', '-9.3997563', '-40.5012086', NULL, ''),
(55, 29, 0, 'Climacenter', 'Rua Doutor José Mariano', '2811', 'A', 'Centro', '56302-090', 'Petrolina', 'PE', '(87) 98858-2372', '(87) 8858-2372', '', '', NULL, NULL, 'climacenterpetrolina@outlook.com', '-9.4019726', '-40.4961041', NULL, ''),
(56, 30, 1, 'Sabor em Dobro', 'Rua Arcoverde', '274', 'Posto Reis', 'Atrás da Banca', '56308-130', 'Petrolina', 'PE', '(87) 99815-8418', '(87) 9815-8418', '(87) 9964-8939', '', NULL, NULL, 'convenienciasaboremdobro@outlook.com', '-9.3952218', '-40.5097824', NULL, ''),
(57, 31, 1, 'Se liga aí loja', 'Rua Dona Ana Flora Pinheiro de Sousa', '258', '', 'Vila Jacui', '08060-150', 'São Paulo', 'SP', '', '(11) 5895-6562', '', '', NULL, NULL, '', '-23.5022874', '-46.4514747', NULL, NULL),
(58, 31, 1, 'Se liga aí loja', 'Rua Dona Ana Pimentel', '258', '', 'Água Branca', '05002-040', 'São Paulo', 'SP', '', '(12) 5458-5655', '', '', NULL, NULL, '', '-23.5280276', '-46.673182', NULL, NULL),
(61, 33, 1, 'D\'ella & D\'elle', 'Avenida São Francisco', '5', '', 'Areia Branca', '56328-420', 'Petrolina', 'PE', '(87) 99905-8692', '(87) 9605-0105', '', '', NULL, NULL, '', '-9.3813169', '-40.492617', NULL, NULL),
(63, 32, 1, 'Loja Valentina', 'Rua da Piedade', '770', 'Casa Rosa', 'Gercino Coelho', '56306-240', 'Petrolina', 'PE', '(74) 98831-3127', '(74) 8831-3127', '', '', NULL, NULL, '', '-9.3909524', '-40.5098649', NULL, NULL),
(64, 35, 1, 'Power Moda Fitness', 'Avenida São Francisco', '53', '', 'Areia Branca', '56302-281', 'Petrolina', 'PE', '(87) 98875-6975', '(87) 8841-8019', '(87) 8834-6106', '', NULL, NULL, '', '-9.3845569', '-40.4926988', NULL, NULL),
(65, 36, 1, 'Charme de Mulher', 'Avenida São Francisco', '6', 'A', 'Areia Branca', '56330-095', 'Petrolina', 'PE', '(87) 99931-9201', '(87) 9931-9201', '(87) 9936-9018', '', NULL, NULL, 'isamilly92@hotmail.com', '-9.3820361', '-40.4926978', NULL, NULL),
(67, 37, 1, 'Rei dos suplementos ', 'Avenida São Francisco', '6', 'loja 3', 'Areia Branca', '56330-095', 'Petrolina', 'PE', '(87) 99980-4113', '(87) 8815-9808', '(87) 9980-4113', '', NULL, NULL, 'reidossuplemento@hotmail.com', '-9.3820361', '-40.4926978', NULL, NULL),
(68, 38, 1, 'Wilde Lima fotografia', 'Avenida Nilo Coelho', '153 a', '', 'Gercino Coelho', '56306-040', 'Petrolina', 'PE', '(87) 98809-2327', '(87) 3861-5583', '(87) 9880-9232', 'https://www.instagram.com/wilde_lima_fotografia', NULL, NULL, 'wildelimafotografia@gmail.com', '-9.3908018', '-40.5103108', NULL, NULL),
(69, 39, 1, 'Geladão Rio Corrente', 'Rua do Morango', '70', '', 'Rio Corrente', '56312-175', 'Petrolina', 'PE', '(74) 99148-6261', '(87) 8818-8740', '(87) 9881-8874', '', '', 'https://www.instagram.com/geladaoriocorrente', '', '-9.3916327', '-40.5470297', NULL, NULL),
(71, 41, 1, 'Marrylla Esmalteria ', 'Avenida Sete de Setembro', '178', 'Loja 04', 'José e Maria', '56306-610', 'Petrolina', 'PE', '(87) 99905-4192', '(87) 9905-4192', '(87) 9654-0295', '', '', 'https://www.instagram.com/marryllaesmalteria', 'marryllaesmalteria@hotmail.com', '-9.3754834', '-40.4925205', NULL, NULL),
(72, 42, 1, 'Maria Fernanda Design', 'Rua da Uniao', '219', 'Casa', 'Atrás da Banca', '56308-040', 'Petrolina', 'PE', '(87) 98868-2827', '(87) 3985-5136', '', '', '', 'https://instagram.com/mariafernanda_designer?utm_source=ig_profile_share&igshid=10mnrsrjgu209', '', '-9.3960069', '-40.5102499', NULL, NULL),
(73, 43, 1, 'Tatty Modas', 'Rua Rio Amazonas', '16', 'A', 'José e Maria', '56320-400', 'Petrolina', 'PE', '(87) 98819-2214', '(87) 3864-2122', '(87) 8819-2214', '', '', 'Https://www.instagram.com/lojatattymodas', 'tathianadsb@hotmail.com', '-9.3728925', '-40.4926395', NULL, NULL),
(74, 44, 1, 'Josefina Esmalteria', 'av. estados unidos', '60', '', 'Areia Branca', '56330-085', 'Petrolina', 'PE', '(87) 98843-1382', '(87) 3024-4382', '(87) 8873-4217', '', '', 'Https://www.instagram.com/josefina.esmalteria', 'josefinaesmalteria@gmail.com', '-9.3860094', '-40.4951457', NULL, NULL),
(75, 45, 1, 'Tatyane Monteiro', 'Avenida da Integração', '1553', 'Predio ', 'Maria Auxiliadora', '56300-000', 'Petrolina', 'PE', '(87) 98872-1216', '(87) 3866-4602', '(87) 8838-9271', '', '', '', '', '-9.3963387', '-40.4831766', NULL, NULL),
(76, 44, 1, 'Josefina Esmalteria', 'Rua da Polonia', '42', '', 'Areia Branca', '56330-085', 'Petrolina', 'PE', '(87) 98873-4217', '(87) 8873-4217', '(87) 8873-4217', '', '', 'https://www.instagram.com/josefinaesmalteria', 'josefinaesmalteria@gmail.com', '-9.3843677', '-40.4927753', NULL, NULL),
(78, 48, 1, 'Moda start', 'Avenida da Integração', '0', 'Loja', 'São José', '56300-000', 'Petrolina', 'PE', '', '(87) 8841-3850', '', '', '', '', '', '-9.3917941', '-40.4859218', NULL, NULL),
(79, 48, 1, 'Moda start', 'Avenida São Francisco', '0', '', 'Areia Branca', '56300-000', 'Petrolina', 'PE', '', '(87) 8841-3850', '', '', '', '', '', '-9.3803239', '-40.492704', NULL, NULL),
(80, 23, 1, 'Loja Valentina', 'Rua da Piedade', '100', '', 'Gercino Coelho', '56306-240', 'Petrolina', 'PE', '(74) 98831-3127', '(74) 8831-3127', '', '', '', '', '', '-9.386422', '-40.510832', NULL, NULL),
(81, 51, 1, 'INFOWORK', 'Rua Dois', '250', '', 'Santa Luzia', '56300-000', 'Petrolina', 'PE', '(87) 98805-8473', '(87) 8805-8473', '', '', NULL, NULL, 'aldair19861@hotmail.com', '-9.3997239', '-40.5475384', NULL, NULL),
(82, 52, 1, 'Mundo Infantil', 'Rua Olinda', '15', '', 'José e Maria', '56320-480', 'Petrolina', 'PE', '(87) 99822-9773', '(87) 9982-2977', '', '', NULL, NULL, '', '-9.3657776', '-40.49825', NULL, NULL),
(85, 55, 1, 'Sushi Dois Irmãos', 'Av 32', '186 A', '', 'Cohab Massangano', '56300-000', 'Petrolina', 'PE', '(87) 98819-3340', '(87) 9881-9334', '', '', '', 'https://www.instagram.com/sushiidoisirmaos', '', '-9.381936', '-40.5397325', NULL, NULL),
(86, 56, 1, 'Bela Unhas ', 'Rua-52', '05', '', 'Cohab Massangano ', '56310-490', 'Petrolina', 'Pe', '(87) 98844-9267', '(87) 9884-4926', '(87) 8844-9267', '', '', 'https://www.instagram.com/belaunhaspnz', '', '-9.381936', '-40.5397325', NULL, NULL),
(87, 25, 1, 'Lojas brasil', 'Rua Rio São Francisco', '0', '', 'José e Maria', '56300-000', 'Petrolina', 'PE', '', '(87) 0000-0000', '', '', '', '', '', '-9.3742428', '-40.4938079', NULL, NULL),
(88, 57, 1, 'BeKa BaKana', 'Av. 32 cohab massangano', '05', '', 'Cohab Massangano', '56310-490', 'Petrolina', 'PE', '(87) 99142-5538', '(87) 9142-5538', '', '', '', 'https://www.instagram.com/bekabakanakidsof', '', '-9.381936', '-40.5397325', NULL, NULL),
(89, 58, 1, 'DS Casa do Bolo ', 'Avenida 32 Cohab Massangano', '346', '', 'Cohab massangano ', '56310-290', 'Petrolina', 'Pe', '(74) 98833-3908', '(74) 8833-3908', '(83) 9165-2598', '', '', '', 'divaninilsonxavier@hotmal.com', '-9.381936', '-40.5397325', NULL, NULL),
(90, 59, 1, 'Naturais Duvalle', 'Rua Bernardino de Amorim Coelho', '16', '', 'José e Maria', '56320-350', 'Petrolina', 'PE', '', '(74) 8812-0056', '', '', '', '', '', '-9.3755079', '-40.492205', NULL, NULL),
(91, 60, 1, 'Buenos Burgue\'s', 'Avenida Mario Rodrigues Coelho', '115', 'D', 'Portal da Cidade', '56313-275', 'Petrolina', 'PE', '', '(87) 8802-5884', '', '', '', 'https://www.instagram.com', '', '-9.379189', '-40.53999', NULL, NULL),
(92, 61, 1, 'Rose Estetica', 'Rua Crispim de Amorim Coelho', '136-A', '', 'Centro', '56304-220', 'Petrolina', 'PE', '(87) 98836-7127', '(87) 8836-7127', '', '', '', 'https://www.instagram.com/rosileneestetica', 'rosegomes-gui@hotmail.com', '-9.3929824', '-40.4999213', NULL, NULL),
(93, 34, 1, 'Kids Store by Areli', 'Rua Benevides Prado', '14', '', 'Dom Malan', '56330-540', 'Petrolina', 'PE', '', '(87) 9946-1447', '', '', '', '', '', '-9.3807467', '-40.5004305', NULL, NULL),
(95, 62, 1, 'Vitrage', 'Rua Edmundo Fernando Souza', '326', '', 'Cohab Massangano', '56310-605', 'Petrolina', 'PE', '(87) 99952-4425', '(87) 3863-2237', '', '', '', 'https://instagram.com/lojavitrage?utm_source=ig_profile_share&igshid=1none8skpfnfa', 'vitrageloja10@hotmail.com', '-9.3865292', '-40.5430053', NULL, NULL),
(96, 63, 1, 'Mabelly', 'Avenida Souza Filho', '516', 'Galeria Eco Center', 'Centro', '56304-020', 'Petrolina', 'PE', '(87) 98811-9952', '(87) 8811-9952', '', '', '', 'mabelly.modas', 'geisa.rodrigues@hotmail.com', '-9.3970611', '-40.5017831', NULL, NULL),
(97, 64, 1, 'Gigatec', 'Avenida São Francisco', '192', 'B', 'Areia Branca', '56330-095', 'Petrolina', 'PE', '(87) 99947-7136', '(87) 3983-0063', '(87) 8804-8161', '', '', 'https://www.instagram.com/gigatecpetrolina', 'gigatec_1@hotmail.com', '-9.3762988', '-40.4926319', NULL, NULL),
(98, 40, 1, 'Techfrio Refrigeração ', 'Rua Coroa de Frade', '285', 'Casa', 'Areia Branca', '56328-470', 'Petrolina', 'PE', '(87) 98838-9793', '(87) 3864-0551', '(87) 9880-1487', '', '', 'SirinoSantos', 'sirino_jc@hotmail.com', '-9.379237', '-40.489955', NULL, NULL),
(99, 65, 1, '12 Tons', 'Avenida Fernando Góes', '302 ', 'A', 'Centro', '56304-020', 'Petrolina', 'PE', '(87) 99905-0090', '(87) 8816-3606', '(87) 8812-5967', '', '', '@12tons_', '', '-9.3953331', '-40.5009531', NULL, NULL),
(100, 66, 1, 'Ellen Modas', 'Avenida dos Tropeiros', '11', '', 'Jardim amazonas', '56318-500', 'Petrolina', 'PE', '(74) 98846-6584', '(74) 8846-6584', '(74) 8846-6584', '', '', '', 'ellenmagazine@homail.com', '-9.377151', '-40.5271825', NULL, NULL),
(101, 67, 1, 'Closet Mania', 'Rua José Braz da Silva', '16', 'Avenida dos Tropeiros', 'Jardim Amazonas', '56318-250', 'Petrolina', 'PE', '(87) 98851-4021', '(87) 8851-4021', '', '', '', '', 'erivaniasouza200@gmail.com', '-9.375764', '-40.527993', NULL, NULL),
(102, 68, 1, 'Rosa Bela Depilação', 'Avenida dos Tropeiros', '13', '', 'Jardim Amazonas ', '56318-500', 'Petrolina', 'PE', '(11) 95239-1095', '(11) 5239-1095', '', '', '', '@jeaneneri', '', '-9.375764', '-40.527993', NULL, NULL),
(103, 69, 1, 'Iana Acessórios ', 'Rua José Inácio Pereira', '21', '', 'Jardim Amazonas', '56318-470', 'Petrolina', 'PE', '(87) 99915-1310', '(87) 9915-1310', '(87) 8852-8193', '', '', '@ianaacessorios', 'ianaconfeccoes@Yahoo.com.br', '-9.373415', '-40.529267', NULL, NULL),
(104, 70, 1, 'Fernanda Modas', 'Rua Teresa Cristina Ventura', '11', 'Avenida dos tropeiro', 'Jardim Amazonas', '56318-430', 'Petrolina', 'PE', '(87) 99943-0702', '(87) 3865-2366', '(87) 8851-1716', '', '', '@fmodasstore', 'maryangelinaa@hotmail.com', '-9.3748753', '-40.5290719', NULL, NULL),
(105, 71, 1, 'Vire Vest', 'Rua Doutor Luíz Numeriano', '10', 'Avenida dos Tropeiro', 'Jardim Amazonas', '56318-410', 'Petrolina', 'PE', '(87) 98878-4425', '(87) 8878-4425', '(87) 8827-5253', '', '', '@virevest', 'vivianbrt6@gmail.com', '-9.375764', '-40.527993', NULL, NULL),
(106, 72, 1, 'Pastelaria do Sul', 'Avenida dos Tropeiros', '10 D', '', 'Pedro Raimundo ', '35400-000', 'Petrolina', 'PE', '(87) 99818-9918', '(87) 9818-9918', '', '', '', '', 'nicole.incrivel@hotmail.com', '-9.375764', '-40.527993', NULL, NULL),
(107, 73, 1, 'Cuscuzeria e Tapiocaria ', 'Avenida dos Tropeiros', '23', '', 'Jardim Amazonas', '56318-140', 'Petrolina', 'PE', '(87) 98859-5039', '(87) 8859-5039', '', '', '', '', 'elizconrado1@gmail.com', '-9.375764', '-40.527993', NULL, NULL),
(108, 75, 1, 'UBS Adesivos', 'Avenida dos Tropeiros', '11', '', 'Pedro Raimundo', '56318-180', 'Petrolina', 'PE', '(87) 98843-1164', '(87) 8872-3772', '', '', '', '', 'uilame12@gmail.com', '-9.377151', '-40.5271825', NULL, NULL),
(109, 76, 1, 'Savanna\'s ', 'Avenida dos Tropeiros', '11', '', 'Pedro Raimundo', '56318-180', 'Petrolina', 'PE', '(87) 98863-6977', '(87) 8863-6977', '', '', '', '', 'nyvalldinho@gmail.com', '-9.373415', '-40.529267', NULL, NULL),
(110, 74, 1, 'Santana Cell', 'Rua D1, Petrolina ', '001', '', 'Monsenhor Bernadino', '56300-000', 'Petrolina ', 'PE', '(87) 99911-2547', '(87) 9911-2547', '', '', '', '', 'josesantanasousa@hotmail.com', '-9.373415', '-40.529267', NULL, NULL),
(111, 77, 1, 'Point da Moda', 'Travessa Benjamin Constant', '108', '', 'Centro', '48904-020', 'Juazeiro', 'BA', '(74) 98863-2301', '(74) 8863-2601', '', '', '', '', 'leideaneldn60@gmail.com', '-9.4146609', '-40.5055551', NULL, NULL),
(112, 78, 1, 'Info Micro House', 'Rua André Vidal de Negreiros', '220', 'esquina', 'maria auxiliadora', '56330-420', 'petrolina', 'pe', '(87) 98828-5240', '(87) 3864-2499', '', '', '', '', 'inmorais@gmail.com', '-9.38898', '-40.4957676', NULL, NULL),
(114, 79, 1, 'Melissa Modas', 'Rua José Inácio Pereira', '21 D', '', 'Jardim Amazonas', '56318-470', 'Petrolina', 'PE', '(87) 98836-2117', '(87) 8836-2117', '(87) 8836-2117', '', '', 'melissamodaevangelica', 'madalenacoelho15@hotmail.com', '-9.373415', '-40.529267', NULL, NULL),
(115, 54, 0, 'Empresa Daniel Triboni', 'Rua Vergueiro', '10', '', 'Vila Nair', '04002-010', 'São Paulo', 'SP', '', '(11) 5845-8458', '', '', '', '', '', '-23.5774596', '-46.6409166', NULL, NULL),
(116, 23, 1, 'Loja Valentina', 'Avenida das Nações', '56', '', 'Centro', '56304-320', 'Petrolina', 'PE', '(74) 98831-3127', '(74) 9883-1312', '', '', '', '', 'mgaldino10@gmail.com', '-9.3947283', '-40.506517', NULL, NULL),
(117, 80, 1, 'Portal USB', 'Rua da Piedade', '532', 'A', 'Gercino Coelho', '56306-240', 'Petrolina', 'PE', '(87) 99888-451', '(87) 8856-0020', '(87) 8835-8467', '', '', '', '', '-9.3902916', '-40.5095', NULL, NULL),
(119, 81, 1, 'Recepções centrais', 'Rua Rio Riacho Seco', '10', '', 'José e Maria', '56320-075', 'Petrolina', 'PE', '(87) 98841-3850', '(87) 9884-1385', '(87) 9884-1385', '', '', '', '', '-9.3707143', '-40.4926249', NULL, NULL),
(120, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '581', '', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(34) 63957-3404', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(121, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '4', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(11) 32423-4324', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(122, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '5543', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(43) 24234-2342', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(123, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '5345', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(54) 35435-4354', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(124, 24, 0, 'Rua Padeiro Joao Luiz, 137', 'Rua Padeiro Joao Luiz, 137', '545', 'Apto 201 Sala A', 'Apto 201 Sala A', '56330-470', 'Petrolina', 'PE', '', '(55) 1198-8584', '(54) 35345-3454', '', '', 'Daniel Triboni', 'seligaaapp@gmail.com', '', '', NULL, NULL),
(125, 24, 0, 'Rua Dona Ana Neri 581, Cambuci', 'Rua Dona Ana Neri 581, Cambuci', '545', 'Apto 201 Sala A', 'Apto 201 Sala A', '56330-470', 'Petrolina', 'PE', '', '(55) 1198-8584', '(54) 35345-3454', '', '', 'Daniel Triboni', 'seligaaapp@gmail.com', '', '', NULL, NULL),
(126, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '543534543', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(53) 45345-3454', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(127, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '354435', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(53) 45345-4353', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(128, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '654645', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(65) 46456-4564', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(129, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '565', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(54) 35656-5464', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(130, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '65465', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(65) 46464-6456', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(131, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '65643646', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(65) 46876-8545', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(132, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '655435', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '(53) 45__-____', '(65) 46456-5464', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(133, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '56546', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(64) 56456-4564', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(134, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '656546', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(64) 56456-4564', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(135, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 5812', '65464564', 'Cambuci2', 'Cambuci2', '01522-000', 'São Paulo/Cambuci2', 'CE', '', '', '(65) 46456-5645', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(136, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '65645', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(65) 64564-6464', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(137, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '54435', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(53) 45345-3453', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(138, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '545345', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(54) 35376-6435', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(139, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '645645', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(64) 56456-4645', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(140, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '645645', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(64) 56456-4645', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(141, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '645645', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(64) 56456-4645', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(142, 24, 0, 'Avenida Paulista', 'Avenida Paulista', '654646', '1938', '1938', '01522-000', 'São Paulo', 'SP', '', '', '(64) 56756-4353', '', '', '', '', '', '', NULL, NULL),
(143, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '545435', '', 'fdggdfgfd', '01522-000', 'São Paulo/Cambuci', 'ES', '', '(55) 1123-6107', '(65) 65464-5654', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(144, 24, 0, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '65464', 'Cambuci', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(46) 45646-5464', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(145, 24, 1, 'Rua Dona Ana Neri 581', 'Rua Dona Ana Neri 581', '656456', 'Cambuci2', 'Cambuci', '01522-000', 'São Paulo/Cambuci', 'SP', '', '', '(45) 37654-4665', '', '', 'Daniel Triboni', 'danieltriboni@gmail.com', '', '', NULL, NULL),
(146, 64, 1, 'Rua Padre Carapuceiro', 'Rua Padre Carapuceiro', '220', 'CASA 1', 'Boa Viagem', '51020-000', 'Recife', 'PE', '(43) 89579-0453', '(12) 1249-8345', '(25) 45789-8898', '', '', '', 'seliga@teste.com', '-8.1179052', '-34.900070700000015', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `storepayments`
--

DROP TABLE IF EXISTS `storepayments`;
CREATE TABLE IF NOT EXISTS `storepayments` (
  `payid` int(11) NOT NULL AUTO_INCREMENT,
  `stpid` int(11) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `gatid` varchar(50) DEFAULT NULL,
  `cancelled` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payid`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `storepayments`
--

INSERT INTO `storepayments` (`payid`, `stpid`, `value`, `expires`, `paid`, `gatid`, `cancelled`) VALUES
(20, 20, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(21, 21, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(22, 22, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(23, 23, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(24, 24, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(25, 25, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(26, 26, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(27, 27, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(28, 28, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(29, 29, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(30, 30, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(31, 31, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(32, 32, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(33, 33, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(34, 34, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(35, 35, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(36, 36, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(38, 38, '1.90', '2019-01-07 00:00:00', 1, 'CAF6B255-4CCF-4F3C-932F-08CAB6984E8E', 0),
(39, 39, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(41, 41, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(42, 42, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(44, 44, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(45, 45, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(46, 46, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(47, 47, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(48, 48, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(49, 49, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(50, 50, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(51, 51, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(52, 52, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(53, 53, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(54, 54, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(55, 55, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(56, 56, '0.00', '2019-01-07 00:00:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(57, 57, '0.00', '2019-01-07 12:49:38', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(58, 58, '0.00', '2019-01-07 13:48:17', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(59, 59, '0.00', '2019-01-07 14:47:04', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(60, 60, '0.00', '2019-01-07 15:27:50', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(61, 61, '0.00', '2019-01-07 18:08:54', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(62, 62, '0.00', '2019-01-07 19:42:14', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(63, 63, '0.00', '2019-01-07 20:46:42', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(64, 64, '0.00', '2019-01-07 21:21:34', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(65, 65, '0.00', '2019-01-07 23:09:01', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(66, 66, '0.00', '2019-01-07 23:32:00', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(67, 67, '0.00', '2019-01-08 00:48:29', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(68, 68, '0.00', '2019-01-08 01:22:48', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(69, 69, '0.00', '2019-01-08 18:03:18', 1, 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6', 0),
(72, 72, '49.90', '2019-03-30 18:22:07', 1, '40B607AC-BC94-477C-B487-192A36E3BA2A', 0),
(99, 99, '179.80', '2019-04-14 00:00:00', 1, '45F6962C-1AA6-407C-9222-BD4562D052C4', 0),
(100, 100, '319.60', '2019-03-31 15:55:12', 0, 'D254CE89-96A9-4A0F-9B86-3453BE519CAF', 1),
(101, 101, '49.90', '2019-04-28 15:02:54', 1, '50BF4BFC-152B-4FBC-9E65-052AD3514637', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `storeplans`
--

DROP TABLE IF EXISTS `storeplans`;
CREATE TABLE IF NOT EXISTS `storeplans` (
  `stpid` int(11) NOT NULL AUTO_INCREMENT,
  `strid` int(11) NOT NULL,
  `plaid` int(11) NOT NULL,
  PRIMARY KEY (`stpid`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `storeplans`
--

INSERT INTO `storeplans` (`stpid`, `strid`, `plaid`) VALUES
(20, 24, 5),
(21, 23, 5),
(22, 25, 5),
(23, 26, 5),
(24, 27, 5),
(25, 29, 5),
(26, 30, 5),
(27, 33, 5),
(28, 35, 5),
(29, 36, 5),
(30, 37, 5),
(31, 38, 5),
(32, 41, 5),
(33, 42, 5),
(34, 43, 5),
(35, 44, 5),
(36, 45, 5),
(37, 24, 6),
(38, 24, 6),
(39, 48, 5),
(41, 52, 5),
(42, 54, 5),
(44, 55, 5),
(45, 56, 5),
(46, 57, 5),
(47, 58, 5),
(48, 59, 5),
(49, 60, 5),
(50, 61, 5),
(51, 34, 5),
(52, 62, 5),
(53, 63, 5),
(54, 64, 5),
(55, 40, 5),
(56, 65, 5),
(57, 66, 5),
(58, 67, 5),
(59, 68, 5),
(60, 69, 5),
(61, 70, 5),
(62, 71, 5),
(63, 72, 5),
(64, 73, 5),
(65, 75, 5),
(66, 76, 5),
(67, 74, 5),
(68, 77, 5),
(69, 79, 5),
(72, 64, 6),
(99, 64, 1),
(100, 64, 2),
(101, 54, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `stores`
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `strid` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL,
  `usrid` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `cpfcnpj` varchar(20) DEFAULT NULL,
  `bank` varchar(6) DEFAULT NULL,
  `agency` varchar(6) DEFAULT NULL,
  `account` varchar(10) DEFAULT NULL,
  `owner` varchar(60) DEFAULT NULL,
  `store` varchar(50) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`strid`),
  KEY `fk_categories_stores` (`catid`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `stores`
--

INSERT INTO `stores` (`strid`, `catid`, `usrid`, `active`, `cpfcnpj`, `bank`, `agency`, `account`, `owner`, `store`, `logo`) VALUES
(23, 8, 134, 1, NULL, NULL, NULL, NULL, NULL, 'Loja Valentina', 'https://em2d.com.br/seligaai/stores/str_029462.jpeg'),
(24, 1, 137, 1, '288.711.918-46', NULL, NULL, NULL, NULL, 'Balada Triboni', 'https://em2d.com.br/seligaai/stores/str_527966.jpeg'),
(25, 3, 139, 1, '31.318.426/0001-11', NULL, NULL, NULL, NULL, 'Lojas brasil', 'https://em2d.com.br/seligaai/stores/str_769463.jpeg'),
(26, 8, 150, 1, '26.909.063/0001-59', NULL, NULL, NULL, NULL, 'Brutus Barbearia', 'https://em2d.com.br/seligaai/stores/str_114106.jpeg'),
(27, 8, 170, 1, '08.401.414/0001-34', '260', '1001-1', '1243548', 'SIETON FERNANDES', 'Climax', 'https://em2d.com.br/seligaai/stores/str_681025.jpeg'),
(28, 8, 183, 1, '15.365.304/0001-20', NULL, NULL, NULL, NULL, 'Guararapes Barber Shop', 'https://em2d.com.br/seligaai/stores/str_484416.jpeg'),
(29, 8, 200, 1, '20.290.166/0001-05', NULL, NULL, NULL, NULL, 'Climacenter', 'https://em2d.com.br/seligaai/stores/clicacerter.png'),
(30, 20, 202, 1, '31.402.485/0001-73', NULL, NULL, NULL, NULL, 'Sabor em Dobro', 'https://em2d.com.br/seligaai/stores/str_274011.jpeg'),
(31, 4, 231, 1, '', NULL, NULL, NULL, NULL, 'Se liga aí loja', 'https://em2d.com.br/seligaai/stores/str_708023.jpeg'),
(32, 15, 232, 1, '', NULL, NULL, NULL, NULL, 'Loja Valentina', 'https://em2d.com.br/seligaai/stores/str_102406.jpeg'),
(33, 3, 234, 1, '', NULL, NULL, NULL, NULL, 'D\'ella & D\'elle', 'https://em2d.com.br/seligaai/stores/str_486548.jpeg'),
(34, 16, 240, 1, '', NULL, NULL, NULL, NULL, 'Kids Store by Areli', 'https://em2d.com.br/seligaai/stores/str_008052.jpeg'),
(35, 3, 249, 0, '', NULL, NULL, NULL, NULL, 'Power Moda Fitness', 'https://em2d.com.br/seligaai/stores/str_082568.jpeg'),
(36, 3, 250, 1, '', NULL, NULL, NULL, NULL, 'Charme de Mulher', 'https://em2d.com.br/seligaai/stores/str_012610.jpeg'),
(37, 14, 252, 0, '', NULL, NULL, NULL, NULL, 'Rei dos Suplementos ', 'https://em2d.com.br/seligaai/stores/str_256229.jpeg'),
(38, 8, 253, 1, '', NULL, NULL, NULL, NULL, 'Wilde Lima fotografia', 'https://em2d.com.br/seligaai/stores/str_448070.jpeg'),
(39, 19, 272, 1, '11.596.468/0001-07', NULL, NULL, NULL, NULL, 'Geladão Rio Corrente', 'https://em2d.com.br/seligaai/stores/str_716427.jpeg'),
(40, 8, 295, 1, '844.141.704-00', NULL, NULL, NULL, NULL, 'Techfrio Refrigeração ', 'https://em2d.com.br/seligaai/stores/str_486792.jpeg'),
(41, 22, 296, 1, '31.318.426/0001-11', NULL, NULL, NULL, NULL, 'Marrylla Esmalteria ', 'https://em2d.com.br/seligaai/stores/str_721903.jpeg'),
(42, 22, 298, 1, '30.588.485/0001-47', NULL, NULL, NULL, NULL, 'Maria Fernanda Design', 'https://em2d.com.br/seligaai/stores/str_638617.jpeg'),
(43, 3, 299, 1, '69.961.662/0001-24', NULL, NULL, NULL, NULL, 'Tatty Modas', 'https://em2d.com.br/seligaai/stores/str_661637.jpeg'),
(44, 22, 314, 1, '28.526.109/0001-86', NULL, NULL, NULL, NULL, 'Josefina Esmalteria', 'https://em2d.com.br/seligaai/stores/str_718916.jpeg'),
(45, 8, 316, 0, '102.978.314-45', NULL, NULL, NULL, NULL, 'Grau Técnico Petrolina', 'https://em2d.com.br/seligaai/stores/grautecnico.png'),
(46, 1, 318, 1, '288.711.918-46', NULL, NULL, NULL, NULL, 'Adriana Teste', 'https://em2d.com.br/seligaai/stores/str_121746.jpeg'),
(48, 16, 326, 1, '052.168.144-85', NULL, NULL, NULL, NULL, 'Moda start', 'https://em2d.com.br/seligaai/stores/str_199290.jpeg'),
(51, 11, 327, 1, '068.319.084-97', NULL, NULL, NULL, NULL, 'INFOWORK', 'https://em2d.com.br/seligaai/stores/str_445449.jpeg'),
(52, 16, 328, 1, '28.623.868/0001-67', NULL, NULL, NULL, NULL, 'Mundo Infantil', 'https://em2d.com.br/seligaai/stores/str_984323.jpeg'),
(54, 15, 330, 1, '288.711.918-46', NULL, NULL, NULL, NULL, 'Empresa Daniel Triboni', 'https://em2d.com.br/seligaai/stores/str_038847.jpeg'),
(55, 4, 338, 1, '22.054.768/0001-80', NULL, NULL, NULL, NULL, 'Sushi Dois Irmãos', 'https://em2d.com.br/seligaai/stores/str_432364.jpeg'),
(56, 22, 339, 1, '13.561.235/0001-02', NULL, NULL, NULL, NULL, 'Bela Unhas ', 'https://em2d.com.br/seligaai/stores/str_326834.jpeg'),
(57, 16, 340, 1, '30.304.178/0001-97', NULL, NULL, NULL, NULL, 'BeKa BaKana', 'https://em2d.com.br/seligaai/stores/str_493916.jpeg'),
(58, 20, 341, 1, '30.360.501/0001-40', NULL, NULL, NULL, NULL, 'DS Casa do Bolo ', 'https://em2d.com.br/seligaai/stores/str_582817.jpeg'),
(59, 21, 342, 1, '029.129.325-56', NULL, NULL, NULL, NULL, 'Naturais Duvalle', 'https://em2d.com.br/seligaai/stores/str_020655.jpeg'),
(60, 4, 349, 1, '108.651.864-05', NULL, NULL, NULL, NULL, 'Buenos Burgue\'s', 'https://em2d.com.br/seligaai/stores/str_017935.jpeg'),
(61, 22, 350, 1, '025.540.074-89', NULL, NULL, NULL, NULL, 'Rose Estetica', 'https://em2d.com.br/seligaai/stores/str_968990.jpeg'),
(62, 3, 354, 1, '04.444.249/0001-47', NULL, NULL, NULL, NULL, 'Vitrage', 'https://em2d.com.br/seligaai/stores/str_987224.jpeg'),
(63, 3, 357, 1, '21.976.341/0001-77', NULL, NULL, NULL, NULL, 'Mabelly', 'https://em2d.com.br/seligaai/stores/str_238947.jpeg'),
(64, 11, 539, 1, '11.349.634/0001-70', NULL, NULL, NULL, NULL, 'Gigatec', 'https://em2d.com.br/seligaai/stores/str_527054.jpeg'),
(65, 3, 365, 1, '883.889.564-34', NULL, NULL, NULL, NULL, '12 Tons', 'https://em2d.com.br/seligaai/stores/str_338732.jpeg'),
(66, 3, 383, 1, '042.063.774-56', NULL, NULL, NULL, NULL, 'Ellen Modas', 'https://em2d.com.br/seligaai/stores/str_354640.jpeg'),
(67, 3, 384, 1, '074.834.894-80', NULL, NULL, NULL, NULL, 'Closet Mania', 'https://em2d.com.br/seligaai/stores/str_963435.jpeg'),
(68, 22, 385, 1, '052.059.024-42', NULL, NULL, NULL, NULL, 'ROSA BELLA DEPILACAO', 'https://em2d.com.br/seligaai/stores/str_221522.jpeg'),
(69, 3, 387, 1, '053.716.104-05', NULL, NULL, NULL, NULL, 'Iana Acessórios ', 'https://em2d.com.br/seligaai/stores/str_089993.jpeg'),
(70, 3, 389, 1, '088.740.834-67', NULL, NULL, NULL, NULL, 'Fernanda Modas', 'https://em2d.com.br/seligaai/stores/str_080241.jpeg'),
(71, 3, 390, 1, '009.836.494-48', NULL, NULL, NULL, NULL, 'VIRE VEST', 'https://em2d.com.br/seligaai/stores/str_647943.jpeg'),
(72, 20, 391, 1, '052.372.539-62', NULL, NULL, NULL, NULL, 'Pastelaria do Sul', 'https://em2d.com.br/seligaai/stores/str_541249.jpeg'),
(73, 20, 392, 1, '708.412.503-59', NULL, NULL, NULL, NULL, 'Cuscuzeria e Tapiocaria ', 'https://em2d.com.br/seligaai/stores/str_143552.jpeg'),
(74, 8, 393, 1, '051.588.444-88', NULL, NULL, NULL, NULL, 'Santana Cell', 'https://em2d.com.br/seligaai/stores/str_841104.jpeg'),
(75, 8, 394, 1, '018.191.775-05', NULL, NULL, NULL, NULL, 'UBS ADESIVOS', 'https://em2d.com.br/seligaai/stores/str_509443.jpeg'),
(76, 4, 395, 1, '039.601.245-03', NULL, NULL, NULL, NULL, 'Savanna\'s ', 'https://em2d.com.br/seligaai/stores/str_449302.jpeg'),
(77, 3, 396, 1, '058.057.725-23', NULL, NULL, NULL, NULL, 'POINT DA MODA', 'https://em2d.com.br/seligaai/stores/str_790876.jpeg'),
(78, 8, 397, 1, '077.350.723-09', NULL, NULL, NULL, NULL, 'Info micro house', 'https://em2d.com.br/seligaai/stores/str_463693.jpeg'),
(79, 3, 398, 1, '24.423.306/0001-19', NULL, NULL, NULL, NULL, 'Melissa Modas', 'https://em2d.com.br/seligaai/stores/str_730491.jpeg'),
(80, 21, 424, 1, '037.879.094-35', NULL, NULL, NULL, NULL, 'Portal USB', 'https://em2d.com.br/seligaai/stores/str_709952.jpeg'),
(81, 8, 430, 1, '31.318.426/0001-11', NULL, NULL, NULL, NULL, 'Recepções centrais', 'https://em2d.com.br/seligaai/stores/str_102242.jpeg'),
(82, 21, 447, 1, '115.310.264-19', NULL, NULL, NULL, NULL, 'Plantas do Vale', 'https://em2d.com.br/seligaai/stores/str_305148.jpeg'),
(83, 21, 482, 1, '19.747.277/0001-00', NULL, NULL, NULL, NULL, 'Personalize Petrolina', 'https://em2d.com.br/seligaai/stores/str_027505.jpeg'),
(84, 11, 490, 1, '703.597.361-30', NULL, NULL, NULL, NULL, 'Ph Informática', 'https://em2d.com.br/seligaai/stores/str_581837.jpeg'),
(85, 22, 516, 1, '029.432.744-45', NULL, NULL, NULL, NULL, 'Estética sintonia do cor', 'https://em2d.com.br/seligaai/stores/str_185579.jpeg'),
(86, 21, 517, 1, '183.254.754-04', NULL, NULL, NULL, NULL, 'Escola novo tempo', 'https://em2d.com.br/seligaai/stores/str_758852.jpeg'),
(87, 3, 551, 1, '09.456.667/0001-78', NULL, NULL, NULL, NULL, 'Kpricho modas ', 'https://em2d.com.br/seligaai/stores/str_524262.jpeg'),
(88, 6, 427, 1, '288.711.918-46', '4', '4324-2', '423423423', 'DANIEL A S TRIBONI', 'DAN CARROS', 'https://em2d.com.br/seligaai/stores/str_630012.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `terms`
--

DROP TABLE IF EXISTS `terms`;
CREATE TABLE IF NOT EXISTS `terms` (
  `terid` int(3) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL,
  PRIMARY KEY (`terid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `terms`
--

INSERT INTO `terms` (`terid`, `active`, `content`) VALUES
(1, 1, '<p>O App Se Liga Aí é um serviço online que permite ao usuário obter descontos e promoções comerciais ativas em uma determinada área geográfica, subdivididas por ramo de marketing de mercado.<br>\r\n          Os usuários que utilizam os serviços oferecidos pelo App Se Liga Aí, declaram conhecer e aceitar às presentes condições gerais deste termo.</p>\r\n\r\n        <h4>Titular do Se Liga Aí</h4>\r\n\r\n        <p>\r\n            EM2D Empreendimentos Publicitários Ltda.<br>\r\n            Rua Padeiro João Luiz, 137, SALA A - <br>\r\n            CEP 56330-470, Petrolina (PE)<br>\r\n            CNPJ: 31.318.426/0001-11<br>\r\n            Sociedade registrada junto à JUCEPE PE<br>\r\n            <a href=\"mailto:seligaai@em2d.com.br\">seligaai@em2d.com.br</a>\r\n        </p>\r\n\r\n        <h4>Conteúdos fornecidos pelo usuário (empresa)</h4>\r\n        <p>Os usuários (empresa) são responsáveis pelo próprios conteúdos e pelo conteúdo de terceiros que compartilham nesse aplicativo mediante ao seu carregamento, a inserção de conteúdos ou qualquer outra modalidade. Os usuários isentam o Titular de qualquer responsabilidade em relação à difusão ilícita de conteúdos de terceiros ou da utilização dessa ou qualquer outra modalidade contrárias à lei.</p>\r\n        <p>O Titular não realiza nenhum tipo de moderação dos conteúdos publicados pelo usuário ou por terceiros, mas se propõe a interferir diante às sinalizações dos usuários ou de ordem judicial em relação aos conteúdos constatados como ofensivos ou ilícitos.</p>\r\n\r\n        <h4>Direitos sobre os conteúdos fornecidos pelos usuários</h4>\r\n        <p>Os únicos direitos concedidos ao Titular em relação aos conteúdos fornecidos pelos usuários são aqueles necessários para o funcionamento e para a manutenção desse aplicativo.</p>\r\n\r\n        <h4>Conteúdos fornecidos por terceiros</h4>\r\n        <p>Os usuários podem utilizar os serviços ou conteúdos incluídos nesse aplicativo e fornecidos por terceiros, mas devem antes de mais nada, levar em consideração os termos e as condições das tais partes terceiras e tê-las aceitado. Em nenhuma circunstância o Titular poderá ser responsabilizado em relação ao correto funcionamento ou à disponibilidade ou ambos, dos serviços oferecidos por terceiros.</p>\r\n\r\n        <h4>O serviço é oferecido “assim com é”</h4>\r\n        <p>O serviço é fornecido pelo Titular “assim com é”, sem alguma garantia expressa ou implícita para a precisão ou disponibilidade.</p>\r\n\r\n        <h4>Revenda do Serviço</h4>\r\n        <p>Os usuários não são autorizados a reproduzir, duplicar, copiar, vender, revender ou explorar qualquer porção desse aplicativo e dos seus serviços sem a permissão expressa do Titular, garantida diretamente ou através um específico programa de revenda.</p>\r\n\r\n        <h4>Da Isenção de Responsabilidade</h4>\r\n        <p>O usuário se empenha em manter ileso o Titular (e as eventuais sociedades por ele controladas ou filiadas, os seus representantes, administradores, agentes, licenciados, parceiros e funcionários) de qualquer obrigação ou responsabilidade, incluindo eventuais despesas legais incorridas para defender-se em tribunal, que poderiam ocorrer diante de danos provocados por outros usuários ou terceiros, em relação aos conteúdos carregados online, à violação dos termos de lei ou dos termos das presentes condições de serviço.</p>\r\n\r\n        <h4>Utilização consentida</h4>\r\n        <p>Os usuários não podem:<br>\r\n\r\n        <p>efetuar engenharia reversa, descompilar, desmontar, modificar ou criar obras derivadas a partir desse aplicativo;\r\n        burlar os sistemas de informação usados por esse aplicativo ou pelos seus licenciados para proteger o conteúdo acessível através da mesma;\r\n        copiar, conservar, modificar, mudar, preparar trabalhos derivados ou alterar, em qualquer modo, os conteúdos fornecidos por esse aplicativo;\r\n        utilizar qualquer espécie de robô, spider, aplicação de pesquisa, phising, recuperação do site ou qualquer outro dispositivo processo ou meio automático para ter acesso, recuperar, efetuar scraping ou dividir por índices qualquer porção desse aplicativo ou de seus conteúdos;\r\n        alugar, licenciar ou sublicenciar este aplicativo;\r\n        utilizar esse aplicativo em qualquer outra modalidade imprópria violando esses Termos.</p>\r\n        \r\n        <h4>Política de Privacidade</h4>\r\n        <p>Para informações sobre a utilização dos dados pessoais, os usuários devem fazer referimento à política de privacidade desse aplicativo.</p>\r\n        \r\n        <h4>Copyright</h4>\r\n        <p>Todos as marcas do aplicativo, figurativas ou nominativas, e todos os outros sinais, nomes comerciais, marcas de serviço marcas denominativas, denominações comerciais, ilustrações, imagens, logos que aparecem nesse aplicativo são e permanecem de propriedade exclusiva do Titular e dos seus licenciados e são protegidos pelas leis vigentes sobre as marcas e pelos relativos tratados internacionais.</p>\r\n        \r\n        <h4>Requisitos de idade</h4>\r\n        <p>Os usuários declaram de ser maiores de idade segundo a legislação aplicada no relativo país. Os menores de dezesseis anos, desassistidos pelos pais ou responsáveis, podem utilizar esse aplicativo. Em nenhum caso menores de 12 anos podem utilizar esse aplicativo.</p>\r\n\r\n        <h4>Limitações de responsabilidade</h4>\r\n        <p>O Titular, nos limites da normativa em vigor, responde pelos eventuais danos de natureza contratual e extra-contratual aos usuários ou terceiros exclusivamente quando tais danos correspondem à uma consequência imediata e direta por dolo ou culpa grave.</p>\r\n        <p>O usuário isenta e suspende expressamente o Titular do aplicativo de cada responsabilidade, nos limites consentidos pela legislação aplicável, em relação às eventuais danos ou reclamações de qualquer tipo e gênero próprias e/ou de terceiros incluindo os danos diretos e indiretos, punitivos, incidentais, especiais, os danos derivados de lucros cessantes, perda de receita, perda de dados ou custos de substituição decorrentes ou de alguma forma relacionados ao presente contrato.</p>\r\n\r\n        <h4>Modificações dos presentes Termos</h4>\r\n        <p>O Titular reserva-se o direito de fazer alterações nesses Termos em qualquer momento, informando ao usuário com a publicação dessas modificações na interface dessa aplicação.</p>\r\n        <p>O usuário que continuar a utilizar este aplicativo depois da publicação das modificações aceita, sem reservas, os novos Termos.</p>\r\n\r\n        <h4>Cessão do contrato</h4>\r\n        <p>O Titular reserva-se o direito de transferir, ceder, dispor por novação ou subcontratação todos ou alguns dos direitos e obrigações que derivam dos presentes Termos, desde que os direitos dos usuários, aqui previstos, não sejam prejudicados.</p>\r\n        <p>O usuário não pode ceder ou transferir, em nenhum modo, os próprios direitos e obrigações nos sensos dos presentes Termos sem a autorização, por escrito, do Titular.</p>\r\n\r\n        <h4>Comunicações</h4>\r\n        <p>Todas as comunicações relacionadas a este aplicativo devem ser enviadas utilizando as informações de contato indicadas.</p>\r\n\r\n        <h4>Leis aplicáveis e Fórum competente</h4>\r\n        <p>Os presentes Termos e todas as controvérsias no mérito da execução, interpretação e validade do presente contrato estão sujeitos às leis brasileiras.</p>\r\n        <p>As partes comprometem-se a entregar qualquer controvérsia, nos limites da legislação aplicada, à jurisdição dos tribunais brasileiros, com foro exclusivo localizado na cidade de Petrolina/PE.</p>\r\n        ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `useraddresses`
--

DROP TABLE IF EXISTS `useraddresses`;
CREATE TABLE IF NOT EXISTS `useraddresses` (
  `uadid` int(11) NOT NULL AUTO_INCREMENT,
  `usrid` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `address` varchar(60) NOT NULL,
  `number` varchar(10) NOT NULL,
  `complement` varchar(30) DEFAULT NULL,
  `neighboor` varchar(30) NOT NULL,
  `zipcode` varchar(9) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `latitude` varchar(16) NOT NULL,
  `longitude` varchar(16) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '1',
  `tel1` varchar(16) NOT NULL,
  PRIMARY KEY (`uadid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `useraddresses`
--

INSERT INTO `useraddresses` (`uadid`, `usrid`, `active`, `address`, `number`, `complement`, `neighboor`, `zipcode`, `city`, `state`, `latitude`, `longitude`, `type`, `tel1`) VALUES
(1, 134, 1, 'Rua da Piedade', '137', NULL, 'Gercino Coelho', '56333-000', 'Petrolina', 'PE', '-23.456', '-46.789', 1, '(11) 93456-0987');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usercoupons`
--

DROP TABLE IF EXISTS `usercoupons`;
CREATE TABLE IF NOT EXISTS `usercoupons` (
  `ucoid` int(11) NOT NULL AUTO_INCREMENT,
  `usrid` int(11) NOT NULL,
  `couid` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ucoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `usrid` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `type` int(1) NOT NULL DEFAULT '1',
  `signup` varchar(8) NOT NULL DEFAULT 'email',
  `gender` varchar(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastlotteryid` int(11) DEFAULT '0',
  `lastlotterynum` varchar(20) DEFAULT NULL,
  `canuseadmin` int(1) NOT NULL DEFAULT '0',
  `canseller` int(1) NOT NULL DEFAULT '0',
  `borndate` varchar(50) DEFAULT NULL,
  `rg` varchar(12) DEFAULT NULL,
  `cpfcnpj` varchar(18) DEFAULT NULL,
  `cellphone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`usrid`)
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`usrid`, `active`, `type`, `signup`, `gender`, `name`, `email`, `password`, `lastlotteryid`, `lastlotterynum`, `canuseadmin`, `canseller`, `borndate`, `rg`, `cpfcnpj`, `cellphone`) VALUES
(132, 1, 1, 'email', 'male', 'Marcos Antunes Galdino Rodrigues Júnior ', 'mgaldino10@gmail.com', '6d071901727aec1ba6d8e2497ef5b709', 1, 'ABC123', 0, 0, NULL, NULL, NULL, NULL),
(133, 1, 1, 'email', 'female', 'Vanderli Dias', 'derlydias_09@yahoo.com.br', '7833d0eac17822ea083155ebc39ceb11', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(134, 1, 2, 'email', 'male', 'Marcos Antunes', 'marcos_junior_@hotmail.com', '6add84506c86a658bc85038f91e35ce7', 1, '555443710026', 0, 0, NULL, NULL, NULL, NULL),
(135, 1, 1, 'email', 'female', 'Maíra Antunes dos Santos Rodrigues', 'may.270182@gmail.com', '6f7a7fdae5ed8e3242fe397a08e40467', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(136, 1, 3, 'email', 'male', 'Daniel Triboni', 'danieltriboni@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 1, '826234109076', 0, 0, NULL, NULL, NULL, NULL),
(137, 1, 4, 'email', 'male', 'Daniel Triboni ', 'danieltriboni38@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, '393589343388', 1, 0, '1981-01-14T00:00:00-03:00', NULL, NULL, NULL),
(138, 1, 1, 'facebook', 'male', 'Erick Srp', 'erick_patriota@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(139, 1, 2, 'email', 'male', 'Se Liga Aí', 'ericksoarespatriota@hotmail.com', '42fcd167ce66dd729a3a4e63bfab9f7f', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(140, 1, 1, 'email', 'female', 'Márcia Antunes dos Santos Rodrigues ', 'marciasr83@gmail.com', '9b984617c4ef1b57f70242a22997a648', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(141, 1, 1, 'facebook', 'male', 'Vitor Luiz', 'vitorswol@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(142, 1, 1, 'facebook', 'male', 'Marcio Ribeiro Barber', 'marcioribeiroimoveis@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(143, 1, 1, 'facebook', 'male', 'Iuri Eça Hohlenwerger', 'iuri_enf@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(144, 1, 1, 'email', 'male', 'FranShow', 'franklinwalla@hotmail.com', '65a8c66db8b1b9c7554d26ba58eda863', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(145, 1, 1, 'email', 'male', 'Leonardo Alves Dias', 'diasleo422@gmail.com', '8b967a817061f173129b3dbcfc5f0b32', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(146, 1, 1, 'facebook', 'male', 'Tatiane Martiins', 'tathy_martiins@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(147, 1, 1, 'email', 'female', 'Keylla Freire Santos', 'keyllafreire@hotmail.com', 'f84f292731a6ac47cafb0f899cf2ae94', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(148, 1, 1, 'facebook', 'male', 'Hércules Barreto', 'leo_irece@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(149, 1, 1, 'email', 'female', 'Ilania Fonseca Cavalcanti', 'ilaniafonseca@hotmail.com', '1c93e7add550041cc3cd3ca43aeb4135', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(150, 1, 2, 'email', 'male', 'Barbearia Brutus', 'ribeirocabeloebarba@gmail.com', '29a00fb8f3aa38afc7eec1e0831e917f', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(151, 1, 1, 'email', 'female', 'ilania fonseca cavalcanti', 'ilanaifonseca@hotmail.com', '1c93e7add550041cc3cd3ca43aeb4135', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(152, 1, 1, 'email', 'male', 'Diego Djale de Andrade Pereira ', 'diegodjale@gmail.com', '2e9ec317e197819358fbc43afca7d837', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(153, 1, 1, 'facebook', 'male', 'Leidiane Eudamidas', 'lidymoreninha1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(154, 1, 1, 'email', 'female', 'Lana siqueira', 'lannynha3983@gmail.com', '4d3ae64fc8fdb978952a65804e14877c', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(155, 1, 1, 'email', 'male', 'Marcos Antunes Galdino Rodrigues', 'marcosgaldinorodrigues@gmail.com', '7cd807489353d905b6fa16ae5934c1e3', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(156, 1, 1, 'facebook', 'male', 'Lidiane Santos', 'autopecas249@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(157, 1, 1, 'email', 'female', 'Maria Ines Soares Ribeiro', 'ines.creche@hotmail.com', '65f099c858bdd2e0dad22d36827f2228', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(158, 1, 1, 'email', 'male', 'Marcos Antunes Galdino Rodrigues ', 'marcosgaldiiinoo2018@gmail.com', '4b91ec4b3aa3243d546efca08d9d8ce6', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(159, 1, 1, 'email', 'male', 'Jorge Lopo', 'jorgelopo19@gmail.com', 'bb8307b7b81e7ca099e1a68a6f31d6e4', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(160, 1, 1, 'facebook', 'male', 'Gustavo Braga', 'gustavo_braga1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(161, 1, 1, 'facebook', 'male', 'Esteffane Vianna', 'cla_ra_9@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(162, 1, 1, 'email', 'male', 'Ismael de Oliveira Prazeres', 'ismaeldop@gmail.com', '8e494a1ffdd4ebc94a77c57f8b4a9abd', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(163, 1, 1, 'email', 'male', 'Fábio Gomes', 'ribeiro.fgr@gmail.com', '8423ba0930eb6b37432417f353d0eb19', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(164, 1, 1, 'facebook', 'male', 'Marcio De Assis Silva', 'marcio_blackcat@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(165, 1, 1, 'facebook', 'male', 'Ewerton Barros', 'ewerton_moto21@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(166, 1, 1, 'facebook', 'male', 'Vagner Santana', 'wagner_santtana@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(167, 1, 1, 'facebook', 'male', 'Vanessa Monteiro', 'vanessasilva231728@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(168, 1, 1, 'facebook', 'male', 'Aristófanes Diêgo Araújo Cabral', 'diego_araujo_cabral@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(169, 1, 1, 'facebook', 'male', 'Sieton Fernandes', 'sieton.fernandes@uol.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(170, 1, 2, 'email', 'male', 'Sieton Soares Fernandes ', 'climaxrefrigeracao18@Outlook.com', '64f22a0bb92dee77297e85826a1e33d4', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(171, 1, 1, 'facebook', 'male', 'Polly Torres', 'pollyromariz@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(172, 1, 1, 'facebook', 'male', 'Waldilene Dias', 'dilenydias@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(173, 1, 1, 'email', 'female', 'Sofia Sorelly', 'sofiasorelly90@gmail.com', '952a7523496a9f01dc9b8aa7d7967b36', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(174, 1, 1, 'email', 'male', 'Wagner krauss de oliveira dias', 'wagnerdankmotos@gmail.com', '0168844e320718396a6524a5fc898f12', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(175, 1, 1, 'facebook', 'male', 'Thiago Soares', 'soarescerqueiraengenharia@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(176, 1, 1, 'facebook', 'male', 'Silvestre Rangel', 'silvestrerangel@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(177, 1, 1, 'facebook', 'male', 'Saulo Rosa', 'saulinhojua@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(178, 1, 1, 'email', 'female', 'Jayana Carvalho de sá Oliveira ', 'jayanacarvalhodesa@hotmail.com', 'd080e269f31c7d98fd6ab919d0c190cf', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(179, 1, 2, 'email', 'male', 'Diego Djale de Andrade Pereira ', 'diegodjale@yahoo.com.br', '2e9ec317e197819358fbc43afca7d837', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(180, 1, 1, 'facebook', 'male', 'Luam Henrique', 'luamhenriquegomes@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(181, 1, 1, 'email', 'female', 'Marivania ferreira feitosa', 'marivaniaf@yahoo.com.br', 'a53d8cc0363c1a57b1b814ab1e00ae92', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(182, 1, 1, 'facebook', 'male', 'Franklin Walla', 'franklinsouzaww@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(183, 1, 2, 'email', 'male', 'Ricardo Bonfim de Queiroz', 'descunfumbelado@gmail.com', 'af2f73a588dcc5696e37514328f2ae76', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(184, 1, 1, 'facebook', 'male', 'Lucas ßelem', 'lucas_belem@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(185, 1, 1, 'facebook', 'male', 'Mariana Lustosa', 'marianna_lustosa@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(186, 1, 1, 'email', 'female', 'Emecarla rodrigies da cruz', 'emecarlarodeigues@bol.com.br', '8d858847038aa02a9b9bf290b5e3b882', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(187, 1, 1, 'email', 'male', 'Sérgio Ribeiro dos Santos', 'sc_jua@hotmail.com', 'fa90e1cf6d06b39bb5096c29a43169d0', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(188, 1, 1, 'facebook', 'male', 'Cynthia Amorim', 'cy_amorimp@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(189, 1, 1, 'email', 'female', 'Andressa Fidelis Dantas Barbosa ', 'fidelisgg@hotmail.com', '16e4dfd7466a65438434c8fa97547a02', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(190, 1, 1, 'email', 'female', 'Thaiselane Leobas de Alencar Alves', 'tatileobas@hotmaim.com', 'da1e09df975072d12e438e60e73706f5', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(191, 1, 1, 'facebook', 'male', 'Carla Bianca S de Mendonça', 'carlabiancasm@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(192, 1, 1, 'email', 'male', 'Ronaldo dos Santos', 'ronabruno50@gmail.com', '5588d0ce0e3ac944ba2e31dce0c6148c', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(193, 1, 1, 'email', 'male', 'Ronaldo Campolino', 'ronaldocampolino@gmail.com', '9bf5e321e9cd9bfa2060cfd9a25eb921', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(194, 1, 1, 'facebook', 'male', 'Fatima Teixeira', 'fatimateixeira_pe@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(195, 1, 1, 'facebook', 'male', 'Fabinho Binho', 'binhofrso@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(196, 1, 1, 'facebook', 'male', 'William Martins', 'willactus@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(197, 1, 1, 'facebook', 'male', 'Rely Alencar Luna', 'alencarlunacosta@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(198, 1, 1, 'email', 'female', 'Ester Rodrigues ', 'tetefa0102@gmail.com', '4013e820539a5be44d97af2ccdad5bf7', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(199, 1, 1, 'facebook', 'male', 'Nonato Ribeiro', 'nonato.ribeiro.flp@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(200, 1, 2, 'email', 'male', 'Anderson', 'climacenterpetrolina@outlook.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(201, 1, 1, 'email', 'female', 'Ana Lucia Damasceno costa ', 'analucia_damasceno@hotmail.com', 'ed3630b54d1eb6ccaf3b133bd212b0a0', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(202, 1, 2, 'email', 'female', 'Ana Lucia Damasceno costa ', 'convenienciasaboremdobro@outlook.com', 'bd89d169ad8478c285a2a048c34d58bc', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(203, 1, 1, 'email', 'male', 'Marcio Tarciso Reis Silva', 'marciotarciso105@gmail.com', '25d55ad283aa400af464c76d713c07ad', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(204, 1, 1, 'facebook', 'male', 'Carol Moraes', 'fernandinha.moraes14@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(205, 1, 1, 'facebook', 'male', 'Júnior Silva', 'alexsandrojunior2018@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(206, 1, 1, 'facebook', 'male', 'Dedeinha Ribeirinha', 'dreiarss@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(207, 1, 1, 'email', 'male', 'Eid Maynarde Caffé Lacerda', 'maynardecaffe@gmail.com', '11ac1e05ff174f32aa393488dbf7ca28', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(208, 1, 1, 'email', 'female', 'belciana coelho de alencar', 'belcianarever@outlook.com', '3d8fe5c70c03775c344c1787d43fc963', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(209, 1, 1, 'facebook', 'male', 'Graciene Cavalcanti', 'graciene5308@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(210, 1, 1, 'facebook', 'male', 'Alex Costa', 'econolex@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(211, 1, 1, 'email', 'male', 'Rodrigo leandro de oliveira', 'rodrigoleandro3087@gmail.com', 'd647fae6c80a1b083adb798cade7ee52', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(212, 1, 1, 'facebook', 'male', 'Rafaela Mendes Gomes', 'rafamfgomees@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(213, 1, 1, 'email', 'male', 'Francislucio Rodrigues dos Santos', 'francislucio@gmail.com', 'a06e893997b0045d3a62234602a0cd2e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(214, 1, 1, 'facebook', 'male', 'Josineide Viana', 'jozy.viana@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(215, 1, 1, 'email', 'female', 'Emily Caroliny Lima Crux', 'emilycarolinylima@outlook.com', '134b26cbe8cbf3cff5c0e7c969b57713', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(216, 1, 1, 'email', 'female', 'Helena Francielly Guedes Ferreira', 'helenaa_francielly_@hotmail.com', '2c13817fca846f6f9a7d934d71a668ee', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(217, 1, 1, 'email', 'male', 'Emerson Celestino', 'emersoncelestino.ba@gmail.com', '25d55ad283aa400af464c76d713c07ad', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(218, 1, 1, 'facebook', 'male', 'Andreia Santos', 'andreiab.santos@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(219, 1, 1, 'email', 'male', 'Manoel Junior', 'manoel.mpjunior@yahoo.com.br', 'd02cda96145f079e41de397da6662ceb', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(220, 1, 1, 'email', 'female', 'Edivânia ', 'edivaniadias1999@outlook.com', '75b5788d31eabf10d9b8bb55ee01c63e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(221, 1, 1, 'facebook', 'male', 'Jéssica Larissa', 'je.larissa@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(222, 1, 1, 'facebook', 'male', 'Taynã Amorim', 'taynaamorim2315@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(224, 1, 1, 'facebook', 'male', 'Carlos Henrique', 'carloshenrique_sm@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(225, 1, 1, 'facebook', 'male', 'Arnaldo Rodrigues', 'arnaldo.hahn@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(226, 1, 1, 'email', 'male', 'Gabriel batista', 'lelouchsdky@gmail.com', '9c3d900347713b9999e2623c99e5e037', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(227, 1, 1, 'email', 'male', 'Daniel Matias da Cruz Itabaiana', 'danielitabaiana@hotmail.com', 'aa47f8215c6f30a0dcdb2a36a9f4168e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(228, 1, 1, 'email', 'male', 'Flávio Alves Santana', 'flaviosantana2016fas@gmail.com', '5e8f4e06a72887b71ce41638fca221cf', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(229, 1, 1, 'facebook', 'male', 'Fernanda Emanuelle', 'nanda_ess@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(231, 1, 2, 'email', 'male', 'Teste Se liga aí', 'danieltriboni37@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(232, 1, 2, 'email', 'male', 'Marci Sá ', 'marci_nena@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(234, 1, 2, 'email', 'male', 'Wilker Freire Ferraz de Lima', 'wilker_ferraz@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(235, 1, 1, 'email', 'male', 'Gabriel Hermes', 'gabrielhrms@hotmail.com', 'b33a4006134bb6ee62b30fc0fac5cec6', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(236, 1, 1, 'facebook', 'male', 'Israel Gonçalves', 'israelobrother@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(237, 1, 1, 'facebook', 'male', 'Claudia Costa', 'claudiaveridiana@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(238, 1, 1, 'email', 'female', 'Jucileide barbosa da Silva', 'jubarbosa945@gmail.com', '32fc1e563561a426f152f834cd7575c9', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(239, 1, 1, 'facebook', 'male', 'Douglas Correia', 'douglasgabrielcorreia@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(240, 1, 2, 'email', 'female', 'Areli Moreira Bezerra Cavalcante de Santana ', 'arelisantanapsi@hotmail.com', '00462f379beed5ef79920fa2c092ef70', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(241, 1, 1, 'facebook', 'male', 'Cleidjane Rodrigues', 'cleidsilva19@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(242, 1, 1, 'facebook', 'male', 'Miguel Guimaraes', 'mjmguimaraes@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(243, 1, 1, 'facebook', 'male', 'Juliana Almeida', 'leiyla.juliana@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(244, 1, 1, 'facebook', 'male', 'Wilde L. Souza', 'wildescroto@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(245, 1, 1, 'email', 'female', 'Larissa Nogueira Guivares', 'larissanogueiraguivares@gmail.com', '9ef42f8869e945a53b2296170108b98e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(246, 1, 1, 'facebook', 'male', 'Wilton Lima', 'wilton_250@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(247, 1, 1, 'facebook', 'male', 'Dominique Ramos', 'dominiqueleandro1@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(248, 1, 1, 'facebook', 'male', 'Micaely Chagas', 'mikaelaspears@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(249, 1, 2, 'email', 'female', 'Jeslye Samyde', 'jeslyesamyde@hotmail.com', '544d9394eeb06c1357504d6946ac8f15', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(250, 1, 2, 'email', 'female', 'Isamilly Rejane Pontes Batita', 'isamilly92@hotmail.com', 'ca24d0256aa957d1a4c03c3c8d65038f', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(251, 1, 1, 'facebook', 'male', 'Jaelson Galindo', 'jaelsongalindo@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(252, 1, 2, 'email', 'male', 'jose Lourenço da silva júnior ', 'junior_beat2@hotmail.com', 'adcb66cf7b21a561e4f877a9ffb4cb38', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(253, 1, 2, 'email', 'male', 'Wildelfrancys Lima de Souza', 'wildelimafotografia@gmail.com', 'eb90d218878686d257ca3e07a7c9b097', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(254, 1, 1, 'facebook', 'male', 'Gabiane Antunes', 'gabyane16@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(255, 1, 1, 'facebook', 'male', 'Stéfane Alencar', 'stefane_peixoto@outlook.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(256, 1, 1, 'email', 'male', 'Juscelino Freitas Santana', 'juscelinofreitas2016@gmail.com', '68dc423c9bbef4e47945f1d0c4a0c098', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(257, 1, 1, 'email', 'male', 'Carlos thyago', 'carlosmudo2014@hotmail.com', '4318749ebd39b68405607b79b6da570c', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(258, 1, 1, 'facebook', 'male', 'Danyelle Moura', 'dany_carla12@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(259, 1, 1, 'facebook', 'male', 'Yuri Da Silva', 'da_silva568@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(260, 1, 1, 'email', 'female', 'Daniela Barros', 'daniela.barros94@outlook.com', 'c71c8821e219dfa33dd17f521660b398', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(261, 1, 1, 'facebook', 'male', 'Eliete Lima', 'elietelima307@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(262, 1, 1, 'email', 'male', 'Alan Chaves', 'alansichaves@gmail.com', '7847cd730c934745c7c65e8885686444', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(263, 1, 1, 'facebook', 'male', 'Raquel Ripardo', 'ripardosousa_cat@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(264, 1, 1, 'facebook', 'male', 'Wagner Rodrigues', 'wagnerboy20@live.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(265, 1, 1, 'facebook', 'male', 'Monique Teixeira', 'moniq_tr@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(266, 1, 1, 'facebook', 'male', 'Renata Alves', 'renata_santos45@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(267, 1, 1, 'facebook', 'male', 'Nara Coelho', 'narahkarol@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(268, 1, 1, 'facebook', 'male', 'Eduarda Menezes', 'dudinha_menezes11@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(269, 1, 1, 'email', 'male', 'Sandro Tavares ', 'sandro.buz@gmail.com', 'ebdfa7a14808faa1affd73e47bfbf536', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(270, 1, 1, 'facebook', 'male', 'Dinho Cell', 'fabiosg465@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(271, 1, 1, 'facebook', 'male', 'Jakeline Gomes', 'jakeline_gomes16@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(272, 1, 2, 'email', 'male', 'Marcone Franklin Dottore', 'mauri.dpaula@gmail.com', '3039223c4884f916a71a0b846b94a08d', 0, NULL, 0, 0, NULL, NULL, '11.596.468/0001-07', NULL),
(273, 1, 1, 'email', 'female', 'Emanuele de barros alves', 'emanuelealves515@gmail.com', '48bd7bd2dbe918b9c4eb97b0c9a03057', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(274, 1, 1, 'email', 'male', 'Joaquim Martins de Lavor', 'joaquimdelavor@gmail.com', '0ed69acf99704b5e960d3a505c217ac5', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(275, 1, 1, 'facebook', 'male', 'Pedro Emanuel', 'pedro_jua2008@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(276, 1, 1, 'facebook', 'male', 'Gabriela Marques', 'gaabrs@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(277, 1, 1, 'facebook', 'male', 'Edneuza Silva', 'edileuza_pucca@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(278, 1, 1, 'facebook', 'male', 'Roberta Mangabeira', 'robertamangabeira@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(279, 1, 1, 'facebook', 'male', 'Uilma Saraiva', 'uilmasaraiva@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(280, 1, 1, 'email', 'female', 'Thaciana Carla Silva Mangabeira', 'thacicarla@hotmail.com', '1ee5cf4b2093df1ff0691b0c2740beb7', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(281, 1, 1, 'email', 'female', 'Juliana Oliveira', 'jubagata00@gmail.com', '7560181bc399878837802c283a2d1dbd', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(282, 1, 1, 'email', 'male', 'Leonel ', 'leonelarrais@yahoo.com.br', '89f8c02378102ebf5f7e3c6d24f5d9b3', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(283, 1, 1, 'facebook', 'male', 'Benício Lira', 'beniciolira.beno@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(284, 1, 1, 'facebook', 'male', 'Anália Mayara', 'analiamayara@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(285, 1, 1, 'email', 'female', 'Kelly Rhaquel ds Silva de Oliveira', 'kellyrhaquel@gmail.com', '99c6a89b36000252801377ed5f88d2d3', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(286, 1, 1, 'facebook', 'male', 'Núbia Ferreira', 'nubinhaferreira_@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(287, 1, 1, 'email', 'female', 'Kelline Claudina da Silva Lima ', 'kelline.tur@hotmail.com', 'a8b9afc70468d7c0b857dce79953174c', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(288, 1, 1, 'facebook', 'male', 'Nannda Oliveira LS', 'nandaoliveira352@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(289, 1, 1, 'email', 'male', 'Murilo Souza', 'bmurilo80@gmail.com', 'ce611e2d5e120add0796d415df0f11c4', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(290, 1, 1, 'facebook', 'male', 'Marina Marques', 'marquesmarinammc@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(291, 1, 1, 'facebook', 'male', 'Sofia Palhares', 'sofia_bomfim@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(292, 1, 1, 'email', 'female', 'Jessica Luana de Souza Brito', 'luanajbrito@gmail.com', '5c51fc44a1b90795cf69b674a1b36242', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(293, 1, 1, 'facebook', 'male', 'Jonhdeible Oliveira Dos Anjos', 'jonhdeible@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(294, 1, 1, 'facebook', 'male', 'Bruno Rodrigues', 'bruno_de_souz@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(295, 1, 2, 'email', 'male', 'Sirino da Silva Santos', 'sirino_jc@hotmail.com', 'ecf83154eeac3d09280f3c5bb78c2d1a', 0, NULL, 0, 0, NULL, NULL, '844.141.704-00', NULL),
(296, 1, 2, 'email', 'female', 'Leticiana Torres da Silva ', 'marryllaesmalteria@hotmail.com', '7e3bd99bb8b4f470b358258032972977', 0, NULL, 0, 0, NULL, NULL, '31.318.426/0001-11', NULL),
(297, 1, 1, 'facebook', 'male', 'Bianca Katarina', 'bianca_katarina@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(298, 1, 2, 'email', 'female', 'Maria Fernanda da Silva e Araujo', 'fernandasilva1096@outlook.com', '3b743a5337195aea43e3d33c81dc97f5', 0, NULL, 0, 0, NULL, NULL, '30.588.485/0001-47', NULL),
(299, 1, 2, 'email', 'female', 'Maria Adeilde dos Santos Barbosa', 'tathianadsb@hotmail.com', '522c3cda1764ede157d4c9ad17dfd9a4', 0, NULL, 0, 0, NULL, NULL, '69.961.662/0001-24', NULL),
(300, 1, 1, 'email', 'female', 'Flor', 'tamires.sabrina.ts@gmail.com', '3f8454b7f2c12cebb1622b6b0dfd1021', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(301, 1, 1, 'facebook', 'male', 'Manoel Messias', 'messias.wagner1163@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(302, 1, 1, 'email', 'female', 'Jussiana de Lima Santos ', 'jussyanapezzini_@hotmail.com', '387ffe597bd39d272b1d7c1b8a8d7203', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(303, 1, 1, 'facebook', 'male', 'Firmino Oliveira', 'tuannymaryana@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(304, 1, 1, 'facebook', 'male', 'Daianee Silva', 'daianee-silvaa@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(305, 1, 1, 'email', 'female', 'GESSICA ALINE COSTA DOS SANTOA', 'ges.alinesantos@gmail.com', 'bdf96d433fbfeb27083e4c2aaff3ec2d', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(306, 1, 1, 'email', 'female', 'Juliana Almeida de Souza', 'juli4nalmeida@gmail.com', '21e8a518eb5d9d4f10d7a2b67d48d49a', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(307, 1, 1, 'email', 'female', 'Samara Cristine Rosendo  de sá', 'samarasa__@hotmail.com', '204a1c3a6eb164798d0bda904c59973a', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(308, 1, 1, 'facebook', 'male', 'Cristiano Santos Silva', 'fansaj_best@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(309, 1, 1, 'facebook', 'male', 'Iris Caliane', 'iicaliane@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(310, 1, 1, 'email', 'male', 'Jonathas Marcello Guimaraes de Souza', 'marcellofx@live.com', '5588d0ce0e3ac944ba2e31dce0c6148c', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(311, 1, 1, 'facebook', 'male', 'Joyce Santos', 'joyce22santos@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(312, 1, 1, 'email', 'male', 'Claudio Deodato De Andrade Filho ', 'deodatoandrade@outlook.com', 'fd3dff9edb7afc818dd2712a6326aa0d', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(313, 1, 1, 'facebook', 'male', 'Maria Lourdes De Souza Silva', 'mlssat@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(314, 1, 2, 'email', 'male', 'Carlejorge alencar', 'josefinaesmalteria@gmail.com', '112fa41e054dc798e26f2f60988b33c0', 0, NULL, 0, 0, NULL, NULL, '28.526.109/0001-86', NULL),
(315, 1, 1, 'facebook', 'male', 'Tatyane Monteiro', 'tatyanne_monteiro@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(316, 1, 2, 'email', 'female', 'Maria Tatiane monteiro da silva', 'tatymontt@gmail.com', 'cce5ade367ed2065495e922979c2251e', 0, NULL, 0, 0, NULL, NULL, '102.978.314-45', NULL),
(317, 1, 1, 'email', 'female', 'Anailde Soares dos Santos', 'anaildesantos@yahoo.com.br', '06ea70e0f3748d147a589777f31e672e', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(318, 1, 2, 'email', 'female', 'Adriana de Oliveira Silva', 'adriteste@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, '288.711.918-46', NULL),
(319, 1, 1, 'facebook', 'male', 'PrisciLa Araújo', 'priscila-day18@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(320, 1, 1, 'email', 'female', 'Natalia Rodrigues dos Santos', 'nataliars1988@hotmail.com', 'b1356ace97f2a31e7652d8c49a256427', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(321, 1, 1, 'email', 'male', 'Ivo Daniel dos Santos Nunes', 'idsnunes1@hotmail.com', 'b60236490bda1b3b9e44f77737fcd528', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(322, 1, 1, 'facebook', 'male', 'Danielly Nunes', 'danynunes.petro@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(323, 1, 1, 'email', 'female', 'Amanda Marcela Silva Brito', 'amanda-brito@live.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(324, 1, 1, 'email', 'male', 'Marcos antonio nobrega bezerra junior ', 'marcosnobrega340@gmail.com', 'a303f53c24a0b03f7adadca9aae4d0d2', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(326, 1, 2, 'email', 'male', 'Erick Soares ', 'naila9499@gmail.com', '050f041779d7e3617b9e2dc93aa4341d', 0, NULL, 0, 0, '1991-01-09', NULL, '052.168.144-85', NULL),
(327, 1, 2, 'email', 'male', 'Aldair José dos Santos Santiago', 'aldair19861@hotmail.com', 'd347466fb255c624f205cb93768f9863', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(328, 1, 2, 'email', 'female', 'Djanete Escolastica', 'djaneteveiculos@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, NULL, NULL),
(329, 1, 2, 'email', 'male', 'Juliana dos santos silva', 'juliana.s.lana@gmail.com', 'bcb543200c99f25630e83826c622e73a', 0, NULL, 0, 0, NULL, NULL, '004.650.693-47', NULL),
(330, 1, 2, 'email', 'male', 'Daniel Teste', 'danieltriboni44@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, NULL, NULL, '288.711.918-46', NULL),
(331, 1, 1, 'facebook', 'male', 'Carol Marques', 'karolzinha_13@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(332, 1, 1, 'facebook', 'male', 'Sirleide Da Mata', 'sirleidemf@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(333, 1, 1, 'facebook', 'male', 'Debora Karine', 'debinha_775@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(334, 1, 1, 'facebook', 'male', 'Ana Patrícia Macedo', 'anapatriciaamiga@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, NULL, NULL, '', NULL),
(335, 1, 1, 'email', 'female', 'Aline Da silva Brandão', 'carloschristiansuarez339@gmail.com', '845fafbca319e7aaacac5ecbf1c8cb65', 0, NULL, 0, 0, NULL, NULL, '000.000.001-91', NULL),
(336, 1, 1, 'email', 'female', 'Ellen Laiane Gomes dos Santos ', 'ellen.llaiane@hotmail.com', 'db92f530000ef7579b320de1793fba7b', 0, NULL, 0, 0, '1999-07-01', NULL, '000.000.001-91', NULL),
(337, 1, 1, 'email', 'female', 'Maria Eduarda Evagelista Nascimento', 'sushi2irmaos@hotmail.com', '5ecaeedf116167a5bc737724946b5d00', 0, NULL, 0, 0, '2018-12-16', NULL, '000.000.001-91', NULL),
(338, 1, 2, 'email', 'female', 'Maria Eduarda Evangelista Nascimento', 'ger.impresso@hotmail.com', '5ecaeedf116167a5bc737724946b5d00', 0, NULL, 0, 0, '2018-12-16', NULL, '22.054.768/0001-80', NULL),
(339, 1, 2, 'email', 'female', 'Patricia Mangabeira ', 'patricia-mangabeira@hotmail.com', '2f7d3ecb0e08586f9dbf5463cee52013', 0, NULL, 0, 0, '1984-05-04', NULL, '13.561.235/0001-02', NULL),
(340, 1, 2, 'email', 'female', 'Fernanda Rodrigues Lima da Costa', 'fernandarldacosta@gmail.com', 'caf90bb9acef562ca231034b18fb382b', 0, NULL, 0, 0, '1984-11-05', NULL, '30.304.178/0001-97', NULL),
(341, 1, 2, 'email', 'male', 'Divanilson xavier dos santos ', 'divanilsonxavier@hotmail.com', '25d55ad283aa400af464c76d713c07ad', 0, NULL, 0, 0, '1977-11-07', NULL, '30.360.501/0001-40', NULL),
(342, 1, 2, 'email', 'female', 'Sheilla rios Assis Santana', 'sheillara@hotmail.com', '0a2f5d0af490e661f2475fcfd7956d93', 0, NULL, 0, 0, '1987-02-16', NULL, '029.129.325-56', NULL),
(343, 1, 1, 'email', 'female', 'Geisy Daniele de Souza ', 'geisydaniely25@gmail.com', '2a9480c6a9d2fac0dfaddbc42632af67', 0, NULL, 0, 0, '1985-10-25', NULL, '000.000.001-91', NULL),
(344, 1, 1, 'email', 'female', 'Nayane martins', 'nayanemaylla18@gmail.com', '49dd43b45b65dd46d6a0c06b4a0e4882', 0, NULL, 0, 0, '1988-10-06', NULL, '000.000.001-91', NULL),
(346, 1, 1, 'email', 'female', 'Deuzimara Dias Da Costa ', 'deuzimara_costa1@yahoo.com.br', 'e35cf7b66449df565f93c607d5a81d09', 0, NULL, 0, 0, '1980-06-30', NULL, '000.000.001-91', NULL),
(347, 1, 1, 'email', 'female', 'Vanessa Moura', 'vanevilb@gmail.com', '4ba070a66d2ac44dba70cef5d10cb82e', 0, NULL, 0, 0, '1997-11-12', NULL, '000.000.001-91', NULL),
(348, 1, 1, 'email', 'female', 'Meiriele Cristine Fernandes de Oliveira', 'cristinemeiriele@gmail.com', 'ee7f0e7e28e620377714a5e195d077e5', 0, NULL, 0, 0, '2000-01-20', NULL, '000.000.001-91', NULL),
(349, 1, 2, 'email', 'female', 'Luana Rodrigues de Andrade', 'ju_gatinhasara@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1996-08-09', NULL, '108.651.864-05', NULL),
(350, 1, 2, 'email', 'female', 'Rosilene vieira gomes de souza ', 'rosegomes-gui@hotmail.com', '25d55ad283aa400af464c76d713c07ad', 0, NULL, 0, 0, '1977-01-14', NULL, '025.540.074-89', NULL),
(351, 1, 1, 'email', 'female', 'Maria da Conceição Silva de Brito', 'mariaceicabrito@hotmail.com', '6209804952225ab3d14348307b5a4a27', 0, NULL, 0, 0, '1969-02-11', NULL, '000.000.001-91', NULL),
(352, 1, 1, 'email', 'female', 'Maria juçara de sa e silva', 'jussarasa04@hotmail.com', 'f300ed364eda400ad64826ba386fcebf', 0, NULL, 0, 0, '1975-09-04', NULL, '000.000.001-91', NULL),
(353, 1, 1, 'email', 'female', 'Tatiana Dias de Carvalho', 'tatianadecarvalho78@gmail.com', '70ec321062087e602efd8169218ed0d8', 0, NULL, 0, 0, '1978-06-18', NULL, '000.000.001-91', NULL),
(354, 1, 2, 'email', 'female', 'Maria luzimar de souza ', 'vitrageloja10@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1961-02-10', NULL, '04.444.249/0001-47', NULL),
(355, 1, 1, 'email', 'female', 'Patrícia Menezes', 'patricia40santosmenezes@gmail.com', '97f819cb73cb70cf5a72dee956578275', 0, NULL, 0, 0, '1977-01-29', NULL, '000.000.001-91', NULL),
(356, 1, 1, 'email', 'male', 'Samuel leonardo nobrega cavalcanti', 'samuelnobrega340@gmail.com', 'a303f53c24a0b03f7adadca9aae4d0d2', 0, NULL, 0, 0, '1988-06-30', NULL, '000.000.001-91', NULL),
(357, 1, 2, 'email', 'female', 'Geisa Rodrigues', 'geisa.rodrigues@hotmail.com', '098976dd28ec9a42684292af8d12268f', 0, NULL, 0, 0, '1979-07-23', NULL, '21.976.341/0001-77', NULL),
(358, 1, 2, 'email', 'female', 'Keila Marinho', 'keilamarinho987@gmail.com', '79d2d812bf677287382b68106237b5ee', 0, NULL, 0, 0, '1986-10-24', NULL, '11.349.634/0001-70', NULL),
(359, 1, 1, 'email', 'male', 'Leandro Frusciante', 'frusciante196@gmail.com', '3f3ce8d94f88d42322e7204f702c138f', 0, NULL, 0, 0, '1991-12-31', NULL, '000.000.001-91', NULL),
(361, 1, 1, 'email', 'female', 'Larissa de Sá', 'larisdsc1725@outlook.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '2001-05-03', NULL, '000.000.001-91', NULL),
(362, 1, 1, 'email', 'female', 'Thalita Souza Lima', 'thalitasouzaa.1998@gmail.com', '08d432f2445f4c4b07dadaa6b1adc3d3', 0, NULL, 0, 0, '1998-05-30', NULL, '000.000.001-91', NULL),
(363, 1, 1, 'email', 'male', 'Rodrigo de Aguiar Coelho ', 'rca_servicos@hotmail.com', '1697bbdcb8bb3584c33110b3918a0ce3', 0, NULL, 0, 0, '1980-07-18', NULL, '000.000.001-91', NULL),
(364, 1, 1, 'email', 'male', 'Jefferson', 'jeffersonmcorreia@gmail.com', '82b6bd810bd61ddfae009d6fddf3ee63', 0, NULL, 0, 0, '2018-12-02', NULL, '000.000.001-91', NULL),
(365, 1, 2, 'email', 'female', 'Francisca Lucimar Luna', 'pedropalomaluna@hotmail.com', '060d6c33497e260002a735fa20bcce3d', 0, NULL, 0, 0, '1969-07-21', NULL, '883.889.564-34', NULL),
(366, 1, 1, 'email', 'male', 'Adão igo dos Santos Barros', 'adaoigobarros@hotmail.com', '9d8280772584b3d7a76f2f4c54d00454', 0, NULL, 0, 0, '1987-05-30', NULL, '000.000.001-91', NULL),
(367, 1, 1, 'email', 'male', 'Rodrigo De Aguiar Coelho ', 'rca_servicos@hotmail', '2e247e2eb505c42b362e80ed4d05b078', 0, NULL, 0, 0, '1980-07-18', NULL, '000.000.001-91', NULL),
(368, 1, 1, 'email', 'male', 'Cirilo Neto', 'cirilogdc@gmail.com', 'e471218f9abcb880b1466ae9ba88e0fc', 0, NULL, 0, 0, '1975-04-20', NULL, '000.000.001-91', NULL),
(369, 1, 1, 'email', 'male', 'CARLOS AUGUSTO MARTINS FERREIRA ', 'carlosaugsuto1212@hotmail.com', 'f71d56b94e2196073e7642155f212223', 0, NULL, 0, 0, '1990-12-12', NULL, '000.000.001-91', NULL),
(370, 1, 1, 'email', 'male', 'Leonardo Emanuel', 'custodio98@outlook.com', '2b889c80b42e85912623f42c446b83b1', 0, NULL, 0, 0, '1997-04-20', NULL, '000.000.001-91', NULL),
(371, 1, 1, 'email', 'male', 'Mateus Henrique Bulhões de Matos', 'matteus.mattos96@gmail.com', '217dc5f06490b40e49f9a7cccf42e5ec', 0, NULL, 0, 0, '1996-12-30', NULL, '000.000.001-91', NULL),
(372, 1, 1, 'email', 'male', 'Julio César Elias dos Santos', 'jullioflamengo2017@gmail.com', '3c4235124f55ea27ce5dd95f7a75b9e6', 0, NULL, 0, 0, '1999-07-03', NULL, '000.000.001-91', NULL),
(373, 1, 1, 'email', 'male', 'Mago da Mídia ', 'vivaz.cds@hotmail', 'e7ed958dd4b53920955d58c124a425ab', 0, NULL, 0, 0, '1995-04-25', NULL, '000.000.001-91', NULL),
(374, 1, 1, 'facebook', 'male', 'Deuzimara Dias da Costa', 'deuzimara_costa@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(375, 1, 1, 'email', 'female', 'Fernanda Souza Costa', 'nandasouzzacosta@gmail.com', 'f484f83fff7010e64bb7c02ca6f04922', 0, NULL, 0, 0, '1995-11-29', NULL, '000.000.001-91', NULL),
(376, 1, 1, 'facebook', 'male', 'Tamires Castro', 'tamirescastrowowapp@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(377, 1, 1, 'facebook', 'male', 'Ricardo Marques', 'riisgi27@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(378, 1, 1, 'email', 'male', 'ialys wesley', 'ialyswesley7@hotmail.com', 'f31853d613b937281757f183654f2130', 0, NULL, 0, 0, '1997-04-30', NULL, '000.000.001-91', NULL),
(379, 1, 1, 'facebook', 'male', 'Kris Oliveira', 'kris_oliveira@live.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(380, 1, 1, 'facebook', 'male', 'Sonilda Rodrigues', 'sonildarodrigues@yahoo.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(381, 1, 1, 'email', 'female', 'Marcilene de sa e silva', 'marcisa04@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1976-12-13', NULL, '000.000.001-91', NULL),
(382, 1, 1, 'email', 'male', 'Gilmar filho', 'gilmarfilho2908@hotmail.com', 'fb49db2d0be0e2546c75875af409404b', 0, NULL, 0, 0, '1989-08-29', NULL, '000.000.001-91', NULL),
(383, 1, 2, 'email', 'female', 'Ednalva da silva Diaa ', 'ellenmagazine@hotmail', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1980-01-19T00:00:00-03:00', NULL, '042.063.774-56', NULL),
(384, 1, 2, 'email', 'female', 'Erivania de Souza Oliveira', 'erivaniasouza200@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1983-02-04', NULL, '074.834.894-80', NULL),
(385, 1, 2, 'email', 'female', 'Damiana jeane neri portugues ', 'jeanedossantos2011@hotmail.com', '5ec11be0b8a026cffe0ee530945154b6', 0, NULL, 0, 0, '1984-06-21', NULL, '052.059.024-42', NULL),
(386, 1, 1, 'facebook', 'male', 'Jéssica Oliveira', 'deca_petro@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(387, 1, 2, 'email', 'female', 'Iana Paula da Silva ', 'ianaconfeccoes@yahoo.com.br', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1985-06-12', NULL, '053.716.104-05', NULL),
(388, 1, 1, 'email', 'female', 'Iandra Maria de Oliveira', 'iandramaria1527@gmail.com', '00dc6e61037e1631421c0fc0dcd37a01', 0, NULL, 0, 0, '2000-11-13', NULL, '000.000.001-91', NULL),
(389, 1, 2, 'email', 'female', 'Maria dos anjos Silva', 'maryangelinaa@hotmail.com', '16d314680bd4a8f4bdc0bb09fae21ca0', 0, NULL, 0, 0, '1990-08-15', NULL, '088.740.834-67', NULL),
(390, 1, 2, 'email', 'female', 'Vivian de Brito Rodrigues ', 'vivibrt6@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1981-10-25', NULL, '009.836.494-48', NULL),
(391, 1, 2, 'email', 'female', 'Silvania Mercedes Marques', 'nicole.incrivel@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1976-11-27', NULL, '052.372.539-62', NULL),
(392, 1, 2, 'email', 'female', 'Elizangela Conrado Pereira de Souza ', 'elizconrado1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1971-10-10', NULL, '708.412.503-59', NULL),
(393, 1, 2, 'email', 'male', 'José Santana de Sousa', 'josesantanasousa@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1983-07-23', NULL, '051.588.444-88', NULL),
(394, 1, 2, 'email', 'male', 'Uilame Barbosa de Souza', 'uilame12@gmail.com', '8f0574aa7986e13609cc845f696e1251', 0, NULL, 0, 0, '1982-02-12', NULL, '018.191.775-05', NULL),
(395, 1, 2, 'email', 'male', 'Jose Nivaldo Barbosa ', 'nyvalldinho@gmail.com', '83bb92ff4f225c0543c987cd8166ac8b', 0, NULL, 0, 0, '1986-08-29', NULL, '039.601.245-03', NULL),
(396, 1, 2, 'email', 'female', 'Leidiane de Souza Silva', 'leideaneldn60@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1993-04-17', NULL, '058.057.725-23', NULL),
(397, 1, 2, 'email', 'female', 'ronilda da silva rodrigues', 'inmorais@gmail.com', '17bdcd7d361385b62d080d0070a61cb6', 0, NULL, 0, 0, '1997-08-09', NULL, '077.350.723-09', NULL),
(398, 1, 2, 'email', 'female', 'Madalena Silva Coelho', 'madalenacoelho15@hotmail.com', 'ff5390bde5a4cf0aa2006cf2198efd29', 0, NULL, 0, 0, '1992-04-15', NULL, '24.423.306/0001-19', NULL),
(399, 1, 1, 'email', 'female', 'Jackeline Maria dos Santos Castro', 'jackelinecastro302@gmail.com', 'd576507d1a736370701cb922515757e8', 0, NULL, 0, 0, '1992-01-29', NULL, '000.000.001-91', NULL),
(400, 1, 1, 'facebook', 'male', 'Iandra Aryel', 'iandra_aryel1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(401, 1, 1, 'facebook', 'male', 'Dadio Gentilli', 'fascianiaguia@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(402, 1, 1, 'email', 'female', 'hilda isabel de souza ', 'hildasouza3281@gmail.com', '803f54470ffd6c8fc368ce52a263bb40', 0, NULL, 0, 0, '1996-11-16', NULL, '000.000.001-91', NULL),
(403, 1, 1, 'facebook', 'male', 'Fernando Coelho', 'phernandho_coelho@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(404, 1, 1, 'facebook', 'male', 'Aline Lima', 'aline10_lima@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(405, 1, 1, 'email', 'female', 'Stefanny de Souza', 'stefanny.castro66@gmail.com', 'ecb632460585cbc8c20e5add3a539647', 0, NULL, 0, 0, '1999-06-11', NULL, '000.000.001-91', NULL),
(406, 1, 1, 'email', 'female', 'Fernanda Kelly', 'naandakelli@gmail.com', 'c52a0d5ac3055bddc6567692fa79a6bd', 0, NULL, 0, 0, '1996-01-29', NULL, '000.000.001-91', NULL),
(407, 1, 1, 'email', 'female', 'Mirtes Cunha', 'mirtescunha83@gmail.com', '577471589b29f54e84ad47cbeb85f7cf', 0, NULL, 0, 0, '1983-09-20', NULL, '000.000.001-91', NULL),
(408, 1, 1, 'facebook', 'male', 'Filipe Rosas', 'filiperosas@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(409, 1, 1, 'facebook', 'male', 'Jesser Pedrosa', 'jesserpedrosa22@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(410, 1, 1, 'email', 'male', 'Socrates miranda', 'socratesaugusto1@gmail.com', '0ab20e738c74e2ea260b13cd36764ad7', 0, NULL, 0, 0, '2000-10-22', NULL, '000.000.001-91', NULL),
(411, 1, 1, 'email', 'female', 'Mariana Barbosa Miranda', 'maribm.1@hotmail.com', 'df6c639b29f93e322058ad2e44126b57', 0, NULL, 0, 0, '1994-09-28', NULL, '000.000.001-91', NULL),
(412, 1, 1, 'facebook', 'male', 'Matheus Ayres', 'matheuselisyo@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(413, 1, 1, 'facebook', 'male', 'Francilene Yasmin', 'francilene.ss@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(414, 1, 1, 'facebook', 'male', 'Dani Sales', 'daniella_s_silva@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(415, 1, 1, 'email', 'male', 'Francisco Juniere Martins Negreiros', 'juniere.mn@gmail.com', 'ac766a75bdd2e43189ce3ae33fb2e287', 0, NULL, 0, 0, '1991-06-24', NULL, '000.000.001-91', NULL),
(416, 1, 1, 'facebook', 'male', 'Ana Clara Oliveira', 'anaclara650@outlook.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(417, 1, 1, 'email', 'female', 'Tatiane Fialho', 'taty.fialho52@gmail.com', 'e60f68ade7ebba90933749b079b31cb3', 0, NULL, 0, 0, '1986-02-22', NULL, '000.000.001-91', NULL),
(418, 1, 1, 'facebook', 'male', 'Eldo Chaves', 'eldocardoso@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(419, 1, 1, 'facebook', 'male', 'Vinicius Alexandre', 'vinicius_newyork@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(420, 1, 1, 'email', 'male', 'James Oliveira', 'james.mix@bol.com.br', 'e82ae5f72b935cbe2a51698dea8854c9', 0, NULL, 0, 0, '1982-01-07', NULL, '000.000.001-91', NULL),
(421, 1, 1, 'facebook', 'male', 'Daniele Menezes', 'menezes.daniele@outlook.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(422, 1, 1, 'facebook', 'male', 'Anny Batista', 'any.star21@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(423, 1, 1, 'facebook', 'male', 'Gabriel Muniz', 'gabrielrso@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(424, 1, 2, 'email', 'male', 'Winston nunes', 'winstombarbosa@gmail.com', '6add84506c86a658bc85038f91e35ce7', 0, NULL, 0, 0, '1981-04-07', NULL, '037.879.094-35', NULL),
(425, 1, 1, 'facebook', 'male', 'Liz Santana', 'lizgrota@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(426, 1, 1, 'facebook', 'male', 'Felipe Moreira', 'alexandrericciardo@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(427, 1, 1, 'email', 'male', 'DAN ', 'daguto33@gmail.com', 'd78ca280c011c24412c3208144928626', 0, NULL, 0, 0, '1998-12-17', NULL, '000.000.001-91', NULL),
(428, 1, 1, 'email', 'male', 'Júlio César Elias dos Santos', 'julioelias032@gmail.com', '3c4235124f55ea27ce5dd95f7a75b9e6', 0, NULL, 0, 0, '1999-07-03', NULL, '000.000.001-91', NULL),
(429, 1, 1, 'facebook', 'male', 'Ana Carla', 'anacarla1804@outlook.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(430, 1, 2, 'email', 'male', 'Suporte Se Liga Aí ', 'homologacao12018@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1988-12-19T00:00:00-03:00', NULL, '31.318.426/0001-11', NULL),
(431, 1, 1, 'email', 'male', 'Antonio Costa', 'homologacao32018@gmail.com', '2e9ec317e197819358fbc43afca7d837', 0, NULL, 0, 0, '1973-03-01', NULL, '000.000.001-91', NULL),
(432, 1, 1, 'facebook', 'male', 'Leonardo Ferreira', 'leonardof@outlook.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(433, 1, 1, 'facebook', 'male', 'Priscila Vieira', 'jpri-vieira@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '', NULL, '', NULL),
(434, 1, 1, 'facebook', 'male', 'Diego Djale', 'diegodjale@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(435, 1, 1, 'facebook', 'male', 'Denise Isabela', 'deniseisabela22@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(436, 1, 1, 'email', 'female', 'Sara Pires Coelho', 'sara.pires.oficial@gmail.com', '5479f3bb2e06467ff8bcfd74cfbb77e3', 0, NULL, 0, 0, '1999-06-27', NULL, '000.000.001-91', NULL),
(437, 1, 1, 'facebook', 'male', 'Junnior Aguiar', 'junnyor93@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(438, 1, 1, 'email', 'female', 'Daniele Matias de souza', 'danielematias6006@gmai.com', '7189dfeac32cea348f25d63eb1f07276', 0, NULL, 0, 0, '1994-10-10', NULL, '000.000.001-91', NULL),
(439, 1, 1, 'email', 'male', 'Yago nascimento santos', 'yago.n.s@hotmail.com', '673246d310815197e72dca5fe574eefe', 0, NULL, 0, 0, '1995-05-06', NULL, '000.000.001-91', NULL),
(440, 1, 1, 'email', 'male', 'Edimar Alpper', 'santosedimar0@gmail.com', 'cf9f85408355c9a103b86fe13707dc81', 0, NULL, 0, 0, '1997-04-14', NULL, '000.000.001-91', NULL),
(441, 1, 1, 'facebook', 'male', 'Luis Filipe Sousa', 'luisfilipesccp@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL);
INSERT INTO `users` (`usrid`, `active`, `type`, `signup`, `gender`, `name`, `email`, `password`, `lastlotteryid`, `lastlotterynum`, `canuseadmin`, `canseller`, `borndate`, `rg`, `cpfcnpj`, `cellphone`) VALUES
(442, 1, 1, 'email', 'male', 'Pedro kauan Gomes Dourado ', 'kauan20142220@gmail.com', 'd3ce9efea6244baa7bf718f12dd0c331', 0, NULL, 0, 0, '2000-06-02', NULL, '000.000.001-91', NULL),
(443, 1, 1, 'email', 'male', 'Henrique', 'riqueegg@hotmail.com', 'a719661a0f98836aa422b874eb6d7f42', 0, NULL, 0, 0, '2000-04-05', NULL, '000.000.001-91', NULL),
(444, 1, 1, 'email', 'female', 'Carolline Souza', 'carolladss@gmail.com', '5a4c441b32b0403f42d09c8d621f797b', 0, NULL, 0, 0, '1995-02-10', NULL, '000.000.001-91', NULL),
(445, 1, 1, 'email', 'male', 'Robson da Costa Lima ', 'costaelima1988@gmail.com', 'af4c84e117a3cb494647cda9b3b8f407', 0, NULL, 0, 0, '1988-09-05', NULL, '000.000.001-91', NULL),
(446, 1, 1, 'facebook', 'male', 'Larissa Guivares', 'lguivares@zipmail.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(447, 1, 2, 'email', 'female', 'Igor Rafael Da Silva Damasceno', 'plantasdovaleis@gmail.com', '24200df49c3c55122f1a8e28f9cc0664', 0, NULL, 0, 0, '1994-01-16', NULL, '115.310.264-19', NULL),
(448, 1, 1, 'email', 'female', 'Deiseane carlos de souza', 'deiseanecarla@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, 0, 0, '1989-07-02', NULL, '000.000.001-91', NULL),
(449, 1, 1, 'email', 'female', 'Mariana', 'mariana.rubro@hotmail.com', '27e3e5d27cf73a604a2f678e860ccf45', 0, NULL, 0, 0, '2000-06-14', NULL, '000.000.001-91', NULL),
(450, 1, 1, 'facebook', 'male', 'Carlos Alberto Lustosa', 'carloslustosadesc@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(451, 1, 1, 'facebook', 'male', 'Patrícia Porto', 'patriciaporto56@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(452, 1, 1, 'facebook', 'male', 'Franco Lopes', 'franco_dbl@live.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(453, 1, 1, 'facebook', 'male', 'Vitória Raquel', 'vitoraquel_2@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(454, 1, 1, 'facebook', 'male', 'Victor Fernandes', 'victor.arf15@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(455, 1, 1, 'facebook', 'male', 'Jose Bispo Franca', 'joseneto43@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(456, 1, 1, 'email', 'female', 'Gabriela G. do Nascimento ', 'gabrielagou2015@gmail.com', 'e9be0420189272d50ea5fbf38e39438f', 0, NULL, 0, 0, '1998-06-08', NULL, '000.000.001-91', NULL),
(457, 1, 1, 'email', 'female', 'Mayara valenca ', 'mayaravalenca2016@outlook.com', 'fd7662b23faec24c1630e0e5c6967d14', 0, NULL, 0, 0, '1997-07-02', NULL, '000.000.001-91', NULL),
(458, 1, 1, 'email', 'female', 'Maria Vitória Oliveira Torres', 'mavitoorres@gmail.com', '34a86abe5a45db5a2bb9310d97f97fee', 0, NULL, 0, 0, '2002-09-05', NULL, '000.000.001-91', NULL),
(459, 1, 1, 'facebook', 'male', 'Jessica Alves', 'rayane12alves@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(460, 1, 1, 'facebook', 'male', 'Damiro Souza Bispo', 'damyrobispo@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(461, 1, 1, 'facebook', 'male', 'Valéria Menezes', 'valml16@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(462, 1, 1, 'facebook', 'male', 'Danilo Taveira Gonçalves', 'danilokabrobo@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(463, 1, 1, 'email', 'female', 'Danielly Ferreira Guimarães ', 'daniellyferreira0705@gmail.com', '29bbffdf494651cebdfa36535e0008c1', 0, NULL, 0, 0, '2000-05-07', NULL, '000.000.001-91', NULL),
(464, 1, 1, 'facebook', 'male', 'Andréa Castro', 'andrea.lacerd@bol.com.br', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(465, 1, 1, 'email', 'female', 'Isla melo ', 'isla911@gmail.com', '543616b7ff061a95bde44d7fd218a3e6', 0, NULL, 0, 0, '1997-12-27', NULL, '000.000.001-91', NULL),
(466, 1, 1, 'facebook', 'male', 'Cynthia Mira Nunes', 'cynthiamira_24@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(467, 1, 1, 'facebook', 'male', 'Duda Silva', 'mariedu-2012@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(468, 1, 1, 'facebook', 'male', 'Munique Daniela Coelho Rodrigues Coelho', 'muniquedaniela@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(469, 1, 1, 'facebook', 'male', 'Maria Fatima', 'fatimafofinha12@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(470, 1, 1, 'facebook', 'male', 'Juazeiro Juazeiro Bahia', 'radiologiatecmedjuazeiroba@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(471, 1, 1, 'facebook', 'male', 'Jukar Equipadora', 'jukarsomequipadora@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(472, 1, 1, 'email', 'male', 'Andre', 'bob36928@gmail.com', 'dd573120e473c889140e34e817895495', 0, NULL, 0, 0, '1997-07-01', NULL, '000.000.001-91', NULL),
(473, 1, 1, 'facebook', 'male', 'Stefany Fiamma', 'fannysouza14@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(474, 1, 1, 'facebook', 'male', 'Natália Teles', 'natalia_talmeida@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(475, 1, 1, 'facebook', 'male', 'William Sena', 'williamseena1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(476, 1, 1, 'email', 'female', 'Carolaine', 'nogueiracarol30@gmail.com', 'd8d510f34346567996b03f4c2e1fe7c1', 0, NULL, 0, 0, '1996-05-03', NULL, '000.000.001-91', NULL),
(477, 1, 1, 'facebook', 'male', 'Adriana Sousa', 'adriiana.91@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(478, 1, 1, 'facebook', 'male', 'Yuwan Takeo', 'yuwantakeo@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(479, 1, 1, 'facebook', 'male', 'Laís Naiara', 'isnaiaraadm@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(480, 1, 1, 'email', 'female', 'Raissa Soares Rosa de Souza', 'raissarsouza1@gmail.com', '2fed78c8483d960f189255b7bda2d0d6', 0, NULL, 0, 0, '1995-06-04', NULL, '000.000.001-91', NULL),
(481, 1, 1, 'email', 'female', 'Márcia Antunes', 'butiquimdiskbeer@gmail.com', '6add84506c86a658bc85038f91e35ce7', 0, NULL, 0, 0, '1983-01-19', NULL, '000.000.001-91', NULL),
(482, 1, 2, 'email', 'female', 'Personalize Petrolina', 'personalizepetrolina@hotmail.com', 'd64776f5e968cd062729a0981e8610b8', 0, NULL, 0, 0, '1999-01-01', NULL, '19.747.277/0001-00', NULL),
(483, 1, 1, 'email', 'female', 'Vanessa Santos da Silva', 'vanessa.portal@hotmail.com', 'c1d92ffadb70fcde8727756aa45270fc', 0, NULL, 0, 0, '1994-01-07', NULL, '000.000.001-91', NULL),
(484, 1, 1, 'email', 'female', 'Laysla Miranda costa', 'layslamiranda2018@gmail.com', '60d0db313eea042de7dd2cf49541ffcc', 0, NULL, 0, 0, '1992-01-17', NULL, '000.000.001-91', NULL),
(485, 1, 1, 'facebook', 'male', 'Pedro Guilherme', 'pedro.guilherme2000@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(486, 1, 1, 'email', 'male', 'Petrick Heynner Palma de Oliveira', 'petrickpalma@outlook.com', 'f9d8f14b3a76e2ba7b16f3e1f55982ad', 0, NULL, 0, 0, '1999-11-04', NULL, '000.000.001-91', NULL),
(487, 1, 1, 'email', 'male', 'Igor Bagagi', 'isillva595@gmail.com', '30a9b6c2ca849074d5f54fe6884ac1b8', 0, NULL, 0, 0, '2001-01-29', NULL, '000.000.001-91', NULL),
(488, 1, 1, 'email', 'female', 'Jayane', 'jayanepaloma160@gmail.com', 'bbee168538cd29507cefacf48c986e3c', 0, NULL, 0, 0, '1996-06-01', NULL, '000.000.001-91', NULL),
(489, 1, 1, 'facebook', 'male', 'Daianne Ferreira', 'dayannef63@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(490, 1, 2, 'email', 'female', 'Thaís Brito de Castro', 'thaiscastro8991@gmail.com', 'eb2db8b1e8e15ca4c4ee94dc14f9caf3', 0, NULL, 0, 0, '1998-04-21', NULL, '703.597.361-30', NULL),
(491, 1, 1, 'facebook', 'male', 'Thaynara Lima', 'thay.lorrane@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(492, 1, 1, 'facebook', 'male', 'Daniela Oliveira', 'danielaa.oliveira1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(493, 1, 1, 'facebook', 'male', 'Martinha Lacerda', 'lacerdamarta@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(494, 1, 1, 'email', 'female', 'Marlene de Carvalho Afonso Cunha ', 'marlenecarvalho82@hotmail.com', '29a5641eaa0c01abe5749608c8232806', 0, NULL, 0, 0, '1982-03-19', NULL, '000.000.001-91', NULL),
(495, 1, 1, 'facebook', 'male', 'Gabriel Albuquerque', 'gabriel9albuquerque@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(496, 1, 1, 'facebook', 'male', 'Oziel Souza', 'latinomalcher@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(497, 1, 1, 'facebook', 'male', 'Alexandre Souza', 'vidae420@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(498, 1, 1, 'facebook', 'male', 'Mayane Almeida', 'mayaninhaalmeida@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(499, 1, 1, 'facebook', 'male', 'Gustavo Barbosa', 'gugu.galolcura@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(500, 1, 1, 'email', 'female', 'Lara Emilena Brito de Oliveira', 'laraemilena19@gmail.com', '3b07c768071e6175ced9ea12764baf2c', 0, NULL, 0, 0, '1999-02-05', NULL, '000.000.001-91', NULL),
(501, 1, 1, 'email', 'female', 'Ha na', 'hannabeatrizm@hotmail.com', '276b605ee10c196a4535e8b9cdb882ca', 0, NULL, 0, 0, '1997-11-07', NULL, '000.000.001-91', NULL),
(502, 1, 1, 'facebook', 'male', 'Gomes Netto', 'n9solucoes@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(503, 1, 1, 'facebook', 'male', 'Lucas Granja', 'lucas_emanoelgs1998@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(504, 1, 1, 'email', 'male', 'victor Gabriel', 'victor123.vl39@gmail.com', '138e58c29d0732988d9f10d55abcbd75', 0, NULL, 0, 0, '2003-12-19', NULL, '000.000.001-91', NULL),
(505, 1, 1, 'facebook', 'male', 'Everton Monteiro', 'everton.grappler@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(506, 1, 1, 'facebook', 'male', 'João Vitor Allarik', 'joao_barakio@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(507, 1, 1, 'facebook', 'male', 'Francisca Lima', 'franciscacordeiro.456@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(508, 1, 1, 'email', 'female', 'Fernanda Alves de Souza', 'fernandinhaalves2011@hotmail.com', '4e25e510bb73f379d9059e5c69d958be', 0, NULL, 0, 0, '1998-09-09', NULL, '000.000.001-91', NULL),
(509, 1, 1, 'facebook', 'male', 'Pedro Lucas', 'linkpark352@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(510, 1, 1, 'facebook', 'male', 'Mirella Mirtes Clérison Belém', 'mirellamlins@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(511, 1, 1, 'facebook', 'male', 'Alisson Raul', 'angelamaria.05@outlook.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(512, 1, 1, 'email', 'male', 'Muacir Tenório', 'nuacir1960@hotnail.com', '7e78da47ca3bcbae935784dba2adf9f6', 0, NULL, 0, 0, '1960-11-17', NULL, '000.000.001-91', NULL),
(513, 1, 1, 'email', 'female', 'Naila Souza', 'naila1234@gmail.com', '00a4ac859821c9c8bce5e3b24e1b2202', 0, NULL, 0, 0, '1999-09-06', NULL, '000.000.001-91', NULL),
(514, 1, 1, 'facebook', 'male', 'Cintia Raylani', 'cintia_raylane_@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(515, 1, 1, 'facebook', 'male', 'Marcone Conceição', 'marcone2020deus@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(516, 1, 2, 'email', 'female', 'Caticile', 'netosupri@hotmail.com', '3644745799cf661e649d531634ce8042', 0, NULL, 0, 0, '1982-03-30', NULL, '029.432.744-45', NULL),
(517, 1, 2, 'email', 'male', 'Paulo', 'netoneto@r7.com', '3644745799cf661e649d531634ce8042', 0, NULL, 0, 0, '1979-04-28', NULL, '183.254.754-04', NULL),
(518, 1, 1, 'facebook', 'male', 'Mug Mug', 'mugmag13@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(519, 1, 1, 'email', 'male', 'Albert', 'gdsgsdgsd@hotmail.com', '36702476a7ab8eeb477f2b47160ef464', 0, NULL, 0, 0, '1997-01-31', NULL, '000.000.001-91', NULL),
(520, 1, 1, 'facebook', 'male', 'Awdryn Souza', 'awdryn@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(521, 1, 1, 'email', 'male', 'Paulo Torres', 'paulo@cclog.com.br', 'b5b905d8fea488a3ed8281a4d015a4d3', 0, NULL, 0, 0, '1982-06-07', NULL, '000.000.001-91', NULL),
(522, 1, 1, 'facebook', 'male', 'Marcos Oliveira', 'marcos2851@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(523, 1, 1, 'email', 'male', 'Ramos', 'edsbil@hotmail.com', '0a643ff322fe15dc5dc8a460f7439b73', 0, NULL, 0, 0, '2009-10-16', NULL, '000.000.001-91', NULL),
(524, 1, 1, 'email', 'male', 'Hamilton jose santos pereira', 'hamiltonpereira622@gmail.com', '6c23ef59329bcbfeb2e9288e4f3ef8b7', 0, NULL, 0, 0, '1983-03-19', NULL, '000.000.001-91', NULL),
(525, 1, 1, 'facebook', 'male', 'Samille Gama', 'samille_gama@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(526, 1, 1, 'facebook', 'male', 'Emanuella Santos', 'emannuella5_@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(527, 1, 1, 'facebook', 'male', 'Maria Mota Castelo Castelo', 'mmcastelo59@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(528, 1, 1, 'facebook', 'male', 'Andrea Barbosa De Lima Nobre', 'andreanobresa@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(529, 1, 1, 'facebook', 'male', 'Romulo Ayala', 'romuloayala@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(530, 1, 1, 'email', 'male', 'William kledy oliveira silva', 'wkledy@hotmail.com', '1943065bc11b895282bea77a43e9d8e8', 0, NULL, 0, 0, '1981-12-02', NULL, '000.000.001-91', NULL),
(531, 1, 1, 'facebook', 'male', 'Ruck Santos', 'ruck.62@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(532, 1, 1, 'facebook', 'male', 'Keren Melo', 'keren_melo@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(533, 1, 1, 'facebook', 'male', 'Lucas Barros', 'lbarros.comp@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(534, 1, 1, 'email', 'male', 'Fábio Tavares de Lima', 'simpatialins@hotmail.com', '67c16eeb951ed9446c716a762da91ce1', 0, NULL, 0, 0, '1975-03-25', NULL, '000.000.001-91', NULL),
(535, 1, 1, 'email', 'male', 'Wilson de barros', 'barroswilson2018@gmail.com', 'f2f5f4978873db72faf3a2eb818fbd92', 0, NULL, 0, 0, '1964-07-21', NULL, '000.000.001-91', NULL),
(536, 1, 1, 'facebook', 'male', 'Katia Cristiane Moraes Vasconcelos', 'katiamoraes07@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(537, 1, 1, 'facebook', 'male', 'Vinicius Moura', 'marcosvinicius_msilva@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(538, 1, 1, 'facebook', 'male', 'Ana Marcia Ribeiro Silva', 'ana_ribeiro195@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(539, 1, 2, 'email', 'male', 'Keila Marinho', 'gigatec_1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '11.349.634/0001-70', NULL),
(540, 1, 1, 'email', 'male', 'Bomfim santos ', 'bomfim.gil@hotmail.com', 'e13012c30c3b03fbfabd553ce484cfcf', 0, NULL, 0, 0, '1984-10-12', NULL, '000.000.001-91', NULL),
(541, 1, 1, 'email', 'male', 'Bomfim santos ', 'bomfim84sts@gmail.com', 'e13012c30c3b03fbfabd553ce484cfcf', 0, NULL, 0, 0, '1984-10-12', NULL, '000.000.001-91', NULL),
(542, 1, 1, 'facebook', 'male', 'Junior Dias', 'juniorpagodart_17@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(543, 1, 1, 'facebook', 'male', 'Smith Ferreira', 'smitogato@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(544, 0, 1, 'facebook', 'male', 'Albert Gu', 't4k9d1x3c0@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(545, 1, 1, 'facebook', 'male', 'Gilberto Avelino de Souza', 'gil.a.souza@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(546, 1, 1, 'email', 'female', 'Maria Carolina', 'carolribeiro1@gmail.com', '7cd9ab0fe4ae1c249a8067166ce1fd9b', 0, NULL, 0, 0, '1996-09-16', NULL, '000.000.001-91', NULL),
(547, 1, 1, 'facebook', 'male', 'Naldo Marinho Terto', 'vicenaldo@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(548, 1, 1, 'facebook', 'male', 'Edson Lopes', 'edsonlopesfotografia@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(549, 1, 1, 'email', 'male', 'Ze ', 'carol@hotmail.com', 'd0970714757783e6cf17b26fb8e2298f', 0, NULL, 0, 0, '1998-06-15', NULL, '000.000.001-91', NULL),
(550, 1, 1, 'email', 'male', 'Elder dos santos Oliveira', 'elderoliveira270690@gmail.com', '9971f9f8914cb2154d17d6a1cc387e07', 0, NULL, 0, 0, '1990-06-27', NULL, '000.000.001-91', NULL),
(551, 1, 2, 'email', 'female', 'Iracema Cordeiro da Luz', 'iracordeiro99@gmail.com', '8daeb3eabba108b1815880ae2bb92bd4', 0, NULL, 0, 0, '1976-07-17', NULL, '09.456.667/0001-78', NULL),
(552, 1, 1, 'facebook', 'male', 'Ângela De S. Lins', 'anginhaangela2010@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(553, 1, 1, 'facebook', 'male', 'Sâmara Souza', 'samara.souza.pesqueira@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(554, 1, 1, 'facebook', 'male', 'João Victor Ribeiro', 'joao_vitinhors@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(555, 1, 1, 'facebook', 'male', 'Jonas Rocha', 'jonas.s.rocha@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(556, 1, 1, 'email', 'male', 'Oziel coelho ', 'oziel.coelho@gmail.com', 'd51bfccf02e53341ba463d502a89ab55', 0, NULL, 0, 0, '1979-04-14', NULL, '000.000.001-91', NULL),
(557, 1, 1, 'email', 'female', 'Mariana do Nascimento Silva', 'mariana_rnv@hotmail.com', '47180b2676a6effc961b4ebed4eaa763', 0, NULL, 0, 0, '1990-11-09', NULL, '000.000.001-91', NULL),
(558, 1, 1, 'facebook', 'male', 'Raul Cesar', 'raulcesar17@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(559, 1, 1, 'facebook', 'male', 'Iandra Costa', 'iandra_a-ha@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(560, 1, 1, 'email', 'female', 'Marina pereira dos santos', 'marinapereira__10@hotmail.com', '46e73c93335d2c21e12a90f1149c2ed8', 0, NULL, 0, 0, '1979-07-26', NULL, '000.000.001-91', NULL),
(561, 1, 1, 'facebook', 'male', 'Willberlon Cisneiros', 'willber.c.s@gmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(562, 1, 1, 'facebook', 'male', 'Gideone Alcides', 'gideonealcides88@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(563, 1, 1, 'facebook', 'male', 'Luciana Reggiani', 'lureggiani@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(564, 1, 1, 'facebook', 'male', 'Sandro Motta', 'sandro-mota1@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(565, 1, 1, 'email', 'female', 'Milca Soraia dos Santos Queiroz ', 'milcasoraia@gmail.com', '57da7dbf18fdce3f7ef75d7d3e0864c4', 0, NULL, 0, 0, '1980-01-04', NULL, '000.000.001-91', NULL),
(566, 1, 1, 'facebook', 'male', 'Vanilda Queiroz', 'vanyeliana@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(567, 1, 1, 'facebook', 'male', 'Cintia Silva', 'cintia.loy@hotmail.com', 'ee5cdcc3183d0c25584957da40b38249', 0, NULL, 0, 0, '1980-01-01', NULL, '', NULL),
(568, 1, 1, 'email', 'female', 'Eva Carla Tores de Souza Galvaô', 'torrescarlarh@outlook.com', 'bb7f3b32797831b2a03ce3106361ff18', 0, NULL, 0, 0, '1980-01-04', NULL, '000.000.001-91', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `userscategories`
--

DROP TABLE IF EXISTS `userscategories`;
CREATE TABLE IF NOT EXISTS `userscategories` (
  `ucaid` int(11) NOT NULL AUTO_INCREMENT,
  `usuid` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  PRIMARY KEY (`ucaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usersoffers`
--

DROP TABLE IF EXISTS `usersoffers`;
CREATE TABLE IF NOT EXISTS `usersoffers` (
  `uofid` int(11) NOT NULL AUTO_INCREMENT,
  `usuid` int(11) NOT NULL,
  `ofeid` int(11) NOT NULL,
  `liked` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uofid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `userwallets`
--

DROP TABLE IF EXISTS `userwallets`;
CREATE TABLE IF NOT EXISTS `userwallets` (
  `walid` int(11) NOT NULL AUTO_INCREMENT,
  `usrid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `value` decimal(10,2) NOT NULL,
  PRIMARY KEY (`walid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `walletextracts`
--

DROP TABLE IF EXISTS `walletextracts`;
CREATE TABLE IF NOT EXISTS `walletextracts` (
  `extid` int(11) NOT NULL AUTO_INCREMENT,
  `walid` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `description` text NOT NULL,
  `value` float(10,2) NOT NULL,
  PRIMARY KEY (`extid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `walletextracts`
--

INSERT INTO `walletextracts` (`extid`, `walid`, `date`, `description`, `value`) VALUES
(1, 1, '2019-03-29 00:00:00', '', -14.53),
(2, 1, '2019-03-28 00:00:00', '', 5.99);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `fk_categories_stores` FOREIGN KEY (`catid`) REFERENCES `categories` (`catid`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
