<?php
/**
* DEFINICOES DE CONEXAO COM BANCO DE DADOS
* @tutorial NOTA: Manter o bloqueio caso este arquivo seja acessado diretamente!
* @author EM2D Development Team
*/

/* Admin em Manutencao */
define("SIS_MANUTENCAO", false);
/* Site Inauguracao */
define("SIS_INAUGURACAO", '2018-04-10 00:00:00');
/* Nome do Proprietario */
define("SIS_TITULO", "EM2D Administrator");
/* Descricao Meta Description */
define("SIS_DESCRICAO", (SIS_TITULO." | Se Liga Aí, Ofertas, Promoções, Vendas Online, E-commerce"));
/* URL do Proprietario - Configurar de acordo com a URL amigavel utilizada */
define("SIS_URL", "https://em2d.com.br/se-liga-ai-administrator/");
/* Cookie secure mode */
define("SIS_SECURE", true);
/* Email de Interacao */
define("SIS_EMAIL", "falecom@em2d.com.br");
/* Idioma do Proprietario */
define("SIS_IDIOMA", "portugues");		
/* Servidor de Conexao ao banco */
define("SIS_HOST", "localhost"); 
/* Porta de Conexao ao banco */
define("SIS_PORTA", "3306");
/* Usuario de Conexao ao banco */
define("SIS_USUARIO", "usrseligaai");
/* Senha de Conexao ao banco */
define("SIS_SENHA", 'Seliga@1981');
/* Banco de dados do cliente */   
define("SIS_BANCO", "seligaaidb");
/* Tempo de uso de Sessao (em segundos) */
define("SIS_TEMPO", 7200);
/* Registros por Pagina */		
define("SIS_PAGINACAO", 5); 
/* Distancia proxima do usuario (em kilometros) */
define("SIS_KM_CLOSE", 2);
/* Comissao (Repasse para a EM2D) dos lojistas (percentual) */
define ("SIS_EM2D_COMISSION_STORES_PERCENT", 2.9);
/* Comissao (Repasse para a EM2D) dos entregadores (percentual) */
define ("SIS_EM2D_COMISSION_LOGISTIC_PERCENT", 5.8);
/* Desconto promocional para renovacoes de plano */
define ("SIS_EM2D_PLANS_DISCOUNT", 15);
/* Path para Upload de Imagens */
define ("SIS_UPLOAD_PATH", "../../seligaai/");
