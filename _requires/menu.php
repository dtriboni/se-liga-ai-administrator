<!-- menu profile quick info -->
<div class="profile clearfix">
              <div class="profile_pic">
                <a href="<?php echo SIS_URL; ?>home"><img src="<?php echo SIS_URL; ?>/production/images/logo/logo-login.png" title="by EM2D" class="img-circle profile_img"></a>
              </div>
              <div class="profile_info">
                <span>Ol&aacute;,</span>
                <h2><?php echo $_SESSION['sPersonPreName']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><?php echo $arrayTypePerson[$_SESSION['sPersonType']]; ?></h3>
                
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo SIS_URL; ?>home">Dashboard</a></li>
                    </ul>
                  </li>

                  <li><a href="javascript:void(0)"><i class="fa fa-connectdevelop"></i> IOT Click! <span class="label label-success pull-right">Em Breve</span></a></li>
                  <!-- <li><a><i class="fa fa-connectdevelop"></i> SeLigaA&iacute; IOT Click <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo SIS_URL; ?>iot-click">Configura&ccedil;&otilde;es</a></li>
                    </ul>
                  </li> -->
                  
                  <li><a><i class="fa fa-money"></i> Controle Financeiro <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php if ($_SESSION['sPersonType'] == 4){ //SOMENTE EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>make-payments">Contas &agrave; Pagar</a></li>
                      <?php } ?>
                      <li><a href="<?php echo SIS_URL; ?>receive-payments">Contas &agrave; Receber</a></li>
                      <?php if ($_SESSION['sPersonType'] == 4){ //SOMENTE EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>plans-list">Gerenciar Planos</a></li>
                      <?php } ?>
                      <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ //SOMENTE EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>plans-manage">Planos Contratados</a></li>
                      <?php } ?>
                    </ul>
                  </li>
                  

                  <?php if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ //SOMENTE LOGISTICO E EM2D VE ?>
                    <li><a><i class="fa fa-truck"></i> Controle Log&iacute;stico <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                      <?php if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ //SOMENTE LOGISTICO E EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>logistic-list">Listar Entregadores</a></li>
                      <?php } if ($_SESSION['sPersonType'] == 4){ //SOMENTE EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>logistic">Novo Entregador</a></li>
                      <?php } if ($_SESSION['sPersonType'] == 3){ ?>
                        <li><a href="<?php echo SIS_URL; ?>taxlog">Taxas e Abrang&ecirc;ncia</a></li>
                      <?php } ?>  
                      </ul>
                    </li>
                  <?php } ?>
                  
                  <li><a><i class="fa fa-bell"></i> Pedidos & Entregas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ //SOMENTE LOJISTA E EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>orders-manage">Listar Pedidos</a></li>
                      <?php } if ($_SESSION['sPersonType'] == 3 || $_SESSION['sPersonType'] == 4){ //SOMENTE LOGISTICO E EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>orders-manage">Listar Entregas</a></li>
                      <?php } ?>
                    </ul>
                  </li>

                  <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ //SOMENTE LOJISTA E EM2D VE ?>
                    <li><a><i class="fa fa-tags"></i> Ofertas & Promo&ccedil;&otilde;es <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo SIS_URL; ?>offers-list">Listar Ofertas/Promo&ccedil;&otilde;es</a></li>
                        <li><a href="<?php echo SIS_URL; ?>offer">Nova Oferta/Promo&ccedil;&atilde;o</a></li>
                      </ul>
                    </li>
                  <?php } ?>

                  <?php if ($_SESSION['sPersonType'] == 2 || $_SESSION['sPersonType'] == 4){ //SOMENTE LOJISTA E EM2D VE ?>
                    <li><a><i class="fa fa-ticket"></i> Cupons de Desconto <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo SIS_URL; ?>coupons-list">Gerenciar Cupons</a></li>
                        <li><a href="<?php echo SIS_URL; ?>coupom">Novo Cupom</a></li>
                      </ul>
                    </li>
                    <li><a><i class="fa fa-building"></i> Empresa & Endere&ccedil;os <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo SIS_URL; ?>store-list">Estabelecimentos & Filiais</a></li>
                        <li><a href="<?php echo SIS_URL; ?>address-list">Endere&ccedil;os Cadastrados</a></li>
                      </ul>
                    </li>
                  <?php } ?>

                  <li><a><i class="fa fa-users"></i> Usu&aacute;rios SeLigaA&iacute; <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo SIS_URL; ?>user-list">Gerenciar Usu&aacute;rios</a></li>
                      <?php if ($_SESSION['sPersonType'] == 4){ //SOMENTE EM2D VE ?>
                        <li><a href="<?php echo SIS_URL; ?>send-push-notifications">Enviar Push Notification</a></li>
                      <?php } ?>
                      </ul>
                  </li>

                  <?php if ($_SESSION['sPersonType'] == 4){ //SOMENTE EM2D VE ?>
                   <!--  <li><a><i class="fa fa-gift"></i> Sorteios e Brindes <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo SIS_URL; ?>lotteries-list">Gerenciar Conte&uacute;do</a></li>
                        <li><a href="<?php echo SIS_URL; ?>lottery">Novo Sorteio/Brinde</a></li>
                      </ul>
                    </li> -->
                    
                    <li><a><i class="fa fa-list-ul"></i> Categorias SeLigaA&iacute; <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo SIS_URL; ?>categories-list">Gerenciar Categorias</a></li>
                      </ul>
                    </li>
                  <?php } ?>

                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->