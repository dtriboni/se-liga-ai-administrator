<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($retChart = $functions->fGetTypeOffers())
{	
    $dataProduct = round((100 / $retChart[0]['numoffers']) * $retChart[0]['product']);
    $dataService = round((100 / $retChart[0]['numoffers']) * $retChart[0]['service']);
    $dataOffer = round((100 / $retChart[0]['numoffers']) * $retChart[0]['offer']);
    $dataPromo = round((100 / $retChart[0]['numoffers']) * $retChart[0]['promotion']);
    $dataDiscount = round((100 / $retChart[0]['numoffers']) * $retChart[0]['discount']);
    
    $dataChart = "[$dataProduct, $dataService, $dataOffer, $dataPromo, $dataDiscount]";
    $dataChart2 = $retChart[0]['product'].",".$retChart[0]['service'].",".$retChart[0]['offer'].",".$retChart[0]['promotion'].",".$retChart[0]['discount'].",".$retChart[0]['numoffers'];

}else{
	
    $dataChart = "[0, 0, 0, 0, 0]";
    $dataChart2 = "0,0,0,0,0,0";

}

$strJS = "if ($('.canvasDoughnut').length){
                var chart_doughnut_settings = {
                    type: 'doughnut',
                    tooltipFillColor: \"rgba(51, 51, 51, 0.55)\",
                    data: {
                        labels: [
                            \"Produto\",
                            \"Serviço\",
                            \"Oferta\",
                            \"Promoção\",
                            \"Desconto\"
                        ],
                        datasets: [{
                            data: {$dataChart},
                            backgroundColor: [
                                \"#BDC3C7\",
                                \"#9B59B6\",
                                \"#E74C3C\",
                                \"#26B99A\",
                                \"#3498DB\"
                            ],
                            hoverBackgroundColor: [
                                \"#CFD4D8\",
                                \"#B370CF\",
                                \"#E95E4F\",
                                \"#36CAAB\",
                                \"#49A9EA\"
                            ]
                        }]
                    },
                    options: { 
                        legend: true, 
                        responsive: false,
                        tooltips: {
                            callbacks: {
                              label: function(tooltipItem, data) {
                                //get the concerned dataset
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                //calculate the total of this data set
                                var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                  return previousValue + currentValue;
                                });
                                //get the current items value
                                var currentValue = dataset.data[tooltipItem.index];
                                var currentItem = data.labels[tooltipItem.index];
                                //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                                var percentage = Math.floor(((currentValue/total) * 100)+0.5);
                          
                                return currentItem + ': ' + currentValue + \"%\";
                              }
                            }
                          }  
                    }
                }
                $('.canvasDoughnut').each(function(){
                    var chart_element = $(this);
                    var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
                });
            }";

echo json_encode(array("js" => $strJS, "dataset" => array("data" => explode(",", $dataChart2))));