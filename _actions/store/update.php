<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($_REQUEST['id'] == '0')
{
	if ($strid = $functions->fSaveNewStore($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "strid" => $strid, "msg" => 'O estabelecimento foi incluido com sucesso.<br><br>N&atilde;o se esque&ccedil;a de cadastrar os endere&ccedil;os para o mesmo!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir o estabelecimento. Tente novamente mais tarde!'));
	}
}else{

	if ($functions->fUpdateStore($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "strid" => $_REQUEST['id'], "msg" => 'Seu estabelecimento foi atualizado com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar seu estabelecimento. Tente novamente mais tarde!'));
	}
}

echo $retJson;