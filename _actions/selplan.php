<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($retPlan = $functions->fGetPlanDetails($_REQUEST['plaid']))
{
    $_SESSION['sPlanData'] = $retPlan;

	$retJson = json_encode(array("ret" => true, "type" => "success"));
	
}else{

	$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao obter dados do plano. Tente novamente!'));
}

echo $retJson;