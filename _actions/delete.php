<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);


if ($functions->fRemoveRecord($_REQUEST['reg'], $_REQUEST['field'], $_REQUEST['id']))
{	
	
	$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Registro removido com sucesso!', "url" => null));
	
}else{
	
	$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao remover este registro. Tente novamente mais tarde!'));
}

echo $retJson;