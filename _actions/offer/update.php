<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

list($numOffers, $dateRemaining, $dateExpires) = explode("|", $functions->fGetOffersRemaining());

$diffEndOffer = $functions->fDateDiff("d", date('d-m-Y'), $_REQUEST['end'], true);

if ($diffEndOffer > $dateRemaining || $diffEndOffer < 0)
{
	$checkEndOfferDate = false;

}else{

	$checkEndOfferDate = true;
}

if ($numOffers < 1 || $dateRemaining < 0)
{
	$canCreateNewOffers = false; //check if user can create new offers
}else{
	
	$canCreateNewOffers = true; 
}

if ($_REQUEST['id'] == '0')
{
	if($canCreateNewOffers)
	{
		if ($checkEndOfferDate)
		{
			if ($ofeid = $functions->fSaveNewOffer($_REQUEST))
			{
				if ($functions->fSaveOfferCaracteristics($ofeid, $_REQUEST['type']))
				{
					unset($_SESSION['sCaracteristicData']);
					$retJson = json_encode(array("ret" => true, "type" => "success", "ofeid" => $ofeid, "msg" => 'Sua oferta/promo&ccedil;&atilde;o foi incluida com sucesso!', "url" => SIS_URL."home"));
				
				}else{
	
					$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir sua oferta/promo&ccedil;&atilde;o. Tente novamente mais tarde!'));
				}
			}else{
	
				$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir sua oferta/promo&ccedil;&atilde;o. Tente novamente mais tarde!'));
			}

		}else{

			if ($diffEndOffer < 0)
			{
				$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'A data de Expira&ccedil;&atilde;o da oferta n&atilde;o pode ser inferior a data de hoje!'));

			}else{

				$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'A data de Expira&ccedil;&atilde;o da oferta n&atilde;o pode ser superior a data de expira&ccedil;&atilde;o do seu plano!<br>'.$dateExpires));
			}
		}

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Voc&ecirc; já lan&ccedil;ou todas as ofertas/promo&ccedil;&otilde;es poss&iacute;veis nesse plano!<br><br>Para lan&ccedil;ar novas ofertas/promo&ccedil;&otilde;es, por favor contrate um novo plano atrav&eacute;s do App ou clique aqui!'));
	}

}else{

	if ($checkEndOfferDate)
	{
		if ($functions->fUpdateOffer($_REQUEST))
		{
			if ($functions->fSaveOfferCaracteristics($_REQUEST['id'], $_REQUEST['type']))
			{
				unset($_SESSION['sCaracteristicData']);
				$retJson = json_encode(array("ret" => true, "type" => "success", "ofeid" => $_REQUEST['id'], "msg" => 'Sua oferta/promo&ccedil;&atilde;o foi atualizada com sucesso!', "url" => SIS_URL."home"));
			}else{
	
				$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar sua oferta/promo&ccedil;&atilde;o. Tente novamente mais tarde!'));
			}
	
		}else{
	
			$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar sua oferta/promo&ccedil;&atilde;o. Tente novamente mais tarde!'));
		}

	}else{

		if ($diffEndOffer < 0)
		{
			$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'A data de Expira&ccedil;&atilde;o da oferta n&atilde;o pode ser inferior a data de hoje!'));

		}else{

			$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'A data de Expira&ccedil;&atilde;o da oferta n&atilde;o pode ser superior a data de expira&ccedil;&atilde;o do seu plano!<br>'.$dateExpires));
		}
	}	
}

echo $retJson;