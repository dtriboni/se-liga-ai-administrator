<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$auth = new auth();

$email = new email();

$_REQUEST = $auth->fSanitizeRequest($_POST);

if ($auth->fAuthenticatePerson($_REQUEST['user'], $_REQUEST['pwd']))
{
	$message = '<html><img src="'.SIS_URL.'assets/images/logos/libidinous-transp-black.png"><br><br>
				Ol&aacute; '.$auth->fReduceName($_SESSION['sPersonAka']).',<br><br>
				Ficamos muito felizes com o seu retorno ao '.SIS_TITULO.'! <br>
				Seus dados foram preservados em nossa base, e caso possua algum plano vigente, voc&ecirc;<br>
				poder&aacute; utiliz&aacute;-lo a qualquer momento, at&eacute; a data de vencimento!<br><br>
				Atenciosamente,<br>
				Equipe '.SIS_TITULO.'.<br><br>
				<strong>Este email foi enviado automaticamente, favor n&atilde;o responder!</strong></html>
			';
	
	$arrEmail = array('name' => $_SESSION['sPersonAka'],
					  'email' => $_SESSION['sPersonEmail'],
					  'subject' => SIS_TITULO.' - Bem vind'.$auth->fReference($_SESSION['sPersonGender']).' de volta',
					  'message' => $message);
	
	if($_SESSION['sPersonComeBack'] == 1)
		$email->fSendEmailToPerson($arrEmail);
	
	$url = ($_REQUEST['page'] != '' ? $_REQUEST['page'] : 'home');
	
	$retJson = json_encode(array("ret" => true, "msg" => null, "url" => $url));
	
}else{
	
	$retJson = json_encode(array("ret" => false, "field" => "login", "msg" => 'Credenciais de acesso incorretas ou perfil com acesso negado. Por favor, tente novamente!<br><br> Se o problema persistir, entre em contato com a equipe EM2D.<br><br>WhatsApp: <a target="_blank" href="https://web.whatsapp.com/send?phone=5587988413850">(87) 98841-3850</a><br>Email: seligaaiapp@gmail.com'));
}

echo $retJson;