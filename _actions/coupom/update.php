<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($_REQUEST['id'] == '0')
{
	if ($functions->fSaveNewCoupom($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Seu cupom de desconto foi incluido com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir seu cupom de desconto. Tente novamente mais tarde!'));
	}
}else{

	if ($functions->fUpdateCoupom($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Seu cupom de desconto foi atualizado com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar seu cupom de desconto. Tente novamente mais tarde!'));
	}
}

echo $retJson;