<?php 

require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);  


for($f = 0; $f < count($_FILES['file']['name']); $f++)
{
    if($_FILES['file']['error'][$f] == 0)
    {
        $_FILES['file']['b64'][] = 'data:image/' . $_FILES['file']['type'][$f] . ';base64,' . base64_encode(file_get_contents($_FILES['file']['tmp_name'][$f]));
    }else{
        header("HTTP/1.0 400 Bad Request");
    }
}

for($b64 = 0; $b64 < count($_FILES['file']['b64']); $b64++)
{
    if($img = $functions->base64_to_jpeg($_FILES['file']['b64'][$b64], 640, 480, "stores"))
    {
        $functions->fSaveStorePhotos($_REQUEST['addid'], $img);
    }
}