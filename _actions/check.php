<?php

error_reporting(0);

set_time_limit(0);

date_default_timezone_set('America/Sao_Paulo');

require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$autoloadFuncs = spl_autoload_functions();

foreach($autoloadFuncs as $unregisterFunc)
{
	spl_autoload_unregister($unregisterFunc);
}

include $_SERVER['DOCUMENT_ROOT']."/se-liga-ai-administrator/_includes/PagSeguroLibrary/PagSeguroLibrary.php";

spl_autoload_register(function ($class_name) {
	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/PagSeguroLibrary/".$class_name.".php";
});

if ($_SESSION['sPersonType'] == 2) //Only Store Profile
{
    if ($retStore = $functions->fGetPaymentStatus($_SESSION['sPersonStoreID']))
    {
        try {  
            $transactionCode = $retStore[0]['gatid'];
            $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()  
            $response = PagSeguroTransactionSearchService::searchByCode(  
            $credentials,  
            $transactionCode  
            ); 

            $currentStatus = $response->getStatus()->getValue();
            
            $functions->fQueryUpdatePaymentStatus($retStore[0]['payid'], $currentStatus, $retStore[0]['periodmonth']);
            
            $retJson = json_encode(array("ret" => false));

        } catch (PagSeguroServiceException $e) {  
            
            $retJson = json_encode(array("ret" => false));
        } 

    }else{

        $retJson = json_encode(array("ret" => false));
    }
}else{

    $retJson = json_encode(array("ret" => false));
}

echo $retJson;