<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($retChart = $functions->fGetIndexChart())
{	
    for ($e = 0; $e < count($retChart); $e++){
        list($y, $m, $d) = explode("-", $retChart[$e]['date']);
        $retEval .= "[gd({$y}, ".(int)$m.", ".(int)$d."), ".($retChart[$e]['value'])."],";
    }
	
}else{
	
    $retEval = "[gd(".date('Y').", ".date('m').", 1), 0],
                [gd(".date('Y').", ".date('m').", ".(date('d'))."), 0],";
}

$strJS = "$var arr_data1 = [
                ".substr($retEval, 0, strlen($retEval)-1)."
            ];
            if ($(\"#chart_plot_01\").length){
                console.log('Plot1');	
                $.plot( $(\"#chart_plot_01\"), [ arr_data1 ],  chart_plot_01_settings );
            }";

echo $strJS;