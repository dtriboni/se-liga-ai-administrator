<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($_REQUEST['id'] == '0')
{
	if ($addid = $functions->fSaveNewAddress($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "addid" => $addid, "msg" => 'Seu endere&ccedil;o foi incluido com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir seu endere&ccedil;o. Tente novamente mais tarde!'));
	}
}else{

	if ($functions->fUpdateAddress($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "addid" => $_REQUEST['id'], "msg" => 'Seu endere&ccedil;o foi atualizado com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar seu endere&ccedil;o. Tente novamente mais tarde!'));
	}
}

echo $retJson;