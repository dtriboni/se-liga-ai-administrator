<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions(false);

$email = new email();

$_REQUEST = $functions->fSanitizeRequest($_POST);

# Save new Person if success
if($functions->fSaveNewPreUser($_REQUEST))
{
	$message = '<html><img src="'.SIS_URL.'assets/images/logos/libidinous-transp-black.png"><br><br>
					Ol&aacute; '.$functions->fReduceName($_REQUEST['apelido']).',<br><br>
					Seu cadastro no '.SIS_TITULO.' foi criado com sucesso!<br>
					Nossa equipe de moderadores est&aacute; avaliando a autenticidade de seu perfil e <br>
					no m&aacute;ximo de 2 horas, voc&ecirc; poder&aacute; cadastrar seus an&uacute;ncios! <br>
					Caso deseje alterar algum dado em seu perfil, clique no link abaixo:<br><br>
					<a href="'.SIS_URL.'dashboard">'.SIS_URL.'dashboard</a><br><br>
					Atenciosamente,<br>
					Equipe '.SIS_TITULO.'.<br><br>
					<strong>Este email foi enviado automaticamente, favor n&atilde;o responder!</strong></html>
				';
									
	$arrEmail = array('email' => $_REQUEST['usr'],
						'subject' => SIS_TITULO.' - Novo Cadastro',
						'message' => $message);
	
	//$email->fSendEmailToPerson($arrEmail);
	
	$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => "Seu pr&eacute;-cadastro foi efetuado com sucesso!", "url" => "login"));
	
}else{ 

	$retJson = json_encode(array("ret" => false, "type" => "danger", "msg" => 'Erro ao salvar seu cadastro'));
}

echo $retJson;