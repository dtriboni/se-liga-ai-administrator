<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($strid = $functions->fUpdateTaxLog($_REQUEST))
{
    $retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Taxas e Abrang&ecirc;ncia atualizadas com sucesso.', "url" => SIS_URL."home"));

}else{

    $retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar as taxas e abrang&ecirc;ncia. Tente novamente mais tarde!'));
}

echo $retJson;