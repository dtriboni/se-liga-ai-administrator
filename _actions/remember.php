<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions(false);

$email = new email();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if (!$functions->fGetEmail($_REQUEST['usr'], 'users'))
{
	$retRetrieve = $functions->fRetrievePassword($_REQUEST['usr'], 'users');
	
	$message = '<html><img src="'.SIS_URL.'production/images/logo/logo-login.png" width="120px"><br><br>
				Ol&aacute; '.$functions->fReduceName($retRetrieve[0]['name']).',<br><br>
				Recebemos uma solicita&ccedil;&atilde;o para a redefini&ccedil;&atilde;o de uma nova senha <br>
				associada a sua conta. Se voc&ecirc; fez essa solicita&ccedil;&atilde;o, por favor, <br>
				confirme clicando no link abaixo:<br><br>	
				<a href="'.SIS_URL.'verify/1/'.$functions->fEncrypt($_REQUEST['usr']).'">Alterar Minha Senha</a><br><br>
				Atenciosamente,<br>		
				Equipe '.SIS_TITULO.'.<br><br>
				<strong>Este email foi enviado automaticamente, favor n&atilde;o responder!</strong></html>		
			';
	
	$arrEmail = array('name' => $retRetrieve[0]['name'], 
					  'email' => $retRetrieve[0]['email'], 
					  'subject' => SIS_TITULO.' - Recuperar Senha', 
					  'message' => $message);
	
	if ($email->fSendEmailToPerson($arrEmail))
	{
		$retJson = json_encode(array("status" => 'info', "msg" => 'Enviamos um email com os passos para alterar sua senha. Verifique sua caixa de email!', "url" => null));
		
	}else{
		
		$retJson = json_encode(array("status" => 'error', "msg" => 'Falha ao enviar o email. Por favor, tente novamente mais tarde!', "url" => null));
	}
	
}else{
	
	$retJson = json_encode(array("status" => 'error', "msg" => 'E-mail inexistente no cadastro. Por favor, informe o email cadastrado e tente novamente!'));
}

echo $retJson;