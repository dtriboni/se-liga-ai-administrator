<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

$urlFile = explode("/", $_POST['url']);

$_DIR = $_POST['dir'];

if ($functions->fRemoveImage($_REQUEST['phoid']))
{	
	unlink(SIS_UPLOAD_PATH.$_DIR.'/'.end($urlFile));
}