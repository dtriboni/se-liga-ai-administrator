<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($_REQUEST['id'] != '')
{
	if ($functions->fUpdateOrderRefundStatus($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Repasse de pedido atualizado com sucesso!'));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar o repasse do pedido. Tente novamente mais tarde!'));
	}
}

echo $retJson;