<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($retOrder = $functions->fCheck4NewOrders())
{
    $curr = ($_SESSION['sPersonType'] == 3 ? $_SESSION['sPersonLoggerOrders'] : $_SESSION['sPersonSellerOrders']);
    $new = $retOrder[0][($_SESSION['sPersonType'] == 3 ? 'numorderslogger' : 'numordersseller')];
    $diff = ($new - $curr);

}else{

    $retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao verificar pedidos. Tentando novamente mais tarde!'));
    echo $retJson;
}

switch($_REQUEST['type'])
{
    case 0:

        $retJson = json_encode(array("ret" => true, "type" => "success", "curr" => $curr, "new" => $new, "diff" => $diff, "msg" => 'Voc&ecirc; tem um novo pedido!'));
        echo $retJson;

    break;

    case 1:

    if($_SESSION['sPersonType'] == 3)
    {
        $_SESSION['sPersonLoggerOrders'] = $new;

    }else{

        $_SESSION['sPersonSellerOrders'] = $new;
    }

    break;
}

