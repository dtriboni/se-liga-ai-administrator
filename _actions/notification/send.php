<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($functions->fSendNotification($_REQUEST))
{
    $retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Seu endere&ccedil;o foi incluido com sucesso!', "url" => SIS_URL."home"));

}else{

    $retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir seu endere&ccedil;o. Tente novamente mais tarde!'));
}

echo $retJson;