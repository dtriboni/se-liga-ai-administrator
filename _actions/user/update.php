<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($_REQUEST['id'] == '0')
{
	if($functions->fGetEmail($_REQUEST['email'], 'users'))
    {
		if ($functions->fSaveNewUser($_REQUEST))
		{
			$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Usu&aacute;rio foi incluido com sucesso!', "url" => SIS_URL."home"));

		}else{

			$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir o usu&aacute;rio. Tente novamente mais tarde!'));
		}
        
    }else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Email j&aacute; cadastrado!'));
	}
	
}else{

	if ($functions->fUpdateUser($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Usu&aacute;rio atualizado com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar o usu&aacute;rio. Tente novamente mais tarde!'));
	}
}

echo $retJson;