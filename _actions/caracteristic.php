<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($_REQUEST)
{
	if (!is_array($_SESSION['sCaracteristicData'])){
		$_SESSION['sCaracteristicData'] = array();
	}
    array_push($_SESSION['sCaracteristicData'], $_REQUEST);

	$retJson = json_encode(array("ret" => true, "type" => "success", "values" => $_SESSION['sCaracteristicData']));
	
}else{

	$retJson = json_encode(array("ret" => false, "type" => "error"));
}

echo $retJson;