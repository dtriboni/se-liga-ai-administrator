<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($_REQUEST['page'] != ""){
    $page = "#".$_REQUEST['page'];
}else{
    $page = "";
}

$file = SIS_URL.'admin-usage.pdf'.$page;

$filename = $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/admin-usage.pdf";

header('Content-type: application/pdf');

header('Content-Disposition: inline; filename="' . $file . '"');

header('Content-Transfer-Encoding: binary');

header('Accept-Ranges: bytes');

@readfile($filename);