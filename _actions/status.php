<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

if ($_REQUEST['status'] == 1)
{
    if ($functions->fSetPlanAsPaid($_REQUEST['stpid']))
    {	
        
        $retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Plano marcado como PAGO!', "url" => null));
        
    }else{
        
        $retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar o status do plano. Tente novamente mais tarde!'));
    }

}else{

    if ($functions->fSetPlanAsCancelled($_REQUEST['stpid']))
    {	
        
        $retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Plano marcado como PAGO!', "url" => null));
        
    }else{
        
        $retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar o status do plano. Tente novamente mais tarde!'));
    }

}

echo $retJson;