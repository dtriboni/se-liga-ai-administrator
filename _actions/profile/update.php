<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$auth = new auth();

$_REQUEST = $auth->fSanitizeRequest($_POST);

if ($auth->fUpdatePersonCredentials($_REQUEST['usr'], $_REQUEST['pwd']))
{
	$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'Sua senha foi alterada com sucesso!', "url" => SIS_URL."home"));

}else{

	$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar seus dados. Tente novamente mais tarde!'));
}

echo $retJson;