<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_GET);

$retPlan = $functions->fGetPlanPreviouslyAquired($_REQUEST['strid']);

if ($_REQUEST['strid'] != 0){
	$_SESSION['sStorePlanID'] = $_REQUEST['strid'];
 }else{
   unset($_SESSION['sStorePlanID']);
 }

if (count($retPlan) == 0)
{
	$retJson = json_encode(array("ret" => true, "type" => "success"));

}else{

	if ($retPlan[0]['paid'] == 0)
	{
		if ($retPlan[0]['cancelled'] == 1)
		{
			if ($retPlan[0]['diffdays'] > 15)
			{
				$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Voc&ecirc; j&aacute; contratou um plano que disp&otilde;e de '.$retPlan[0]['diffdays'].' dias de utiliza&ccedil;&atilde;o.<br><br>S&oacute; &eacute; poss&iacute;vel contratar novo plano faltando 15 dias para vencer o plano atual!'));
		
			}else{
		
				$retJson = json_encode(array("ret" => true, "type" => "success"));
			}
		}else{

			$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Voc&ecirc; j&aacute; contratou um plano recentemente.<br><br>N&atilde;o &eacute; poss&iacute;vel contratar um novo plano no momento.'));
		}

	}else{

		if ($retPlan[0]['diffdays'] > 15)
		{
			$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Voc&ecirc; j&aacute; contratou um plano que disp&otilde;e de '.$retPlan[0]['diffdays'].' dias de utiliza&ccedil;&atilde;o.<br><br>S&oacute; &eacute; poss&iacute;vel contratar novo plano faltando 15 dias para vencer o plano atual!'));

		}else{

			$retJson = json_encode(array("ret" => true, "type" => "success"));
		}
	}
}

echo $retJson;