<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_REQUEST = $functions->fSanitizeRequest($_POST);

if ($_REQUEST['id'] == '0')
{
	if ($functions->fSaveNewLogistic($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'O entregador foi incluido com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao incluir o entregador. Tente novamente mais tarde!'));
	}
}else{

	if ($functions->fUpdateLogistic($_REQUEST))
	{
		$retJson = json_encode(array("ret" => true, "type" => "success", "msg" => 'O entregador foi atualizado com sucesso!', "url" => SIS_URL."home"));

	}else{

		$retJson = json_encode(array("ret" => false, "type" => "error", "msg" => 'Falha ao atualizar o entregador. Tente novamente mais tarde!'));
	}
}

echo $retJson;