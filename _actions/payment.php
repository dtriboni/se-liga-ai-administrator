<?php

error_reporting(0);

set_time_limit(0);

date_default_timezone_set('America/Sao_Paulo');

require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_config/config.ini.php";

session_start2();

$functions = new functions();

$_POST = $functions->fSanitizeRequest($_POST);

$strid = (isset($_SESSION['sStorePlanID']) ? $_SESSION['sStorePlanID'] : $_SESSION['sPersonStoreID']);

$retSender = $functions->fGetUserRegister($strid);

if (!empty($retSender[0]['whatsapp']))
	$telefone = $retSender[0]['whatsapp'];
elseif (!empty($retSender[0]['tel1']))
	$telefone = $retSender[0]['tel1'];
elseif (!empty($retSender[0]['tel2']))
	$telefone = $retSender[0]['tel2'];	
	

if ($_POST['type'] == 1){
	$paymentMethod = "CREDIT_CARD";
}elseif ($_POST['type'] == 2){
	$paymentMethod = "BOLETO";
}else{
	$paymentMethod = "MONEY";
}

$autoloadFuncs = spl_autoload_functions();

foreach($autoloadFuncs as $unregisterFunc)
{
	spl_autoload_unregister($unregisterFunc);
}

include $_SERVER['DOCUMENT_ROOT']."/se-liga-ai-administrator/_includes/PagSeguroLibrary/PagSeguroLibrary.php";

spl_autoload_register(function ($class_name) {
	require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/PagSeguroLibrary/".$class_name.".php";
});

$_POST['method'] = 'paid';

if ($_POST['method'] != 'free') //Only paid plans - Invoke PagSeguro Payment Gateway
{
	if ($paymentMethod == "CREDIT_CARD" || $paymentMethod == "BOLETO")
	{
		$directPaymentRequest = new PagSeguroDirectPaymentRequest();
	
		$directPaymentRequest->setPaymentMode('DEFAULT');
		
		$directPaymentRequest->setPaymentMethod($paymentMethod);
		
		$directPaymentRequest->setReceiverEmail('seligaaiapp@gmail.com');
		
		$directPaymentRequest->setCurrency("BRL");
		
		$directPaymentRequest->addItem('0001', "Plano ".$_SESSION['sPlanData'][0]['plan'], 1,  $_SESSION['sPlanData'][0]['price']);
		
		$directPaymentRequest->setReference("Plano ".$_SESSION['sPlanData'][0]['plan']." - R$ ".$_SESSION['sPlanData'][0]['price']);
		
		$senderName = explode(" ", $retSender[0]['store']);

		$directPaymentRequest->setSender(
				(count($senderName) < 2 ? $retSender[0]['store']." do Se Liga Ai" : $retSender[0]['store']),
				"comprador@sandbox.pagseguro.com.br",//$retSender[0]['email'],
				substr($telefone, 1, 2),
				str_replace("-", "", substr($telefone, 5, 9)),
				(strlen($retSender[0]['cpfcnpj']) == 14 ? 'CPF' : 'CNPJ'),
				$functions->fremoveNumFormat($retSender[0]['cpfcnpj']),
				true);
				
		$directPaymentRequest->setSenderHash($_POST['sh']);
		
		$sedexCode = PagSeguroShippingType::getCodeByType('NOT_SPECIFIED');
		
		$directPaymentRequest->setShippingType($sedexCode);
		
		$directPaymentRequest->setShippingAddress(
				'56330470',
				'RUA PADEIRO JOAO LUIS',
				'137',
				'SALA A',
				'CENTRO',
				'PETROLINA',
				'PE',
				'BRA');
		
		$paymentMethodLabel = "Boleto";
		
		if ($paymentMethod == "CREDIT_CARD")
		{
			$paymentMethodLabel = "Cart&atilde;o de Cr&eacute;dito";
			
			$billing = new PagSeguroBilling(
							array('postalCode' 	=> '56330470',
								'street' 		=> 'RUA PADEIRO JOAO LUIS',
								'number' 		=> '137',
								'complement' 	=> 'SALA A',
								'district' 	=> 'CENTRO',
								'city' 		=> 'PETROLINA',
								'state' 		=> 'PE',
								'country' 	=> 'BRA'));

			list($combo_quantity, $combo_value) = explode("|", $_POST['installments']);
			
			$installment = new PagSeguroDirectPaymentInstallment(
								array('quantity'					  => $combo_quantity,
									'value' 						  => number_format($combo_value, 2),
									'noInterestInstallmentQuantity' => ($_SESSION['sPlanData'][0]['periodmonth'] == 1 ? 2 : $_SESSION['sPlanData'][0]['periodmonth'])));
								
			$arrDocuments = array('type' => 'CPF', 'value' => $functions->fremoveNumFormat($_POST['doc']));	
			
			$holder = new PagSeguroCreditCardHolder(
						array('name' 	 => $_POST['cardname'],
								'documents' => $arrDocuments,
								'birthDate' => $retSender[0]['datanasc'],
								'areaCode'  => substr($telefone, 1, 2),
								'number' 	 => str_replace("-", "", substr($telefone, 5, 9))));
			
			$cardCheckout = new PagSeguroCreditCardCheckout(
								array('token' 		 => $_POST['ct'], 
									'installment' => $installment,
									'billing' 	 => $billing,
									'holder' 	 => $holder));
			
			$directPaymentRequest->setCreditCard($cardCheckout);
		}
												
		if ($paymentMethod == "EFT")
		{
			$paymentMethodLabel = "D&eacute;bito Online";
			
			$directPaymentRequest->setOnlineDebit(array("bankName" => 'ITAU'));
		}
		
		try {
		
			$credentials = PagSeguroConfig::getAccountCredentials();
		
			$return = $directPaymentRequest->register($credentials);
			
			require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_classes/PHPMailerAutoload.php";
			
			spl_autoload_register(function ($class_name) {
				require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_classes/".$class_name.".class.php";
			});
			
			$email = new email();
					
			if ($return)
			{
				$arrPlano = array("strid"				=> $strid,
								"plaid"   			=> $_SESSION['sPlanData'][0]['plaid'],
								"plan" 				=> $_SESSION['sPlanData'][0]['plan'],
								"periodmonth" 		=> $_SESSION['sPlanData'][0]['periodmonth'],
								"value" 				=> $return->getGrossAmount(),
								"gatid" 				=> $return->getCode());
		
				if($functions->fQuerySavePersonPlan($arrPlano))
				{
					$message = '<html><img src="'.SIS_URL.'assets/images/logos/libidinous-transp-black.png"><br><br>
							Ol&aacute; '.$retSender[0]['store'].',<br><br>
							Seu Plano '.$_SESSION['sPlanData'][0]['plan'].' no Se Liga A&iacute; foi inclu&iacute;do com sucesso!<br>
							O mesmo estar&aacute; dispon&iacute;vel ap&oacute;s a aprova&ccedil;&atilde;o do pagamento<br>
								<strong>Plano '.$_SESSION['sPlanData'][0]['plan'].'</strong> no valor de 
								<strong>R$ '.number_format($return->getGrossAmount(), 2, ",", ".").'</strong> com validade at&eacute;  
								<strong>'.date('d/m/Y', strtotime("+".($_SESSION['sPlanData'][0]['periodmonth'] * 30)." day")).'</strong><br>
							Com este plano voc&ecirc; poder&aacute; incluir '.($_SESSION['sPlanData'][0]['numoffers'] == 9999 ? 'quantas ofertas/promo&ccedil;&otilde;es quiser' : 'at&eacute; '.$_SESSION['sPlanData'][0]['numoffers'].' ofertas/promo&ccedil;&otilde;es').' no App<br>
							'.($_SESSION['sPlanData'][0]['canuseadmin'] == 1 ? ', bem como utilizar o painel administrativo' : '.').'
							'.($_SESSION['sPlanData'][0]['canseller'] == 1 ? ' e vender online atrav&eacute;s da plataforma' : '').'<br><br>
							'.($paymentMethod == "BOLETO" ? '<a href="'.$return->getPaymentLink().'">Clique aqui para gerar seu boleto!</a><br>' : '').'
							Caso deseje acessar sua conta, clique no link abaixo:<br><br>
							<a href="'.SIS_URL.'login">'.SIS_URL.'login</a><br><br>
							Atenciosamente,<br>
							Equipe '.SIS_TITULO.'.<br><br>
							<strong>Este email foi enviado automaticamente, favor n&atilde;o responder!</strong></html>
						';
				
					$arrEmail = array('aka' => $retSender[0]['store'],
									'email' => $retSender[0]['email'],
									'subject' => SIS_TITULO.' - Plano '.$_SESSION['sPlanData'][0]['plan'].' Contratado!',
									'message' => $message);
						
					$email->fSendEmailToPerson($arrEmail);

					$linkBoleto = ($paymentMethod == "BOLETO" ? '<br>Deseja imprimir o boleto agora? <a target="_blank" href="'.$return->getPaymentLink().'"><strong>Clique aqui!</strong></a>' : "");
					
					echo json_encode(array("ret" => true, "type" => "success", "msg" => "Sua compra foi processada com sucesso!<br><br>Um email com detalhes da transa&ccedil;&atilde;o foi enviado para ".$retSender[0]['email'].".".$linkBoleto."<br><br>Clique em <strong>Avan&ccedil;ar</strong> para continuar."));
					
				}else{
					
					echo json_encode(array("ret" => false, "type" => "danger", "msg" => "Ocorreu um erro ao efetuar sua compra com {$paymentMethodLabel}.<br><br>Clique em <strong>Voltar</strong> para tentar novamente."));
				}
			}
		
		} catch (PagSeguroServiceException $e) {
		
			$catchError = $e->getMessage();
		
			echo json_encode(array("ret" => false, "type" => "danger", "msg" => "Ocorreu um erro ao efetuar sua compra com {$paymentMethodLabel}.\n\n{$catchError}<br><br>Clique em <strong>Voltar</strong> para tentar novamente."));
		}

	}else{

			
		require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_classes/PHPMailerAutoload.php";
		
		spl_autoload_register(function ($class_name) {
			require_once $_SERVER["DOCUMENT_ROOT"]."/se-liga-ai-administrator/_includes/_classes/".$class_name.".class.php";
		});
		
		$email = new email();
					
		$arrPlano = array("strid"				=> $strid,
						"plaid"   				=> $_SESSION['sPlanData'][0]['plaid'],
						"plan" 					=> $_SESSION['sPlanData'][0]['plan'],
						"periodmonth" 			=> $_SESSION['sPlanData'][0]['periodmonth'],
						"value" 				=> $_SESSION['sPlanData'][0]['price'],
						"gatid" 				=> 'A1B2C3D4E5-T5R4E3W2Q1-P0O9I8U7Y6');

		if($functions->fQuerySavePersonPlan($arrPlano, 1))
		{
			$message = '<html><img src="'.SIS_URL.'assets/images/logos/libidinous-transp-black.png"><br><br>
					Ol&aacute; '.$retSender[0]['store'].',<br><br>
					Seu Plano '.$_SESSION['sPlanData'][0]['plan'].' no Se Liga A&iacute; foi inclu&iacute;do com sucesso!<br>
					O mesmo estar&aacute; dispon&iacute;vel ap&oacute;s a aprova&ccedil;&atilde;o do pagamento<br>
						<strong>Plano '.$_SESSION['sPlanData'][0]['plan'].'</strong> no valor de 
						<strong>R$ '.number_format($_SESSION['sPlanData'][0]['price'], 2, ",", ".").'</strong> com validade at&eacute;  
						<strong>'.date('d/m/Y', strtotime("+".($_SESSION['sPlanData'][0]['periodmonth'] * 30)." day")).'</strong><br>
					Com este plano voc&ecirc; poder&aacute; incluir '.($_SESSION['sPlanData'][0]['numoffers'] == 9999 ? 'quantas ofertas/promo&ccedil;&otilde;es quiser' : 'at&eacute; '.$_SESSION['sPlanData'][0]['numoffers'].' ofertas/promo&ccedil;&otilde;es').' no App<br>
					'.($_SESSION['sPlanData'][0]['canuseadmin'] == 1 ? ', bem como utilizar o painel administrativo' : '.').'
					'.($_SESSION['sPlanData'][0]['canseller'] == 1 ? ' e vender online atrav&eacute;s da plataforma' : '').'<br><br>
					Caso deseje acessar sua conta, clique no link abaixo:<br><br>
					<a href="'.SIS_URL.'login">'.SIS_URL.'login</a><br><br>
					Atenciosamente,<br>
					Equipe '.SIS_TITULO.'.<br><br>
					<strong>Este email foi enviado automaticamente, favor n&atilde;o responder!</strong></html>
				';
		
			$arrEmail = array('aka' => $retSender[0]['store'],
							'email' => $retSender[0]['email'],
							'subject' => SIS_TITULO.' - Plano '.$_SESSION['sPlanData'][0]['plan'].' Contratado!',
							'message' => $message);
				
			//$email->fSendEmailToPerson($arrEmail);
			
			echo json_encode(array("ret" => true, "type" => "success", "msg" => "A compra foi processada com sucesso!<br><br>Um email com detalhes da transa&ccedil;&atilde;o foi enviado para ".$retSender[0]['email']."<br><br>Clique em <strong>Avan&ccedil;ar</strong> para continuar."));
			
		}else{
			
			echo json_encode(array("ret" => false, "type" => "danger", "msg" => "Ocorreu um erro ao efetuar sua compra com Dinheiro<br><br>Clique em <strong>Voltar</strong> para tentar novamente."));
		}
			

	}
	
}else{ // Only N days free plan
	
	/* require_once $_SERVER["DOCUMENT_ROOT"]."/_includes/_classes/PHPMailerAutoload.php";
	spl_autoload_register(function ($class_name) {
		require_once $_SERVER["DOCUMENT_ROOT"]."/_includes/_classes/".$class_name.".class.php";
	});
	
	$email = new email();
		
	$arrPlano = array("plaid" 		=> $retPlan[0]['plaid'],
					  "name" 		=> $retPlan[0]['plano'],
					  "ads" 		=> $retPlan[0]['anuncios'],
					  "photos" 		=> $retPlan[0]['fotos'],
					  "videos" 		=> $retPlan[0]['videos'],				
					  "vloriginal" 	=> 0,
					  "pago" 		=> 1,
					  "lido"        => 1,
			          "planexpires" => SIS_DIAS_GRATIS,
					  "vencimento" 	=> date('Y-m-d H:i:s', strtotime("+".SIS_DIAS_GRATIS." day")));
	
	if($functions->fQuerySavePersonPlan($arrPlano))
	{
		$message = '<html><img src="'.SIS_URL.'assets/images/logos/libidinous-transp-black.png"><br><br>
						Ol&aacute; '.$functions->fReduceName($_SESSION['sPersonUrl']).',<br><br>
						Seu Plano Basic de '.SIS_DIAS_GRATIS.' dias gr&aacute;tis no '.SIS_TITULO.' foi criado com sucesso!<br>
						Com este plano voc&ecirc; poder&aacute; incluir at&eacute; 1 (um) an&uacute;ncio no site <br>
						cujo mesmo estar&aacute; online, se aprovado, por no m&aacute;ximo '.SIS_DIAS_GRATIS.' dias corridos. <br>
						Desfrute do que h&aacute; de melhor em '.SIS_TITULO.'!<br><br>
						Atenciosamente,<br>
						Equipe '.SIS_TITULO.'.<br><br>
						<strong>Este email foi enviado automaticamente, favor n&atilde;o responder!</strong></html>
					';
			
		$arrEmail = array('aka' => $_SESSION['sPersonAka'],
						  'email' => $_SESSION['sPersonEmail'],
						  'subject' => SIS_TITULO.' - Plano Basic por '.SIS_DIAS_GRATIS.' Dias',
						  'message' => $message);
			
		$email->fSendEmailToPerson($arrEmail);
			
		echo json_encode(array("ret" => 1, "msg" => "Seu plano de 30 dias foi assinado com sucesso!"));
			
	}else{
			
		echo json_encode(array("ret" => 0, "msg" => "Ocorreu um erro ao assinar seu plano. Por favor tente novamente mais tarde!"));
	} */
	
}